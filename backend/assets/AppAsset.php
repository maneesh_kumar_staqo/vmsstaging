<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'https://use.fontawesome.com/releases/v5.8.2/css/all.css',
//        'public/css/bootstrap.min.css',
//        'public/css/mdb.min.css',
        'public/css/style.css',
        'public/css/owl.carousel.min.css',
        'public/css/owl.theme.default.min.css',
        'public/css/animate/animate.css',
        'public/css/animate/animate.min.css',
        'public/css/fSelect.css',
        'public/css/form-style.css',
        'public/css/application.css',
        'public/css/daterangepicker.css',
    ];
    public $js = [
        //        'public/js/jquery-3.4.1.min.js',
        'public/js/popper.min.js',
//        'public/js/bootstrap.min.js',
        'public/js/fSelect.js',
//        'public/js/animate/wow.js',
//        'public/js/animate/wow.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/mdb.min.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyBTzDMr5csTrmlNzj0B-KoRpRtgTIbCO_A&callback=initMap',
        'public/js/custom.js',
        'public/js/upload-files.js?v=1',

//        'js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        'public/bower_components/moment/moment.js',
//        'js/bootstrap-datepicker.js',
        'public/js/daterangepicker.js',

        'public/js/application.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
