<?php
/* @var $this yii\web\View */

$this->title = 'Dashboard';

$menuItem = [];
?>
<?php
if (Yii::$app->user->can('manage-service')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-list',
    'title' => 'Type of Services',
    'url' => '/services',
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-commodities')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-list',
    'title' => 'Type of Commodities',
    'url' => '/commodities',
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-bussiness-types')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-list',
    'title' => 'Type of Business',
    'url' => '/company-types',
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-vendor-types')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-list',
    'title' => 'Type of Vendor',
    'url' => '/vendor-types'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-organisation-type')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-list',
    'title' => 'Type of Organisations',
    'url' => '/organisation-types'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-countries')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-globe',
    'title' => 'Countries Master',
    'url' => '/country'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-user-request')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-user',
    'title' => 'Vendor Requests',
    'url' => '/user-request'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-po')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-retweet',
    'title' => 'Track Vendor wise PO',
    'url' => '/po-tracking'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-reports')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-search',
    'title' => 'Reports',
    'url' => '/reports/default/dashboard'
];
endif;
?>
<?php
if (Yii::$app->user->can('manage-permissions')):
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-cog',
    'title' => 'User Role Management',
    'url' => '/user-management'
];
endif;
?>

<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            Dashboard
        </div>
        <div class="panel-body">
            <?=
            $this->render('partials/single-box', [
                'menuItem' => $menuItem
            ])
            ?>
        </div>
    </div>
</div>