<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PoInvoice */

$this->title = 'Create Po Invoice';
$this->params['breadcrumbs'][] = ['label' => 'Po Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="po-invoice-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
