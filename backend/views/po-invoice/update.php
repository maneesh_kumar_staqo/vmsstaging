<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PoInvoice */

$this->title = 'Update Po Invoice: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Po Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="po-invoice-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
