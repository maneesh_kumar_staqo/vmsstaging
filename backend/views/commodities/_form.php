<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Commodities;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-sm-10 col-sm-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span>
                <?= $this->title ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['index'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="">
                <div class="col-sm-12">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'upper_turnover')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-12">
                <?= $form->field($model, 'lower_turnover')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="">
                <div class="col-sm-6">
                    <?= $form->field($model, 'status')->dropDownList(Commodities::$statusArray) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'priority')->textInput() ?>
                </div>
            </div>
            <div class="">
                <div class="col-sm-12">
                    <?= $form->field($model, 'display_other_field')->checkbox(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-sm-12">
                <?=
                Html::submitButton('Save', [
                    'class' => 'btn btn-success',
                ])
                ?>
                <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

