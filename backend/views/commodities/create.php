<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Commodities */

$this->title = 'Create Commodities';
$this->params['breadcrumbs'][] = ['label' => 'Commodities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commodities-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
