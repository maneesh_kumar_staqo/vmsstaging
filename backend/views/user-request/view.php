<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\UserRequest */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => 'User Requests', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

$canApprove = false;
if ((!empty($vendorInfo['id']) && $vendorInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($contactInfo['id']) && $contactInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($addressInfo['id']) && $addressInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($bankInfo['id']) && $bankInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($taxInfo['id']) && $taxInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($accreditationInfo['id']) && $accreditationInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($declarationInfo['id']) && $declarationInfo['status'] != User::APP_STATUS_PENDING) &&
        (!empty($registrationInfo['id']) && $registrationInfo['status'] != User::APP_STATUS_PENDING)
) {
    $canApprove = true;
}
?>
<?=
$this->render('partials/update-status', [
    'model' => $model,
    'orgCode' => $orgCode,
    'paymentCode' => $paymentCode,
    'accountCode' => $accountCode,
    'companyCode' => $companyCode
])
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                View detail for => <?= Html::encode($model->email) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?php
                //Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['create'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Registration Status</th>
                            <th>Assigned To</th>
                            <th>Approval Status</th>
                            <th>Approval Remarks (if any)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Registration Information Form</td>
                            <td>
                                <?= 'Completed' ?>
                            </td>
                            <td>CPC Department</td>
                            <td>
                                <?php if (!empty($registrationInfo) && !empty(User::$applicationStatus[$registrationInfo['status']])): ?>
                                    <?= User::$applicationStatus[$registrationInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($registrationInfo['status_remark'])): ?>
                                    <?= Html::encode($registrationInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ((!empty($registrationInfo))): ?>
                                    <a
                                        class="btn btn-primary btn-xs"
                                        href="<?= Url::toRoute('/user-request/registration-information?id=' . $model->id) ?>"
                                        >
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Vendor Information Form</td>
                            <td>
                                <?= (!empty($vendorInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>Finance Department</td>
                            <td>
                                <?php if (!empty($vendorInfo) && !empty(User::$applicationStatus[$vendorInfo['status']])): ?>
                                    <?= User::$applicationStatus[$vendorInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($vendorInfo['status_remark'])): ?>
                                    <?= Html::encode($vendorInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($vendorInfo['id'])): ?>
                                    <a
                                        class="btn btn-primary btn-xs"
                                        href="<?= Url::toRoute('/user-request/vendor-information?id=' . $model->id) ?>"
                                        >
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Contact Information Form</td>
                            <td>
                                <?= (!empty($contactInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>CPC Department</td>
                            <td>
                                <?php if (!empty($contactInfo) && !empty(User::$applicationStatus[$contactInfo['status']])): ?>
                                    <?= User::$applicationStatus[$contactInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($contactInfo['status_remark'])): ?>
                                    <?= Html::encode($contactInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($contactInfo['id'])): ?>
                                    <a
                                        class="btn btn-primary btn-xs"
                                        href="<?= Url::toRoute('/user-request/contact-information?id=' . $model->id) ?>"
                                        >
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Address Information Form</td>
                            <td>
                                <?= (!empty($addressInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>CPC Department</td>
                            <td>
                                <?php if (!empty($addressInfo) && !empty(User::$applicationStatus[$addressInfo['status']])): ?>
                                    <?= User::$applicationStatus[$addressInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($addressInfo['status_remark'])): ?>
                                    <?= Html::encode($addressInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($addressInfo['id'])): ?>
                                    <a
                                        href="<?= Url::toRoute('/user-request/address-information?id=' . $model->id) ?>"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Bank Information Form</td>
                            <td>
                                <?= (!empty($bankInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>Finance Department</td>
                            <td>
                                <?php if (!empty($bankInfo) && !empty(User::$applicationStatus[$bankInfo['status']])): ?>
                                    <?= User::$applicationStatus[$bankInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($bankInfo['status_remark'])): ?>
                                    <?= Html::encode($bankInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($bankInfo['id'])): ?>
                                    <a
                                        href="<?= Url::toRoute('/user-request/bank-information?id=' . $model->id) ?>"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Tax Information Form</td>
                            <td>
                                <?= (!empty($taxInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>Finance Department</td>
                            <td>
                                <?php if (!empty($taxInfo) && !empty(User::$applicationStatus[$taxInfo['status']])): ?>
                                    <?= User::$applicationStatus[$taxInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($taxInfo['status_remark'])): ?>
                                    <?= Html::encode($taxInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($taxInfo['id'])): ?>
                                    <a
                                        href="<?= Url::toRoute('/user-request/tax-information?id=' . $model->id) ?>"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Accreditations Information Form</td>
                            <td>
                                <?= (!empty($accreditationInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>CPC Department</td>
                            <td>
                                <?php if (!empty($accreditationInfo) && !empty(User::$applicationStatus[$accreditationInfo['status']])): ?>
                                    <?= User::$applicationStatus[$accreditationInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($accreditationInfo['status_remark'])): ?>
                                    <?= Html::encode($accreditationInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($accreditationInfo['id'])): ?>
                                    <a
                                        href="<?= Url::toRoute('/user-request/accreditation-information?id=' . $model->id) ?>"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>

                        <tr>
                            <td>Declaration Information Form</td>
                            <td>
                                <?= (!empty($declarationInfo['id'])) ? 'Completed' : 'Pending' ?>
                            </td>
                            <td>CPC Department</td>
                            <td>
                                <?php if (!empty($declarationInfo) && !empty(User::$applicationStatus[$declarationInfo['status']])): ?>
                                    <?= User::$applicationStatus[$declarationInfo['status']] ?>
                                <?php else: ?>
                                    Pending
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($declarationInfo['status_remark'])): ?>
                                    <?= Html::encode($declarationInfo['status_remark']) ?>
                                <?php else: ?>
                                    --
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if (!empty($declarationInfo['id'])): ?>
                                    <a
                                        href="<?= Url::toRoute('/user-request/declaration-information?id=' . $model->id) ?>"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-eye-open"></i> View
                                    </a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <ul class="list-group list-group-flush">

                    <?php if (Yii::$app->user->can('application-status')): ?>
                        <li class="list-group-item list-group-item-info">
                            <span>
                                Application Status:
                            </span>
                            <span class="">
                                <i class="badge badge-info">
                                    <?= (!empty(User::$applicationStatus[$model->application_status])) ? User::$applicationStatus[$model->application_status] : 'Pending' ?>
                                </i>
                                <?php if ($model->application_status == User::APP_STATUS_INCOMPLETE): ?>
                                    <a
                                        href="javascript: void(0)"
                                        data-confirm="Vendor Application is not submitted for review."
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>

                                <?php elseif (!$canApprove): ?>
                                    <a
                                        href="javascript: void(0)"
                                        data-confirm="Respective team members approval is Pending to proceed further."
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>

                                <?php else: ?>
                                    <a
                                        href="javascript: void(0)"
                                        data-toggle="modal"
                                        data-target="#updateStatusModel"
                                        class="btn btn-primary btn-xs">
                                        <i class="glyphicon glyphicon-pencil"></i>
                                    </a>
                                <?php endif; ?>
                            </span>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

</div>
