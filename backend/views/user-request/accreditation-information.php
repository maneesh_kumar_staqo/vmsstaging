<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;
use yii\helpers\Html;

$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));

$this->title = 'Accreditations Information';
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('accreditation-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">
                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle">
                        <h2>Accreditations</h2>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="multioption">
                                        <label style="padding-top:0px">Accreditation Details</label>
                                    </div>
                                    <?= $form->field($model, 'accreditation_details')->textarea(['rows' => 5])->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?= FormHelper::fileInputMulti($form, $model, 'documents') ?>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>