<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;

/* @var $this yii\web\View */
/* @var $model common\models\BankInformation */
/* @var $form yii\widgets\ActiveForm */


$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));

$this->title = 'Bank Information';
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('bank-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">
                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle">
                        <h2>Primary Bank Details</h2>
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_account_holder_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_account_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_account_type')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_bank_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_branch')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_city')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_micr_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'pb_ifsc_code')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?=
                                FormHelper::fileInputMulti($form, $model, 'pb_documents', [
                                    'class' => ''
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="margin-top:15px;">Secondary Bank Details</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_account_holder_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_account_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_account_type')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_bank_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_branch')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_city')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_micr_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'sb_ifsc_code')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?=
                                FormHelper::fileInputMulti($form, $model, 'sb_documents', [
                                    'class' => ''
                                ])
                                ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>