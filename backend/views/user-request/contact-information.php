<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contact Information';

$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('contact-informations')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">
                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle">
                        <h2>Business Person Details</h2>
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'b_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'b_designation')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'b_land_line_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'b_mobile_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'b_email')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="margin-top:15px;">Finance & Accounts Person Details</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'f_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'f_designation')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'f_land_line_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'f_mobile_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'f_email')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>