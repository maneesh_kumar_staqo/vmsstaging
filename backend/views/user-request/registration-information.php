<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\RegistrationVendorInfo;
use common\helpers\FormHelper;
use yii\helpers\Html;

$this->title = 'Register Vendor Form';

$this->registerJsFile(Url::toRoute('/public/js/signup.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);

$serviceTurnover = $communityTurnover = '';

if (!empty($commodities[$model->fk_primary_commodity_type])) {
    if ($model->annual_turnover == 1) {
        $communityTurnover = $commodities[$model->fk_primary_commodity_type]['upper_turnover'];
    } else {
        $communityTurnover = $commodities[$model->fk_primary_commodity_type]['lower_turnover'];
    }
}
if (!empty($services[$model->primary_service_type])) {
    if ($model->primary_service_turnorver == 1) {
        $serviceTurnover = $services[$model->primary_service_type]['upper_turnover'];
    } else {
        $serviceTurnover = $services[$model->primary_service_type]['lower_turnover'];
    }
}
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('registration-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">

                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle ">
                        <h2><?= $this->title ?></h2>

                        <?php $form = ActiveForm::begin(); ?>
                        <?=
                        FormHelper::radioList($form, $model, 'bussiness_registration', RegistrationVendorInfo::$bussinessRegistrationTypes);
                        ?>

                        <div class="row" style="padding-top:15px;padding-left:5px;">
                            <div class="col-md-6">
                                <div class=" md-form mb-0">
                                    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6 d-flex  row">
                                <div class="md-form mb-0 row">
                                    <div class="d-none trading_name col-md-6">
                                        <?= $form->field($model, 'trading_name')->textInput(['maxlength' => true]) ?>
                                    </div>

                                    <div class=" same_as_company_name col-md-6">
                                        <div class="">
                                            <?=
                                            FormHelper::checkbox($form, $model, 'sameAsCompanyName');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" >
                            <div class="col-md-6">
                                <?=
                                FormHelper::dropDownList($model, 'fk_primary_commodity_type', $commodities, [
                                    'isDependent' => true,
                                    'other-field' => '.primary-other-field',
                                    'turn-over-dropdown' => '.primany-fist-turn-over',
                                    'turn-over-field-name' => 'annual_turnover'
                                ]);
                                ?>
                            </div>

                            <div class="col-md-6">
                                <div class="md-form mb-0 w-100">
                                    <?=
                                    $form->field($model, 'annual_turnover')->textInput([
                                        'class' => 'mx-2 form-control',
                                        'readonly' => true,
                                        'disabled' => true,
                                        'value' => $communityTurnover
                                    ])
                                    ?>
                                </div>
                            </div>

                            <?php
                            $class = 'd-none';
                            if ($model->other_primary_commodity || $model->getFirstError('other_primary_commodity')) {
                                $class = '';
                            }
                            ?>
                            <div class="col-md-6 primary-other-field <?= $class ?>">
                                <div class="md-form mb-0">
                                    <?=
                                    $form->field($model, 'other_primary_commodity')->textInput([
                                        'class' => 'w-100 mx-2'
                                    ])
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-left:5px">
                            <div class="col-md-8">
                                <?=
                                FormHelper::multiDropDownList($model, 'fk_secondary_commodity_type', $commodities, [
                                    'isDependent' => true,
                                    'other-field' => '.secondary-other-field'
                                ])
                                ?>
                            </div>
                            <?php
                            $class = 'd-none';
                            if ($model->other_secondary_commodity || $model->getFirstError('other_secondary_commodity')) {
                                $class = '';
                            }
                            ?>
                            <div class="col-md-4 mt-38 secondary-other-field <?= $class ?>">
                                <div class="md-form mb-0">
                                    <?=
                                    $form->field($model, 'other_secondary_commodity')->textInput([
                                        'class' => 'w-100'
                                    ])
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <?=
                                FormHelper::dropDownList($model, 'primary_service_type', $services, [
                                    'isDependent' => true,
                                    'other-field' => '.primary-service-other-field',
                                    'turn-over-dropdown' => '.primany-service-fist-turn-over',
                                    'turn-over-field-name' => 'primary_service_turnorver'
                                ]);
                                ?>
                            </div>

                            <div class="col-md-6">
                                <div class="md-form mb-0 w-100">
                                    <?=
                                    $form->field($model, 'primary_service_turnorver')->textInput([
                                        'class' => 'mx-2 form-control',
                                        'readonly' => true,
                                        'disabled' => true,
                                        'value' => $serviceTurnover
                                    ])
                                    ?>
                                </div>
                            </div>

                            <?php
                            $class = 'd-none';
                            if ($model->other_primary_service_type || $model->getFirstError('other_primary_service_type')) {
                                $class = '';
                            }
                            ?>
                            <div class="col-md-6 primary-service-other-field <?= $class ?>">
                                <div class="md-form mb-0">
                                    <?=
                                    $form->field($model, 'other_primary_service_type')->textInput([
                                        'class' => 'w-100 mx-2'
                                    ])
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-left:5px">
                            <div class="col-md-8">
                                <?=
                                FormHelper::multiDropDownList($model, 'secondary_service_type', $services, [
                                    'isDependent' => true,
                                    'other-field' => '.secondary-serviceother-field'
                                ])
                                ?>
                            </div>
                            <?php
                            $class = 'd-none';
                            if ($model->other_secondary_service_type || $model->getFirstError('other_secondary_service_type')) {
                                $class = '';
                            }
                            ?>
                            <div class="col-md-4 mt-38 secondary-serviceother-field  <?= $class ?>">
                                <div class="md-form mb-0">
                                    <?=
                                    $form->field($model, 'other_secondary_service_type')->textInput([
                                        'class' => 'w-100'
                                    ])
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="row"style="padding-top:15px;padding-left:5px">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'contact_person_name')->textInput() ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'contact_person_email')->textInput() ?>
                                </div>
                            </div>
                        </div>

                        <div class="row" >
                            <div class="col-md-12" style="padding-top: 15px;">
                                <?=
                                FormHelper::checkbox($form, $model, 'agreement_check');
                                ?>
                            </div>
                        </div>
                        <div class="row"  style="padding-left:5px">
                            <div class="col-md-12" style="padding-top: 15px;padding-left:5px;">
                                <?=
                                FormHelper::checkbox($form, $model, 'legal_entity_check');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="padding-top:15px;padding-left:10px;">
                                <label style="padding-left:20px;" class="vender-text"> (The company or subsidiary will not finance or support the activities or business of any person or entity, or for the benefit of any country or government, that is a target of any economic sanctions administered by (i) the United Nations Security Council (as a whole and not its individual members), (ii) the Office of Foreign Assets Control of the US Department of Treasury, (iii) the US Department of Commerce Bureau of Industry and Security, (iv) the US Department of State, (v) the European Union (as a whole and not its individual member states) and (vi) Her Majesty’s Treasury of the United Kingdom, (vi) ineligible to be awarded a World Bank Group-financed contract or otherwise sanctioned by the World Bank Group.)</label>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </section>
    </div>
</div>