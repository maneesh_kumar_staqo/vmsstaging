<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\RegistrationVendorInfo;
use common\helpers\FormHelper;

$this->title = 'Reason to add offline users';

$this->registerJsFile(Url::toRoute('/public/js/signup.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>
<section class="section hfe-login">
    <div class="row">
        <div class="col-md-12">
            <div class="hfe-reg-vender-middle ">
                <h2>
                    <?= $this->title ?>
                </h2>
                <?php $form = ActiveForm::begin(); ?>

                <div class="row" style="padding-top:15px;padding-left:5px;">
                    <div class="col-md-12">
                        <div class=" md-form mb-0">
                            <?= $form->field($model, 'reason')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="login-btn">
                            <div class="vender-circle-btn">
                                <button>
                                    <img src="<?= Url::toRoute('/public/img/register-button.png') ?>">
                                    <span>Next</span>
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>