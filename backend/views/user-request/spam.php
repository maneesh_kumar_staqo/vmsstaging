<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Requests : Spam Registrations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                <?= Html::encode($this->title) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?php
                //Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['create'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li>
                    <a href='<?= Url::toRoute('/user-request'); ?>'>Tier 1 Registrations</a>
                </li>
                <li>
                    <a href="<?= Url::toRoute('/user-request/tier2'); ?>">Tier 2 Registrations</a>
                </li>
                <li class="active">
                    <a href="javascript: void(0)">Spam Registrations</a>
                </li>
            </ul>
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'label' => 'Company name',
                            'attribute' => 'name',
                            'value' => function ($model) {
                                if ($model['vendor']) {
                                    return $model['vendor']['company_name'];
                                }

                                return 'N/A';
                            }
                        ],
                        'email:email',
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => '',
                           // 'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        
                        'created_at:datetime',
                        //'updated_at',
                        //'verification_token',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}'
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
