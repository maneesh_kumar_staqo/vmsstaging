<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\VendorInformation;
use common\helpers\FormHelper;
use yii\helpers\Html;
use common\models\VendorInformationPartner;
use common\models\VendorTypes;
use common\models\OrganisationTypes;

$this->registerJsFile(Url::toRoute('/public/js/signup.js?v=3'), [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile(Url::toRoute('/public/js/vendor-information.js?v=3'), [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));

$this->title = 'Vendor Information';
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<script>
    const IS_PARENT_DOMESTIC = <?= VendorTypes::IS_PARENT_DOMESTIC ?>;
    const IS_PARENT_OVERSEAS = <?= VendorTypes::IS_PARENT_OVERSEAS ?>;

    const ID_TRUST = <?= OrganisationTypes::ID_TRUST ?>;
</script>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('vendor-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <div class="hfe-reg-vender-middle">
			<h2>Vendor Details</h2>
			<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
			<div class="row">
				<div class="col-md-6">
					<div class=" md-form mb-0">
						<?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
					</div>
				</div>
			</div>
			<div class="row" >
				<div class="col-md-6" style="margin-left:-5px">
					<?=
					FormHelper::vendorTypeDropDownList($model, 'vendor_type', $vendors);
					?>
				</div>
				<div class="col-md-6">
					<?= FormHelper::organisationDropDownList($model, 'organization_type', $organisations); ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6">
					<?=
					FormHelper::multiDropDownList($model, 'business_type', $business, [
						'isDependent' => true,
						'other-field' => '.company-other-field',
						'checkTrasco' => true,
						'trasco-field' => '.transco-fields'
					]);
					?>
				</div>
				<div class="col-md-6 mt-38 company-other-field d-none">
					<div class="md-form mb-0">
						<?=
						$form->field($model, 'other_business')->textInput([
							'class' => 'w-100'
						])
						?>
					</div>
				</div>

				<?= FormHelper::fileInput($form, $model, 'company_image', [], $disableError) ?>

			</div>

			<div class="row transco-fields d-none" style="padding-top:30px">
				<div class="col-md-6  mt-38" style="">
					<div class="md-form mb-0">
						<?= $form->field($model, 'trasco_name')->textInput(['maxlength' => true]) ?>
					</div>
				</div>

				<?= FormHelper::fileInput($form, $model, 'trasco_document', [], $disableError) ?>
			</div>
			<div
				class="row" style="padding-top:30px"
				data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>"
				data-organisation-action="org-conditions"
				data-organisation-field-hide-on="*"
				data-organisation-field-show-on="<?= json_encode([
					OrganisationTypes::ID_HUF, OrganisationTypes::ID_INDIVIDUAL
				]) ?>"
			>
				<div class="col-md-6  mt-38" style="">
					<div class="md-form mb-0">
						<?= $form->field($model, 'adhaar_number')->textInput(['maxlength' => true]) ?>
					</div>
				</div>

				<?= FormHelper::fileInput($form, $model, 'adhaar_document', [], $disableError) ?>
			</div>

			<div
				data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>"
				data-organisation-action="org-conditions"
				data-organisation-field-show-on="*"
				data-organisation-field-hide-on="<?= json_encode([
					OrganisationTypes::ID_INDIVIDUAL, OrganisationTypes::ID_TRUST
				]) ?>"
			>
				<div class="row" style="padding-bottom:20px;">
					<div class="col-md-12">
						<div class="multioption">
							<label>Whether Registered under MSME Act 2006 or not?</label>
						</div>
					</div>
					<div class="col-md-6">
						<?=
						FormHelper::radioListV1($form, $model, 'is_msme_act_2006', VendorInformation::$optionArray, [
							'selected' => $model->is_msme_act_2006 ? $model->is_msme_act_2006 : VendorInformation::SELECTION_OPTION,
							'data-action' => 'is_msme_act_2006'
						])
						?>
					</div>
				</div>

				<div
					data-field="msme-yes"
					class="row"
					style="">
					<div class="col-md-6  mt-38">
						<div class="md-form mb-0">
							<?= $form->field($model, 'ssi_number')->textInput(['maxlength' => true]) ?>
						</div>
					</div>

					<?= FormHelper::fileInput($form, $model, 'ssi_certificate', [], $disableError) ?>
				</div>
			</div>

			<div class="row" data-section="registration_fields">
				<div class="col-md-6  mt-38">
					<div class="md-form mb-0">
						<?= $form->field($model, 'registration_number')->textInput(['maxlength' => true]) ?>
					</div>
				</div>

				<?= FormHelper::fileInput($form, $model, 'registration_document', [], $disableError) ?>
			</div>

			<hr style="border-top: 3px dotted #225776;">
			<!-- changes start list of director field according to selection field--->
			<div data-section="directors">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>List of Directors</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="directors"]'
							data-single-field='.single-director'
							data-total='<?= count($directorForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_DIRECTOR ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($directorForm as $key => $singleDirector): ?>
					<?=
					$this->render('partials/list-directors', [
						'key' => $key,
						'form' => $form,
						'model' => $singleDirector,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of list of directorchange feld -->

			<!-- changes start field according to selection field--->
			<div data-section="owners">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>List of Trustees</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="owners"]'
							data-single-field='.single-owner'
							data-total='<?= count($ownerForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_OWNER ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($ownerForm as $key => $singleOwer): ?>
					<?=
					$this->render('partials/list-owners', [
						'key' => $key,
						'form' => $form,
						'model' => $singleOwer,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of change field -->

			<!-- changes start field according to selection field--->
			<div data-section="partners">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>List of Partners</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="partners"]'
							data-single-field='.single-partner'
							data-total='<?= count($partnerForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_PARTNER ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($partnerForm as $key => $singlePartner): ?>
					<?=
					$this->render('partials/list-partner', [
						'key' => $key,
						'form' => $form,
						'model' => $singlePartner,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of change field -->

			<!-- changes start field according to selection field--->
			<div data-section="persons">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>Person Name</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="persons"]'
							data-single-field='.single-person'
							data-total='<?= count($personForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_PERSON ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($personForm as $key => $singlePerson): ?>
					<?=
					$this->render('partials/list-person', [
						'key' => $key,
						'form' => $form,
						'model' => $singlePerson,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of change field -->

			<!-- changes start field according to selection field--->
			<div data-section="kartas">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>List of Kartas</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="kartas"]'
							data-single-field='.single-karta'
							data-total='<?= count($kartaForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_KARTA ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($kartaForm as $key => $singleKarta): ?>
					<?=
					$this->render('partials/list-karta', [
						'key' => $key,
						'form' => $form,
						'model' => $singleKarta,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of change field -->

			<!-- changes start field according to selection field--->
			<div data-section="proprietors">
				<div class="multioption col-md-12 d-flex justify-content-between">
					<div>
						<label>List of Proprietor</label>
					</div>
					<div>
						<div
							class="col-md-12 cursor-pointer"
							style="margin-top: 20px;"
							data-action="add-more-list"
							data-fields='[data-section="proprietors"]'
							data-single-field='.single-proprietor'
							data-total='<?= count($proprietorForm) ?>'
							data-type="<?= VendorInformationPartner::TYPE_PROPRIETOR ?>"
							data-form='<?= json_encode($form) ?>'
							>
							<i class="fa fa-plus"></i>
						</div>
					</div>
				</div>
				<?php foreach ($proprietorForm as $key => $singleProprietor): ?>
					<?=
					$this->render('partials/list-proprietor', [
						'key' => $key,
						'form' => $form,
						'model' => $singleProprietor,
						'countries' => $countries,
						'disableError' => $disableError
					])
					?>
				<?php endforeach; ?>
			</div>
			<!---end of change field -->

			<div
				data-organisation-action="org-conditions"
				data-organisation-field-show-on="*"
				data-organisation-field-hide-on="<?= json_encode([OrganisationTypes::ID_INDIVIDUAL]) ?>"
			>
				<?=
				$this->render('partials/list-shareholders', [
					'form' => $form,
					'model' => $model,
					'shareHolders' => $shareHolders
				]);
				?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
    </div>
</div>