<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row">
    <div class="col-md-12">
        <div class="multioption">
            <label>Shareholder Details</label>
            <div>
                <span>
                    <i style="font-size:14px;font-family:FreightSansProMedium;">(min 1 and max 200)</i> </span><br><span style="font-family:FreightSansProMedium;">If a company is the direct shareholder, then list key shareholders up the chain until there are individual name to be screen.</span>
            </div>

        </div>
    </div>
</div>
<!--$shareHolder-->
<?php if ($shareHolders): ?>
    <?php foreach ($shareHolders as $key => $shareHolder): ?>
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => $key,
        'fistName' => $shareHolder['first_name'],
        'lastName' => $shareHolder['last_name'],
        'id' => $shareHolder['id']
    ])
    ?>
<?php endforeach; ?>
<?php else: ?>
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => 0,
    ])
    ?>
<?php endif ?>
<div class="row">
    <div class="col-md-12 cursor-pointer" style="text-align:right" data-action="add-share-holders">
        <i class="fa fa-plus"></i>
    </div>
</div>
<div data-append="share-holders"></div>

<div class="d-none hidden-shareholder" style="padding-top:20px;">
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => '{counter}',
    ])
    ?>
</div>