<?php

use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\Html;
use common\forms\UpdateStatus;
use yii\helpers\ArrayHelper;
use common\helpers\ReportHelper;

if (!$model->hfe_vendor_code) {
    $model->hfe_vendor_code = ReportHelper::generateHfeCode($model->id);
}
?>

<!-- Modal -->
<div id="updateStatusModel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body row">

                <?php $form = ActiveForm::begin() ?>
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'hfe_vendor_code')->textInput([
                            'readonly' => true
                        ]) ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'sap_vendor_code')->textInput() ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'region')->textInput() ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'company_code')->dropDownList(ArrayHelper::map($companyCode, 'id', 'company_name'), [
                            'prompt' => 'Select Option'
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'purchasing_organisation')->dropDownList(ArrayHelper::map($orgCode, 'id', 'org_name'), [
                            'prompt' => 'Select Option'
                        ]) ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'account_group')->dropDownList(ArrayHelper::map($accountCode, 'id', 'title'), [
                            'prompt' => 'Select Option'
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'payment_terms')->dropDownList(ArrayHelper::map($paymentCode, 'id', 'title'), [
                            'prompt' => 'Select Option'
                        ]) ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'application_status')->dropDownList(User::$applicationStatus) ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'application_status_message')->textarea()->label('Message') ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'send_mail')->checkbox() ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>