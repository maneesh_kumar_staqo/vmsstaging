<?php

use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\Html;

?>

<!-- Modal -->
<div id="updateInternalStatusModel" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Status</h4>
            </div>
            <div class="modal-body row">

                <?php $form = ActiveForm::begin() ?>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'status')->dropDownList(User::$applicationStatus) ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= $form->field($model, 'status_remark')->textarea()->label('Remarks') ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>