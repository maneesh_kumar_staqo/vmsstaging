<?php

use common\helpers\FormHelper;
?>
<div data-section="directors">
    <div class="single-director">
        <div class="row">
            <div class="multioption col-md-12">
                <label>List of Directors</label>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Director Name') ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'din_number')->textInput(['maxlength' => true])->label('DIN No') ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0" style="margin-top:55px;">
                    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true])->label('Director PAN No') ?>
                </div>
            </div>

            <?= FormHelper::fileInput($form, $model, 'din_document') ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="multioption">
                    <label>Director Residential Address</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="md-form mb-0" style="width:98%;">
                    <?= $form->field($model, 'address_line_1')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="md-form mb-0" style="width:98%;">
                    <?= $form->field($model, 'address_line_2')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'zip_code')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6" style="margin-top:20px">
                <?= FormHelper::simpleDropDownList($model, 'country', $countries, [
                    'hideLabel' => true
                ]); ?>
            </div>
        </div>
        <div class="row">
            <?= FormHelper::fileInput($form, $model, 'director_id_proof') ?>

            <?= FormHelper::fileInput($form, $model, 'director_address_proof') ?>
        </div>

        <div class="row" style="padding:20px 0px 5px 0px;">
            <div class="col-md-12" >
                <div class="custom-control custom-checkbox">
                    <?= $form->field($model, 'is_qualified_under_164')->checkbox(['maxlength' => true]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?= FormHelper::fileInput($form, $model, 'authorized_signatory') ?>

            <?= FormHelper::fileInput($form, $model, 'incorporation_certificate') ?>
        </div>
    </div>
</div>