<?php

use common\helpers\FormHelper;
?>
<div data-section="owners">
    <div class="single-owner">
        <div class="row">
            <div class="multioption col-md-12">
                <label>List of Owners</label>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Owner Name') ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'pan_number')->textInput(['maxlength' => true])->label('Owner PAN No') ?>
                </div>
            </div>

            <?= FormHelper::fileInput($form, $model, 'pan_document') ?>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="multioption">
                    <label>Owner Residential Address</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="md-form mb-0" style="width:98%;">
                    <?= $form->field($model, 'address_line_1')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-12">
                <div class="md-form mb-0" style="width:98%;">
                    <?= $form->field($model, 'address_line_2')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="md-form mb-0">
                    <?= $form->field($model, 'zip_code')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-md-6" style="margin-top:20px">
                <?= FormHelper::simpleDropDownList($model, 'country', $countries, [
                    'hideLabel' => true
                ]); ?>
            </div>
        </div>
        <div class="row">
            <?= FormHelper::fileInput($form, $model, 'director_id_proof') ?>

            <?= FormHelper::fileInput($form, $model, 'director_address_proof') ?>
        </div>

        <div class="row">
            <?= FormHelper::fileInput($form, $model, 'partnership_deed') ?>

            <?= FormHelper::fileInput($form, $model, 'authorisation_letter') ?>
        </div>
    </div>
</div>
