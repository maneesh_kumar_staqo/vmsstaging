<?php

use common\helpers\FormHelper;
use common\models\VendorTypes;
?>

<div class="single-owner single-owner-<?= $key ?>">
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']name')->textInput(['maxlength' => true])->label('Trustee Name') ?>
            </div>
        </div>
        <div class="col-md-6" data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>">
            <div class="md-form mb-0">
                <?=
                        $form->field($model, '[' . $key . ']pan_number')
                        ->textInput([
                            'maxlength' => true
                        ])->label('Trustee PAN No')
                ?>
            </div>
        </div>        
        <div class="col-md-6" data-vendor-field="<?= VendorTypes::IS_PARENT_OVERSEAS ?>" >
            <div class="md-form mb-0">
                <?=
                $form->field($model, '[' . $key . ']passport_no')->textInput([
                    'maxlength' => true
                ])->label('Trustee Passport No')
                ?>
            </div>
        </div>
        <div class="" data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>">
            <?=
            FormHelper::fileInput($form, $model, '[' . $key . ']pan_document', [
                'originalField' => 'pan_document',
                'key' => $key,
                'class' => 'col-sm-12'
                    ], $disableError)
            ?>
        </div>
        <div class="" data-vendor-field="<?= VendorTypes::IS_PARENT_OVERSEAS ?>" >
            <?=
            FormHelper::fileInput($form, $model, '[' . $key . ']upload_passport', [
                'originalField' => 'upload_passport',
                'key' => $key,
                'class' => 'col-sm-12'
                    ], $disableError)
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="multioption">
                <label>Trustee Residential Address</label>
            </div>
        </div>
        <div class="col-md-12">
            <div class="md-form mb-0" style="width:98%;">
                <?= $form->field($model, '[' . $key . ']address_line_1')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="md-form mb-0" style="width:98%;">
                <?= $form->field($model, '[' . $key . ']address_line_2')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']city')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']state')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']zip_code')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6" style="margin-top:20px">
            <?=
            FormHelper::simpleDropDownList($model, '[' . $key . ']country', $countries, [
                'hideLabel' => true,
                'originalField' => 'country',
                'key' => $key
            ]);
            ?>
        </div>
    </div>
    <div class="row">
        <?=
        FormHelper::fileInput($form, $model, '[' . $key . ']director_address_proof', [
            'originalField' => 'director_address_proof',
            'key' => $key
                ], $disableError)
        ?>
        
        <?=
        FormHelper::fileInput($form, $model, '[' . $key . ']partnership_deed', [
            'originalField' => 'partnership_deed',
            'key' => $key
                ], $disableError)
        ?>
    </div>
</div>
