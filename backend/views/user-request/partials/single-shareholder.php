<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="row single-share-holder" style="padding-top:20px;">
    <?php if (!empty($id)): ?>
        <?=
        $form->field($model, 'shareHolders[' . $key . '][id]')->hiddenInput([
            'maxlength' => true,
            'value' => $id
        ])->label(false)
        ?>
    <?php endif; ?>
    <div class="col-md-6">
        <div class="md-form mb-0">
            <?=
            $form->field($model, 'shareHolders[' . $key . '][first_name]')->textInput([
                'maxlength' => true,
                'value' => !empty($fistName) ? $fistName : ''
            ])->label('First Name')
            ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="md-form mb-0">
            <?=
            $form->field($model, 'shareHolders[' . $key . '][last_name]')->textInput([
                'maxlength' => true,
                'value' => !empty($lastName) ? $lastName : ''
            ])->label('Last Name')
            ?>
        </div>
    </div>
</div>