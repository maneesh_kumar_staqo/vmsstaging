<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;
use common\models\TaxInformation;

$this->registerJsFile(Url::toRoute('/public/js/tax-information.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile(Url::toRoute('/public/js/depended-radio.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);

$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));

$this->title = 'Tax Information';
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('tax-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">
                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle">
						<h2>Tax Information</h2>
						<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="multioption">
											<label style="padding-top:0px">Whether GSTIN is enrolled or not?</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<?=
								FormHelper::radioListV1($form, $model, 'is_gstin_enrolled', TaxInformation::$gstArray, [
									'data-action' => 'is_gstin_enrolled',
									'selected' => $model->is_gstin_enrolled ? $model->is_gstin_enrolled : TaxInformation::SELECTED_GST
								])
								?>
							</div>
						</div>
						<div class="row" data-field="gst-yes">                        
							<div class="col-md-6" style="padding-top:20px">
								<div class="md-form mb-0">
									<?= $form->field($model, 'gstin_number')->textInput(['maxlength' => true]) ?>
								</div>
								<div class="md-form mb-0">
									<?= $form->field($model, 'gst_address')->textInput(['maxlength' => true]) ?>
								</div>                           
							</div>
							<?= FormHelper::fileInput($form, $model, 'gst_certificate') ?>
						</div>
						<div class="row" data-field="gst-no">   
							<div class="col-md-12">
								<?=
								FormHelper::fileInput($form, $model, 'gst_non_enrollment_declaration', [
									'class' => ' '])
								?>
								<span><i><a href="https://www.herofutureenergies.com/VMS/frontend/web/public/img/Declaration-of-GST-Non-Enrollment.docx" target="_blank">Download Format here</a></i></span>
							</div>                        
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="multioption">
											<label style="padding-top:0px">Certificate for Lower Deduction of TDS</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<?=
								FormHelper::radioListV1($form, $model, 'certification_for_lower_deduction_tds', TaxInformation::$gstArray, [
									'data-action' => 'certification_for_lower_deduction_tds',
									'selected' => $model->certification_for_lower_deduction_tds ? $model->certification_for_lower_deduction_tds : TaxInformation::SELECTED_GST
								])
								?>
							</div>
						</div>
						<div class="row" data-field="tds-yes">
							<div class="col-md-6">
								<div class="md-form mb-0">
									<?= $form->field($model, 'tds_certificate_number')->textInput(['maxlength' => true]) ?>
								</div>
							</div>
							<?= FormHelper::fileInput($form, $model, 'tds_deduction_certificate') ?>
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-12">
										<div class="multioption">
											<label style="padding-top:0px">Whether PAN is in India?</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<?php
								if ($isDomestic) {
									$model->is_pan = 1;
								}
								?>
								<div class="<?= ($isDomestic) ? 'd-none' : '' ?>">
									<?=
									FormHelper::radioListV1($form, $model, 'is_pan', TaxInformation::$panArray, [
										'data-action' => 'depended-radio',
										'data-no-field' => 'pan-no',
										'data-yes-field' => 'pan-yes',
										'selected' => $model->is_pan ? $model->is_pan : TaxInformation::SELECTED_PAN
									])
									?>
								</div>
							</div>
						</div>
						<div class="row" data-field="pan-yes">
							<div class="col-md-6">
								<div class="md-form mb-0">
									<?= $form->field($model, 'pan_number')->textInput(['maxlength' => true]) ?>
								</div>
							</div>
							<?= FormHelper::fileInput($form, $model, 'upload_pan') ?>
						</div>
						<?php if ($isOverseas): ?>
							<div class="row" data-field="pan-no">
								<div class="col-md-6">
									<div class="row">
										<div class="col-md-12">
											<?=
											FormHelper::fileInput($form, $model, 'no_pan_in_india_declaration', [
												'class' => ''])
											?>
											<span>
												<i>
													<a href="https://www.herofutureenergies.com/VMS/frontend/web/public/img/NO-PAN-declaration.docx" target="_blank">
														Download Format here
													</a>
												</i>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-12">
											<div class="multioption">
												<label style="padding-top:0px">Whether Tax Residency Certificate (TRC) is applicable?</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<?=
									FormHelper::radioListV1($form, $model, 'is_trc', TaxInformation::$optionArray, [
										'selected' => $model->is_trc ? $model->is_trc : TaxInformation::SELECTION_OPTION,
										'data-action' => 'depended-radio',
										'data-no-field' => 'trc-no',
										'data-yes-field' => 'trc-yes',
									])
									?>
								</div>                    
							</div>
							<div class="row" data-field="trc-yes">
								<div class="col-md-8">
									<div class="md-form mb-0">
										<?= $form->field($model, 'trc')->textInput(['maxlength' => true]) ?>
									</div>                        
								</div>
								<?= FormHelper::fileInput($form, $model, 'upload_trc') ?>
								<div class="col-md-12" data-field="trc-no"></div>
							</div>

							<div class="row">	
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-12">
											<div class="multioption">
												<label style="padding-top:0px">Permanent Establishment in India ?</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<?=
									FormHelper::radioListV1($form, $model, 'permanent_est_india', TaxInformation::$optionArray, [
										'selected' => $model->permanent_est_india ? $model->permanent_est_india : TaxInformation::SELECTION_OPTION,
										'data-action' => 'depended-radio',
										'data-no-field' => 'est-no',
										'data-yes-field' => 'est-yes',
									])
									?>
								</div>
							</div>
							<div class="row" data-field="est-yes">
								<?= FormHelper::fileInput($form, $model, 'upload_pe_certificate') ?>
							</div>
							<div class="row" data-field="est-no">
								<div class="col-md-12" style="padding-left: 0">
									<?= FormHelper::fileInput($form, $model, 'no_pe_india') ?>
									<span style="padding-left: 15px;"><i><a href="https://www.herofutureenergies.com/VMS/frontend/web/public/img/HFE-No-PE-declaration.docx" target="_blank">Download Format here</a></i></span>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="row">
										<div class="col-md-12">
											<div class="multioption">
												<label style="padding-top:0px">IEC Code/ Import, Export License (Y/N)</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<?=
									FormHelper::radioListV1($form, $model, 'is_iec', TaxInformation::$optionArray, [
										'selected' => $model->is_iec ? $model->is_iec : TaxInformation::SELECTION_OPTION,
										'data-action' => 'depended-radio',
										'data-no-field' => 'iec-no',
										'data-yes-field' => 'iec-yes',
									])
									?>
								</div>
							</div>
							<div class="row" data-field="iec-yes">
								<div class="col-md-8">
									<div class="md-form mb-0">
										<?= $form->field($model, 'iec_code')->textInput(['maxlength' => true]) ?>
									</div>                        
								</div>
								<?= FormHelper::fileInput($form, $model, 'upload_iec') ?>
							</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-md-8">
								<div class="multioption" style="margin-top:7px;">
									<label style="padding-top:0px">Whether TIN/CST/VAT is applicable?</label>
								</div>
							</div>
							<div class="col-md-4">
								<?=
								FormHelper::radioListV1($form, $model, 'is_tin_cst_vat', TaxInformation::$panArray, [
									'data-action' => 'depended-radio',
									'data-no-field' => 'tin-no',
									'data-yes-field' => 'tin-yes',
									'selected' => $model->is_tin_cst_vat ? $model->is_tin_cst_vat : TaxInformation::SELECTED_TIN
								])
								?>
							</div>
						</div>
						<div class="row" data-field="tin-yes">
							<div class="col-md-6">
								<div class="md-form mb-0">
									<?= $form->field($model, 'tin_cst_vat_number')->textInput(['maxlength' => true]) ?>
								</div>
							</div>
							<?= FormHelper::fileInput($form, $model, 'upload_tin_cst_vat', ['class' => 'col-sm-12']) ?>
						</div>
						<div class="row">
							<div class="col-md-8 col-6">
								<div class="login-btn">
									<div class="vender-circle-btn">
										<a href="<?= Url::toRoute('/detail-completion/bank-information') ?>">
											<img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
												Go Back
											</span>
										</a>
									</div> 
								</div>
								<div class="login-btn">
									<div class="save-and-go-next">
										<button type="submit">
											<img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
												Save & Go Next
											</span>
										</button>
									</div> 
								</div>
							</div>
						</div>
						<?php ActiveForm::end(); ?>
					</div>
                </div>
            </div>
        </section>
    </div>
</div>