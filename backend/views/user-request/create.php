<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\RegistrationVendorInfo;
use common\helpers\FormHelper;

$this->title = 'Register Vendor Account';

$this->registerJsFile(Url::toRoute('/public/js/signup.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);
$this->registerJsFile(Url::toRoute('/public/js/offline_registration.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);
?>
<section class="section hfe-login">
    <div class="row">

        <div class="col-md-12">
            <div class="hfe-reg-vender-middle ">
                <h2><?= $this->title ?></h2>

                <?php $form = ActiveForm::begin(); ?>
                <div class="offline-form-one">
                    <div class="row">
                        <div class="col-md-6">
                            <div class=" md-form mb-0">
                                <?= $form->field($model, 'offline_reason')->textInput([
                                    'maxlength' => true,
                                    'class' => 'offline_reason form-control'
                                ]) ?>
                            </div>
                        </div>
                        <?= FormHelper::fileInput($form, $model, 'offline_document', [
                            'class' => 'col-sm-6 offline_document'
                        ]); ?>
                    </div>
                    <div class="">
                        <div class="login-btn">
                            <div class="vender-circle-btn">
                                <a
                                    href="javascript: void(0)"
                                    data-action="display-next-offline"
                                >
                                    <img src="<?= Url::toRoute('/public/img/register-button.png') ?>">
                                    <span>Next</span>
                                </a>
                            </div> 
                        </div>
                    </div>
                </div>

                <div class="d-none offline-form-two" >
                    <?=
                    FormHelper::radioList($form, $model, 'bussiness_registration', RegistrationVendorInfo::$bussinessRegistrationTypes, [
                        'value' => 1
                    ]);
                    ?>

                    <div class="row" style="padding-top:15px;padding-left:5px;">
                        <div class="col-md-6">
                            <div class=" md-form mb-0">
                                <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6 d-flex  row">
                            <div class="md-form mb-0 row">
                                <div class="d-none trading_name col-md-6">
                                    <?= $form->field($model, 'trading_name')->textInput(['maxlength' => true]) ?>
                                </div>

                                <div class=" same_as_company_name col-md-6">
                                    <div class="">
                                        <?=
                                        FormHelper::checkbox($form, $model, 'sameAsCompanyName');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" >
                        <div class="col-md-6">
                            <?=
                            FormHelper::dropDownList($model, 'fk_primary_commodity_type', $commodities, [
                                'isDependent' => true,
                                'other-field' => '.primary-other-field',
                                'turn-over-dropdown' => '.primany-fist-turn-over',
                                'turn-over-field-name' => 'annual_turnover'
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6">
                            <?=
                            FormHelper::turnOverDropdown($model, 'annual_turnover', RegistrationVendorInfo::$annualTurnovers, [
                                'class' => 'primany-fist-turn-over',
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6 primary-other-field d-none">
                            <div class="md-form mb-0">
                                <?=
                                $form->field($model, 'other_primary_commodity')->textInput([
                                    'class' => 'w-100 mx-2'
                                ])
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-left:5px">
                        <div class="col-md-8">
                            <?=
                            FormHelper::multiDropDownList($model, 'fk_secondary_commodity_type', $commodities, [
                                'isDependent' => true,
                                'other-field' => '.secondary-other-field'
                            ])
                            ?>
                        </div>
                        <div class="col-md-4 mt-38 secondary-other-field d-none">
                            <div class="md-form mb-0">
                                <?=
                                $form->field($model, 'other_secondary_commodity')->textInput([
                                    'class' => 'w-100'
                                ])
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?=
                            FormHelper::dropDownList($model, 'primary_service_type', $services, [
                                'isDependent' => true,
                                'other-field' => '.primary-service-other-field',
                                'turn-over-dropdown' => '.primany-service-fist-turn-over',
                                'turn-over-field-name' => 'primary_service_turnorver'
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6">
                            <?=
                            FormHelper::turnOverDropdown($model, 'primary_service_turnorver', RegistrationVendorInfo::$annualTurnovers, [
                                'class' => 'primany-service-fist-turn-over',
                            ]);
                            ?>
                        </div>

                        <div class="col-md-6 primary-service-other-field d-none">
                            <div class="md-form mb-0">
                                <?=
                                $form->field($model, 'other_primary_service_type')->textInput([
                                    'class' => 'w-100 mx-2'
                                ])
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-left:5px">
                        <div class="col-md-8">
                            <?=
                            FormHelper::multiDropDownList($model, 'secondary_service_type', $services, [
                                'isDependent' => true,
                                'other-field' => '.secondary-serviceother-field'
                            ])
                            ?>
                        </div>
                        <div class="col-md-4 mt-38 secondary-serviceother-field d-none">
                            <div class="md-form mb-0">
                                <?=
                                $form->field($model, 'other_secondary_commodity')->textInput([
                                    'class' => 'w-100'
                                ])
                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="row"style="padding-top:15px;padding-left:5px">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'contact_person_name')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'contact_person_email')->textInput() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding-left:5px;padding-left:5px">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'password')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-12" style="padding-top: 15px;">
                            <?=
                            FormHelper::checkbox($form, $model, 'agreement_check');
                            ?>
                        </div>
                    </div>
                    <div class="row"  style="padding-left:5px">
                        <div class="col-md-12" style="padding-top: 15px;padding-left:5px;">
                            <?=
                            FormHelper::checkbox($form, $model, 'legal_entity_check');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="padding-top:15px;padding-left:10px;">
                            <label style="padding-left:20px;" class="vender-text"> (for Sanction Country please refer list of sanctioned countries as per US treasury department)</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="login-btn">
                                <div class="vender-circle-btn">
                                    <button>
                                        <img src="<?= Url::toRoute('/public/img/register-button.png') ?>">
                                        <span>Register</span>
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>

                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>