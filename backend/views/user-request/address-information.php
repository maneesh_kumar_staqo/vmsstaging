<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AddressInformation */
/* @var $form yii\widgets\ActiveForm */


$this->registerCssFile(Url::toRoute('/public/css/mdb.min.css'));

$this->title = 'Address Information';
?>
<?= $this->render('partials/update-internal-status', [
    'model' => $model
]) ?>
<div class="panel panel-success">
    <div class="panel-heading">
        <span>
            <?= Html::encode($this->title) ?>
        </span>
        <span class="pull-right">
            <?=
            Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/user-request/view?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
            ?>
            <?php if (Yii::$app->user->can('address-information')): ?>
            <?=
            Html::a('<i class="glyphicon glyphicon-pencil"></i> UPDATE STATUS', 'javascript: void(0)', [
                'class' => 'btn btn-primary btn-xs',
                'data-toggle' => 'modal',
                'data-target' => '#updateInternalStatusModel'
            ]);
            ?>
            <?php endif; ?>
        </span>
    </div>
    <div class="panel-body">
        <section class="section hfe-login">
            <div class="row">
                <div class="col-md-12">
                    <div class="hfe-reg-vender-middle">
                        <h2>Registered Address</h2>
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_house_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_street_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_city')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_state')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_post_office')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_telephone_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_fax_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_country')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ra_pin_code')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="margin-top:15px;">Communication Address</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_house_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_street_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_city')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_state')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_post_office')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_telephone_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_fax_number')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_country')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <?= $form->field($model, 'ca_pin_code')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>