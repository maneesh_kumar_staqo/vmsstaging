<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VendorTypes */

$this->title = 'Update Vendor Types: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Vendor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vendor-types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
