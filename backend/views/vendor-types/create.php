<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VendorTypes */

$this->title = 'Create Vendor Types';
$this->params['breadcrumbs'][] = ['label' => 'Vendor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vendor-types-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
