<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\VendorTypes;

/* @var $this yii\web\View */
/* @var $model common\models\Services */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-sm-10 col-sm-offset-1">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span>
                <?= $this->title ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['index'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'status')->dropDownList(VendorTypes::$statusArray) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'priority')->textInput() ?>
                </div>
            </div>
<!--            <div class="col-sm-12">
                <div class="col-sm-4" style="margin-top: 28px;">
                    <?php // $form->field($model, 'is_domestic')->checkbox() ?>
                </div>
                <div class="col-sm-4" style="margin-top: 28px;">
                    <?php // $form->field($model, 'is_government')->checkbox() ?>
                </div>
                <div class="col-sm-4" style="margin-top: 28px;">
                    <?php // $form->field($model, 'is_landowner')->checkbox() ?>
                </div>
            </div>-->

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?=
                    Html::submitButton('Save', [
                        'class' => 'btn btn-success',
                    ])
                    ?>
                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>