<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\CompanyTypes;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Business Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                <?= Html::encode($this->title) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?=
                Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['create'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">

            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'title',
                    'priority',
                    'status',
                    'created_at',
                    [
                        'label' => 'Status',
                        'attribute' => 'status',
                        'filter' => CompanyTypes::$statusArray,
                        'value' => function ($model) {
                            return CompanyTypes::$statusArray[$model->status];
                        }
                    ],
                    'priority',
                    'display_other_field',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{update}'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
