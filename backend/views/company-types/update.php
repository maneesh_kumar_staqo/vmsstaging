<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyTypes */

$this->title = 'Update Business Types: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Business Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="company-types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
