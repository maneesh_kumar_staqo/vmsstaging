<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CompanyTypes */

$this->title = 'Create Business Types';
$this->params['breadcrumbs'][] = ['label' => 'Business Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="company-types-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
