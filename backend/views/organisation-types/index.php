<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\OrganisationTypes;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Organisation Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                <?= Html::encode($this->title) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?=
                Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['create'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [

                    'title',
                    'priority',
                    [
                        'label' => 'Status',
                        'attribute' => 'status',
                        'filter' => OrganisationTypes::$statusArray,
                        'value' => function ($model) {
                            return OrganisationTypes::$statusArray[$model->status];
                        }
                    ],
                    [
                        'label' => 'Type',
                        'attribute' => 'type',
                        'filter' => OrganisationTypes::$typeArray,
                        'value' => function ($model) {
                            return OrganisationTypes::$typeArray[$model->type];
                        }
                    ],
                    [
                        'label' => 'Owner Type',
                        'attribute' => 'owners_type',
                        'filter' => OrganisationTypes::$ownerTypeArray,
                        'value' => function ($model) {
                            return OrganisationTypes::$ownerTypeArray[$model->owners_type];
                        }
                    ],
                    //'created_at',
                    //'updated_at',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{update}'],
                ],
            ]);
            ?>
        </div>
    </div>
</div>
