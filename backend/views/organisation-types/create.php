<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrganisationTypes */

$this->title = 'Create Organisation Types';
$this->params['breadcrumbs'][] = ['label' => 'Organisation Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="organisation-types-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
