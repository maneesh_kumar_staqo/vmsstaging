<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrganisationTypes */

$this->title = 'Update Organisation Types: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Organisation Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="organisation-types-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
