<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PoTracking */

$this->title = 'Update PO Tracking: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Po Trackings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="po-tracking-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
