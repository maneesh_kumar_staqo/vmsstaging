<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PO Trackings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                <?= Html::encode($this->title) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?=
                Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['create'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'label' => 'Vendor',
                        'attribute' => 'fk_vendor',
                        'value' => function ($model) {
                            return $model['vendor']['email'];
                        }
                    ],
                    'po_number',
                    'project_title',
                    'date_of_issue:date',
                    'amount',
                    'tax',
                    'total_amount',
                    'total_invoice',
//                    'description',
                    //'document',
                    //'status',
                    //'created_by',
                    //'created_at',

                    [
                        'class' => 'yii\grid\ActionColumn', 
                        'template' => '{update}{view}'
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
