<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PoTracking */

$this->title = 'Create PO Tracking';
$this->params['breadcrumbs'][] = ['label' => 'Po Trackings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="po-tracking-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
