<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\PoTracking;
use common\helpers\FormHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\PoTracking */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js', [
    'depends' => yii\web\JqueryAsset::className()
]);

$this->registerJsFile(Url::toRoute('/public/js/gst_calculation.js'), [
    'depends' => yii\web\JqueryAsset::className()
]);

$this->registerCssFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css');

$vendors = PoTracking::getAll();
?>

<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span>
                <?= $this->title ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['index'], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?= $form->field($model, 'project_title')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-sm-12">

                <div class="col-sm-6">
                    <?= $form->field($model, 'fk_vendor')->dropDownList($vendors, [
                        'prompt' => 'Select a vendor'
                    ]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'po_number')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'date_of_issue')->textInput([
                        'class' => 'form-control datePicker'
                    ]) ?>
                </div>
                <?= FormHelper::fileInput($form, $model, 'document') ?>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <?= $form->field($model, 'amount')->textInput([
                        'maxlength' => true,
                        'data-field' => 'amount',
                        'placeholder' => 'Enter Amount'
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'igst')->textInput([
                        'maxlength' => true,
                        'data-field' => 'igst',
                        'placeholder' => 'Enter IGST'
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'cgst')->textInput([
                        'maxlength' => true,
                        'data-field' => 'cgst',
                        'placeholder' => 'Enter CGST'
                    ]) ?>
                </div>
                <div class="col-sm-2">
                    <?= $form->field($model, 'sgst')->textInput([
                        'maxlength' => true,
                        'data-field' => 'sgst',
                        'placeholder' => 'Enter SGST'
                    ]) ?>
                </div>

                <div class="col-sm-3">
                    <?= $form->field($model, 'total_amount')->textInput([
                        'maxlength' => true,
                        'data-field' => 'total_amount',
                        'placeholder' => 'Total Amount'
                    ]) ?>
                </div>
                <div class="col-sm-1 btn-refresh">
                    <i class="glyphicon glyphicon-refresh" data-field='reset'></i>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?=
                    Html::submitButton('Save', [
                        'class' => 'btn btn-success',
                    ])
                    ?>
                    <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
