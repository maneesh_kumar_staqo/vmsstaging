<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\PoInvoice;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PO Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <span>
                <?= Html::encode($this->title) ?>
            </span>
            <span class="pull-right">
                <?=
                Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/po-tracking'], ['class' => 'btn btn-primary btn-xs']);
                ?>
                <?=
                Html::a('<i class="glyphicon glyphicon-plus-sign"></i> Add New', ['add-invoice?id=' . $id], ['class' => 'btn btn-primary btn-xs']);
                ?>
            </span>
        </div>
        <div class="panel-body">
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'invoice_number',
                    [
                       'label'  => 'Status',
                        'attribute' => 'status',
                        'filter' => PoInvoice::$statusArray,
                        'value' => function ($model) {
                            if (!empty(PoInvoice::$statusArray[$model['status']])) {
                                return PoInvoice::$statusArray[$model['status']];
                            }

                            return 'N/A';
                        }
                    ],
                    [
                       'label'  => 'Payment',
                        'attribute' => 'payment',
                        'filter' => PoInvoice::$paymentArray,
                        'value' => function ($model) {
                            if (!empty(PoInvoice::$paymentArray[$model['payment']])) {
                                return PoInvoice::$paymentArray[$model['payment']];
                            }

                            return 'N/A';
                        }
                    ],
                    'amount',
                    'total_amount',
                    [
                        'class' => 'yii\grid\ActionColumn', 
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model) use ($id) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',
                                    Url::toRoute('/po-tracking/add-invoice?id=' . $id . '&idInvoice=' . $model['id']));
                            }
                            ]
                    ],
                    //'created_at',
                ],
            ]);
            ?>
        </div>
    </div>
</div>
