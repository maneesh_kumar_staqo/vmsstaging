<?php
/* @var $this yii\web\View */

$this->title = 'Report Dashboard';

$menuItem = [];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor Registrations - Tier 1',
    'url' => '/reports/reports/register',
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor Registrations - Tier 2',
    'url' => '/reports/reports/registert2',
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Spam Registrations',
    'url' => '/reports/reports/spam',
];
//
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Commodity wise Registered Vendors',
    'url' => '/reports/reports/commodity'
];
//
$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Service-wise Registered Vendors',
    'url' => '/reports/reports/service'
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor Contact Report',
    'url' => '/reports/reports/contact'
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor Finance Report',
    'url' => '/reports/reports/finance'
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor Shareholder',
    'url' => '/reports/reports/shareholder'
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor SAP - All',
    'url' => '/reports/reports/sap'
];

$menuItem[] = [
    'icon' => 'dash-lg glyphicon glyphicon-download',
    'title' => 'Vendor SAP - Yet to be Processed',
    'url' => '/reports/reports/sappending'
];

//
//
//
//$menuItem[] = [
//    'icon' => 'dash-lg glyphicon glyphicon-download',
//    'title' => 'Registered Vendors Contact | Approved Vendors Contact',
//    'url' => '/reports/reports/registerdefault/dashboard'
//];
//$menuItem[] = [
//    'icon' => 'dash-lg glyphicon glyphicon-download',
//    'title' => 'Rejected/Disapproved/Pending Vendor',
//    'url' => '/reports/reports/registerdefault/dashboard'
//];
//$menuItem[] = [
//    'icon' => 'dash-lg glyphicon glyphicon-download',
//    'title' => 'Registered under SPAM category Vendor',
//    'url' => '/reports/reports/registerdefault/dashboard'
//];

?>

<div class="">
    <div class="panel panel-success">
        <div class="panel-heading">
            <?= $this->title ?>
        </div>
        <div class="panel-body">
            <?=
            $this->render('partials/single-box', [
                'menuItem' => $menuItem
            ])
            ?>
        </div>
    </div>
</div>