<?php

///////////////////////////////////////////////
//      Available Options Are Given Below   //
/////////////////////////////////////////////
// $panel like defualt,danger,primary
// $imageType like image or icon
// $imageUrl url to the image
// $icon is glyphicon or font-awesome
// $title is Title of the box
// $url is redirection url on view detail option
// $colClass columns per rows bootstrap
use yii\helpers\Url;

if (isset($menuItem) && is_array($menuItem)):
    foreach ($menuItem as $key => $item):
        ?>

        <div class="<?= !empty($item['colClass']) ? $item['colClass'] : 'col-lg-4 col-sm-4' ?>">
            <div class="panel panel-<?= empty($item['panel']) ? 'info' : $item['panel'] ?>">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3 dash-icon-color ">
                            <?php if (!empty($item['imageType']) && $item['imageType'] == 'image'): ?>
                                <img src="<?= Url::base(true) . $item['imageUrl'] ?>" height="72px" width="72px">
                            <?php else: ?>
                                <i class="<?= $item['icon'] ?>"></i>
                            <?php endif; ?>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="dash-title"><?= $item['title'] ?></div>
                        </div>
                    </div>
                </div>
                <a href="<?php echo Url::toRoute([$item['url']], true) ?>">
                    <div class="panel-footer">
                        <span class="pull-left">
                            View & Export
                        </span>
                        <span class="pull-right"><i class="glyphicon glyphicon-circle-arrow-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    <?php endforeach;
endif;
?>