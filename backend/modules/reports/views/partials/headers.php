<?php

use yii\helpers\Html;
?>
<div class="panel-heading">
    <span>
        <?= Html::encode($this->title) ?>
    </span>
    <span class="pull-right">
        <?=
        Html::a('<i class="glyphicon glyphicon-circle-arrow-left"></i> Go back', ['/reports/default/dashboard'], ['class' => 'btn btn-primary btn-xs']);
        ?>
        <?php if (isset($actionExport)): ?>
        <?=
        Html::a('<i class="glyphicon glyphicon-export"></i> Export', [ $actionExport .'?' . Yii::$app->request->queryString], [
            'class' => 'btn btn-primary btn-xs'
        ]);
        ?>
        <?php endif; ?>
    </span>
</div>