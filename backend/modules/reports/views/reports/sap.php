<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'SAP Reports - All';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Url::toRoute('/public/js/sap.js'), [
    'depends' => yii\web\JqueryAsset::className()
]);
?>
<?= $this->render('partials/update_code', [
    'model' => $updateModel
]) ?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
            'actionExport' => 'sap-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'VMS Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'SAP Vendor Code',
                            'attribute' => 'sap_vendor_code',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if (!empty($model['sap_vendor_code'])) {
                                    return $model['sap_vendor_code'];
                                } else {
                                    return Html::a("update <i class='fa fa-edit'></i>", 'javascript: void(0)', [
                                        'data-action' => 'sap_vendor_code_btn',
                                        'data-id' => $model['id'],
                                        'data-sap_vendor_code' => $model['sap_vendor_code']
                                    ]);
                                }
                            }
                        ],
                        [
                            'label' => 'Region',
                            'attribute' => 'region',
                        ],
                        [
                            'label' => 'Company Code',
                            'attribute' => 'company_code',
                            'value' => function ($model) {
                                if ($model['companyCode']) {
                                    return $model['companyCode']['company_name'];
                                }
                            }
                        ],
                        [
                            'label' => 'Org Code',
                            'attribute' => 'purchasing_organisation',
                            'value' => function ($model) {
                                if ($model['orgCode']) {
                                    return $model['orgCode']['org_name'];
                                }
                            }
                        ],
                        [
                            'label' => 'Account Group',
                            'attribute' => 'account_group',
                            'value' => function ($model) {
                                if ($model['accountCode']) {
                                    return $model['accountCode']['title'];
                                }
                            }
                        ],
                        [
                            'label' => 'Payment Terms',
                            'attribute' => 'payment_terms',
                            'value' => function ($model) {
                                if ($model['paymentCode']) {
                                    return $model['paymentCode']['title'];
                                }
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
