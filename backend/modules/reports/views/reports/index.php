<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendor Registered';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
            'actionExport' => 'register-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Application status',
                            'attribute' => 'application_status',
                            'filter' => User::$applicationStatus,
                            'value' => function ($model) {
                                if (!empty(User::$applicationStatus[$model['application_status']])) {
                                    return User::$applicationStatus[$model['application_status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Vendor Type',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['vendor']['vendorType']) {
                                    return $model['vendor']['vendorType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Organisation Type',
                            'attribute' => 'company_type',
                            'value' => function ($model) {
                                if ($model['vendor']['organisationType']) {
                                    return $model['vendor']['organisationType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'City',
                            'attribute' => 'city',
                            'value' => function ($model) {
                                if ($model['address']) {
                                    return $model['address']['ra_city'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Pin Code',
                            'attribute' => 'pincode',
                            'value' => function ($model) {
                                if ($model['address']) {
                                    return $model['address']['ra_pin_code'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Country',
                            'attribute' => 'country',
                            'value' => function ($model) {
                                if ($model['address']) {
                                    return $model['address']['ra_country'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Email',
                            'attribute' => 'contact_detail',
                            'value' => function ($model) {
                                if ($model['contact']) {
                                    return $model['contact']['b_name'] . ' ( ' . $model['contact']['b_email'] . ' )';
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Mobile Number',
                            'attribute' => 'mobile_number',
                            'value' => function ($model) {
                                if ($model['contact']) {
                                    return $model['contact']['b_mobile_number'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Commodities',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['registration']['primaryCommodity']) {
                                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                                        return $model['registration']['other_primary_commodity'];
                                    } else {
                                        return $model['registration']['primaryCommodity']['title'];
                                    }
                                }

                                return 'N/A';
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
