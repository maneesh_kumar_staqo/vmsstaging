<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shareholder Report';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
//            'actionExport' => 'shareholder-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Application status',
                            'attribute' => 'application_status',
                            'filter' => User::$applicationStatus,
                            'value' => function ($model) {
                                if (!empty(User::$applicationStatus[$model['application_status']])) {
                                    return User::$applicationStatus[$model['application_status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Vendor Type',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['vendor']['vendorType']) {
                                    return $model['vendor']['vendorType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{export}',
                            'buttons' => [
                                'export' => function ($url, $model) {
                                    return Html::a('<i class="glyphicon glyphicon-export"></i>',
                                        Url::toRoute('/reports/reports/shareholder-export?id_vendor=' . $model['id']), []);
                                }
                            ]
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
