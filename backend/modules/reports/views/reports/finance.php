<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Finance Report';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
            'actionExport' => 'finance-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Vendor Type',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['vendor']['vendorType']) {
                                    return $model['vendor']['vendorType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'GSTN',
                            'attribute' => 'GSTN',
                            'value' => function ($model) {
                                $tax = 'N/A';
                                if ($model['tax']) {
                                    $tax = $model['tax']['gstin_number'];
                                }

                                return $tax;
                            }
                        ],
                        [
                            'label' => 'Primary Service',
                            'attribute' => 'primary_service',
                            'value' => function ($model) {
                                if ($model['registration']['primaryService']) {
                                    if ($model['registration']['primaryService']['display_other_field']) {
                                        return $model['registration']['other_primary_service_type'];
                                    } else {
                                        return $model['registration']['primaryService']['title'];
                                    }
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Service Turnover',
                            'attribute' => 'turnover',
                            'value' => function ($model) {
                            $turnover = 'N/A';

                                if ($model['registration']['primaryService']) {

                                    if (!empty($model['registration']['primary_service_turnorver'])) {
                                        if ($model['registration']['primary_service_turnorver'] == 1) {
                                            $turnover = $model['registration']['primaryService']['upper_turnover'];
                                        } else {
                                            $turnover = $model['registration']['primaryService']['lower_turnover'];
                                        }
                                    } else {
                                        $turnover = 'N/A';
                                    }
                                }

                                return $turnover;
                            }
                        ],
                        [
                            'label' => 'Primary Commodity',
                            'attribute' => 'primary_commodity',
                            'value' => function ($model) {
                                if ($model['registration']['primaryCommodity']) {
                                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                                        return $model['registration']['other_primary_commodity'];
                                    } else {
                                        return $model['registration']['primaryCommodity']['title'];
                                    }
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Commodity Turnover',
                            'attribute' => 'turnover',
                            'value' => function ($model) {
                                $turnover = 'N/A';

                                if ($model['registration']['primaryCommodity']) {

                                    if (!empty($model['registration']['annual_turnover'])) {
                                        if ($model['registration']['annual_turnover'] == 1) {
                                            $turnover = $model['registration']['primaryCommodity']['upper_turnover'];
                                        } else {
                                            $turnover = $model['registration']['primaryCommodity']['lower_turnover'];
                                        }
                                    } else {
                                        $turnover = 'N/A';
                                    }
                                }

                                return $turnover;
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
