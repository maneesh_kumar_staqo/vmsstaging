<?php

use yii\grid\GridView;
use common\models\User;
use common\models\VendorInformation;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Vendor Contact Report';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
            'actionExport' => 'contact-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Application status',
                            'attribute' => 'application_status',
                            'filter' => User::$applicationStatus,
                            'value' => function ($model) {
                                if (!empty(User::$applicationStatus[$model['application_status']])) {
                                    return User::$applicationStatus[$model['application_status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Vendor Type',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['vendor']['vendorType']) {
                                    return $model['vendor']['vendorType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Business Contact Name',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['b_name'];
                                }
                            }
                        ],
                        [
                            'label' => 'Business Contact Email',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['b_email'];
                                }
                            }
                        ],
                        [
                            'label' => 'Business Contact Phone',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['b_mobile_number'];
                                }
                            }
                        ],

                        [
                            'label' => 'Finance Contact Name',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['f_name'];
                                }
                            }
                        ],
                        [
                            'label' => 'Finance Contact Email',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['f_email'];
                                }
                            }
                        ],
                        [
                            'label' => 'Finance Contact Phone',
                            'attribute' => 'primary_name',
                            'value' => function ($model) {
                                if (!empty($model['contact'])) {
                                    return $model['contact']['f_mobile_number'];
                                }
                            }
                        ]
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
