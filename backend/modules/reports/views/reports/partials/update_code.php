<?php

use yii\widgets\ActiveForm;
use common\helpers\FormHelper;
?>
<!-- Modal -->
<div
    class="modal fade"
    id="sap_vendor_code_model"
    tabindex="-1"
    role="dialog"
    aria-labelledby="myModalLabel"
    >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button
                    type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <?php $form = ActiveForm::begin([
                    'enableAjaxValidation' => true
                ]); ?>
                <?= $form->field($model, 'sap_vendor_code')->textInput(['id' => 'sap_vendor_code']) ?>
                <?= $form->field($model, 'id')->hiddenInput(['id' => 'id_user'])->label(false) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>