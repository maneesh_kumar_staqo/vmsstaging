<?php

use yii\grid\GridView;
use common\models\User;
use common\models\VendorInformation;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Commodity Wise Registration';
$this->params['breadcrumbs'][] = ['label' => 'Reports', 'url' => ['/reports/default/dashboard']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="">
    <div class="panel panel-success">
        <?= $this->render('../partials/headers', [
            'actionExport' => 'commodity-export'
        ]) ?>

        <div class="panel-body table-responsive">
            <div class="p-2">
                <?=
                GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        [
                            'attribute' => 'created_at',
                            'label' => 'Created At',
                            'filterInputOptions' => [
                                'class' => 'form-control dateRangePickerSelection'
                            ],
                            'value' => function ($model) {
                                return Yii::$app->formatter->asDatetime($model['created_at']);
                            }
                        ],
                        [
                            'label' => 'Vendor Code',
                            'attribute' => 'id',
                            'value' => function ($model) {
                                return $model['id'];
                            }
                        ],
                        [
                            'label' => 'Vendor Name',
                            'attribute' => 'company_name',
                            'value' => function ($model) {
                                return $model['registration']['company_name'];
                            }
                        ],
                        [
                            'label' => 'Account Status',
                            'attribute' => 'status',
                            'filter' => User::$statusArray,
                            'value' => function ($model) {
                                if (!empty(User::$statusArray[$model['status']])) {
                                    return User::$statusArray[$model['status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Application status',
                            'attribute' => 'application_status',
                            'filter' => User::$applicationStatus,
                            'value' => function ($model) {
                                if (!empty(User::$applicationStatus[$model['application_status']])) {
                                    return User::$applicationStatus[$model['application_status']];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Vendor Type',
                            'attribute' => 'vendor_type',
                            'value' => function ($model) {
                                if ($model['vendor']['vendorType']) {
                                    return $model['vendor']['vendorType']['title'];
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'Primary Commodity',
                            'attribute' => 'primary_commodity',
                            'value' => function ($model) {
                                if ($model['registration']['primaryCommodity']) {
                                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                                        return $model['registration']['other_primary_commodity'];
                                    } else {
                                        return $model['registration']['primaryCommodity']['title'];
                                    }
                                }

                                return 'N/A';
                            }
                        ],
                        [
                            'label' => 'GSTN',
                            'attribute' => 'GSTN',
                            'value' => function ($model) {
                                $tax = 'N/A';
                                if ($model['tax']) {
                                    $tax = $model['tax']['gstin_number'];
                                }

                                return $tax;
                            }
                        ],
                        [
                            'label' => 'MSME',
                            'attribute' => 'MSME',
                            'value' => function ($model) {
                                $msme = 'N/A';
                                if ($model['vendor'] && !empty(VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']])) {
                                    $msme = VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']];
                                }

                                return $msme;
                            }
                        ],
                        [
                            'label' => 'IEC code',
                            'attribute' => 'IEC',
                            'value' => function ($model) {

                                if ($model['vendor'] && empty($model['vendor']['vendorType']['is_domestic'])) {
                                    if ($model['tax']) {
                                       $iec = $model['tax']['iec_code'];
                                    } else {
                                        $iec = 'N/A';
                                    }
                                } else {
                                    $iec = 'N/A';
                                }

                                return $iec;
                            }
                        ],
                        [
                            'label' => 'Turnover',
                            'attribute' => 'turnover',
                            'value' => function ($model) {
                                $turnover = 'N/A';

                                if ($model['registration']['primaryCommodity']) {

                                    if (!empty($model['registration']['annual_turnover'])) {
                                        if ($model['registration']['annual_turnover'] == 1) {
                                            $turnover = $model['registration']['primaryCommodity']['upper_turnover'];
                                        } else {
                                            $turnover = $model['registration']['primaryCommodity']['lower_turnover'];
                                        }
                                    } else {
                                        $turnover = 'N/A';
                                    }
                                }

                                return $turnover;
                            }
                        ],
                        [
                            'label' => 'Secondary Commodity',
                            'attribute' => 'secondary_commodity',
                            'value' => function ($model) use ($commodities) {
                                $html = '';
                                if ($model['registration'] && $model['registration']['fk_secondary_commodity_type']) {
                                    $selected = json_decode($model['registration']['fk_secondary_commodity_type']);
                                    if (is_array($selected)) {
                                        foreach ($selected as $single) {
                                            if (!empty($commodities[$single]) && $commodities[$single]['display_other_field']) {
                                                $html .= $model['registration']['other_secondary_commodity'] . ',';
                                            } else if (!empty($commodities[$single])) {
                                                $html .= $commodities[$single]['title'] . ',';
                                            }
                                        }
                                    }
                                } else {
                                    $html = 'N/A';
                                }

                                return $html;
                            }
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>
</div>
