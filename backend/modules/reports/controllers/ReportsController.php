<?php

namespace backend\modules\reports\controllers;

use Yii;
use common\models\reports\UserRequestSearch;
use common\models\reports\ServiceWiseSearch;
use common\models\reports\CommodityWiseSearch;
use common\models\reports\ContactReportSearch;
use common\models\Commodities;
use common\models\Services;
use common\models\reports\FinanceSearch;
use common\models\reports\SapSearch;
use common\models\reports\ShareholderSearch;
use common\models\User;
use common\forms\UpdateSap;

/**
 * ReportsController implements the CRUD actions for UserRequest model.
 */
class ReportsController extends DefaultController
{

    /**
     * Lists all UserRequest models.
     * @return mixed
     */
    public function actionRegister()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegisterExport()
    {
        $searchModel = new UserRequestSearch();
        return $searchModel->export(Yii::$app->request->queryParams, false);
    }

    public function actionRegistert2()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('registert2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegistert2Export()
    {
        $searchModel = new UserRequestSearch();
        return $searchModel->export(Yii::$app->request->queryParams, true);
    }

    public function actionSpam()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->searchspam(Yii::$app->request->queryParams);

        return $this->render('spam', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSpamExport()
    {
        $searchModel = new UserRequestSearch();
        return $searchModel->exportspam(Yii::$app->request->queryParams);
    }


    public function actionCommodity()
    {
        $searchModel = new CommodityWiseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $commodities = Commodities::getAllInternal();

        return $this->render('commodity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'commodities' => $commodities
        ]);
    }

    public function actionCommodityExport()
    {
        $searchModel = new CommodityWiseSearch();
        $commodities = Commodities::getAllInternal();
        return $searchModel->export(Yii::$app->request->queryParams, $commodities);
    }

    public function actionService()
    {
        $searchModel = new ServiceWiseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $services = Services::getAllInternal();

        return $this->render('service', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services' => $services
        ]);
    }

    public function actionServiceExport()
    {
        $searchModel = new ServiceWiseSearch();
        $services = Services::getAllInternal();

        return $searchModel->export(Yii::$app->request->queryParams, $services);
    }

    public function actionContact()
    {
        $searchModel = new ContactReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('contact', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionContactExport()
    {
        $searchModel = new ContactReportSearch();

        return $searchModel->export(Yii::$app->request->queryParams);
    }

    public function actionFinance()
    {
        $searchModel = new FinanceSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('finance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFinanceExport()
    {
        $searchModel = new FinanceSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;

        return $searchModel->export(Yii::$app->request->queryParams);
    }

    public function actionShareholder()
    {
        $searchModel = new ShareholderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('shareholder', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionShareholderExport($id_vendor)
    {
        $searchModel = new ShareholderSearch();
        $searchModel->id = $id_vendor;

        return $searchModel->export(Yii::$app->request->queryParams);
    }

    public function actionSap()
    {
        $updateModel = new UpdateSap();
        $searchModel = new SapSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isAjax && $updateModel->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($updateModel);
        }

        if ($updateModel->load(Yii::$app->request->post()) && $updateModel->saveCode()) {
            Yii::$app->session->setFlash('success', 'Vendor code updated successfully');
            return $this->refresh();
        }

        return $this->render('sap', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'updateModel' => $updateModel
        ]);
    }

    public function actionSapExport()
    {
        $searchModel = new SapSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;

        return $searchModel->export(Yii::$app->request->queryParams);
    }

    public function actionSappending()
    {
        $searchModel = new SapSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;
        $dataProvider = $searchModel->searchpending(Yii::$app->request->queryParams);

        return $this->render('sappending', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSappendingExport()
    {
        $searchModel = new SapSearch();
        $searchModel->application_status = User::APP_STATUS_APPROVE;

        return $searchModel->pendingexport(Yii::$app->request->queryParams);
    }
}
