<?php

namespace backend\modules\reports\controllers;

use backend\controllers\MainController;

/**
 * Default controller for the `reports` module
 */
class DefaultController extends MainController
{
    public function actionDashboard()
    {
        return $this->render('index');
    }
}
