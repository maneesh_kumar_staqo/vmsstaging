<?php

namespace backend\modules\userManagement\controllers;

use Yii;
use backend\modules\userManagement\models\User;
use backend\modules\userManagement\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new User();
        $leftListBox = $model->getAllPermission();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            $model->auth_key = Yii::$app->security->generateRandomString();
            if($model->validate()){
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
                $model->password_repeat = $model->password_hash;
                $model->save();                
                $postValues = Yii::$app->request->post();   


                if (!empty($postValues['ListBox'])) {
                    $postValues['ListBox'] = array_values(array_unique($postValues['ListBox']));
                    $model->assignedList($model->id, $postValues['ListBox']);
                }
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'leftListBox' => $leftListBox
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $leftListBox = $model->getAllPermission();
        $rightListBox = $model->getAssignedList($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if(empty($model->password_hash)){
                $model->password_hash = $model->oldAttributes['password_hash'];
                $model->password_repeat = $model->oldAttributes['password_hash'];
            }else{
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
                $model->password_repeat = $model->password_hash;
            }
            $model->save();
            $postValues = Yii::$app->request->post();
            $model->deleteAssignedList($id);

            //updating roles 
            if (!empty($postValues['ListBox'])) {
                $postValues['ListBox'] = array_values(array_unique($postValues['ListBox']));
                $model->assignedList($id, $postValues['ListBox']);
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'leftListBox' => $leftListBox,
            'rightListBox' => $rightListBox
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
