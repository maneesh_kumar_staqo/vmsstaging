<?php
namespace backend\modules\userManagement\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class UserAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/userManagement/web';
    public $css = [
        'css/duallistbox.css',
    ];
    public $js = [
        'js/duallistbox.js',
        'js/main.js', 
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
