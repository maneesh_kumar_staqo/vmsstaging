<?php

namespace backend\modules\userManagement\models;

use Yii;
use backend\modules\userManagement\models\AuthItem;
use backend\modules\userManagement\models\AuthAssignment;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $password_repeat;
    // public $original_password;
    public static function tableName()
    {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['password_hash'], 'required', 'on' => 'create'],
            [['password_repeat'], 'required', 'when' => function ($model) { return $model->password_hash != null;},  'enableClientValidation' => false],
            [['status'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            // [['original_password'], 'safe'],
            [['username'], 'unique'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password_hash', 'message'=>"Passwords don't match" ],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    public function create($runValidation = true, $attributeNames = null)
    {
        $this->scenario = 'create';
        return parent::update($runValidation, $attributeNames);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            // 'original_password' => 'Original Password',
            // 'password_repeat' => 'Repeat Password'
        ];
    }

    ///////////////////////////////////////
    //RBAC QUERIES                Starts//
    /////////////////////////////////////
        public function getAllPermission(){
            $model = new AuthItem();
            return $model->find()
                        ->select(['name as id','name as name'])
                        ->where(['type' => 1])
                        ->orderBy(['updated_at' => SORT_DESC])
                        ->asArray()
                        ->all();
        }
        public function getAssignedList($userId){
            $model = new AuthAssignment();
            return $model->find()
                        ->select(['item_name as id', 'item_name as name'])
                        ->where(['user_id' => $userId])
                        ->asArray()
                        ->all();
        }
        public function deleteAssignedList($userId){
            $model = new AuthAssignment();
            return $model->deleteAll(['user_id' => $userId]);
        }

        // public function insertAssignedList($userId,$permission){
        //     $db = Yii::$app->db;
        //     $sql = " insert into auth_assignment (user_id,item_name) values(:userId,:permission)";
        //     $command = $db->createCommand($sql);
        //     $command->bindValue(":userId",$userId);
        //     $command->bindValue(":permission",$permission);
        //     $result = $command->execute();
        //     return $result;
        // }

        public function assignedList($userId,$permission){
            $db = Yii::$app->db;
            $table = 'auth_assignment';
            $fields = ['user_id','item_name'];
            $rows = [];
            foreach ($permission as $key => $value) {
                $rows[] = [
                    $userId,
                    $value
                ];
            }
            $sql = $db->queryBuilder->batchInsert($table, $fields, $rows);
            return $db->createCommand($sql)->execute();
        }
    ///////////////////////////////////////
    //RBAC QUERIES                Ends  //
    /////////////////////////////////////
}
