<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\userManagement\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
?>
    <div class="col-lg-3">
        <?= $this->render('../widgets/role-side-menu',[ 'selected' => 'user-list']) ?>
    </div>
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div id="sticky" class="panel-heading text-right">
                <button type="button" style="cursor:pointer;" class="btn btn-primary btn-sm" onclick="location.href = '<?php echo Url::toRoute('create') ?>'">Create</button>
            </div>
            <div class="panel-body wrapper border-bottom  page-heading fix-page-height">              
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [

                        'username',
                        'email:email',
                        [
                            'label' => 'Status',
                            'attribute' => 'status',
                            'filter' => [10 => 'Active', 0 => 'Inactive'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                if($model->status == 10){
                                    return 'Active';
                                }else{
                                    return 'Inactive';
                                }
                            }
                        ],
                        // 'created_at',
                        // 'updated_at',

                        ['class' => 'yii\grid\ActionColumn',
                            'template' => '{update}'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
