<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\userManagement\models\User */

$this->title = 'Create User';
?>
<div class="user-create">
    <?= $this->render('_form', [
        'model' => $model,
        'leftListBox' => $leftListBox,
        'rightListBox' => ''
    ]) ?>

</div>
