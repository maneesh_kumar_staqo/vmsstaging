<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\modules\userManagement\assets\UserAsset;

UserAsset::register($this);
?>

<div class="col-lg-3">
        <?= $this->render('../widgets/role-side-menu',[ 'selected' => 'user-create']) ?>
    </div>
<div class="col-lg-9">
    <div class="panel panel-default">
        <div class="panel-heading">
            User 
        </div>   
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>            
            <?php
                $model->password_hash = '';
                $model->password_repeat = '';
            ?>
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>                    
                </div>
            </div>
             <div class="col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true])->label('Password') ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($model, 'password_repeat')->passwordInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <?= $form->field($model, 'status')->dropDownList(['10' => 'Active' , '0' => 'Inactive'], ['prompt' => 'Select Status']) ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-12 form-group">
                  <label class="control-label">Select Permissions For User</label>
                  <?= $this->render("../widgets/_list_box.php", ['leftListBox' => $leftListBox, 'rightListBox' => $rightListBox]) ?>
              </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>