<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\userManagement\models\User */

$this->title = 'Update User: ' . $model->id;
?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
        'leftListBox' => $leftListBox,
         'rightListBox' => $rightListBox
    ]) ?>

</div>
