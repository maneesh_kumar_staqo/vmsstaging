<?php
$this->title = "User Management";
use yii\helpers\Url;
?>

<div class="panel panel-danger">
    <div class="panel-heading">
        Dashboard
    </div>
    <div class="panel-body">

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="dash-lg glyphicon glyphicon-retweet"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Users</div>
                        </div>
                    </div>
                </div>
                <a href="<?= Url::toRoute('/user-management/user') ?>">
                    <div class="panel-footer">
                        <span class="pull-left">
                            View Details
                        </span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="dash-lg glyphicon glyphicon-retweet"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Roles</div>
                        </div>
                    </div>
                </div>
                <a href="<?= Url::toRoute('/user-management/role/updaterole') ?>">
                    <div class="panel-footer">
                        <span class="pull-left">
                            View Details
                        </span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

<!--        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="dash-lg glyphicon glyphicon-retweet"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div>Permissions</div>
                        </div>
                    </div>
                </div>
                <a href="<?php // Url::toRoute('/user-management/role/createpermission') ?>">
                    <div class="panel-footer">
                        <span class="pull-left">
                            View Details
                        </span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>-->
    </div>
</div>