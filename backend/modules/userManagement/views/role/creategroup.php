<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Utility;

$utility = new Utility();

/* @var $this yii\web\View */
/* @var $model common\models\Department */
/* @var $form yii\widgets\ActiveForm */
if (!empty($id)) {
    $this->title = 'Update - ' . $model->name;
    $pageType = 'Update';
} else {
    $this->title = 'Create - Group';
    $pageType = 'Create';
}
?>
    <div class="col-lg-3">
        <?= $this->render('../widgets/role-side-menu',[ 'selected' => 'create-group']) ?>
    </div>
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                Add New Group
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'name')->textInput(array('placeholder' => ''))->label(" Group Name"); ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($model, 'data')->textarea()->label("description") ?>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <?= Html::submitButton($pageType, ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                <a class="btn btn-warning" href="<?= Url::toRoute('role/index') ?>">Cancel</a>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
