<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\models\Utility;

$utility = new Utility();

/* @var $this yii\web\View */
/* @var $model common\models\Department */
/* @var $form yii\widgets\ActiveForm */
;
if (!$model->isNewRecord) {
    $this->title = 'Update - ' . $model->name ;
    $pageType = 'Update';
} else {
    $this->title = 'Create-Role';
    $pageType = 'Create';
}
?>
    <div class="col-lg-3">
        <?= $this->render('../widgets/role-side-menu',[ 'selected' => 'create-role']) ?>
    </div>
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= $this->title ?>
            </div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= $form->field($model, 'name')->textInput(array('placeholder' => 'example: staff')) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($model, 'description')->textarea() ?>
                        </div>
                    </div>
                    <hr>
                    
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            Permissions
                        </div>
                        <div class="panel-body">
                            <?= $this->render("../widgets/_list_permission.php", ['leftListBox' => $leftListBox, 'rightListBox' => $rightListBox]) ?>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <?= Html::submitButton($pageType, ['class' => $model->isNewRecord ? 'btn btn-primary btn-sm' : 'btn btn-primary btn-sm']) ?>
                                <a class="btn btn-primary btn-sm" href="<?= Url::toRoute('role/index') ?>">Cancel</a>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
