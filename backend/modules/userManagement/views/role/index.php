<?php

use yii\helpers\Url;
use common\models\Utility;

$utility = new Utility();

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Role List';
?>
<div class="col-lg-3">
    <?= $this->render('../widgets/role-side-menu',[ 'selected' => 'role-list']) ?>
</div>
    <div class="col-lg-9">
        
    <div class="panel panel-default">
         <div id="sticky" class="panel-heading text-right">
         <button type="button" style="cursor:pointer;" class="btn btn-primary btn-sm" onclick="location.href = '<?php echo Url::toRoute('role/updaterole') ?>'">Create</button>

        </div>
        <div class="panel-body wrapper border-bottom  page-heading fix-page-height">

            <div class="wrapper animated fadeInRight ecommerce">          
                    <div class="table-responsive">
                       

                        <table class="table table-striped table-bordered table-hover <?= empty($userList) ? '' : 'dataTables-example' ?>">
                            <thead>
                                <tr>
                                    <th>Role Name</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($userList)) {
                                    foreach ($userList as $user) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?= $user['name'] ?>
                                            </td>
                                            <td>
                                                <?= $user['description'] ?>
                                            </td>
                                            <td class="text-center">
                                                <div class="btn-group">
                                                    <a href="<?= Url::toRoute('role/updaterole?name=' . $user['name']) ?>" title="Update"><i class="glyphicon glyphicon-pencil"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    echo "<tr><td style='text-align:center;' colspan='7'> No records found.</td></tr>";
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>