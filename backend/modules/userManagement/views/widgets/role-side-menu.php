<?php

use yii\helpers\Url;
?>
<div class="panel panel-info">
    <div role="presentation" class="panel-heading text-center">
        <a href="<?= Url::toRoute(['/user-management'], true) ?>">
            <i class="glyphicon glyphicon-globe fa-1x"></i>
            User Management
        </a>
    </div>
    <ul class="nav nav-pills nav-stacked panel-body" style="">
        <li role="presentation" class=<?= ( $selected == 'user-list' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/user'], true) ?>">
                User List
            </a>
        </li>
        <li role="presentation" class=<?= ( $selected == 'user-create' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/user/create'], true) ?>">
                Add New User
            </a>
        </li>
        <li role="presentation" class=<?= ( $selected == 'role-list' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/role'], true) ?>">
                Role List
            </a>
        </li>
        <li role="presentation" class=<?= ( $selected == 'create-role' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/role/updaterole'], true) ?>">
                Add New Role
            </a>
        </li>
<!--        <li role="presentation" class=<?= ( $selected == 'create-group' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/role/creategroup'], true) ?>">
                Add New Group
            </a>
        </li>-->
<!--        <li role="presentation" class=<?= ( $selected == 'create-permission' ? "active" : "") ?> >
            <a href="<?= Url::toRoute(['/user-management/role/createpermission'], true) ?>">
                Add New Permission
            </a>
        </li>-->
    </ul>
</div>
