<?php

$attributeIdArray = [];
if (!empty($rightListBox)) {
	// echo "<pre>";
	// print_r($rightListBox);
	// die;
	foreach ($rightListBox as $values) {
		$attributeIdArray[] = $values['child'];
	}
}

?>
<!--<style type="text/css">
	.listPremis {
		/*padding: 20px;*/
		border: 1px solid #F1F1F1;
		background-color: #F1F1F1;
		box-shadow: 10px 10px 5px #888888;
		margin: 10px;
	}
</style>-->
<div class="wrapper  animated fadeInRight">
	<div class="row">
		<?php if (!empty($leftListBox)) { ?>
			<?php foreach ($leftListBox as $data) { ?>
				<div class="col-md-6 col-sm-6 listPremis">
					<div class="ibox">
						<div class="ibox-content product-box ">
							<div class="product-desc">

								<div class="panel panel-heading row text-center">
									<?= ucfirst($data['name']); ?>
								</div>
								<?php if (!empty($data['authItems'])) { ?>
									<?php foreach ($data['authItems'] as $permission) { ?>
										<?php if (!in_array($permission['name'], $attributeIdArray)) { ?>
											<div class="i-checks"><label> <input type="checkbox" name="ListBox[]" value="<?= $permission['name'] ?>"> <i></i> <?= ucwords(str_replace('-', ' ', $permission['name'])); ?></label></div>
										<?php } else {
											?>
											<div class="i-checks"><label> <input type="checkbox" checked  name="ListBox[]" value="<?= $permission['name'] ?>"> <i></i> <?= ucwords(str_replace('-', ' ', $permission['name'])); ?> </label></div>
												<?php } ?>
											<?php
											}
										}
										?>
							</div>

						</div>
					</div>
				</div>
			<?php } ?>
<?php } ?>
	</div>
</div>