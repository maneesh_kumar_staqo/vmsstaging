$(document).ready(function () {

    const OPTION_DIRECTOR = 'director';
    const OPTION_OWNER = 'owner';
    const OPTION_PARTNER = 'partner';
    const OPTION_PERSON = 'person';
    const OPTION_KARTA = 'karta';
    const OPTION_PROPRIETOR = 'proprietor';

    const OWNER_TYPE_SHAREHOLDERS = 1;
    const OWNER_TYPE_OWNERSHIPS = 2;

    const MAX_SHARE_HOLDER = 200;

    const OPTION_NO = 0;
    const OPTION_YES = 1;
    const LOAD_LIST_URL = BASE_URL + '/detail-completion/load-list';

    const VENDOR_SELECTORS = {
        MAIN_SELECTOR: '[data-action="organisation-dropdown"]',

        DIRECTOR_FIELDS: '[data-section="directors"]',
        OWNER_FIELDS: '[data-section="owners"]',
        PARTNER_FIELDS: '[data-section="partners"]',
        PERSON_FIELDS: '[data-section="persons"]',
        KARTA_FIELDS: '[data-section="kartas"]',
        PROPRIETOR_FIELDS: '[data-section="proprietors"]',

        SINGLE_DIRECTOR: '.single-director',
        SINGLE_OWNER: '.single-owner',

        SINGLE_PARTNER: '.single-partner',
        SINGLE_PERSON: '.single-person',
        SINGLE_KARTA: '.single-karta',
        SINGLE_PROPRIETOR: '.single-proprietor',

        ADD_SHARE_HOLDER: '[data-action="add-share-holders"]',
        HIDDEN_SHARE_HOLDER: '.hidden-shareholder',
        SHARE_HOLDER_APPEND: '[data-append="share-holders"]',
        SINGLE_SHARE_HOLDER: '.single-share-holder',

        MSME_ACTION: '[data-action="is_msme_act_2006"]',
        MSME_YES: '[data-field="msme-yes"]',

        LOAD_MORE_LIST: '[data-action="add-more-list"]',
        OWNERS_TYPE_TITLE: '.owners-type-title',
        OWNERS_TYPE_TEXT: '.owners-type-text',
        COMPANY_PAN: '.company-pan-info'
    };

    //share holder js STARTS
    $(VENDOR_SELECTORS.ADD_SHARE_HOLDER).click(function () {
        let html = $(VENDOR_SELECTORS.HIDDEN_SHARE_HOLDER).html();
        const fieldNumber = $(VENDOR_SELECTORS.SINGLE_SHARE_HOLDER).length;

        if (fieldNumber > MAX_SHARE_HOLDER) {
            alert('Max 200 share holder allowed');
        } else {
            html = html.replace(/{counter}/g, fieldNumber - 1);

            $(VENDOR_SELECTORS.SHARE_HOLDER_APPEND).append(html);
        }
    });
    //share holder js ENDS

    //MSME section js STARTS
    handleMSME = (elm) => {
        const selected = $(elm).find(':checked').val();

        $(VENDOR_SELECTORS.MSME_YES).hide();
        if (selected == OPTION_YES) {
            $(VENDOR_SELECTORS.MSME_YES).show();
        }
    };

    if ($(VENDOR_SELECTORS.MSME_ACTION).length) {
        handleMSME($(VENDOR_SELECTORS.MSME_ACTION));

        $(VENDOR_SELECTORS.MSME_ACTION).change(function () {
            handleMSME($(this));
        });
    }
    //MSME section js ENDS

    //manage director and owner forms STARTS
    handleVendorForm = (elm) => {
        const selectOption = elm.data('display-fields');
        const selectedOwner = elm.data('owners-type');
        let panField = 'Company PAN Number';
        let panDocument = 'Upload Company PAN';
        let title = 'Shareholder Details';
        $(VENDOR_SELECTORS.OWNERS_TYPE_TEXT).removeClass('d-none');

        if (selectedOwner == OWNER_TYPE_OWNERSHIPS) {
            panField = 'Partnership Firm PAN Number';
            panDocument = 'Upload Partnership Firm PAN';
            title = 'Ownership Details';
            $(VENDOR_SELECTORS.OWNERS_TYPE_TEXT).addClass('d-none');
        }
        $(VENDOR_SELECTORS.OWNERS_TYPE_TITLE).html(title);
        $(VENDOR_SELECTORS.COMPANY_PAN + ' .multioption label').html(panDocument);
        $(VENDOR_SELECTORS.COMPANY_PAN + ' .field-vendorinformation-company_pan_number label').html(panField);

        $(VENDOR_SELECTORS.OWNER_FIELDS).hide();
        $(VENDOR_SELECTORS.DIRECTOR_FIELDS).hide();
        $(VENDOR_SELECTORS.PARTNER_FIELDS).hide();
        $(VENDOR_SELECTORS.PERSON_FIELDS).hide();
        $(VENDOR_SELECTORS.KARTA_FIELDS).hide();
        $(VENDOR_SELECTORS.PROPRIETOR_FIELDS).hide();

        $('.removecin').addClass('hidecin');

        if (selectOption == OPTION_DIRECTOR) {
            $(VENDOR_SELECTORS.DIRECTOR_FIELDS).show();


        } else if (selectOption == OPTION_OWNER) {

            $(VENDOR_SELECTORS.OWNER_FIELDS).show();

        } else if (selectOption == OPTION_PARTNER) {

            $(VENDOR_SELECTORS.PARTNER_FIELDS).show();

        } else if (selectOption == OPTION_PERSON) {

            $(VENDOR_SELECTORS.PERSON_FIELDS).show();

        } else if (selectOption == OPTION_KARTA) {

            $(VENDOR_SELECTORS.KARTA_FIELDS).show();

        } else if (selectOption == OPTION_PROPRIETOR) {

            $(VENDOR_SELECTORS.PROPRIETOR_FIELDS).show();

        }
    };

    if ($(VENDOR_SELECTORS.MAIN_SELECTOR).length) {

        $(VENDOR_SELECTORS.OWNER_FIELDS).hide();
        $(VENDOR_SELECTORS.DIRECTOR_FIELDS).hide();
        $(VENDOR_SELECTORS.PARTNER_FIELDS).hide();
        $(VENDOR_SELECTORS.PERSON_FIELDS).hide();
        $(VENDOR_SELECTORS.KARTA_FIELDS).hide();
        $(VENDOR_SELECTORS.PROPRIETOR_FIELDS).hide();

        $(VENDOR_SELECTORS.MAIN_SELECTOR).each(function () {
            if ($(this).find('input').is(':checked')) {
                handleVendorForm($(this));
            }
        });
        
        $(VENDOR_SELECTORS.MAIN_SELECTOR).click(function () {

            handleVendorForm($(this));
        });
    }
    //manage director and owner forms ENDS

    //load more list STARTS
    if ($(VENDOR_SELECTORS.LOAD_MORE_LIST).length) {
//        $(VENDOR_SELECTORS.LOAD_MORE_LIST).click(function () {
//            loadList($(this));
//        });
    }
    loadList = (elm) => {
        const appendDiv = elm.data('fields');
        const singleClass = elm.data('single-field');
        const type = elm.data('type');
        const total = parseInt(elm.data('total'));
        const form = elm.data('form');

        $.ajax({
            url: LOAD_LIST_URL,
            type: 'POST',
            data: {type: type, total: total, form: form},
            success: function (res) {
                if (res) {
                    $(appendDiv).append(res);
                    $('html, body').animate({
                        scrollTop: $(singleClass + '-' + total).offset().top
                    }, 1000);
                    elm.data('total', total + 1);
                    initializeFileUploader();
                }
            }
        });
    }
    //load more list ENDS
});
// single option dropdown close after select 

$(".dropdown-radio").click(function(){
    console.log("sel");
    $(".dropdown").removeClass("show");
    $(".dropdown-menu-select").removeClass("show");
});