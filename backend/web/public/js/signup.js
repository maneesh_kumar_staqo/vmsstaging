
let dropdownHtml  = '<li><label class="dropdown-radio">';
    dropdownHtml += '<input type="checkbox" value="%value%" name="%field_name%" class="chb">';
    dropdownHtml += '<label for="%field_name%"></label>';
    dropdownHtml += '<span class="limited">%field_value%</span>';
    dropdownHtml +='</label></li>';

$(document).ready(function () {
    if ($('#sameAsCompanyName').length) {
        checkTradingName();
        $('#sameAsCompanyName').change(function () {
            checkTradingName();
        });
    }
});


checkTradingName = () => {
    const isSame = $('#sameAsCompanyName').is(':checked');

    if (isSame) {
        $('.trading_name').addClass('d-none');
    } else {
        $('.trading_name').removeClass('d-none');
    }
};

$(document).ready(function () {
    if ($('[data-action="domestic-field"]').length) {

        let isDomestic = 0;

        $('[data-action="domestic-field"]').each(function () {

            if ($(this).find('input').is(':checked')) {
                isDomestic = $(this).data('is-domestic');
            }
        });

        domesticField(isDomestic);

        $('[data-action="domestic-field"]').click(function () {
            let isDomestic = $(this).data('is-domestic');

            domesticField(isDomestic)
        });
    }

    if ($('[data-action="dependent-dropdown"]').length) {
        $('[data-action="dependent-dropdown"]').change(function () {
            dependentDropDowns(this);
        });
    }
    if ($('[data-action="dependent-multi-dropdown"]').length) {
        dependentMultiDropDowns($('[data-action="dependent-multi-dropdown"]'));
        $('[data-action="dependent-multi-dropdown"]').change(function () {
            dependentMultiDropDowns(this);
        });
    }
});

domesticField = (isDomestic) => {
    if (isDomestic) {
        $('.domestic-feilds').addClass('d-none');
    } else {
        $('.domestic-feilds').removeClass('d-none');
    }
};

dependentMultiDropDowns = (elm) => {
    const selectedOptions = $(elm).val();
    let display = false;
    let displayTrasco = false;
    let otherFieldName = $(elm).data('default-other-field');
    let trascoFieldName = $(elm).data('default-trasco-field');

    selectedOptions.forEach(function (val) {
        const selectedElm = $(elm).parents('.single-dropdown-multi-selection').find('.single_hidden_' + val);
        
        if (selectedElm.data('display-other-feild')) {
            display = true;
        }

        if (selectedElm.data('display-trasco-feild')) {
            displayTrasco = true;
        }

        otherFieldName = selectedElm.data('other-field');

    });

    if (display) {
        $(otherFieldName).removeClass('d-none');
    } else {
        $(otherFieldName).addClass('d-none');
    }

    if (displayTrasco) {
        $(trascoFieldName).removeClass('d-none');
    } else {
        $(trascoFieldName).addClass('d-none');
    }
};

dependentDropDowns = (elm) => {
    hideNShowOtherField(elm);
    changeTurnOverDropDown(elm);
};

changeTurnOverDropDown = (elm) => {
    const dropDownClass = $(elm).data('turn-over-dropdown');
    const upperOption = $(elm).data('upper-turnover');
    const lowerOption = $(elm).data('lower-turnover');
    const fieldName = $(elm).data('turn-over-field-name');

    if (dropDownClass) {
        let upperOpt = dropdownHtml;
        let lowerOpt = dropdownHtml;

        upperOpt = upperOpt.replace('%field_value%', upperOption);
        upperOpt = upperOpt.replace('%value%', 1);
        upperOpt = upperOpt.replace('%field_name%', fieldName);
        upperOpt = upperOpt.replace('%field_name%', fieldName);

        lowerOpt = lowerOpt.replace('%field_value%', lowerOption);
        lowerOpt = lowerOpt.replace('%value%', 2);
        lowerOpt = lowerOpt.replace('%field_name%', fieldName);
        lowerOpt = lowerOpt.replace('%field_name%', fieldName);

        $(dropDownClass).html(upperOpt + lowerOpt);
        initializerDropDown(dropDownClass);
    }
}

hideNShowOtherField = (elm) => {
    const otherField = $(elm).data('other-field');

    if ($(elm).data('display-other-feild')) {
        $(otherField).removeClass('d-none');
    } else {
        $(otherField).addClass('d-none');
    }
};