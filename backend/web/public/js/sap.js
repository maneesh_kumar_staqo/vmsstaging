/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//sap_vendor_code_model

$('[data-action="sap_vendor_code_btn"]').click(function () {
    sapVendorCodeModel($(this));
});


sapVendorCodeModel = (elm) => {
    const id = elm.data('id');
    const code = elm.data('sap_vendor_code');
    $('#sap_vendor_code').val(code);
    $('#id_user').val(id);

    $('#sap_vendor_code_model').modal('show');
}