/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    checkPasswordEdit();
})

$('.changespasswordbtn').click(function() {

    checkPasswordEdit(true);
});

checkPasswordEdit = (isClicked = false) => {
    const val = $('.changePassword').val();

    if (!isClicked) {
        if (val == 1) {
            $('.changespasswordbtn').text('Hide Password fields');
          $('.editprofilehidepasswordsec').show();  
        }
    } else if (val == 1) {
        $('.changePassword').val(0);
        $('.editprofilehidepasswordsec').hide();
        $('.changespasswordbtn').text('Change Password');
    } else {
        $('.changespasswordbtn').text('Hide Password fields');
        $('.changePassword').val(1);
        $('.editprofilehidepasswordsec').show();
    }
}