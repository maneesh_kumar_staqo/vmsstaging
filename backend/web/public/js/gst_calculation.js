/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


const GST_SELECTORS = {
    AMOUNT: '[data-field="amount"]',
    IGST: '[data-field="igst"]',
    SGST: '[data-field="sgst"]',
    CGST: '[data-field="cgst"]',
    TOTAL_AMOUNT: '[data-field="total_amount"]',
    RESET_BTN: "[data-field='reset']"
};

calculateGST = () => {
    $(GST_SELECTORS.IGST).attr('readonly', false);
    $(GST_SELECTORS.SGST).attr('readonly', false);
    $(GST_SELECTORS.CGST).attr('readonly', false);
    $(GST_SELECTORS.TOTAL_AMOUNT).attr('readonly', true);

    const igst = parseInt($(GST_SELECTORS.IGST).val()) || 0;
    const sgst = parseInt($(GST_SELECTORS.SGST).val()) || 0;
    const cgst = parseInt($(GST_SELECTORS.CGST).val()) || 0;
    const amount = parseInt($(GST_SELECTORS.AMOUNT).val()) || 0;

    if (igst && igst !== 0) {
        $(GST_SELECTORS.SGST).attr('readonly', true);
        $(GST_SELECTORS.CGST).attr('readonly', true);
    } else if ((sgst && sgst !== 0 )|| (cgst && cgst !== 0)) {
        $(GST_SELECTORS.IGST).attr('readonly', true);
    }

    const totalAmount = igst + sgst + cgst + amount;

    $(GST_SELECTORS.TOTAL_AMOUNT).val(totalAmount);
};

resetValues = () => {
    $(GST_SELECTORS.IGST).attr('readonly', false);
    $(GST_SELECTORS.SGST).attr('readonly', false);
    $(GST_SELECTORS.CGST).attr('readonly', false);

    $(GST_SELECTORS.IGST).val('');
    $(GST_SELECTORS.SGST).val('');
    $(GST_SELECTORS.CGST).val('');
    $(GST_SELECTORS.AMOUNT).val('');
    $(GST_SELECTORS.TOTAL_AMOUNT).val(0);
};

$(document).ready(function () {
    calculateGST();

    $(GST_SELECTORS.AMOUNT + ', ' + GST_SELECTORS.IGST + 
    ', ' + GST_SELECTORS.SGST + ', ' + GST_SELECTORS.CGST).on('keyup', function () {
        calculateGST();
    });

    $(GST_SELECTORS.RESET_BTN).click(function () {
        resetValues();
    });
});