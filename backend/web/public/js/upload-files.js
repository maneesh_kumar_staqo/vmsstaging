/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    const FILE_SELECTORS = {
        MAIN_DIV: '[data-action="file-upload"]',

        ADD_TEXT_DIV: '.addtext1',
        UPLOAD_TEXT_DIV: '.hidetext1',

        COMPLETE_ACTION_DIV: '.onefilecomplete1'
    };

    $(FILE_SELECTORS.MAIN_DIV + ' input[type="file"]').change(function () {
        const count = $(this).parents(FILE_SELECTORS.MAIN_DIV).find('input[type="file"]').get(0).files.length

        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).addClass('uplaodenable');

        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.COMPLETE_ACTION_DIV).show();
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).html('<span style="color:green;font-size:16px;line-height:40px;" >Complete!</span>');
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).addClass('addbgcolor');
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.COMPLETE_ACTION_DIV).html('<span>'+ count +' file uploaded</span><span style="padding-left:15px;color:red">X</span>');

        $(this).prop("readonly", true);
    });

    $(FILE_SELECTORS.COMPLETE_ACTION_DIV).on('click', function () {
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).removeClass('addbgcolor');
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).removeClass('uplaodenable');

        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.UPLOAD_TEXT_DIV).show();

        $(this).parents(FILE_SELECTORS.MAIN_DIV).find(FILE_SELECTORS.ADD_TEXT_DIV).html('<span style="color:#fff;font-size:16px;line-height:40px;" >Upload</span>');

        $(this).hide();

        $(this).parents(FILE_SELECTORS.MAIN_DIV).find('input[type="file"]').val('');
        $(this).parents(FILE_SELECTORS.MAIN_DIV).find('input[type="file"]').prop("readonly", false);
    });
});