/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    $('[data-action="display-next-offline"]').click(function () {
//        const file = $('.offline_document input').val();
        const reason = $('.offline_reason').val().trim();
        if (reason) {
            $('.offline-form-one').slideUp();
            $('.offline-form-two').slideDown();
        } else {
            alert('Please add your reason to choose offline registration');
        }
    });
});