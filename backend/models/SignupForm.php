<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            [['name'], 'safe'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup($model)
    {
        $this->email = $model->contact_person_email;
        $this->username = $model->contact_person_email;
        $this->name = $model->contact_person_name;
        $this->password = $model->password;

        if (!$this->validate()) {
            if ($this->getFirstError('username')) {
                $model->addError('contact_person_email', $this->getFirstError('username'));
            }
            if ($this->getFirstError('email')) {
                $model->addError('contact_person_email', $this->getFirstError('email'));
            }
            return false;
        }
        
        $user = new User();
        $user->username = $this->username;
        $user->name = $this->name;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->status = User::STATUS_ACTIVE;

        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        if ($user->save()) {
            return $user->id;
        }

        if ($user->errors) {
            $model->addError('contact_person_email', 'Email already has been taken');
        }
    }
}
