<?php

namespace backend\controllers;

use Yii;
use common\models\UserRequest;
use common\models\UserRequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ContactInformation;
use common\models\AddressInformation;
use common\models\BankInformation;
use common\models\TaxInformation;
use common\models\VendorInformation;
use common\models\VendorTypes;
use common\models\OrganisationTypes;
use common\models\CompanyTypes;
use common\models\AccreditationInformation;
use common\models\DeclarationInformation;
use common\models\Country;
use common\models\ShareHolders;
use common\forms\DirectorForm;
use common\forms\OwnerForm;
use common\forms\PartnerForm;
use common\forms\KartaForm;
use common\forms\ProprietorForm;
use common\forms\PersonForm;
use common\models\RegistrationVendorInfo;
use backend\models\SignupForm;
use common\models\Commodities;
use common\models\Services;
use common\forms\UpdateStatus;
use common\models\OrgCodeSap;
use common\models\PaymentTermsSap;
use common\models\AccountGroupSap;
use common\models\CompanyCodeSap;
use common\forms\ReasonForm;
use common\components\UploadComponent;

/**
 * UserRequestController implements the CRUD actions for UserRequest model.
 */
class UserRequestController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all UserRequest models.
     * @return mixed
     */
    public function actionSpam()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->searchspam(Yii::$app->request->queryParams);

        return $this->render('spam', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTier2()
    {
        $searchModel = new UserRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('tier2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $vendorInfo = VendorInformation::findModel($id) ;
        $contactInfo = ContactInformation::findModel($id);
        $addressInfo = AddressInformation::findModel($id);
        $bankInfo = BankInformation::findModel($id);
        $taxInfo = TaxInformation::findModel($id);
        $accreditationInfo = AccreditationInformation::findModel($id);
        $declarationInfo = DeclarationInformation::findModel($id);
        $registrationInfo = RegistrationVendorInfo::findModel($id);
        $model = UpdateStatus::findModel($id);

        $orgCode = OrgCodeSap::getAll();
        $paymentCode = PaymentTermsSap::getAll();
        $accountCode = AccountGroupSap::getAll();
        $companyCode = CompanyCodeSap::getAll();

        if ($model->load(Yii::$app->request->post()) && $model->saveStatusModel()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('view', [
            'model'             => $this->findModel($id),
            'registrationInfo'  => $registrationInfo,
            'vendorInfo'        => $vendorInfo,
            'contactInfo'       => $contactInfo,
            'addressInfo'       => $addressInfo,
            'bankInfo'          => $bankInfo,
            'taxInfo'           => $taxInfo,
            'accreditationInfo' => $accreditationInfo,
            'declarationInfo'   => $declarationInfo,
            'model'             => $model,
            'orgCode'           => $orgCode,
            'paymentCode'       => $paymentCode,
            'accountCode'       => $accountCode,
            'companyCode'       => $companyCode
        ]);
    }

    public function actionRegistrationInformation($id)
    {
        $model = RegistrationVendorInfo::findModel($id);

        $commodities = Commodities::getAll();
        $services = Services::getAll();

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('registration-information', [
            'id'            => $id,
            'model'         => $model,
            'commodities'   => $commodities,
            'services'      => $services
        ]);
    }

    public function actionVendorInformation($id)
    {
        $model = VendorInformation::findModel($id);
        $vendors = VendorTypes::getAll();
        $organisations = OrganisationTypes::getAll();
        $business = CompanyTypes::getAll();
        $countries = Country::getAll();
        $shareHolders = ShareHolders::getAll($model->id);
        $disableError = false;

        $directorForm = DirectorForm::findModel($model->id, DirectorForm::TYPE_DIRECTOR, count(Yii::$app->request->post('DirectorForm', [])));
        $ownerForm = OwnerForm::findModel($model->id, OwnerForm::TYPE_OWNER, count(Yii::$app->request->post('OwnerForm', [])));

        $partnerForm = PartnerForm::findModel($model->id, PartnerForm::TYPE_PARTNER, count(Yii::$app->request->post('PartnerForm', [])));
        $kartaForm = KartaForm::findModel($model->id, KartaForm::TYPE_KARTA, count(Yii::$app->request->post('KartaForm', [])));
        $proprietorForm = ProprietorForm::findModel($model->id, ProprietorForm::TYPE_PROPRIETOR, count(Yii::$app->request->post('ProprietorForm', [])));
        $personForm = PersonForm::findModel($model->id, PersonForm::TYPE_PERSON, count(Yii::$app->request->post('PersonForm', [])));

        DirectorForm::loadMultiple($directorForm, Yii::$app->request->post());
        OwnerForm::loadMultiple($ownerForm, Yii::$app->request->post());
        PartnerForm::loadMultiple($partnerForm, Yii::$app->request->post());
        KartaForm::loadMultiple($kartaForm, Yii::$app->request->post());
        ProprietorForm::loadMultiple($proprietorForm, Yii::$app->request->post());
        PersonForm::loadMultiple($personForm, Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        if ($model->errors || OwnerForm::modelHasErr($ownerForm) || DirectorForm::modelHasErr($directorForm)
            || PartnerForm::modelHasErr($partnerForm) || KartaForm::modelHasErr($kartaForm) || ProprietorForm::modelHasErr($proprietorForm)
            || PersonForm::modelHasErr($personForm)) {
            $disableError = true;
        }

        if ($model->business_type && !is_array($model->business_type)) {
            $model->business_type = json_decode($model->business_type);
        }

        return $this->render('vendor-information', [
            'id' => $id,
            'model' => $model,
            'vendors' => $vendors,
            'organisations' => $organisations,
            'business' => $business,
            'directorForm' => $directorForm,
            'ownerForm' => $ownerForm,
            'countries' => $countries,
            'shareHolders' => $shareHolders,
            'disableError' => $disableError,
            'partnerForm' => $partnerForm,
            'kartaForm' => $kartaForm,
            'proprietorForm' => $proprietorForm,
            'personForm' => $personForm
        ]);
    }

    public function actionContactInformation($id)
    {
        $model = ContactInformation::findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('contact-information', [
            'id' => $id,
            'model' => $model,
        ]);
    }

    public function actionAddressInformation($id)
    {
        $model = AddressInformation::findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('address-information', [
            'id' => $id,
            'model' => $model,
        ]);
    }

    public function actionBankInformation($id)
    {
        $model = BankInformation::findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('bank-information', [
            'id' => $id,
            'model' => $model,
        ]);
    }

    public function actionTaxInformation($id)
    {
        $model = TaxInformation::findModel($id);
        $vendorModel = VendorInformation::findModel($id);

        $isGovernment = CompanyTypes::isGovernment($vendorModel);
        $isLandOwner = CompanyTypes::isLandowner($vendorModel);
        $isDomestic = OrganisationTypes::isDomestic($vendorModel);
        $isOverseas = OrganisationTypes::isOverseas($vendorModel);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('tax-information', [
            'id' => $id,
            'model' => $model,
            'isDomestic' => $isDomestic,
            'isOverseas' => $isOverseas,
            'isGovernment' => $isGovernment,
            'isLandOwner' => $isLandOwner,
        ]);
    }

    public function actionAccreditationInformation($id)
    {
        $model = AccreditationInformation::findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('accreditation-information', [
            'id' => $id,
            'model' => $model,
        ]);
    }

    public function actionDeclarationInformation($id)
    {

        $model = DeclarationInformation::findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveStatus()) {
            Yii::$app->session->setFlash('success', 'Status updated successfully');
            return $this->refresh();
        }

        return $this->render('declaration-information', [
            'id' => $id,
            'model' => $model,
        ]);
    }

    public function actionReasion()
    {
        $model = new ReasonForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->set('tempReason', $model->reason);
            return $this->redirect(['create']);
        }

        return $this->render('reasion', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new UserRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegistrationVendorInfo();
        $signupForm = new SignupForm();
        $commodities = Commodities::getAll();
        $services = Services::getAll();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            UploadComponent::uploadModelFile($model, UploadComponent::$fileLocations['REGISTRATION'], 'offline_document');

            $transaction = Yii::$app->db->beginTransaction();

            $idUser = $signupForm->signup($model);
            $model->fk_user = $idUser;
            

            if ($idUser && $model->saveInfo($commodities, $services)) {

                $transaction->commit();

                return $this->redirect(['index']);
            } else {
                $transaction->rollBack();
            }
        }

        return $this->render('create', [
            'model'         => $model,
            'commodities'   => $commodities,
            'services'      => $services
        ]);
    }

    /**
     * Finds the UserRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
