<?php

namespace backend\controllers;

use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class MainController extends Controller
{
    public $enableCsrfValidation = false;

    public static $allowedActions = [
        'login', 'logout', 'signup', 'error', 'forgot-password', 'reset-password'
    ];

    /**
    * Task related to user authentication
    *
    * @author Aabir Hussain <aabir.hussain1@gmail.com>
    */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => self::$allowedActions,
                        'allow' => true,
                        'roles' => ['?']
                    ],
                    [
                       'allow' => '*',
                       'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }
}