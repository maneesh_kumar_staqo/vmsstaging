<?php

namespace backend\controllers;

use Yii;
use common\models\PoTracking;
use common\models\PoTrackingSearch;
use yii\web\NotFoundHttpException;
use common\models\PoInvoice;
use common\models\PoInvoiceSearch;

/**
 * PoTrackingController implements the CRUD actions for PoTracking model.
 */
class PoTrackingController extends MainController
{

    /**
     * Lists all PoTracking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PoTrackingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PoTracking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new PoInvoiceSearch();
        $searchModel->fk_po_tracking = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    
    /**
     * Creates a new PoInvoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionAddInvoice($id, $idInvoice = false)
    {
        $model = $this->findInvoiceModel($idInvoice);

        $model->fk_po_tracking = $id;
        $model->created_by = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->saveImages() && $model->save()) {
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('add-invoice', [
            'model' => $model,
            'id'    => $id
        ]);
    }

    /**
     * Creates a new PoTracking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PoTracking();
        $model->created_by = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->saveImages() && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PoTracking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->created_by = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->saveImages() && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the PoTracking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PoTracking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PoTracking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findInvoiceModel($id)
    {
        if (($model = PoInvoice::findOne($id)) !== null) {
            return $model;
        }

        return new PoInvoice();
    }
}
