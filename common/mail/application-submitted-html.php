<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>
<div class="password-reset">
    <p>Dear <?= Html::encode($user->username) ?>,</p>

    <p>Your personal information has been verified by HFE ! Your data will be maintained by HFE only for the purpose of conducting business. For any further queries drop a line to Chandrani.das@herofutureenergies.com</p>
<p><i>Thanks & Regards,<br><?= Yii::$app->name ?></i></p>
</div>
