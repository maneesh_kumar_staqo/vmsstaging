<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
Hello <?= $user->username ?>,

<p>Congratulations! You have been registered as a vendor successfully with HFE. For any further queries drop a line to Chandrani.das@herofutureenergies.com</p>
<p>Thanks & Regards,</p>
<p>HFE Team</p>
