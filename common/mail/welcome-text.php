<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
Dear <?= $user->username ?>,

<p>Congratulations! You have been successfully registered as a vendor with Hero Future Energies. For any further queries, please email us at chandrani.das@herofutureenergies.com</p>
<p><i>Thanks & Regards,<br><?= Yii::$app->name ?></i></p>
