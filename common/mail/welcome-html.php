<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>
<div class="password-reset">
    <p>Dear <?= Html::encode($user->username) ?>,</p>

    <p>Congratulations! You have been successfully registered as a vendor with Hero Future Energies. For any further queries, please email us at chandrani.das@herofutureenergies.com</p>
<p><i>Thanks & Regards,<br><?= Yii::$app->name ?></i></p>
</div>


