<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
<div class="verify-email">
    <p>Dear <?= Html::encode($user->username) ?>,</p>
    <p>Click the link below to verify your registered email address. Once the email address is verified, you may continue to fill the form.</p>
    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
<p>For any further queries, please email us at chandrani.das@herofutureenergies.com</p><br/>
<p>Thanks & Regards<br/>
HFE Team</p>
</div>