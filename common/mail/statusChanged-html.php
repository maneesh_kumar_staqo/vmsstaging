Hello <?= $username ?>,

<br>
<br>
Your application status has been changed to: <b><?= $status ?></b>
<br>

Remark: <?= $message ?>

<br>
<br>
For any further queries, please email us at  chandrani.das@herofutureenergies.com

<br>
Thanks & Regards
<br>
HFE Team