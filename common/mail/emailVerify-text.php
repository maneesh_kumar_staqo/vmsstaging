<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?= $user->username ?>,

Click the link below to verify your registered email address. Once the email address is verified, you may continue to fill the form.

<?= $verifyLink ?>

For any further queries, please email us at  chandrani.das@herofutureenergies.com

Thanks & Regards
HFE Team