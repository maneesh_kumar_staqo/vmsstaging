<?php

namespace common\components;

use Yii;

class EmailComponent
{

    const TYPE_WELCOME = 1;
    const TYPE_APPLICATION_SUBMITTED = 2;

    public static $emailType = [
        1 => [
            'subject' => 'Welcome',
            'html' => 'welcome-html',
            'text' => 'welcome-text'
        ],
        2 => [
            'subject' => 'Application Submitted',
            'html' => 'application-submitted-html',
            'text' => 'application-submitted-text'
        ]
    ];

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail($type, $email, $params)
    {

        $emailer = self::$emailType[$type];

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => $emailer['html'], 'text' => $emailer['text']],
                ['user' => $params]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($email)
            ->setBcc('sarita@uncindia.in')
            ->setSubject($emailer['subject'])
            ->send();
    }

    public function sendEmailtohfe($type, $email, $params)
    {

        $emailer = self::$emailType[$type];

        return Yii::$app
            ->mailer
            ->compose()
            ->setTextBody('Dear Admin, A new vendor registration has been received from '. $email.'. Please login into admin panel and review the same')
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo('vms_cpc@herofutureenergies.com')
            ->setBcc('aman@uncindia.in')
            ->setSubject('New Vendor Request')
            ->send();
    }
}