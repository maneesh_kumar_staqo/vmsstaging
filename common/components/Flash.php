<?php

namespace common\components;

use Yii;

/**
 * Content Thanks Custom Widgets, First include font-awesome
 * bootstrap then use this widgets.
 *
 * @uses @ToSetErrors : Yii::$app->Flash->setType(self::SUCCESS)
 *       ->setHead($header)->setMessage($message)->showModal();
 *      @ToGetErrors : include _flash_message in layouts/main.php
 *
 *      @Yii::$app->Flash->success(Optional $message);
 *      @Yii::$app->Flash->error(Optional $message);
 *
 * @configuration in config/main.php file add => 'Flash' =>
 *              [ 'class' => 'common\components\Flash',],
 *                  in components array.
 *
 * @copyright (c) 2018, Notesgen
 *
 * @author Aabir Hussain <aabir.hussain1@gmail.com>
 */
class Flash
{

    /**
     * Defined the type of message is error or success
     * so that we can add image or any icon to the view
     *
     * @var string
     */
    public static $type;

    /**
     * Set the header of the success message like
     * Thanks, Congratulations or Updated etc.
     *
     * @var string
     */
    public static $head;

    /**
     * Set the main message which will be display to the end user.
     *
     * @var string
     */
    public static $p;

    public static $image;

    public static $actionButton;

    public static $btnText;

    const DEFAULT_FAILURE_MESSAGE = 'Something went wrong. please try again.';
    const DEFAULT_FAILURE_HEADER = 'Failure';

    const DEFAULT_SUCCESS_MESSAGE = 'Data has been updated!';
    const DEFAULT_SUCCESS_HEADER = 'Success';

    const TYPE_SUCCESS = 'success';
    const TYPE_FAILURE = 'failure';

    const IMAGE_FAILURE = ''; //fa-cross
    const IMAGE_SUCCESS = ''; //fa-check

    /**
     * Content: Setting Type Of Message
     *
     * @param string $message
     * @param string $options
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function error($message = '', $header = '', $options = [])
    {
        self::$type = self::TYPE_FAILURE;
        self::$head = ($header) ? $header : self::DEFAULT_FAILURE_HEADER;
        self::$p = ($message) ? $message : self::DEFAULT_FAILURE_MESSAGE;
        self::$image = self::IMAGE_FAILURE;

        $this->setOption($options);

        return $this->showModal();
    }

    /**
     * Content: Setting Type Of Message
     *
     * @param string $message
     * @param string $options
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function success($message = '', $header = '', $options = [])
    {
        self::$type = self::TYPE_SUCCESS;
        self::$image = self::IMAGE_SUCCESS;
        self::$head = ($header) ? $header : self::DEFAULT_SUCCESS_HEADER;
        self::$p = ($message) ? $message : self::DEFAULT_SUCCESS_MESSAGE;

        $this->setOption($options);

        return $this->showModal();
    }

    /**
     * Set message and display it.
     *
     * @param array $options
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function display($options)
    {
        if (!empty($options['header'])) {
            self::$head = $options['header'];
        }
        if (!empty($options['message'])) {
            self::$p = $options['message'];
        }
        if (!empty($options['image'])) {
            self::$image = $options['image'];
        }
        if (!empty($options['type'])) {
            self::$type = $options['type'];
        }
        if (!empty($options['actionButton'])) {
            self::$actionButton = $options['actionButton'];
        }
        if (!empty($options['btnText'])) {
            self::$btnText = $options['btnText'];
        }

        return $this->showModal();
    }

    /**
     * Content: Setting Type Of Message
     *
     * @param string $type : success, error
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function setType($type)
    {
        self::$type = $type;

        return $this;
    }

    /**
     * Show Images
     *
     * @param string $image : images from web/public/images folder.
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function setImage($image)
    {
        self::$image = $image;

        return $this;
    }

    /**
     * Content: Setting header like Thankyou, Welcome etc
     *
     * @param string $head
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function setHead($head)
    {
        self::$head = $head;

        return $this;
    }

    /**
     * Content: Setting Message
     *
     * @param string $p
     *
     * @return same object.
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public function setMessage($p)
    {
        self::$p = $p;

        return $this;
    }

    /**
     * Content: Setting Session Values for Message Modal.
     *
     * @return boolean $true.
     */
    public function showModal()
    {
        $session = Yii::$app->session;

        if (!$session->isActive) {
            $session->open();
        }

        $session['alert'] = [
            'type' => self::$type,
            'h1' => self::$head,
            'p' => self::$p,
            'image' => self::$image,
            'actionButton' => self::$actionButton,
            'btnText' => self::$btnText
        ];

        $session->close();

        return true;
    }

    /**
     * Set Options to the popup
     *
     * @param array $options
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    protected function setOption($options)
    {
        if (!empty($options['header'])) {
            self::$head = $options['header'];
        }

        if (!empty($options['message'])) {
            self::$p = $options['message'];
        }

        if (!empty($options['image'])) {
            self::$image = $options['image'];
        }

        if (!empty($options['actionButton'])) {
            self::$actionButton = $options['actionButton'];
        }

        if (!empty($options['btnText'])) {
            self::$btnText = $options['btnText'];
        }
    }
}