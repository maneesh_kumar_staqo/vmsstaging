<?php

namespace common\components;

use Yii;
use yii\web\UploadedFile;
use common\components\notifications\Notify;

/**
 * Upload Component is responsible to upload file to the given locations.
 *
 * @copyright (c) 2018, HFE
 *
 * @author Aabir Hussain <aabir.hussain1@gmail.com>
 */
class UploadComponent
{

    const DEFAULT_FILE_NAME_SIZE = 255;
    const UTF8_ENABLED = true;

    const VendorInformation = 'vendor/';
    const BankInformation = 'bank/';
    const TaxInformation = 'tax/';
    const AccreditationInformation = 'accreditations/';
    const PoTracking = 'po_tracking/';

    public static $filePublicLocations = [
        'VendorInformation' => 'vendor/',
        'BankInformation' => 'bank/',
        'TaxInformation' => 'tax/',
        'AccreditationInformation' => 'accreditations/',
        'PoTracking' => 'po_tracking/',
        'RegistrationVendorInfo' => 'registration/',
        'DirectorForm' => 'vendor/',
        'KartaForm' => 'vendor/',
        'OwnerForm' => 'vendor/',
        'PartnerForm' => 'vendor/',
        'PersonForm' => 'vendor/',
        'ProprietorForm' => 'vendor/',
        'ProprietorForm' => 'vendor/',
        'ShareHolderForm' => 'vendor/',
    ];

    /**
     * All possible files folders
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static $fileLocations = [
        'VENDOR_FILES' => 'vendor/',
        'BANK_FILES' => 'bank/',
        'TAX_FILES' => 'tax/',
        'ACCREDITATION_FILES' => 'accreditations/',
        'PO_TRACKING' => 'po_tracking/',
        'REGISTRATION' => 'registration/'
    ];

    /**
     * Upload File with the help of name
     *
     * @param string $fieldName
     * @param string $location
     *
     * @return string $fileName
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function uploadByNameFile($fieldName = 'file', $location)
    {

        $fileName = '';

        try {

            self::verifyLocation(Yii::$app->params['imgUploadPath'] . $location);

            $file = UploadedFile::getInstancesByName($fieldName);

            if ($file) {

                $file = $file[0];
                $fileName = self::generateFileName($file->baseName) . '.' .$file->extension;

                $completePath = Yii::$app->params['imgUploadPath'] . $location . $fileName;

                $file->saveAs($completePath);
            }

        } catch (\Exception $ex) {

            echo '<pre>';
            print_r($ex->getMessage());
            die;
        }

        return $fileName;
    }

    /**
     * Only to Upload a file from modal to the given location.
     *
     * @param object $model database modal.
     * @param string $location
     * @param string $fieldName
     *
     * @return object $model
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function uploadModelFile(&$model, $location, $fieldName = 'image')
    {
        $result = false;
        try {

            self::verifyLocation(Yii::$app->params['imgUploadPath'] . $location);

            $file = UploadedFile::getInstance($model, $fieldName);
            $fileName = '';

            if ($file) {
                $fileName = self::generateFileName($file->baseName)
                    . '.' .$file->extension;

                $completePath = Yii::$app->params['imgUploadPath'] . $location . $fileName;

                $file->saveAs($completePath);

                $model->$fieldName = $fileName;
                $result = true;
            } else if (!empty($model->oldAttributes) && !empty ($model->oldAttributes[$fieldName])) {
                $model->$fieldName = $model->oldAttributes[$fieldName];
                $result = true;
            }

        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex->getMessage());
            print_r($ex->getLine());
            print_r($ex->getFile());
            print_r($ex->getTraceAsString());
            die;
        }

        return $result;
    }

    /**
     * Only to Upload a file from modal to the given location.
     *
     * @param object $model database modal.
     * @param string $location
     * @param string $fieldName
     *
     * @return object $model
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function uploadModelFileKeys(&$model, $location, $fieldName = 'image', $key = 0)
    {
        $result = false;
        try {

            self::verifyLocation(Yii::$app->params['imgUploadPath'] . $location);

            $file = UploadedFile::getInstance($model, '[' . $key . ']' . $fieldName);

            $fileName = '';

            if ($file) {
                $fileName = self::generateFileName($file->baseName)
                    . '.' .$file->extension;

                $completePath = Yii::$app->params['imgUploadPath'] . $location . $fileName;

                $file->saveAs($completePath);

                $model->$fieldName = $fileName;
                $result = true;
            } else if (!empty($model->oldAttributes) && !empty ($model->oldAttributes[$fieldName])) {
                $model->$fieldName = $model->oldAttributes[$fieldName];

                $result = true;
            }

        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex->getMessage());
            print_r($ex->getLine());
            print_r($ex->getFile());
            print_r($ex->getTraceAsString());
            die;
        }

        return $result;
    }

    /**
     * Only to Upload a file from modal to the given location.
     *
     * @param object $model database modal.
     * @param string $location
     * @param string $fieldName
     *
     * @return object $model
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function uploadMultiModelFile(&$model, $location, $fieldName = 'image')
    {
        $result = false;
        try {

            self::verifyLocation(Yii::$app->params['imgUploadPath'] . $location);

            $files = UploadedFile::getInstances($model, $fieldName);
            $fileNames = [];

            if ($files) {
                foreach ($files as $file) {
                    $fileName = self::generateFileName($file->baseName)
                    . '.' .$file->extension;

                    $completePath = Yii::$app->params['imgUploadPath'] . $location . $fileName;

                    $file->saveAs($completePath);

                    $fileNames[] = $fileName;
                    $result = true;
                }

                $model->$fieldName = json_encode($fileNames);

            } else if (!empty($model->oldAttributes) && !empty ($model->oldAttributes[$fieldName])) {
                $model->$fieldName = $model->oldAttributes[$fieldName];
                $result = true;
            }

        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex->getMessage());
            print_r($ex->getLine());
            print_r($ex->getFile());
            print_r($ex->getTraceAsString());
            die;
        }

        return $result;
    }

    /**
     * Upload File with the help of name
     *
     * @param string $fileName
     * @param string $location
     *
     * @return string|boolean $fileName|false
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function uploadWithRealName($fileName, $location)
    {
        $result = false;

        try {

            self::verifyLocation(Yii::$app->params['imgUploadPath'] . $location);

            $fileDetails = UploadedFile::getInstanceByName($fileName);

            if ($fileDetails) {
                $result = $fileDetails->saveAs(Yii::$app->params['imgUploadPath'] . $location . $fileDetails->name);
            }

            if ($result) {
                $result =  $fileDetails->name;
            }
        } catch (\Exception $ex) {
            $result = false;
            echo '<pre>';
            print_r($ex->getMessage());
            die;
        }

        return $result;
    }

    /**
     * Check location if not exist than create the located folder.
     *
     * @param string $path
     *
     * @return boolean $result
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    protected static function verifyLocation($path)
    {
        try {
            if (!is_dir($path)) {
                mkdir($path);
                chmod($path, 0777);
            }
        } catch (\Exception $ex) {
            Notify::exception($ex);
        }

        return true;
    }

        /**
     * Generate Seo Url
     *
     * @param string $text
     *
     * @return string $seoString
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function generateSeoURL($text)
    {
        $separator = '-';

        $quoteSeparator = preg_quote($separator, '#');

        $trans = [
            '&.+?;' => '', '[^\w\d _-]' => '', '\s+'   => $separator,
            '('.$quoteSeparator.')+'=> $separator
        ];

        $string = strip_tags($text);

        foreach ($trans as $key => $val) {
            $string = preg_replace('#'.$key.'#i'.(self::UTF8_ENABLED ? 'u' : ''), $val, $string);
        }

        $string = strtolower($string);

        return trim(trim($string, $separator));
    }

    /**
     * Generate a file name with random string
     *
     * @param string $fileName
     *
     * @return string  $string
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function generateFileName($fileName)
    {
        $string = self::generateSeoURL($fileName);

        return self::beautify($string . '_' . uniqid(), self::DEFAULT_FILE_NAME_SIZE);
    }

    /**
     * Truncate a string from start to a particular length
     *
     * @param string $string
     * @param int $length
     *
     * @return string  $string
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function beautify($string, $length = 24)
    {
        $result = $string;

        if (strlen($string) > $length) {
            $result = substr($string, 0, $length) . '..';
        }

        return $result;
    }
}