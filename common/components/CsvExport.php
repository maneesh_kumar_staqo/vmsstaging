<?php

namespace common\components;

use Yii;

class CsvExport
{
    public static $output;

    public static function exportToCSV($data, $fileName)
    {
        header("Content-Type: text/csv");
        header("Content-Disposition: attachment; filename=$fileName");
        header("Cache-Control: no-cache, no-store, must-revalidate");
        header("Pragma: no-cache");
        header("Expires: 0");
        $output = fopen("php://output", "w");
        foreach ($data as $row) {
            fputcsv($output, $row);
        }
        fclose($output);

        Yii::$app->response->isSent = true;
    }

    /**
     * In order to use this function properly, use params properly i.e
     * $query must be query object with asArray and select statement must contain
     * fields as passed in the $header with the same sequence.
     *
     * @param object $query
     * @param type $header
     * @param type $fileNamePrefix
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function exportByQuery($query, $header, $fileNamePrefix)
    {
        $limit = 10000;
        $offset = 0;
        $allCount = $query->count();
        $userDetails = $query->limit($limit)->all();
        $dataCount = count($userDetails);
        $loopCount = 0;
        if ($dataCount) { //when data is not found then show header only.
            $loopCount = $allCount / $dataCount;
        }
        $fileName = $fileNamePrefix . time() .'.csv';

        $data = array_merge([0 => $header], $userDetails);
        if ($allCount <= $dataCount) {
            self::exportToCSV($data, $fileName);
        } else {
            $finalData = false;
            for ($i = 1; $i < $loopCount + 1; $i++) {
                if ($i > $loopCount) {
                    $finalData = true;
                }
                if ($i == 1) {
                    self::exportCSVPartially($data, $fileName, $finalData);
                } else {
                    $offset = $limit + $offset;
                    $nData = $query->offset($offset)->limit($limit)->all();
                    self::exportCSVPartially($nData, $fileName, $finalData);
                    if ($finalData) {
                        break;
                    }
                }
            }
        }
    }

    public static function exportCSVPartially($data, $fileName, $finalData = false)
    {
        if (!self::$output) {
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=$fileName");
            header("Cache-Control: no-cache, no-store, must-revalidate");
            header("Pragma: no-cache");
            header("Expires: 0");
            self::$output = fopen("php://output", "w");
        }
        foreach ($data as $row) {
            fputcsv(self::$output, $row);
        }
        if ($finalData) {
            fclose(self::$output);
        }
    }

    public static function readCsvFile($filePath)
    {
        try {
            $result = [];
            $lines = file($filePath, FILE_SKIP_EMPTY_LINES | FILE_IGNORE_NEW_LINES);
            foreach ($lines as $line) {
                $result[] = str_getcsv($line);
            }
        } catch (\yii\base\Exception $e) {
            $result = false;
        }
        return $result;
    }

    /**
    * Parse Csv file by fgetcsv method
    *
    * @author Nonita <nonita@notesgen.com>
    */
    public static function parseCsvFile($file, $buffer = 1024, $delimiter = ',', $enclosure = '"')
    {
        try {
            $result = [];
            if (file_exists($file) && is_readable($file)) {
                ini_set("auto_detect_line_endings", true);
                if (($handle = fopen($file, 'r')) !== FALSE) {
                    while (($data = fgetcsv($handle, $buffer, $delimiter, $enclosure)) !== FALSE) {
                        $totalValues = count(array_filter($data));
                        if ($totalValues == 0) {
                            continue;
                        } else {
                            $result[] = $data;
                        }
                    }
                }
            }
        } catch (\yii\base\Exception $e) {
            $result = false;
        }
        return $result;
    }
}
