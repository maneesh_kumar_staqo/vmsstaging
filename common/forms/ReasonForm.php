<?php

namespace common\forms;

/**
 * ReasonForm
 */
class ReasonForm extends \yii\base\Model
{

    public $reason;

    public function rules()
    {
        return [
            [['reason'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'reason' => 'Reason',
        ];
    }
}