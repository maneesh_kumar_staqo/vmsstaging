<?php

namespace common\forms;

use Yii;
use common\models\VendorInformationPartner;
use common\components\UploadComponent;

class KartaForm extends VendorInformationPartner
{

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor_information' => 'Vendor Information ID',
            'name' => 'Name',
            'din_number' => 'DIN No',
            'din_document' => 'Upload DIN Document',

            'pan_number' => 'PAN Numbers',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'country' => 'Country',

            'director_id_proof' => 'Upload Karta ID Proof',
            'director_address_proof' => 'Upload Karta Residential Address',

            'is_qualified_under_164' => 'Whether Director is dis-qualified under Section 164 of the Companies Act, 2013?',

            'authorized_signatory' => 'Upload Board Resolution in favour of authorized signatory',
            'partnership_deed' => 'Upload HUF Deed',
            'pan_document' => 'Karta PAN',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            'upload_passport' => 'Upload Passport'
        ];
    }

    protected function saveImages($key = 0, $isDomestic)
    {
        $result = true;

        $addressProof = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'director_address_proof', $key);
        $deedDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'partnership_deed', $key);
        $passportDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'upload_passport', $key);
        
        if (!$deedDoc) {
            $result = false;
            $this->addError('partnership_deed', 'Document is required');
        }

        if (!$passportDoc && !$isDomestic) {
            $result = false;
            $this->addError('upload_passport', 'Passport is required');
        }

        return $result;
    }

    public function validateForm()
    {
        $result = false;

        if ($this->validate() && $this->saveImages()) {
            $result = true;
        }

        return $result;
    }

    public static function validateForms($forms, $isDomestic)
    {
        $result = true;

        foreach ($forms as $key => $form ) {
            
            if (!$form->saveImages($key, $isDomestic) || $form->errors || !$form->validate()) {
                $result = false;
            }

            if (!$isDomestic && !$form->passport_no) {
                $form->addError('passport_no', 'Passport Number is required');
                $result = false;
            }
        }

        return $result;
    }

    public function saveForm($id)
    {
        $this->fk_vendor_information = $id;
        $this->type = self::TYPE_KARTA;
        return $this->save();
    }
    
    public static function saveForms($id, $forms)
    {
        $result = true;
        if ($forms) {
            foreach ($forms as $form) {
                $form->fk_vendor_information = $id;
                $form->type = self::TYPE_KARTA;
                $result = $form->save();
            }
        }

        return $result;
    }
}