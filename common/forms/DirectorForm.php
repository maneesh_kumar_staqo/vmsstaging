<?php

namespace common\forms;

use Yii;
use common\models\VendorInformationPartner;
use common\components\UploadComponent;
use common\models\OrganisationTypes;

class DirectorForm extends VendorInformationPartner
{
    const FIELD_REQUIRED_INDEX = 0;

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor_information' => 'Vendor Information ID',
            'name' => 'Name',
            'din_number' => 'DIN No',
            'din_document' => 'Upload DIN Document',

            'pan_number' => 'PAN Number',
            'pan_document' => 'Upload PAN',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'country' => 'Country',

            'director_address_proof' => 'Upload Director Residential Address',

            'is_qualified_under_164' => 'Whether Director is dis-qualified under Section 164 of the Companies Act, 2013?',

            'authorized_signatory' => 'Upload Board Resolution in favour of authorized signatory',
            'partnership_deed' => 'Upload JV Deed',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            'upload_passport' => 'Upload Passport'
        ];
    }

    protected function saveImages($key, $isDomestic, $organizationType)
    {
        $result = true;

        $dinDocument = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'din_document', $key);
        $addressProof = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'director_address_proof', $key);
        $authoziredSig = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'authorized_signatory', $key);
        $passportDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'upload_passport', $key);
        $deedDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'partnership_deed', $key);
        $panDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'pan_document', $key);
        
        if ($organizationType == OrganisationTypes::ID_JV || $organizationType == OrganisationTypes::ID_OPC) {
            if ($isDomestic && !$dinDocument) {
                $result = false;
                $this->addError('din_document', 'Document is required');
            }
        }

        if (!$passportDoc && !$isDomestic) {
            $result = false;
            $this->addError('upload_passport', 'Passport is required');
        }

        if (!$authoziredSig) {
            $result = false;
            $this->addError('authorized_signatory', 'Document is required');
        }

        if ($organizationType == OrganisationTypes::ID_JV && $key == self::FIELD_REQUIRED_INDEX) {
            if (!$deedDoc && $isDomestic) {
                $result = false;
                $this->addError('partnership_deed', 'Document is required');
            }
        }

        return $result;
    }

    public function validateForm()
    {
        $result = false;

        if ($this->validate() && $this->saveImages()) {
            $result = true;
        }

        return $result;
    }

    public static function validateForms($directorForms, $isDomestic, $organizationType)
    {
        $result = true;

        foreach ($directorForms as $key => $directorForm ) {

            if (!$directorForm->saveImages($key, $isDomestic, $organizationType) || $directorForm->errors || !$directorForm->validate()) {
                $result = false;
            }

            if (!$isDomestic && !$directorForm->passport_no) {
                $directorForm->addError('passport_no', 'Passport Number is required');
                $result = false;
            }
            if ($isDomestic && !$directorForm->din_number) {
                $directorForm->addError('din_number', 'DIN Number is required');
                $result = false;
            }
        }

        return $result;
    }

    public function saveForm($id)
    {
        $this->fk_vendor_information = $id;
        $this->type = self::TYPE_DIRECTOR;
        return $this->save();
    }

    public static function saveForms($id, $directorForms)
    {
        $result = true;
        if ($directorForms) {
            foreach ($directorForms as $directorForm) {
                $directorForm->fk_vendor_information = $id;
                $directorForm->type = self::TYPE_DIRECTOR;
                $result = $directorForm->save();
            }
        }

        return $result;
    }
}