<?php

namespace common\forms;

use Yii;
use common\models\User;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UpdateStatus extends User
{
    public $send_mail;

    public function rules()
    {
        return [
            [['application_status_message'], 'trim'],
            [['application_status_message', 'application_status'], 'required'],
            [['application_status_message'], 'string'],
            [['application_status'], 'safe'],
            [['sap_vendor_code', 'region'], 'string', 'max' => 11],
            [['hfe_vendor_code'], 'string', 'max' => 100],
            [['send_mail'], 'safe'],

            [['company_code', 'purchasing_organisation', 'account_group', 'payment_terms'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'sap_vendor_code' => 'SAP vendor code',
            'hfe_vendor_code' => 'HFE vendor code',
        ];
    }

    public static function findModel($id)
    {
        $query = self::find();
        $query->andWhere(['id' => $id]);
        return $query->one();
    }

    public function saveStatusModel()
    {
        if ($this->validate()) {
            if ($this->send_mail) {
                $this->sendMail();
            }
            return $this->save();
        }
    }

    public function sendMail()
    {
        $result = false;

        try {
            $result = Yii::$app->mailer
            ->compose(
                ['html' => 'statusChanged-html', 'text' => 'statusChanged-text'],
                [
                    'message'   => $this->application_status_message,
                    'status'    => self::$applicationStatus[$this->application_status],
                    'username'  => ($this->name) ? $this->name : $this->username
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setBcc('vms_cpc@herofutureernegies.com')
            ->setSubject('Vendor Registration Status | ' . Yii::$app->name)
            ->send();
        } catch (\Exception $ex) {
            $result = false;
        }

        return $result;
    }
}