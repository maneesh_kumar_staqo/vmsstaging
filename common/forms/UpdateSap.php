<?php

namespace common\forms;

use common\models\User;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class UpdateSap extends User
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sap_vendor_code', 'id'], 'required'],
            ['sap_vendor_code', 'string', 'max' => 6, 'min' => 6],
            [['sap_vendor_code'], 'uniqueSapCode']
        ];
    }

    public function uniqueSapCode()
    {
        $result = true;
        $query = self::find();
        $query->andWhere(['sap_vendor_code' => $this->sap_vendor_code]);
        $query->andWhere(['!=','id', $this->id]);

        if ($query->count()) {
            $this->addError('sap_vendor_code', 'This code has been taken');
            $result = false;
        }
        
        return $result;
    }

    public function saveCode()
    {
        $result = false;

        if ($this->validate() && $this->loadUser()) {

            $update = self::updateAll(['sap_vendor_code' => $this->sap_vendor_code], [
                'id' => $this->id
            ]);
            if ($update) {
                $result = true;
            }
        }

        return $result;
    }

    public function loadUser()
    {
        $result = false;
        $model = self::findOne(['id' => $this->id]);

        if ($model) {
            $result = true;
        } else {
            $this->addError('sap_vendor_code', 'User not found');
        }

        return $result;
    }
}