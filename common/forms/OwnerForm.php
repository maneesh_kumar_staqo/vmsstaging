<?php

namespace common\forms;

use Yii;
use common\models\VendorInformationPartner;
use common\components\UploadComponent;

class OwnerForm extends VendorInformationPartner
{

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor_information' => 'Vendor Information ID',
            'name' => 'Name',
            'din_number' => 'DIN No',
            'din_document' => 'Upload DIN Document',

            'pan_number' => 'PAN Number',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'country' => 'Country',

            'director_id_proof' => 'Upload Owner ID Proof',
            'director_address_proof' => 'Upload Residential Address',

            'is_qualified_under_164' => 'Whether Director is dis-qualified under Section 164 of the Companies Act, 2013?',

            'authorized_signatory' => 'Upload Board Resolution in favour of authorized signatory',
            'partnership_deed' => 'Upload Trust Deed',
            'pan_document' => 'Upload Owner PAN',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'upload_passport' => 'Upload Passport'
        ];
    }

    protected function saveImages($key = 0, $isDomestic)
    {
        $result = true;

        $panDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'pan_document', $key);
        //$idProof = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'director_id_proof', $key);
        $addressProof = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'director_address_proof', $key);
        $deedDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'partnership_deed', $key);
        $passportDoc = UploadComponent::uploadModelFileKeys($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'upload_passport', $key);

        // if ($isDomestic && !$panDoc) {
        //     $result = false;
        //     $this->addError('pan_document', 'Document is required');
        // }

        // if (!$idProof) {
        //     $result = false;
        //     $this->addError('director_id_proof', 'Document is required');
        // }

        // if (!$addressProof) {
        //     $result = false;
        //     $this->addError('director_address_proof', 'Document is required');
        // }

        if (!$deedDoc) {
            $result = false;
            $this->addError('partnership_deed', 'Document is required');
        }

        if (!$passportDoc && !$isDomestic) {
            $result = false;
            $this->addError('upload_passport', 'Passport is required');
        }

        return $result;
    }

    public function validateForm()
    {
        $result = false;

        if ($this->validate() && $this->saveImages()) {
            $result = true;
        }

        return $result;
    }

    public static function validateForms($ownerForms, $isDomestic)
    {
        $result = true;

        foreach ($ownerForms as $key => $ownerForm ) {
            $ownerForm->din_number = '';
            // if ($isDomestic && !$ownerForm->pan_number) {
            //     $ownerForm->addError('pan_number', 'PAN number is required');
            // }

            if (!$ownerForm->saveImages($key, $isDomestic) || $ownerForm->errors || !$ownerForm->validate()) {
                $result = false;
            }

            if (!$isDomestic && !$ownerForm->passport_no) {
                $ownerForm->addError('passport_no', 'Passport Number is required');
                $result = false;
            }
        }

        return $result;
    }

    public function saveForm($id)
    {
        $this->fk_vendor_information = $id;
        $this->type = self::TYPE_OWNER;
        return $this->save();
    }
    
    public static function saveForms($id, $ownerForms)
    {
        $result = true;
        if ($ownerForms) {
            foreach ($ownerForms as $ownerForm) {
                $ownerForm->din_number = '';
                $ownerForm->fk_vendor_information = $id;
                $ownerForm->type = self::TYPE_OWNER;
                $result = $ownerForm->save();
            }
        }

        return $result;
    }
}