<?php

namespace common\forms;

use Yii;
use common\models\ShareHolders;

class ShareHolderForm extends ShareHolders
{
    public function validateForm($shareHolders)
    {
        unset($shareHolders['{counter}']);
        $result = true;

        if ($shareHolders) {
            foreach ($shareHolders as $shareHolder) {
                $model = new static();
                $model->first_name = $shareHolder['first_name'];
                $model->last_name = $shareHolder['last_name'];

                if ($model->first_name && $model->last_name && !$model->validate()) {
                    $result = false;
                    break;
                }
            }
        }

        return $result;
    }

    public function saveForm($id, $shareHolders)
    {
        unset($shareHolders['{counter}']);
        $result = true;

        if ($shareHolders) {

            foreach ($shareHolders as $shareHolder) {
                if (!empty($shareHolder['id'])) {
                    if ($shareHolder['first_name'] || $shareHolder['last_name']) {
                        self::updateAll([
                            'first_name' => $shareHolder['first_name'],
                            'last_name' => $shareHolder['last_name']
                        ], ['id' => $shareHolder['id']]);
                    } else {
                        self::deleteAll(['id' => $shareHolder['id']]);
                    }
                } else if ($shareHolder['first_name'] || $shareHolder['last_name']) {
                    $model = new static();
                    $model->fk_vendor_information = $id;
                    $model->first_name = $shareHolder['first_name'];
                    $model->last_name = $shareHolder['last_name'];

                    if (!$model->save()) {
                        $result = false;
                        break;
                    }
                }
            }
        }

        return $result;

    }
}