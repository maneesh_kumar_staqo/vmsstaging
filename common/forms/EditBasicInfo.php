<?php

namespace common\forms;

use Yii;
use common\models\User;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class EditBasicInfo extends User
{
    public $oldPassword;
    public $password;
    public $confirmPassword;
    public $changePassword = 0;

    public function rules()
    {
        return [
            [['name', 'email'], 'required'],
            ['password', 'string', 'min' => 6],
            ['confirmPassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            [['email'], 'email'],
            [['name'], 'string', 'max' => 255],
            [['changePassword', 'username'], 'safe'],
            [['password', 'confirmPassword', 'oldPassword'], 'required', 'when' => function ($model) {
                return $model->changePassword;
            }, 'enableClientValidation' => false]
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Current Password',
            'password' => 'New Password',
            'confirmPassword' => 'Confirm New Password',
            'name' => 'Contact Person Name',
            'email' => 'Primary Email'
        ];
    }

    public function saveInfo()
    {
        $this->username = $this->email;

        if (!$this->changePassword) {
            $this->password = '';
            $this->confirmPassword = '';
            $this->oldPassword = '';
        } else if ($this->oldPassword){
            if (!Yii::$app->security->validatePassword($this->oldPassword, $this->password_hash)) {
                $this->addError('oldPassword', 'Password doest not match');
            } else if ($this->password == $this->confirmPassword) {
                $this->setPassword($this->password);
            }
        }

        if (!$this->errors) {
            return $this->save();
        }
        return false;
    }
}