<?php

namespace common\helpers;

use Yii;
use common\models\User;
use common\models\VendorInformation;
use common\models\OrganisationTypes;
use common\models\CompanyTypes;

class ReportHelper
{
    public static $registerHeader = [
        'Vendor Code', 'Vendor Name', 'Account Status', 'Application status', 'Vendor Type',
        'Company Type', 'City', 'Pin Code', 'Country', 'Email', 'Mobile Number', 'Commodities', 'Created At'
    ];

    public static $commodityHeader = [
        'Vendor Code', 'Vendor Name', 'Account Status', 'Application status', 'Vendor Type',
        'Primary Commodity', 'GSTN', 'MSME', 'IEC code', 'Turnover', 'Secondary Commodity', 'Created At'
    ];

    public static $serviceHeader = [
        'Vendor Code', 'Vendor Name', 'Account Status', 'Application status', 'Vendor Type',
        'Primary Service', 'GSTN', 'MSME', 'IEC code', 'Service Turnover', 'Secondary Service', 'Created At'
    ];

    public static $contactHeader = [
        'Vendor Code', 'Vendor Name', 'Account Status', 'Application status', 'Vendor Type',
        'Business Contact Name', 'Business Contact Email', 'Business Contact Phone',
        'Finance Contact Name', 'Finance Contact Email', 'Finance Contact Phone', 'Created At'
    ];

    public static $sapHeader = [
        'LIFNR (Vendor Code)', 'BUKRS (Company Code)', 'EKORG (Purchasing Org)', 'KTOKK (Account Group)',
        'NAME1', 'NAME2', 'SORT1', 'SORT2', 'STREET', 'STR_SUPPL1', 'HOUSE_NUM1', 'POST_CODE1', 'CITY1',
        'COUNTRY', 'REGION', 'MOB_NUMBER', 'SMTP_ADDR (E-Mail)', 'STCD3 (Tax Number 3)', 'STCD4 (SSI Certificate)',
        'J_1KFREPRE (Rep\'s Name)', 'J_1KFTBUS (Type of Company)', 'J_1KFTIND (Type of Organization)',
        'BANKS (Bank Country Key)', 'BANKL (Bank Key)', 'Bank Name', 'Bank Branch', 'Bank City', 'BANKN (Bank Account)',
        'AKONT (Recon Accoun)', 'ZTERM (Payment Term)', 'REPRF (Check Double Invoices)', 'KALSK (Schema Group)',
        'WEBRE (GR-Based Invoice Verification)', 'LEBRE (Service-Based Invoice Verification)', 'J_1IPANNO (PAN Number)',

        'WITHT (W/H Tax Type)', 'WT_WITHCD (W/H Tax Code)', 'WT_SUBJCT (Liable)'
    ];

    public static $shareholderHeader = [
        'First Name', 'Last Name'
    ];

    public static $financeHeader = [
        'Vendor Code', 'Vendor Name', 'GSTIN', 'Primary Service Type', 'Service Annual Turnover',
        'Primary Commodity', 'Commodity Annual Turnover'
    ];
    
    const NAME_LIMIT = 40;

    public static function restructureFinanceCsv($data)
    {
        $result = [];
        $result[] = self::$financeHeader;

        if ($data) {
            foreach ($data as $model) {
                $service = $serviceTurnover = $commodity = $commodityTurnover = $gst = 'NA';

                if ($model['registration']['primaryService']) {
                    if ($model['registration']['primaryService']['display_other_field']) {
                        $service =  $model['registration']['other_primary_service_type'];
                    } else {
                        $service = $model['registration']['primaryService']['title'];
                    }
                    if (!empty($model['registration']['primary_service_turnorver'])) {
                        if ($model['registration']['primary_service_turnorver'] == 1) {
                            $serviceTurnover = $model['registration']['primaryService']['upper_turnover'];
                        } else {
                            $serviceTurnover = $model['registration']['primaryService']['lower_turnover'];
                        }
                    }
                }

                if ($model['registration']['primaryCommodity']) {
                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                        $commodity =  $model['registration']['other_primary_commodity'];
                    } else {
                        $commodity = $model['registration']['primaryCommodity']['title'];
                    }

                    if (!empty($model['registration']['annual_turnover'])) {
                        if ($model['registration']['annual_turnover'] == 1) {
                            $commodityTurnover = $model['registration']['primaryCommodity']['upper_turnover'];
                        } else {
                            $commodityTurnover = $model['registration']['primaryCommodity']['lower_turnover'];
                        }
                    }
                }

                if ($model['tax']) {
                    $gst = $model['tax']['gstin_number'];
                }

                $result[] = [
                    $model['id'],
                    $model['registration']['company_name'],
                    $gst,
                    $service,
                    $serviceTurnover,
                    $commodity,
                    $commodityTurnover
                ];
            }
        }

        return $result;
    }

    public static function restructureShareholderCsv($data)
    {
        $result = [];
        $result[] = self::$shareholderHeader;

        if (!empty($data['vendorShareholders']) && !empty($data['vendorShareholders']['shareholders'])) {
            foreach ($data['vendorShareholders']['shareholders'] as $model) {

                $result[] = [
                    $model['first_name'],
                    $model['last_name']
                ];
            }
        }

        return $result;
    }

    public static function getShareholderFileName($data, $defaultName)
    {
        if (!empty($data['vendorShareholders']) && !empty($data['vendorShareholders']['companyType'])) {
            if ($data['vendorShareholders']['companyType'] == OrganisationTypes::OWNER_TYPE_OWNERSHIPS) {
                $suffix = '_owners.csv';
            } else {
                $suffix = '_shareholders.csv';
            }

            if ($data['hfe_vendor_code']) {
                $prefix = $data['hfe_vendor_code'];
            } else {
                $prefix = $data['id'];
            }

            $fileName = $prefix . $suffix;

        } else if ($data['hfe_vendor_code']) {
            $fileName = $data['hfe_vendor_code'] . '_.csv'; 
        } else {
            $fileName = $defaultName;
        }

        return $fileName;
    }

    public static function restructureSapCsv($data, $companies = false)
    {
        $result = [];
        $result[] = self::$sapHeader;

        if ($data) {

            foreach ($data as $model) {

                $nameOne = $nameTwo = '';
                if (!empty($model['registration'])) {
                    if (strlen($model['registration']['trading_name']) >= self::NAME_LIMIT) {
                        $nameOne = substr($model['registration']['trading_name'], 0, self::NAME_LIMIT);
                        $nameTwo = substr($model['registration']['trading_name'], self::NAME_LIMIT);
                    } else {
                        $nameOne = $model['registration']['trading_name'];
                    }
                }

                if ($companies && $model['vendor']['business_type']) {
                    $tempArray = json_decode($model['vendor']['business_type']);
                    $businessTypes = [];
                    foreach ($tempArray as $single ) {
                        if (!empty($companies[$single])) {
                            if ($companies[$single]['display_other_field']) {
                                $businessTypes[] = $model['vendor']['other_business'];
                            } else {
                                $businessTypes[] = $companies[$single]['title'];
                            }
                        }
                    }

                    $businessTypes = implode(', ', $businessTypes);
                } else {
                    $businessTypes = $model['vendor']['business_type'];
                }

                if (!empty($model['bank'])) {
                    $account_number = '`'. $model['bank']['pb_account_number'];
                } else {
                    $account_number = 'NA';
                }

                $result[] = [
                    $model['hfe_vendor_code'],
                    !empty($model['companyCode']) ? $model['companyCode']['company_name'] : '',
                    !empty($model['orgCode']) ? $model['orgCode']['org_name'] : '',
                    !empty($model['accountCode']) ? $model['accountCode']['title'] : '',

                    $nameOne, $nameTwo, 'NA', 'NA',

                    !empty($model['address']) ? $model['address']['ra_street_name'] : 'NA',
                    'NA',
                    !empty($model['address']) ? $model['address']['ra_house_number'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_pin_code'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_city'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_country'] : 'NA',
                    $model['region'],

                    !empty($model['contact']) ? $model['contact']['b_mobile_number'] : 'NA',
                    !empty($model['contact']) ? $model['contact']['b_email'] : 'NA',

                    !empty($model['tax']) ? $model['tax']['gstin_number'] : 'NA',
                    !empty($model['vendor']) ? $model['vendor']['ssi_number'] : 'NA',
                    $model['name'],                    
                    $businessTypes,
                    !empty($model['vendor']['companyType']) ? $model['vendor']['companyType']['title'] : 'NA',

                    'NA',
                    !empty($model['bank']) ? $model['bank']['pb_ifsc_code'] : 'NA',
                    !empty($model['bank']) ? $model['bank']['pb_bank_name'] : 'NA',
                    !empty($model['bank']) ? $model['bank']['pb_branch'] : 'NA',
                    !empty($model['bank']) ? $model['bank']['pb_city'] : 'NA',
                    $account_number,
                    //!empty($model['bank']) ? $model['bank']['pb_account_number'] : 'NA',
                    'NA',
                    !empty($model['paymentCode']) ? $model['paymentCode']['title'] : '',
                    '', '', '', '',
                    !empty($model['vendor']) ? $model['vendor']['company_pan_number'] : 'NA',
                    '', '', '', ''

                ];
            }
        }

        return $result;
    }

    public static function restructureContactCsv($data)
    {
        $result = [];
        $result[] = self::$contactHeader;

        if ($data) {
            foreach ($data as $model) {

                $result[] = [
                    $model['id'],
                    $model['registration']['company_name'],
                    !empty(User::$statusArray[$model['status']]) ? User::$statusArray[$model['status']] : 'NA',
                    !empty(User::$applicationStatus[$model['application_status']]) ? User::$applicationStatus[$model['application_status']] : 'NA',
                    !empty($model['vendor']['vendorType']) ? $model['vendor']['vendorType']['title'] : 'NA',

                    !empty($model['contact']) ? $model['contact']['b_name'] : 'NA',
                    !empty($model['contact']) ? $model['contact']['b_email'] : 'NA',
                    !empty($model['contact']) ? $model['contact']['b_mobile_number'] : 'NA',

                    !empty($model['contact']) ? $model['contact']['f_name'] : 'NA',
                    !empty($model['contact']) ? $model['contact']['f_email'] : 'NA',
                    !empty($model['contact']) ? $model['contact']['f_mobile_number'] : 'NA',

                    Yii::$app->formatter->asDatetime($model['created_at'])
                ];
            }
        }

        return $result;
    }

    public static function restructureServiceCsv($data, $services)
    {
        $result = [];
        $result[] = self::$serviceHeader;

        if ($data) {
            foreach ($data as $model) {

                if ($model['registration']['primaryService']) {
                    if ($model['registration']['primaryService']['display_other_field']) {
                        $service =  $model['registration']['other_primary_service_type'];
                    } else {
                        $service = $model['registration']['primaryService']['title'];
                    }
                } else {
                    $service = 'NA';
                }

                if ($model['tax']) {
                    $tax = $model['tax']['gstin_number'];
                } else {
                    $tax = 'NA';
                }

                if ($model['vendor'] && !empty(VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']])) {
                    $msme = VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']];
                } else {
                    $msme = 'NA';
                }

                if ($model['vendor'] && empty($model['vendor']['vendorType']['is_domestic'])) {
                    if ($model['tax']) {
                       $iec = $model['tax']['iec_code'];
                    } else {
                        $iec = 'NA';
                    }
                } else {
                    $iec = 'NA';
                }

                if ($model['registration']['primaryService']) {

                    if (!empty($model['registration']['primary_service_turnorver'])) {
                        if ($model['registration']['primary_service_turnorver'] == 1) {
                            $turnover = $model['registration']['primaryService']['upper_turnover'];
                        } else {
                            $turnover = $model['registration']['primaryService']['lower_turnover'];
                        }
                    } else {
                        $turnover = 'NA';
                    }
                } else {
                    $turnover = 'NA';
                }

                $secondaryService = '';
                if ($model['registration'] && $model['registration']['secondary_service_type']) {
                    $selected = json_decode($model['registration']['secondary_service_type']);

                    if (is_array($selected)) {
                        foreach ($selected as $single) {
                            if (!empty($services[$single]) && $services[$single]['display_other_field']) {
                                $secondaryService .= $model['registration']['other_secondary_service_type'] . ',';
                            } else if (!empty($services[$single])) {
                                $secondaryService .= $services[$single]['title'] . ',';
                            }
                        }
                    }
                } else {
                    $secondaryService = 'NA';
                }

                $result[] = [
                    $model['id'],
                    $model['registration']['company_name'],
                    !empty(User::$statusArray[$model['status']]) ? User::$statusArray[$model['status']] : 'NA',
                    !empty(User::$applicationStatus[$model['application_status']]) ? User::$applicationStatus[$model['application_status']] : 'NA',
                    !empty($model['vendor']['vendorType']) ? $model['vendor']['vendorType']['title'] : 'NA',
                    $service,
                    $tax,
                    $msme,
                    $iec,
                    $turnover,
                    $secondaryService,
                    Yii::$app->formatter->asDatetime($model['created_at'])
                ];
            }
        }

        return $result;
    }

    public static function restructureCommodityCsv($data, $commodities)
    {
        $result = [];
        $result[] = self::$commodityHeader;

        if ($data) {
            foreach ($data as $model) {

                if ($model['registration']['primaryCommodity']) {
                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                        $commodity =  $model['registration']['other_primary_commodity'];
                    } else {
                        $commodity = $model['registration']['primaryCommodity']['title'];
                    }
                } else {
                    $commodity = 'NA';
                }

                if ($model['tax']) {
                    $tax = $model['tax']['gstin_number'];
                } else {
                    $tax = 'NA';
                }

                if ($model['vendor'] && !empty(VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']])) {
                    $msme = VendorInformation::$optionArray[$model['vendor']['is_msme_act_2006']];
                } else {
                    $msme = 'NA';
                }

                if ($model['vendor'] && empty($model['vendor']['vendorType']['is_domestic'])) {
                    if ($model['tax']) {
                       $iec = $model['tax']['iec_code'];
                    } else {
                        $iec = 'NA';
                    }
                } else {
                    $iec = 'NA';
                }

                if ($model['registration']['primaryCommodity']) {

                    if (!empty($model['registration']['annual_turnover'])) {
                        if ($model['registration']['annual_turnover'] == 1) {
                            $turnover = $model['registration']['primaryCommodity']['upper_turnover'];
                        } else {
                            $turnover = $model['registration']['primaryCommodity']['lower_turnover'];
                        }
                    } else {
                        $turnover = 'NA';
                    }
                } else {
                    $turnover = 'NA';
                }

                $secondaryCommodity = '';
                if ($model['registration'] && $model['registration']['fk_secondary_commodity_type']) {
                    $selected = json_decode($model['registration']['fk_secondary_commodity_type']);
                    if (is_array($selected)) {
                        foreach ($selected as $single) {
                            if (!empty($commodities[$single]) && $commodities[$single]['display_other_field']) {
                                $secondaryCommodity .= $model['registration']['other_secondary_commodity'] . ',';
                            } else if (!empty($commodities[$single])) {
                                $secondaryCommodity .= $commodities[$single]['title'] . ',';
                            }
                        }
                    }
                } else {
                    $secondaryCommodity = 'NA';
                }

                $result[] = [
                    $model['id'],
                    $model['registration']['company_name'],
                    !empty(User::$statusArray[$model['status']]) ? User::$statusArray[$model['status']] : 'NA',
                    !empty(User::$applicationStatus[$model['application_status']]) ? User::$applicationStatus[$model['application_status']] : 'NA',
                    !empty($model['vendor']['vendorType']) ? $model['vendor']['vendorType']['title'] : 'NA',
                    $commodity,
                    $tax,
                    $msme,
                    $iec,
                    $turnover,
                    $secondaryCommodity,
                    Yii::$app->formatter->asDatetime($model['created_at'])
                ];
            }
        }

        return $result;
    }

    public static function restructureRegisterCsv($data)
    {
        $result = [];
        $result[] = self::$registerHeader;

        if ($data) {
            foreach ($data as $model) {

                if ($model['contact']) {
                    $contact = $model['contact']['b_name'] . ' ( ' . $model['contact']['b_email'] . ' )';
                } else {
                    $contact = 'NA';
                }

                if ($model['registration']['primaryCommodity']) {
                    if ($model['registration']['primaryCommodity']['display_other_field']) {
                        $commodity =  $model['registration']['other_primary_commodity'];
                    } else {
                        $commodity = $model['registration']['primaryCommodity']['title'];
                    }
                } else {
                    $commodity = 'NA';
                }

                $result[] = [
                    $model['id'],
                    $model['registration']['company_name'],
                    !empty(User::$statusArray[$model['status']]) ? User::$statusArray[$model['status']] : 'NA',
                    !empty(User::$applicationStatus[$model['application_status']]) ? User::$applicationStatus[$model['application_status']] : 'NA',
                    !empty($model['vendor']['vendorType']) ? $model['vendor']['vendorType']['title'] : 'NA',
                    !empty($model['vendor']['companyType']) ? $model['vendor']['companyType']['title'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_city'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_pin_code'] : 'NA',
                    !empty($model['address']) ? $model['address']['ra_country'] : 'NA',
                    $contact,
                    !empty($model['contact']) ? $model['contact']['b_mobile_number'] : 'NA',
                    $commodity,
                    Yii::$app->formatter->asDatetime($model['created_at'])
                ];
            }
        }

        return $result;
    }

    public static function generateHfeCode($id)
    {
        return 'HFE' . date('Y') . '-' . $id;
    }
}