<?php

namespace common\helpers;

use Yii;
use common\components\notifications\Notify;
use DateTime;

/**
 * DateTimeHelper contains all date related task.
 *
 * @copyright (c) 2018, SchoolManagement
 *
 * @author Aabir Hussain <aabir.hussain1@gmail.com>
 */
class DateTimeHelper
{
    //a static date from when we support all data.
    const PROJECT_START_DATE = '2015';

    const ACCESS_TOKEN_TIME = 60 * 24 * 7; //should be in minutes
    /**
     * Date Formats
     */
    const DATE_FORMAT_MYSQL = 'Y-m-d';

    /**
     * Time Formats
     */
    const TIME_FORMAT_MYSQL = 'H:i:s';

    /**
     * Date Time Formats
     */
    const DATETIME_FORMAT_MYSQL = 'Y-m-d H:i:s';

    const SOLD_NOTES_DATE_FORMAT = 'd-m-Y';

    const SOLD_NOTES_TIME_FORMAT = 'H:i A';

    const COURSE_EXPIRY_DATE_FORMAT = 'd M, Y';

    const START_YEAR = 1941;

    //static array of months
    public static $months = [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December'
    ];

    public static function setDefaultZone()
    {
        date_default_timezone_set('Asia/Kolkata');
    }

    /**
     * Gets a number which is the timestamp with added microseconds
     *
     * @param int $accuracy
     * @return string
     */
    public static function getmicrotime($accuracy = 5)
    {
        list ($u, $s) = explode(' ', microtime());
        $number = bcadd($u, $s, $accuracy);

        return str_replace('.', '', $number);
    }

    /**
     * Get current datetime.
     *
     * @return datetime
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getDateTime()
    {
        return date(self::DATETIME_FORMAT_MYSQL);
    }

    /**
     * Return time for access token.
     *
     * @return datetime
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getTokenTime()
    {
        $date = self::getDateTime();
        $minutes = self::ACCESS_TOKEN_TIME;
        return date(self::DATETIME_FORMAT_MYSQL, strtotime("$date + $minutes minutes"));
    }

//    public static function beautifyDateTime($datetime)
//    {
//        //date_default_timezone_set('Asia/Kolkata');
//        return Yii::$app->formatter->asDatetime($datetime);
//    }

    /**
     * It will check if time is less than 24 then return ago time
     * else return date i.e
     *
     * just now, 5 min ago, 1 hour ago, days ago, weeks ago.
     *
     * @param string $datetime
     * @param boolean $isTimeStamp
     *
     * @return string $newDateTime
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function beautifyDateTime($datetime, $isTimeStamp = false)
    {
        try {
            date_default_timezone_set('Asia/Kolkata');
        if ($isTimeStamp) {
            $localDatetime = new \DateTime(gmdate( 'Y-m-d\TH:i:s\Z',$datetime));
        } else {
            $localDatetime = new \DateTime($datetime);
        }

        $nowDate = new \DateTime('now');

        $newTime = '';

        $interval = $nowDate->diff($localDatetime);

        if ($interval->days == 0) {
            if (!$interval->h && !$interval->i) {
                $newTime = 'just now';
            } else if (!$interval->h) {
                $newTime = $interval->i . ' min ago';
            } else if ($interval->h == 1) {
                $newTime = $interval->h . ' hour ago';
            } else {
                $newTime = $interval->h . ' hours ago';
            }
        } else if ($interval->days < 7) {
            if ($interval->days == 1) {
                $newTime = $interval->d . ' day ago';
            } else {
                $newTime = $interval->d . ' days ago';
            }
        } else {
            $weeks = round($interval->days / 7, 0);
            if ($weeks == 1) {
                $newTime = '1 week ago';
            } else {
                $newTime = $weeks. ' weeks ago';
            }
        }

        } catch (\Exception $ex) {
            $newTime = '--';
            Notify::exception($ex);
        }

        return $newTime;
    }

    public static function beautifyDate($datetime)
    {
        //date_default_timezone_set('Asia/Kolkata');
        return Yii::$app->formatter->asDate($datetime);
    }

    /**
     * Get current date in self::DATE_FORMAT_MYSQL
     *
     * @return date
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getDate($format = false)
    {
        if ($format) {
            return date($format);
        } else {
            return date(self::DATE_FORMAT_MYSQL);
        }
    }

    /**
     * 
     * @param type $date
     * @return type
     */
    public static function getDateByMsqlFormat($date = false)
    {
        if (!$date) {
            $date = self::getDate();
        }

        return date(self::DATE_FORMAT_MYSQL, strtotime($date));
    }

    public static function getYear($datetime)
    {
        return date('Y', strtotime($datetime));
    }

    /**
     * Returns date in ISO 8601 format, usually used in sitemap generation
     *
     * @param string $minutes
     * @param string $operation like +, -
     * @return string
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getDateByCondition($minutes, $operation)
    {
        $date = self::getDateTime();
        return date(self::DATETIME_FORMAT_MYSQL, strtotime("$date $operation $minutes minutes"));
    }

    /**
     * Return Previous dates of 9 days
     *
     * @param string $minutes
     * @return string
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getLastWeekDate()
    {
        $date = self::getDateTime();
        return date('Y-m-d', strtotime("$date - 8 days"));
    }

    /**
     * Return date of next days of a current date
     *
     * @param int $days
     *
     * @return date $date
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function getNextDate($days = 15)
    {
        $date = self::getDateTime();
        return date('Y-m-d', strtotime("$date + $days days"));
    }

    /**
     * Get year of a given date.
     *
     * @param date $date
     *
     * @return int $year
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getDateYear($date = false)
    {
        if (!$date) {
            $date = self::getDateTime();
        }
        return date('Y', strtotime($date));
    }

    /**
     * Get year of a given date.
     *
     * @param date $date
     *
     * @return int $year
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getNextYear()
    {

        return date('Y', strtotime('+1 years'));;
    }

    /**
     * Get first date of a given date if date is false then return first date of current month.
     *
     * @param date $date
     *
     * @return date $date first date of the month
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getMonthFirstDate($date = false)
    {
        if (!$date) {
            $date = self::getDateTime();
        }

        return date('Y-m-01', strtotime($date));
    }

    /**
     * Get last date of a given date if date is false then return last date of current month.
     *
     * @param date $date
     *
     * @return date $date last date of the month
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getMonthLastDate($date = false)
    {
        if (!$date) {
            $date = self::getDateTime();
        }

        return date('Y-m-t', strtotime($date));
    }

    /**
     * Convert date format from anything to Y-m-d
     *
     * @param date $date
     *
     * @return date $date
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function convertDateformat($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * check if given value is valid date or not
     *
     * @param date $date
     * @param string $format
     *
     * @return boolean $result
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * Return first date of the current year
     *
     * @return date $date first date of the current year
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function minAttendanceStartDate()
    {
        return date('Y-01-01');
    }

    /**
     * return next date of the given date.
     *
     * @param date $date
     *
     * @return date $date next date of given date
     *
     * @author Aabir Hussain <aabir.hussain1@gmail.com>
     */
    public static function getNextDay($date)
    {
        return date("Y-m-d", strtotime("+1 day", strtotime($date)));
    }

    public static function convertHourToSec($timeString)
    {
        $timeString = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $timeString);

        sscanf($timeString, "%d:%d:%d", $hours, $minutes, $seconds);

        $timeSeconds = $hours * 3600 + $minutes * 60 + $seconds;

        return $timeSeconds;
    }

    public static function getDuration($time)
    {
        $duration = false;
        if ($time) {
            $duration = Yii::$app->formatter->asDuration(DateTimeHelper::convertHourToSec($time));
        }
        return $duration;
    }

    public static function getShortTime($time)
    {
        $newTime = false;
        if ($time) {
            $newTime = Yii::$app->formatter->asTime($time, 'short');
        }
        return $newTime;
    }

    public static function getShortDate($date)
    {
        $newDate = false;
        if ($date) {
            $newDate = Yii::$app->formatter->asDate($date);
        }
        return $newDate;
    }

    /**
     * Add number of month in the given month
     *
     * @param date $months
     * @param int $plusMonth
     *
     * @return date Description
     *
     * @author Aabir Hussain <aabir@notesgen.com>
     */
    public static function getFutureMonth($months, $plusMonth)
    {
        $date = date("Y-m", strtotime("+$plusMonth month", strtotime($months)));

        return $date;
    }

    public static function getDaysDiff($dateOne, $dateTwo)
    {
        try {
            $d1 = new \DateTime($dateOne);
            $d2 = new \DateTime($dateTwo);

            $interval = $d2->diff($d1);
            $diff = $interval->days;
        } catch (Exception $ex) {
            $diff = 0;
        }

        return $diff;
    }

    public static function getMonthDiff($dateOne, $dateTwo)
    {
        $diff = 12;
        try {
            if (self::validateDate($dateOne, 'Y-m') && self::validateDate($dateTwo, 'Y-m')) {
                $d1 = new \DateTime($dateOne);
                $d2 = new \DateTime(self::getMonthLastDate($dateTwo));

                $interval = $d2->diff($d1);

                $diff = $interval->format('%m') + 1;
                $yearDiff = $interval->format('%y');

                if ($yearDiff == 1) {
                    $diff += 11;
                } else if ($yearDiff == 2) {
                    $diff += 23;
                }
            }
        } catch (Exception $ex) {
        }

        return $diff;
    }

    public function microToDate($date)
    {
        if (!empty($date)) {
            $dateFomat = date('d M Y', $date);
            return $dateFomat;
        }
    }

    public static function isWithInAWeek($date)
    {
        $dateOne = new \DateTime(date('Y-m-d', strtotime($date)));
        $dateTwo = new \DateTime(date('Y-m-d'));

        $dayDiff = $dateOne->diff($dateTwo)->days;

        return ( $dayDiff < 7) ? true : false;
    }

    public static function getDiffInMin($startDate, $endDate)
    {
        $dateOne = new DateTime($startDate);
        $dateTwo = $dateOne->diff(new DateTime($endDate));

        $minutes = $dateTwo->days * 24 * 60;
        $minutes += $dateTwo->h * 60;
        $minutes += $dateTwo->i;

        return $minutes;
    }

//NOTE MAY BE NEEDED FOR INTERNATIONAL TIMING STARTS    
//    public static function humanizeDateTime($datetime)
//    {
//        $timestamp = strtotime($datetime);
//        return sprintf('%s | %s'
//                , date(self::SOLD_NOTES_DATE_FORMAT, $timestamp)
//                , date(self::SOLD_NOTES_TIME_FORMAT, $timestamp));
//    }
//
//    public static function getLocalDateTime($datetime)
//    {
//        $countryDetails = UserHelper::getTimezoneDetails();
//        $userTimeZone = $countryDetails ? $countryDetails['zone_name'] : 'UTC';
//        $new_str = new \DateTime($datetime, new \DateTimeZone('UTC') );
//        $new_str->setTimeZone(new \DateTimeZone($userTimeZone));
//        return $new_str->format(self::DATETIME_FORMAT_MYSQL);
//    }
//    public static function isGreaterThenProjStartDate($date)
//    {
//        if (date('Y-m-t', strtotime($date)) >= self::PROJECT_START_DATE) {
//            return true;
//        }
//        return false;
//    }
//NOTE MAY BE NEEDED FOR INTERNATIONAL TIMING ENDS
}