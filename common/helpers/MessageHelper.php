<?php

namespace common\helpers;

class MessageHelper
{
    const SIGNUP_COMPLETED = 1;
    const TYPE_EMAIL_VERIFY = 2;
    const TYPE_VERIFICATION_FAIL = 3;
    const TYPE_VERIFICATION_FAIL_AGAIN = 4;
    const TYPE_RESEND = 5;
    const TYPE_CANNOT_EDIT = 6;
    const TYPE_REQUEST_PASSWORD_SUCCESS = 7;
    const TYPE_REQUEST_PASSWORD_FAIL = 8;
    const TYPE_PASSWORD_RESET_SUCCESS = 9;

    public static $messages = [
        1 => 'Signup has been successfully completed. Please check your email to verify.',
        2 => 'Your email address has been verified, please continue to complete the form.',
        3 => 'Sorry, we are unable to send verification email for the provided email address.',
        4 => 'Sorry, we are unable to resend verification email for the provided email address.',
        5 => 'Check your email for further instructions.',
        6 => 'Sorry your application is under review, please wait untill the review process has been completed.',
        7 => 'Check your email for further instructions.',
        8 => 'Sorry, we are unable to reset password for the provided email address.',
        9 => 'Your password has been successfully updated.',
    ];
}