<?php

namespace common\helpers;

use Yii;
use common\models\OrganisationTypes;
use common\components\UploadComponent;

class FormHelper
{
    /**
     * Get File of a form.
     *
     */
    public static function getFile($model, $originalField)
    {
        try {
            if ($model->$originalField && !empty($model->oldAttributes[$originalField])) {
                $reflect = new \ReflectionClass($model);
                $class = $reflect->getShortName();

                $file = self::getSingleFileName($model, $originalField);

                $result = Yii::$app->params['imgPath'] . UploadComponent::$filePublicLocations[$class] . $file;
            }
        } catch (\Exception $ex) {
            $result = false;
        }

        return $result;
    }

    public static function getSingleFileName($model, $originalField)
    {
        $file = false;
        try {
            if (!empty(json_decode($model->$originalField))) {
                $file = json_decode($model->$originalField)[0];
            } else {
                $file = $model->$originalField;
            }
        } catch (\Exception $ex) {
            $file = $model->$originalField;
        }

        return $file;
    }

    public static function getFilesHtml($model, $originalField)
    {
        $html = '';

        try {
            if ($model->$originalField && !empty($model->oldAttributes[$originalField])) {
                $reflect = new \ReflectionClass($model);
                $class = $reflect->getShortName();
                $files = json_decode($model->$originalField);

                foreach ($files as $key => $file) {
                    if ($file) {
                        $fullPath = Yii::$app->params['imgPath'] . UploadComponent::$filePublicLocations[$class] . $file;
                        $html .= '<span class="text-center ml-3" style="width: 190px;"><span>'
                                . '<a href="'. $fullPath .'" target="_blank">' . ($key + 1) . 'View</a></span></span>';
                    }
                }
            }
        } catch (\Exception $ex) {
            $html = '';
        }

        return $html;
    }

    protected static function label($model, $field)
    {
        $label = '';
        $allLabels = $model->attributeLabels();

        if ($allLabels[$field]) {
            $label = $allLabels[$field];
        } else {
            $label = $field;
        }

        return $label;
    }

    /**
     * 
     * @param type $form
     * @param type $model
     */
    public static function checkbox($form, $model, $fieldName)
    {
        return $form->field($model, $fieldName, [
            'template' => '{input}{label}{error}{hint}',
            'options' => [
                'class' => 'custom-control custom-checkbox'
            ],
            'labelOptions' => [
                'class' => 'custom-control-label vender-text',
            ]
        ])->checkbox([
            'class' => ' custom-control-input',
            'id' => $fieldName,
        ], false);
    }

    /**
     * Dropdown list of options with multiple selection option
     *
     * @param object $model
     * @param string $fieldName
     * @param array $optionArray
     * @param array $options [
     *                          'isDependent' => 'true or false',
     *                          'other-field'  => 'class of other-field',
     *                          'checkTrasco' => 'true or false'
     *                      ]
     * @return string
     */
    public static function multiDropDownList($model, $fieldName, $optionArray = [], $options = [])
    {
        $selected = '';
        $label = self::label($model, $fieldName);
        $formField = $model->formName() . '[' . $fieldName.  ']';
        if (!empty($options['other-field'])) {
            $defaultOther = $options['other-field'];
        } else {
            $defaultOther = '';
        }
        if (!empty($options['trasco-field'])) {
            $defaultTrasco = $options['trasco-field'];
        } else {
            $defaultTrasco = '';
        }
        $html = '<div class="single-dropdown-multi-selection"><div class="multioption"><label>'. $label .'</label></div>';
        $html .= '<select data-action="dependent-multi-dropdown" data-default-trasco-field="'.$defaultTrasco.'" data-default-other-field="'.$defaultOther.'" class="multiSelect" multiple="multiple" name="'.$formField.'[]">';
        $html .= '<optgroup>';

        $hiddenOptions = '';
        if ($optionArray) {
            foreach ($optionArray as $single) {

                if (!empty($options['isDependent'])) {
                    $labelOptions = '
                        data-display-other-feild="'. $single['display_other_field'] .'"
                        data-other-field="'. $options['other-field'] .'"';
                } else {
                    $labelOptions = '';
                }

                if (!empty($options['checkTrasco']) && !empty($single['display_trascco'])) {
                    $labelOptions .= 'data-display-trasco-feild="'. $single['display_trascco'] .'"
                        data-trasco-field="'. $defaultTrasco .'"';
                }

                if ($model->$fieldName && is_array($model->$fieldName)) {
                    $selected = (in_array($single['id'], $model->$fieldName)) ? 'selected="true"' : '';
                }

                $html .= '<option value="'. $single['id'] .'" '. $selected .'>'. $single['title'] .'</option>';
                $hiddenOptions .= '<span class="single_hidden_'.$single['id'].'" '.$labelOptions.'>'. $single['title'] .'</span>';
            }
        }

        $html .= '</optgroup></select>';

        $html .= '<div class="hidden_options d-none">'. $hiddenOptions .'</div>';

        if ($model->getFirstError($fieldName)) {
            $html .= '<span class="help-block">' . $model->getFirstError($fieldName) . '</span>';
        }

        $html .= '</div>';

        return $html;
    }

    /**
     * Dropdown list of options
     *
     * @param object $model
     * @param string $fieldName
     * @param array $optionArray
     * @param array $options [
     *                          'otherField' => 'true or false',
     *                          'other-field'  => 'class of other-field',
     *                      ]
     * @return string
     */
    public static function simpleDropDownList($model, $fieldName, $optionArray = [], $options = [])
    {
        if (isset($options['originalField'])) {
            $originalField = $options['originalField'];
        } else {
            $originalField = $fieldName;
        }

        $label = self::label($model, $originalField);

        $html = '';
        if (empty($options['hideLabel'])) {
            $html = '<div class="multioption"><label style="padding-left: 5px;">'. $label .'</label></div>';
        }
        $html .= '<div class="customsingedropdowncon"><div class="dropdown">';

        $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $label . '</button>';

        $htmlContent = '<span class="fs-arrow"></span><ul class="dropdown-menu dropstoptop dropdown-menu-select primary">';

        if ($fieldName == $originalField) {
            $formField = $model->formName() . '[' . $fieldName.  ']';
        } else {
            $formField = $model->formName() . '['. $options['key'] .']'.  '[' . $originalField.  ']';
        }

        if ($optionArray) {
            foreach ($optionArray as $single) {

                if (!empty($options['otherField'])) {
                    $labelOptions = 'data-action="simple-dropdown"
                        data-display-other-feild="'. $single['display_other_field'] .'"
                        data-other-field="'. $options['other-field'] .'"';
                } else {
                    $labelOptions = '';
                }

                if (isset($single['is_domestic'])) {
                    $labelOptions .= 'data-action="domestic-field" data-is-domestic=' . $single['is_domestic'];
                }

                $selected = ($single['id'] == $model->$originalField) ? 'checked="true"' : '';

                if ($selected) {
                    $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $single['title'] . '</button>';
                }

                $htmlContent .= '<li>
                    <label class="dropdown-radio" '. $labelOptions .' >
                        <input type="checkbox" value="'. $single['id'] .'" name="' . $formField . '" class="chb" '.$selected.'>
                        <label for="Ford"></label>
                        <span class="limited">'. $single['title'] .'</span>
                    </label>
                </li>';
            }
        }

        $htmlContent .= '</ul></div>';

        if ($model->getFirstError($originalField)) {

            $htmlContent .= '<span class="help-block">' . $model->getFirstError($originalField) . '</span>';
        }

        $htmlContent .= '</div>';

        $html .= $buttonHtml;
        $html .= $htmlContent;

        return $html;
    }

    /**
     * Dropdown list of options
     *
     * @param object $model
     * @param string $fieldName
     * @param array $optionArray
     * @param array $options [
     *                          'otherField' => 'true or false',
     *                          'other-field'  => 'class of other-field',
     *                      ]
     * @return string
     */
    public static function vendorTypeDropDownList($model, $fieldName, $optionArray = [], $options = [])
    {
        if (isset($options['originalField'])) {
            $originalField = $options['originalField'];
        } else {
            $originalField = $fieldName;
        }

        $label = self::label($model, $originalField);

        $html = '';
        if (empty($options['hideLabel'])) {
            $html = '<div class="multioption"><label style="padding-left: 5px;">'. $label .'</label></div>';
        }
        $html .= '<div class="customsingedropdowncon"><div class="dropdown">';

        $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $label . '</button>';

        $htmlContent = '<span class="fs-arrow"></span><ul class="dropdown-menu dropstoptop dropdown-menu-select primary">';

        if ($fieldName == $originalField) {
            $formField = $model->formName() . '[' . $fieldName.  ']';
        } else {
            $formField = $model->formName() . '['. $options['key'] .']'.  '[' . $originalField.  ']';
        }

        if ($optionArray) {
            foreach ($optionArray as $single) {

                $labelOptions = '';

                $selected = ($single['id'] == $model->$originalField) ? 'checked="true"' : '';

                if ($selected) {
                    $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $single['title'] . '</button>';
                }

                $htmlContent .= '<li data-action-vendor="vendor-type" >
                    <label class="dropdown-radio" '. $labelOptions .'>
                        <input type="checkbox" value="'. $single['id'] .'" name="' . $formField . '" class="chb" '.$selected.'>
                        <label for="Ford"></label>
                        <span class="limited">'. $single['title'] .'</span>
                    </label>
                </li>';
            }
        }

        $htmlContent .= '</ul></div>';

        if ($model->getFirstError($originalField)) {

            $htmlContent .= '<span class="help-block">' . $model->getFirstError($originalField) . '</span>';
        }

        $htmlContent .= '</div>';

        $html .= $buttonHtml;
        $html .= $htmlContent;

        return $html;
    }
    

    /**
     * Dropdown list of options
     *
     * @param object $model
     * @param string $fieldName
     * @param array $optionArray
     * @param array $options [
     *                          'otherField' => 'true or false',
     *                          'other-field'  => 'class of other-field',
     *                      ]
     * @return string
     */
    public static function organisationDropDownList($model, $fieldName, $optionArray = [], $options = [])
    {

        $label = self::label($model, $fieldName);

        $html = '<div class="multioption"><label style="padding-left: 5px;">'. $label .'</label></div>';
        $html .= '<div class="customsingedropdowncon"><div class="dropdown">';

        $buttonHtml = '<button type="button"  class="btn btn-select organisation-selected-val" data-toggle="dropdown">'. $label . '</button>';

        $htmlContent = '<span class="fs-arrow"></span><ul class="dropdown-menu dropstoptop dropdown-menu-select primary">';

        $formField = $model->formName() . '[' . $fieldName.  ']';

        if ($optionArray) {
            foreach ($optionArray as $single) {
                if ($single['type'] == OrganisationTypes::TYPE_DIRECTOR) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="director"';
                } elseif ($single['type'] == OrganisationTypes::TYPE_OWNER) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="owner"';
                } elseif ($single['type'] == OrganisationTypes::TYPE_KARTA) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="karta"';
                } elseif ($single['type'] == OrganisationTypes::TYPE_PARTNER) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="partner"';
                } elseif ($single['type'] == OrganisationTypes::TYPE_PROPRIETOR) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="proprietor"';
                } elseif ($single['type'] == OrganisationTypes::TYPE_PERSON) {
                    $labelOptions = 'data-action="organisation-dropdown"
                        data-display-fields="person"';
                } else {
                    $labelOptions = '';
                }

                if ($single['owners_type']) {
                    $labelOptions .= ' data-owners-type=' . $single['owners_type'];
                }
                $labelOptions .= ' data-is-domestic="' . $single['is_domestic'] . '"';
                $labelOptions .= ' data-is-overseas="' . $single['is_overseas'] .'"';
                

                $selected = ($single['id'] == $model->$fieldName) ? 'checked="true"' : '';

                if ($selected) {
                    $buttonHtml = '<button type="button" class="btn btn-select organisation-selected-val" data-toggle="dropdown">'. $single['title'] . '</button>';
                }

                $htmlContent .= '<li>
                    <label class="dropdown-radio" '. $labelOptions .' >
                        <input type="checkbox" value="'. $single['id'] .'" name="' . $formField . '" class="chb" '.$selected.'>
                        <label for="Ford"></label>
                        <span class="limited">'. $single['title'] .'</span>
                    </label>
                </li>';
            }
        }

        $htmlContent .= '</ul></div>';

        if ($model->getFirstError($fieldName)) {
            $htmlContent .= '<span class="help-block">' . $model->getFirstError($fieldName) . '</span>';
        }

        $htmlContent .= '</div>';

        $html .= $buttonHtml;
        $html .= $htmlContent;

        return $html;
    }

    /**
     * Dropdown list of options
     *
     * @param object $model
     * @param string $fieldName
     * @param array $optionArray
     * @param array $options [
     *                          'isDependent' => 'true or false',
     *                          'other-field'  => 'class of other-field',
     *                          'turn-over-dropdown' => '.'
     *                      ]
     * @return string
     */
    public static function dropDownList($model, $fieldName, $optionArray = [], $options = [])
    {
        $label = self::label($model, $fieldName);

        $html = '<div class="multioption"><label style="padding-left: 5px;">'. $label .'</label></div>';
        $html .= '<div class="customsingedropdowncon"><div class="dropdown">';

        $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $label . '</button>';

        $htmlContent = '<span class="fs-arrow"></span><ul class="dropdown-menu dropstoptop dropdown-menu-select primary">';

        $formField = $model->formName() . '[' . $fieldName.  ']';
        $turnOverField = $model->formName() . '[' . $options['turn-over-field-name'] .  ']';

        if ($optionArray) {
            foreach ($optionArray as $single) {
                if (!empty($options['isDependent'])) {
                    $labelOptions = 'data-action="dependent-dropdown"
                        data-upper-turnover="'. $single['upper_turnover'] .'"
                        data-lower-turnover="'. $single['lower_turnover'] .'"
                        data-display-other-feild="'. $single['display_other_field'] .'"
                        data-other-field="'. $options['other-field'] .'"
                        data-turn-over-field-name="'. $turnOverField .'"
                        data-turn-over-dropdown="'. $options['turn-over-dropdown'] .'"';
                } else {
                    $labelOptions = '';
                }

                $selected = ($single['id'] == $model->$fieldName) ? 'checked="true"' : '';

                if ($selected) {
                    $buttonHtml = '<button type="button" class="btn btn-select" data-toggle="dropdown">'. $single['title'] . '</button>';
                }

                $htmlContent .= '<li>
                    <label class="dropdown-radio" '. $labelOptions .' >
                        <input type="checkbox" value="'. $single['id'] .'" name="' . $formField . '" class="chb" '.$selected.'>
                        <label for="Ford"></label>
                        <span class="limited">'. $single['title'] .'</span>
                    </label>
                </li>';
            }
        }

        $htmlContent .= '</ul></div>';

        if ($model->getFirstError($fieldName)) {
            $htmlContent .= '<span class="help-block">' . $model->getFirstError($fieldName) . '</span>';
        }

        $htmlContent .= '</div>';

        $html .= $buttonHtml;
        $html .= $htmlContent;

        return $html;
    }

    public static function turnOverDropdown($model, $fieldName, $optionArray = [], $options = [])
    {
        $label = self::label($model, $fieldName);

        if (!empty($options['class'])) {
            $class = $options['class'];
        } else {
            $class = $options['class'];
        }

        $html = '<div class="multioption"><label style="padding-left: 5px;">'. $label .'</label></div>';
        $html .= '<div class="customsingedropdowncon"><div class="dropdown">';

        $buttonHtml = '<button type="button" data-default-text="'. $label .'" class="btn btn-select" data-toggle="dropdown">'. $label . '</button>';

        $htmlContent = '<span class="fs-arrow"></span><ul class="'. $class .' dropdown-menu dropstoptop dropdown-menu-select primary">';

        $formField = $model->formName() . '[' . $fieldName.  ']';

        if ($optionArray) {
            foreach ($optionArray as $key => $value) {
                $selected = ($key == $model->$fieldName) ? 'checked="true"' : '';

                if ($selected) {
                    $buttonHtml = '<button type="button" data-default-text="'. $label .'" class="btn btn-select" data-toggle="dropdown">'. $value . '</button>';
                }

                $htmlContent .= '<li>
                    <label class="dropdown-radio">
                        <input type="checkbox" value="'. $key .'" name="' . $formField . '" class="chb" '. $selected .'>
                        <label for="' . $formField . '"></label>
                        <span class="limited">'. $value .'</span>
                    </label>
                </li>';
            }
        }

        $htmlContent .= '</ul></div>';

        if ($model->getFirstError($fieldName)) {
            $htmlContent .= '<span class="help-block">' . $model->getFirstError($fieldName) . '</span>';
        }

        $htmlContent .= '</div>';

        $html .= $buttonHtml;
        $html .= $htmlContent;

        return $html;
    }

    public static function radioList($form, $model, $fieldName, $optionArray = [], $options = [])
    {
        $options['item'] = function($index, $label, $name, $checked, $value) {

            $selected = ($checked) ? 'checked="true"' : '';

            $html = '<div class="col-md-6"><div class="custom-control custom-radio">';
            $html .= '<input '.$selected.' type="radio" class="custom-control-input " name="' . $name . '" value="' . $value . '" id="'. $index .'" name="groupOfDefaultRadios">';
            $html .= '<label class="custom-control-label vender-text" for="'. $index .'">'. $label .'</label>';
            $html .= '</div></div>';

            $return = '<label class="modal-radio">';
            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
            $return .= '<i></i>';
            $return .= '<span>' . ucwords($label) . '</span>';
            $return .= '</label>';

            return $html;
        };

        if (!empty($options['class'])) {
            $options['class'] = 'd-flex justify-content-between row' . $options['class'];
        } else {
            $options['class'] = 'd-flex justify-content-between row';
        }

        return $form->field($model, $fieldName)
            ->radioList($optionArray, $options)
            ->label(false);
    }

    
    public static function radioListV1($form, $model, $fieldName, $optionArray = [], $options = [])
    {
        $options['item'] = function($index, $label, $name, $checked, $value) use ($options, $fieldName) {

            $selected = ($checked) ? 'checked="true"' : '';

            if (!$selected && isset($options['selected']) && $options['selected'] == $value) {
                $selected = 'checked="true"';
            }

            $html = '<div class="col-md-2 col-2"><div class="custom-control custom-radio">';
            $html .= '<input '.$selected.' type="radio" class="custom-control-input " name="' . $name . '" value="' . $value . '" id="'. $fieldName . $index .'" name="groupOfDefaultRadios">';
            $html .= '<label class="custom-control-label vender-text" for="'. $fieldName . $index .'">'. $label .'</label>';
            $html .= '</div></div>';

            $return = '<label class="modal-radio">';
            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
            $return .= '<i></i>';
            $return .= '<span>' . ucwords($label) . '</span>';
            $return .= '</label>';

            return $html;
        };

        if (!empty($options['class'])) {
            $options['class'] = 'row' . $options['class'];
        } else {
            $options['class'] = 'row';
        }

        return $form->field($model, $fieldName)
            ->radioList($optionArray, $options)
            ->label(false);
    }

    public static function fileInput($form, $model, $fieldName, $options = [], $disableError = false)
    {
        if (isset($options['originalField'])) {
            $originalField = $options['originalField'];
        } else {
            $originalField = $fieldName;
        }

        $label = self::label($model, $originalField);

        if (isset($options['class'])) {
            $class = $options['class'];
        } else {
            $class = 'col-md-6';
        }

        if ($fieldName == $originalField) {
            $formField = $model->formName() . '[' . $fieldName.  ']';
        } else {
            $formField = $model->formName() . '['. $options['key'] .']'.  '[' . $originalField.  ']';
        }
        $html = '';
        $html .= '<div class="'. $class .'" data-action="file-upload"><div class="multioption">';
        $html .= '<label>'. $label .'</label></div>';
        $html .= '<div class="wrap-custom-file leftdiv">';

        $html .= $form->field($model, $fieldName)->fileInput([
            'id' => $formField,
            'accept' => '.gif, .jpg, .png, .doc, .docx, .pdf',
        ])->label(false);

        if ($model->$originalField && (!$disableError && !$model->errors)) {
            $html .= '<label class="addtext1 uplaodenable addbgcolor" for="'.$formField.'">';
            $html .= '<span style="color:green;font-size:16px;line-height:40px;">Complete!</span></label>';
        } else {
            $html .= '<label class="addtext1" for="'. $formField .'"><span class="hidetext1">Upload</span></label>';
        }

        $html .= '</div>';

        $html .= '<div class="onefilecomplete1 onefilecomplete">';

        if ($model->$originalField && (!$disableError && !$model->errors)) {
            $html .= '<span>1 file uploaded</span><span style="padding-left:15px;color:red">X</span>';
        }

        if ($model->getFirstError($originalField)) {
            $html .= '<span class="help-block">' . $model->getFirstError($originalField) . '</span>';
        }

        $html .= '</div>';

        if ($model->$originalField && (!$disableError && !$model->errors)) {
            $file = self::getFile($model, $originalField);
            if ($file) {
                $html .= '<div class="text-center" style="width: 190px;"><span><a href="'. $file .'" target="_blank">View</a></span></div>';
            }
        }

        $html .= '</div>';

        return $html;
    }

    public static function fileInputMulti($form, $model, $fieldName, $options = [], $disableError = false)
    {
        $label = self::label($model, $fieldName);

        if (isset($options['class'])) {
            $class = $options['class'];
        } else {
            $class = 'col-md-6';
        }
        $formField = $model->formName() . '[' . $fieldName.  ']';
        $html = '';
        $html .= '<div class="'.$class.'" data-action="file-upload"><div class="multioption">';
        $html .= '<label>'. $label .'</label></div>';
        $html .= '<div class="wrap-custom-file leftdiv">';

        $html .= $form->field($model, $fieldName . '[]')->fileInput([
            'id' => $formField,
            'accept' => '.gif, .jpg, .png, .doc, .docx, .pdf',
            'multiple' => true
        ])->label(false);

        if ($model->$fieldName && (!$disableError && !$model->errors)) {
            $html .= '<label class="addtext1 uplaodenable addbgcolor" for="'.$formField.'">';
            $html .= '<span style="color:green;font-size:16px;line-height:40px;">Complete!</span></label>';
        } else {
            $html .= '<label class="addtext1" for="'. $formField .'"><span class="hidetext1">Upload</span></label>';
        }

        $html .= '</div>';
//        $html .= '<div class="leftdiv"> Or drag files here</div>';
        $html .= '<div class="onefilecomplete1 onefilecomplete">';

        if ($model->$fieldName && (!$disableError && !$model->errors)) {
            try {
                $counter = count(json_decode($model->$fieldName));
            } catch (\Exception $ex) {
                $counter = 1;
            }

            $html .= '<span>'. $counter .' file uploaded</span><span style="padding-left:15px;color:red">X</span>';
        }

        if ($model->getFirstError($fieldName)) {
            $html .= '<span class="help-block">' . $model->getFirstError($fieldName) . '</span>';
        }

        $html .= '</div>';

        if ($model->$fieldName && (!$disableError && !$model->errors)) {
            $html .= self::getFilesHtml($model, $fieldName);
        }

        $html .= '</div>';

        return $html;
    }
}
