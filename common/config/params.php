<?php
return [
    'adminEmail' => 'communication@herofutureenergies.com',
    'supportEmail' => 'communication@herofutureenergies.com',
    'senderEmail' => 'communication@herofutureenergies.com',
    'senderName' => 'communication mailer',
    'user.passwordResetTokenExpire' => 3600,

    "imgUploadPath" => "../../uploads/",
    "imgPath" => "http://localhost/hfe/uploads/",
];
