<?php

namespace common\models;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\S3;

class Utility {

    public function uploadFiles($file_name, $tmp_name, $sizes = "") {

        $bucket = Yii::$app->params['aws']['STATIC_S3_BUCKET'];
        $bucketUrl = Yii::$app->params['aws']['STATIC_S3_URL'] . $bucket;
        // S3 Connection
        $s3 = new S3(Yii::$app->params['aws']['AWS_ACCESS_KEY'], Yii::$app->params['aws']['AWS_SECRET_KEY']);
        $imageName = '';
        $pos = explode(".", $file_name);
        $imagename = $pos[0];
        $ext = end(explode(".", $file_name));
        $imageName = $this->fn_url2($imagename);
        if (!empty($imageName)) {
            $imageName = uniqid() . '_' . $imageName;
        } else {
            $imageName = uniqid();
        }
        $fileName = $imageName . '.' . $ext;
        $src = $tmp_name;
        $file_saved = move_uploaded_file($tmp_name, Yii::$app->params['aws']['UPLOAD_TEMP_FILE_PATH'] . $fileName);
        $sourcePath = Yii::$app->params['aws']['UPLOAD_TEMP_FILE_PATH'] . $fileName;
        $bucket = 'image';
        $date = date('Y/m/d');
        // $folder_direc = $bucket;
        $thumbImage = $imageName . '_thumb.' . $ext;
        // $sourceThumb = Yii::$app->params['aws']['UPLOAD_TEMP_FILE_PATH'] . $thumbImage;
        $file_size_name = $imageName . '.' . $ext;
        // $this->resize(200, $sourcePath, $sourceThumb);
        // $s3->putObjectFile($sourceThumb, $bucketUrl, $bucket . $thumbImage, S3::ACL_PUBLIC_READ);
        $file_name_val = $bucket . $thumbImage;

        //size of 72 image starts
        // $thumbImage72 = $imageName . '_72.' . $ext;
        // $sourceThumb72 = Yii::$app->params['aws']['UPLOAD_TEMP_FILE_PATH'] . $thumbImage72;
        // $file_size_name = $imageName . '.' . $ext;
        // $this->resize(72, $sourcePath, $sourceThumb72);
        // $s3->putObjectFile($sourceThumb72, $bucketUrl, $bucket . $thumbImage72, S3::ACL_PUBLIC_READ);
        //size of 72 image ends
        // $sizes = [200,72];
        $sizes = Yii::$app->params['user_image_sizes'];
        foreach ($sizes as  $size) {
          if($size == 200){
              $thumbImage = $imageName . '_thumb.' . $ext;
          }else {
            $thumbImage = $imageName . '_'.$size.'.' . $ext;
          }
          $sourceThumb = Yii::$app->params['aws']['UPLOAD_TEMP_FILE_PATH'] . $thumbImage;
          $this->resize($size, $sourcePath, $sourceThumb);
          $s3->putObjectFile($sourceThumb, $bucketUrl, $bucket . $thumbImage, S3::ACL_PUBLIC_READ);
          unlink($sourceThumb);
        }
        //putting in array
        unlink($sourcePath);

        return $file_name_val;
    }

    function resize($newWidth, $originalFile, $targetFile) {

        $info = getimagesize($originalFile);

        list($width, $height) = getimagesize($originalFile);
        $mime = $info['mime'];
        switch (
        $mime) {
            case 'image/jpeg':
                $image_create_func = 'imagecreatefromjpeg';
                $image_save_func = 'imagejpeg';
                $new_image_ext = 'jpg';
                break;

            case 'image/png':
                $image_create_func = 'imagecreatefrompng';
                $image_save_func = 'imagepng';
                $new_image_ext = 'png';
                break;

            case 'image/gif':
                $image_create_func = 'imagecreatefromgif';
                $image_save_func = 'imagegif';
                $new_image_ext = 'gif';
                break;

            default:
                throw Exception('Unknown image type.');
        }

        $img = $image_create_func($originalFile);
        if ($newWidth <= $width) {
            $newHeight = ($height / $width ) * $newWidth;
        } else {
            $newWidth = $width;
            $newHeight = $height;
        }
        $tmp_img = imagecreatetruecolor($newWidth, $newHeight);
        imagealphablending($tmp_img, false);
        imagesavealpha($tmp_img, true);
        // copy and resize old image into new image
        imagecopyresampled($tmp_img, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        if ($image_save_func == 'imagejpeg') {
            $thumbFinal = $image_save_func($tmp_img, $targetFile, 100);
        } else {
            $thumbFinal = $image_save_func($tmp_img, $targetFile);
        }
    }

    public function DBFetchAll($sql, $params = array()) {
        $command = \Yii::$app->db->createCommand($sql);

        if (!empty($params)) {
            $dataArray = $command->queryAll($params);
        } else {
            $dataArray = $command->queryAll();
        }

        return $dataArray;
    }

    function fn_url2($title) {
        $title = str_replace("%", " percentage ", $title);
        $title = str_replace("$", " dollar ", $title);
        $url = preg_replace('/[^a-zA-Z0-9-]/', '-', ltrim(rtrim($title)));
        $url = preg_replace('/[-]+/', '-', $url);
        if (!empty($url)) {
            $url = strtolower($url);
        }
        $url = preg_replace('/[-]+/', '-', $url);
        return $url;
    }

    public function DBExecute($sql, $params = array()) {
        $command = \Yii::$app->db->createCommand($sql);
        $dataArray = $command->execute();
    }

    public function ellipsis($text, $max = 100, $append = '&hellip;') {
        if (strlen($text) <= $max)
            return $text;
        $out = substr($text, 0, $max - 3);
        if (strpos($text, ' ') === FALSE)
            return (rtrim($out) . $append);
        return preg_replace('/\w+$/', '', rtrim($out)) . $append;
    }

    public function fn_urlid($title) {
        $str = preg_replace('/[^a-zA-Z0-9-]/', '-', trim($title));
        if (!empty($title)) {
            $str = strtolower($str);
        }
        $str = preg_replace('/[-]+/', '-', $str);
        return $str;
    }

    public function fn_url_id($id, $title) {
        $str = preg_replace('/[^a-zA-Z0-9-]/', '-', trim($title));
        if (!empty($title) && !empty($id)) {
            $str = strtolower($str) . '-' . $id;
        } elseif (!empty($title)) {
            $str = strtolower($str);
        } else {
            $str = $id;
        }
        $str = preg_replace('/[-]+/', '-', $str);
        return $str;
    }

    public function getDateTime() {
        return date('Y-m-d H:i:s');
    }

    public function getDateFormat($date) {
        return date('Y-m-d', strtotime($date));
    }

// for date format
    public function getDateFormatForList($date) {
        if (!empty($date)) {
            $dateFomat = date('d M Y', strtotime($date));
            return $dateFomat;
        }
    }

// for date time format
    public function getDateTimeFormatForList($dateTime) {
        if (!empty($dateTime)) {
            $dateFomat = date('d M Y H:i:s', strtotime($dateTime));
            return $dateFomat;
        }
    }

//    function to validate the search text and to encode the text of input
    public function validateSearchKeywords($keyword) {
        $valid_keyword = "";
        if (!empty($keyword)) {
            $valid_keyword = htmlspecialchars($keyword);
        }
        return $valid_keyword;
    }

//    for set values in cookies
    public function setCookiesValues($cookieName, $cookieValues) {
        if (!empty($cookieName) && !empty($cookieValues)) {
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => $cookieName,
                'value' => $cookieValues,
            ]));
        }
    }

//    for get values from cookies
    public function getCookiesValues($cookieName) {
        if (!empty($cookieName)) {
            $cookies = Yii::$app->request->cookies;
            // Check the availability of the cookie
            if ($cookies->has($cookieName)) {
                return $cookies->getValue($cookieName);
            }
        }
    }

//    for remove cookies values
    public function removeCookiesValues($cookieName) {
        if (!empty($cookieName)) {
            $cookies = Yii::$app->response->cookies;
            $cookies->remove($cookieName);
            unset($cookies[$cookieName]);
        }
    }

//    for encrypt app key
    public function encryptAppKey($appKey) {
        if (!empty($appKey)) {
            $appKeyLimit = strlen($appKey);
            $encryptKey = str_repeat('*', ($appKeyLimit - 4)) . substr($appKey, ($appKeyLimit - 4), 4);
            return $encryptKey;
        }
    }

//    checking the image is valid or not
    public function isImage($url) {
        $is = @getimagesize($url);
        if ($is) {
            return true;
        } else {
            return false;
        }
    }

    public function updateSequence($tableName, $sequenceVal, $fieldName, $fieldId, $id) {
        //  echo $tableName;echo $sequenceVal;echo $fieldName;echo $fieldId; die;
        $sql = "update " . $tableName . " set " . $fieldName . " = :sequence
                 where " . $fieldId . " = :id";
        $command = \Yii::$app->db->createCommand($sql);
        $command->bindValue(':sequence', $sequenceVal);
        $command->bindValue(':id', $id);
        return $command->execute();

    }


}
