<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "org_code_sap".
 *
 * @property int $id
 * @property int $org_code
 * @property string $org_name
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class OrgCodeSap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'org_code_sap';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['org_code', 'org_name', 'status'], 'required'],
            [['org_code', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'org_code' => 'Org Code',
            'org_name' => 'Org Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        return ArrayHelper::index($query->all(), 'id');
    }
}
