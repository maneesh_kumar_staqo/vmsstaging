<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\VendorTypes;

/**
 * This is the model class for table "organisation_types".
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $status
 * @property int $type
 * @property string $created_at
 * @property string $updated_at
 */
class OrganisationTypes extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public static $statusArray = [
        1 => 'Active',
        2 => 'In Active'
    ];

    public static $typeArray = [
        1 => 'Display Owners',
        2 => 'Display Directors',
        3 => '​Name of Proprietor',
        4 => '​Name of Partners​',
        5 => '​Name of Karta',
        6 => '​Name of Person'
    ];

    public static $ownerTypeArray = [
        1 => 'Get Shareholder Details',
        2 => 'Get Ownership Details',
    ];

    const OWNER_TYPE_SHAREHOLDERS = 1;
    const OWNER_TYPE_OWNERSHIPS = 2;

    const TYPE_OWNER = 1;
    const TYPE_DIRECTOR = 2;
    const TYPE_PROPRIETOR = 3;
    const TYPE_PARTNER = 4;
    const TYPE_KARTA = 5;
    const TYPE_PERSON = 6;

    const ID_LIMITED_COMPANY = 1;
    const ID_PRIVATE_LIMITED_COMPANY = 2;
    const ID_TRUST = 3;
    const ID_OPC = 4;
    const ID_JV = 6;
    const ID_INDIVIDUAL = 7;
    const ID_PROPRIETOR_FIRM = 8;
    const ID_PARTNERSHIP_FIRM = 9;
    const ID_LIMILED_LIABILITY_PARTNERSHIP = 10;
    const ID_HUF = 11;
    const ID_OTHERS = 12;
    

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organisation_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status', 'type'], 'required'],
            [['priority', 'status', 'type', 'owners_type'], 'integer'],
            [['is_domestic', 'is_overseas'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'type' => 'Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        $query->andWhere(['status' => self::STATUS_ACTIVE]);
        $query->orderBy(['priority' => SORT_DESC]);

        return ArrayHelper::index($query->all(), 'id');
    }

    public static function isDomestic($vendor)
    {
        $result = false;

        if (!empty($vendor->vendor_type) && $vendor->vendor_type == VendorTypes::IS_PARENT_DOMESTIC) {
            $result = true;
        }

        return $result;
    }

    public static function isOverseas($vendor)
    {
        $result = false;

        if (!empty($vendor->vendor_type) && $vendor->vendor_type == VendorTypes::IS_PARENT_OVERSEAS) {
            $result = true;
        }

        return $result;
    }
}
