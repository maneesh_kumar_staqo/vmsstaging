<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class Country extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public static $statusArray = [
        1 => 'Active',
        2 => 'In Active'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'title', 'status', 'country_code'], 'required'],
            [['id', 'priority', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['country_code'], 'string', 'max' => 15],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Name',
            'priority' => 'Priority',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        $query->andWhere(['status' => self::STATUS_ACTIVE]);
        $query->orderBy(['title' => SORT_ASC]);

        return ArrayHelper::index($query->all(), 'id');
    }
}
