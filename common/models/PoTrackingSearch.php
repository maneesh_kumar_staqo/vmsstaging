<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PoTracking;

/**
 * PoTrackingSearch represents the model behind the search form of `common\models\PoTracking`.
 */
class PoTrackingSearch extends PoTracking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fk_vendor', 'status', 'created_by'], 'integer'],
            [['po_number', 'project_title', 'description', 'date_of_issue', 'document', 'created_at'], 'safe'],
            [['amount', 'tax', 'total_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PoTracking::find()->joinWith(['vendor']);

        $subQuery = '(SELECT COUNT(*) FROM po_invoice WHERE fk_po_tracking = po_tracking.id) AS total_invoice';
        $query->select(['po_tracking.*', $subQuery]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fk_vendor' => $this->fk_vendor,
            'date_of_issue' => $this->date_of_issue,
            'amount' => $this->amount,
            'tax' => $this->tax,
            'total_amount' => $this->total_amount,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
        ]);
        $query->asArray();

        $query->andFilterWhere(['like', 'po_number', $this->po_number])
            ->andFilterWhere(['like', 'project_title', $this->project_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'document', $this->document]);

        return $dataProvider;
    }
}
