<?php

namespace common\models;

use Yii;
use common\components\UploadComponent;

/**
 * This is the model class for table "bank_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property string $pb_account_holder_name
 * @property string $pb_account_number
 * @property string $pb_account_type
 * @property string $pb_bank_name
 * @property string $pb_branch
 * @property string $pb_city
 * @property string $pb_micr_number
 * @property string $pb_ifsc_code
 * @property string $pb_documents
 * @property string $sb_account_holder_name
 * @property string $sb_account_number
 * @property string $sb_account_type
 * @property string $sb_bank_name
 * @property string $sb_branch
 * @property string $sb_city
 * @property string $sb_micr_number
 * @property string $sb_ifsc_code
 * @property string $sb_documents
 * @property string $created_at
 * @property string $updated_at
 */
class BankInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user'], 'required'],
            [['fk_user', 'status'], 'integer'],
            [['created_at', 'updated_at','sb_documents'], 'safe'],
            [['pb_account_holder_name', 'pb_bank_name', 'pb_branch', 'sb_account_holder_name', 'sb_bank_name', 'sb_branch'], 'string', 'max' => 256],
            [['pb_account_number', 'pb_account_type', 'pb_ifsc_code', 'sb_account_number', 'sb_account_type', 'sb_ifsc_code'], 'string', 'max' => 50],
            [['pb_city', 'sb_city'], 'string', 'max' => 100],

            [['pb_ifsc_code', 'pb_ifsc_code'], 'string', 'max' => 11],
            [['sb_micr_number', 'pb_micr_number'], 'string', 'max' => 9],
            [['sb_micr_number', 'pb_micr_number'], 'integer'],
            [['pb_documents'], 'string', 'max' => 512],

            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',
            'pb_account_holder_name' => 'Account Holder Name',
            'pb_account_number' => 'Account Number',
            'pb_account_type' => 'Account Type',
            'pb_bank_name' => 'Bank Name',
            'pb_branch' => 'Branch',
            'pb_city' => 'City',
            'pb_micr_number' => 'MICR No',
            'pb_ifsc_code' => 'IFSC/NEFT Code of Bank',
            'pb_documents' => 'Upload Bank Details (i.e. Cancelled Cheque, Supporting Certificate, etc)',
            'sb_account_holder_name' => 'Account Holder Name (optional)',
            'sb_account_number' => 'Account Number (optional)',
            'sb_account_type' => 'Account Type (optional)',
            'sb_bank_name' => 'Bank Name (optional)',
            'sb_branch' => 'Branch (optional)',
            'sb_city' => 'City (optional)',
            'sb_micr_number' => 'MICR No (optional)',
            'sb_ifsc_code' => 'IFSC/NEFT Code of Bank (optional)',
            'sb_documents' => 'Upload Bank Details (i.e. Cancelled Cheque, Supporting Certificate, etc) (optional)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public function saveModel($isLandOwner)
    {
        $result = true;
        $this->status = User::APP_STATUS_PENDING;
        if (!$isLandOwner) {
            if (!$this->pb_account_holder_name) {
                $result = false;
                $this->addError('pb_account_holder_name', 'This is required');
            }
            if (!$this->pb_account_number) {
                $result = false;
                $this->addError('pb_account_number', 'This is required');
            }
            if (!$this->pb_account_type) {
                $result = false;
                $this->addError('pb_account_type', 'This is required');
            }
            if (!$this->pb_bank_name) {
                $result = false;
                $this->addError('pb_bank_name', 'This is required');
            }
            if (!$this->pb_branch) {
                $result = false;
                $this->addError('pb_branch', 'This is required');
            }
            if (!$this->pb_city) {
                $result = false;
                $this->addError('pb_city', 'This is required');
            }
            if (!$this->pb_micr_number) {
                $result = false;
                $this->addError('pb_micr_number', 'This is required');
            }
            if (!$this->pb_ifsc_code) {
                $result = false;
                $this->addError('pb_ifsc_code', 'This is required');
            }            
        }

        if ($result && $this->save()) {
            $result = true;
        }

        return $result;
    }

    public function saveImages($isLandOwner)
    {
        $result = true;

        $pdDoc = UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['BANK_FILES'], 'pb_documents');
        $sbDoc = UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['BANK_FILES'], 'sb_documents');

        if (!$pdDoc) {
            if (!$isLandOwner) {
                $result = false;
                $this->addError('pb_documents', 'This is required');
            }else{
            $this->pb_documents = '';
            }
        }

        if (!$sbDoc) {
            $this->sb_documents = '';
        }
        
        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
