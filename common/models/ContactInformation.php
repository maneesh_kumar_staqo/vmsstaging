<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property string $b_name
 * @property string $b_designation
 * @property string $b_land_line_number
 * @property string $b_mobile_number
 * @property string $b_email
 * @property string $f_name
 * @property string $f_designation
 * @property string $f_land_line_number
 * @property string $f_mobile_number
 * @property string $f_email
 * @property string $created_at
 * @property string $updated_at
 */
class ContactInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user'], 'required'],
            [['fk_user', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['b_name', 'f_name'], 'string', 'max' => 256],
            [['b_designation', 'b_email', 'f_designation', 'f_email'], 'string', 'max' => 100],
            [['b_land_line_number', 'f_land_line_number'], 'string', 'max' => 15, 'min' => 10],
            [['b_mobile_number', 'f_mobile_number'], 'string', 'max' => 15, 'min' => 10],
            [['b_mobile_number', 'f_mobile_number', 'b_land_line_number', 'f_land_line_number'], 'number'],
            [['b_email', 'f_email'], 'email'],
            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',
            'b_name' => 'Name',
            'b_designation' => 'Designation',
            'b_land_line_number' => 'Land Line Number (optional)',
            'b_mobile_number' => 'Mobile Number',
            'b_email' => 'Email',
            'f_name' => 'Name',
            'f_designation' => 'Designation',
            'f_land_line_number' => 'Land Line Number (optional)',
            'f_mobile_number' => 'Mobile Number',
            'f_email' => 'Email',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public function saveModel($isLandOwner, $isGovernment)
    {
        $result = true;
        $this->status = User::APP_STATUS_PENDING;
        if (!$isLandOwner && !$isGovernment) {
            if (!$this->b_name) {
                $result = false;
                $this->addError('b_name', 'This is required');
            }
            if (!$this->b_designation) {
                $result = false;
                $this->addError('b_designation', 'This is required');
            }
            if (!$this->b_mobile_number) {
                $result = false;
                $this->addError('b_mobile_number', 'This is required');
            }
            if (!$this->b_email) {
                $result = false;
                $this->addError('b_email', 'This is required');
            }
            if (!$this->f_name) {
                $result = false;
                $this->addError('f_name', 'This is required');
            }
            if (!$this->f_designation) {
                $result = false;
                $this->addError('f_designation', 'This is required');
            }
            if (!$this->f_mobile_number) {
                $result = false;
                $this->addError('f_mobile_number', 'This is required');
            }
            if (!$this->f_email) {
                $result = false;
                $this->addError('f_email', 'This is required');
            }            
        }

        if ($result && $this->save()) {
            $result = true;
        }

        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
