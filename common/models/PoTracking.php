<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\components\UploadComponent;

/**
 * This is the model class for table "po_tracking".
 *
 * @property int $id
 * @property int $fk_vendor
 * @property string $po_number
 * @property string $project_title
 * @property string $description
 * @property string $date_of_issue
 * @property string $amount
 * @property string $tax
 * @property string $total_amount
 * @property string $document
 * @property int $status
 * @property int $created_by
 * @property string $created_at
 */
class PoTracking extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'po_tracking';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_vendor', 'po_number', 'project_title', 'date_of_issue', 'amount', 'total_amount'], 'required'],
            [['fk_vendor', 'status', 'created_by'], 'integer'],
            [['description'], 'string'],
            [['date_of_issue', 'created_at'], 'safe'],
            [['amount', 'tax', 'cgst', 'sgst', 'igst', 'total_amount'], 'number'],
            [['po_number'], 'string', 'min' => 10,'max' => 256],
            [['project_title', 'document'], 'string', 'max' => 512],
            [['amount'], 'validateGst']
        ];
    }

    public function validateGst()
    {
        $result = true;
        if (!$this->igst && !$this->sgst && !$this->cgst) {
            $this->addError('amount', 'Please add tax information');
            $result = false;
        } elseif (!$this->igst && (!$this->sgst || !$this->cgst)) {
            if (!$this->sgst) {
                $this->addError('sgst', 'SGST is required with CGST');
                $result = false;
            }
            if (!$this->cgst) {
                $this->addError('cgst', 'CGST is required with SGST');
                $result = false;
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor' => 'Vendor',
            'po_number' => 'PO Number',
            'project_title' => 'Project Title',
            'description' => 'Remark',
            'date_of_issue' => 'Date Of Issue',
            'amount' => 'Amount',
            'tax' => 'Tax',
            'total_amount' => 'Total Amount',
            'document' => 'Document',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'cgst' => 'CGST',
            'sgst' => 'SGST',
            'igst' => 'IGST',
        ];
    }

    public function getVendor()
    {
        return $this->hasOne(User::className(), ['id' => 'fk_vendor']);
    }

    public static function getAll()
    {
        $query = User::find();
        $query->andWhere(['status' => User::STATUS_ACTIVE]);
        $query->andWhere(['application_status' => User::APP_STATUS_APPROVE]);
        $query->asArray();

        return ArrayHelper::map($query->all(), 'id', 'email');
    }

    public function saveImages()
    {
        $result = true;

        UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['PO_TRACKING'], 'document');

        return $result;
    }
}
