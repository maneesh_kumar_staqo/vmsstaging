<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "address_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property string $ra_house_number
 * @property string $ra_street_name
 * @property string $ra_city
 * @property string $ra_state
 * @property string $ra_post_office
 * @property string $ra_telephone_number
 * @property string $ra_fax_number
 * @property string $ra_country
 * @property string $ra_pin_code
 * @property string $ca_house_number
 * @property string $ca_street_name
 * @property string $ca_city
 * @property string $ca_state
 * @property string $ca_post_office
 * @property string $ca_telephone_number
 * @property string $ca_fax_number
 * @property string $ca_country
 * @property string $ca_pin_code
 * @property string $created_at
 * @property string $update_at
 */
class AddressInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user'], 'required'],
            [['fk_user', 'status'], 'integer'],
            [['created_at', 'update_at'], 'safe'],
            [['ra_house_number', 'ra_street_name', 'ra_city', 'ra_state', 'ra_post_office', 'ra_country', 'ca_house_number', 'ca_street_name', 'ca_city', 'ca_state', 'ca_post_office', 'ca_country'], 'string', 'max' => 100],
            [['ra_telephone_number', 'ca_telephone_number'], 'string', 'max' => 15, 'min' => 10],
            [['ra_fax_number', 'ca_fax_number'], 'string', 'max' => 15, 'min' => 10],
            [['ra_pin_code', 'ca_pin_code'], 'string', 'max' => 20, 'min' => 3],
            [['ra_pin_code', 'ca_pin_code', 'ra_telephone_number', 'ca_telephone_number', 'ra_fax_number', 'ca_fax_number'], 'number'],

            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',
            'ra_house_number' => 'House Number',
            'ra_street_name' => 'Street Name',
            'ra_city' => 'City',
            'ra_state' => 'State',
            'ra_post_office' => 'Post Office',
            'ra_telephone_number' => 'Telephone Number',
            'ra_fax_number' => 'Fax Number (optional)',
            'ra_country' => 'Country',
            'ra_pin_code' => 'Pin Code',
            'ca_house_number' => 'House Number',
            'ca_street_name' => 'Street Name',
            'ca_city' => 'City',
            'ca_state' => 'State',
            'ca_post_office' => 'Post Office',
            'ca_telephone_number' => 'Telephone Number',
            'ca_fax_number' => 'Fax Number (optional)',
            'ca_country' => 'Country',
            'ca_pin_code' => 'Pin Code',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public function saveModel($isLandOwner, $isGovernment)
    {
        $result = true;
        $this->status = User::APP_STATUS_PENDING;
        if (!$isLandOwner && !$isGovernment) {
            if (!$this->ra_house_number) {
                $result = false;
                $this->addError('ra_house_number', 'This is required');
            }
            if (!$this->ra_street_name) {
                $result = false;
                $this->addError('ra_street_name', 'This is required');
            }
            if (!$this->ra_city) {
                $result = false;
                $this->addError('ra_city', 'This is required');
            }
            if (!$this->ra_state) {
                $result = false;
                $this->addError('ra_state', 'This is required');
            }
            if (!$this->ra_post_office) {
                $result = false;
                $this->addError('ra_post_office', 'This is required');
            }
            if (!$this->ra_telephone_number) {
                $result = false;
                $this->addError('ra_telephone_number', 'This is required');
            }
            if (!$this->ra_country) {
                $result = false;
                $this->addError('ra_country', 'This is required');
            }
            if (!$this->ra_pin_code) {
                $result = false;
                $this->addError('ra_pin_code', 'This is required');
            } 
            if (!$this->ca_house_number) {
                $result = false;
                $this->addError('ca_house_number', 'This is required');
            }
            if (!$this->ca_street_name) {
                $result = false;
                $this->addError('ca_street_name', 'This is required');
            }
            if (!$this->ca_city) {
                $result = false;
                $this->addError('ca_city', 'This is required');
            }
            if (!$this->ca_state) {
                $result = false;
                $this->addError('ca_state', 'This is required');
            }
            if (!$this->ca_post_office) {
                $result = false;
                $this->addError('ca_post_office', 'This is required');
            }
            if (!$this->ca_telephone_number) {
                $result = false;
                $this->addError('ca_telephone_number', 'This is required');
            }
            if (!$this->ca_country) {
                $result = false;
                $this->addError('ca_country', 'This is required');
            }
            if (!$this->ca_pin_code) {
                $result = false;
                $this->addError('ca_pin_code', 'This is required');
            }           
        }

        if ($result && $this->save()) {
            $result = true;
        }

        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
