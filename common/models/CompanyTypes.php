<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_types".
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $status
 * @property int $display_other_field
 * @property string $created_at
 * @property string $updated_at
 */
class CompanyTypes extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    const ID_GOVERNMENT = 11;
    const ID_LANDOWNER = 12;

    public static $statusArray = [
        1 => 'Active',
        2 => 'In Active'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['priority', 'status', 'display_other_field', 'display_trascco'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'display_other_field' => 'Display Other Field',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        $query->andWhere(['status' => self::STATUS_ACTIVE]);
        $query->orderBy(['priority' => SORT_DESC]);

        return ArrayHelper::index($query->all(), 'id');
    }

    public static function isGovernment($vendor)
    {
        $result = false;
        if (!empty($vendor->business_type)) {
            $types = json_decode($vendor->business_type);
            if (in_array(self::ID_GOVERNMENT, $types)) {
                $result = true;
            }
        }

        return $result;
    }

    public static function isLandowner($vendor)
    {
        $result = false;
        if (!empty($vendor->business_type)) {
            $types = json_decode($vendor->business_type);
            if (in_array(self::ID_LANDOWNER, $types)) {
                $result = true;
            }
        }

        return $result;
    }
}
