<?php

namespace common\models;

use Yii;
use common\components\UploadComponent;

/**
 * This is the model class for table "accreditation_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property string $accreditation_details
 * @property string $documents
 * @property string $created_at
 * @property string $updated_at
 */
class AccreditationInformation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accreditation_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user'], 'required'],
            [['fk_user', 'status'], 'integer'],
            [['accreditation_details'], 'string'],
            [['created_at', 'updated_at', 'documents'], 'safe'],

            [['document_one'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_two'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_three'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_four'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_five'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_six'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_seven'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_eight'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_nine'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],
            [['document_ten'], 'file', 'maxFiles' => 1, 'maxSize' => 1024 * 1024 * 5],

            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',
            'accreditation_details' => 'Other Accreditation Details (optional)',
            'documents' => 'Others (optional)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            'document_one' => 'ISO 9000 - Quality Management Certification',
            'document_two' => 'ISO 14000 - Environmental Management Certification',
            'document_three' => 'TS 16949 - Automotive Industry Certification',
            'document_four' => 'BIS - Bureau of Indian Standards Certification',
            'document_five' => 'OHSAS 1800 - Occupational Health and Safety Management Certification',
            'document_six' => 'IEC - International Electrotechnical Commission Certification',
            'document_seven' => 'Dun & Bradstreet - Business Credit Profile',
            'document_eight' => 'Fitch / Moody\'s/ Standard & Poor\'s -  Credit Rating',            
            'document_nine' => 'Last 3 Years Balance Sheet',
            'document_ten' => 'Last 3 Years IT returns'
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public function saveImages()
    {
        $result = true;

        $documents = UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'documents');

        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_one');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_two');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_three');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_four');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_five');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_six');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_seven');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_eight');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_nine');
        UploadComponent::uploadMultiModelFile($this, UploadComponent::$fileLocations['ACCREDITATION_FILES'], 'document_ten');

        if (!$documents) {
            $this->documents = '';
        }

        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
