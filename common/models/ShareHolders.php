<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "share_holders".
 *
 * @property int $id
 * @property int $fk_vendor_information
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 * @property string $updated_at
 */
class ShareHolders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'share_holders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['fk_vendor_information'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor_information' => 'Fk Vendor Information',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll($idVendor)
    {
        $query = self::find();
        $query->andWhere(['fk_vendor_information' => $idVendor]);

        return $query->all();
    }
}
