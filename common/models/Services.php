<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "services".
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $status
 * @property string $upper_turnover
 * @property string $lower_turnover
 * @property string $created_at
 * @property string $updated_at
 */
class Services extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    public static $statusArray = [
        1 => 'Active',
        2 => 'In Active'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status', 'upper_turnover', 'lower_turnover'], 'required'],
            [['priority', 'status', 'display_other_field'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'upper_turnover', 'lower_turnover'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'upper_turnover' => 'Upper Turnover',
            'lower_turnover' => 'Lower Turnover',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        $query->andWhere(['status' => self::STATUS_ACTIVE]);
        $query->orderBy(['priority' => SORT_DESC]);

        return ArrayHelper::index($query->all(), 'id');
    }

    public static function getAllInternal()
    {
        $query = self::find();

        $query->orderBy(['priority' => SORT_DESC]);

        return ArrayHelper::index($query->all(), 'id');
    }
}
