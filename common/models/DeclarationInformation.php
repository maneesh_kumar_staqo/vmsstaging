<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "declaration_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property int $relevant_jurisdiction_check
 * @property int $offence_of_corruption_or_bribery_check
 * @property int $human_trafficking_check
 * @property int $terrorist_financing_check
 * @property int $tax_evasion_check
 * @property string $created_at
 * @property string $updated_at
 */
class DeclarationInformation extends \yii\db\ActiveRecord
{
    public static $optionArray = [
        1 => 'Yes',
        0 => 'No',
    ];
    
    const SELECTION_OPTION = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'declaration_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user', 'relevant_jurisdiction_check', 'offence_of_corruption_or_bribery_check', 'human_trafficking_check', 'terrorist_financing_check', 'tax_evasion_check'], 'required'],
            [['fk_user', 'status', 'relevant_jurisdiction_check', 'offence_of_corruption_or_bribery_check', 'human_trafficking_check', 'terrorist_financing_check', 'tax_evasion_check'], 'integer'],
//            [['relevant_jurisdiction_check', 'offence_of_corruption_or_bribery_check', 'human_trafficking_check', 'terrorist_financing_check', 'tax_evasion_check'], 'number', 'min' => 1, 'max' => 1, 'message' => '{label} must be yes'],
            [['created_at', 'updated_at'], 'safe'],

            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'ID User',
            'status' => 'Status',
            'relevant_jurisdiction_check' => 'Relevant Jurisdiction Check',
            'offence_of_corruption_or_bribery_check' => 'Offence Of Corruption Or Bribery Check',
            'human_trafficking_check' => 'Human Trafficking Check',
            'terrorist_financing_check' => 'Terrorist Financing Check',
            'tax_evasion_check' => 'Tax Evasion Check',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
