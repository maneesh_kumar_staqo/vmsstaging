<?php

namespace common\models;

use Yii;
use common\models\OrganisationTypes;
use common\components\UploadComponent;
use common\forms\ShareHolderForm;
use common\models\RegistrationVendorInfo;
use common\forms\DirectorForm;
use common\forms\OwnerForm;
use common\forms\PartnerForm;
use common\forms\ProprietorForm;
use common\forms\PersonForm;
use common\forms\KartaForm;

/**
 * This is the model class for table "vendor_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property string $company_name
 * @property int $vendor_type
 * @property int $organization_type
 * @property string $business_type
 * @property string $company_image
 * @property int $is_msme_act_2006
 * @property string $ssi_number
 * @property string $ssi_certificate
 * @property string $registration_number
 * @property string $registration_document
 * @property string $company_pan_number
 * @property string $company_pan_document
 * @property string $created_at
 * @property string $updated_at
 */
class VendorInformation extends \yii\db\ActiveRecord
{
    public static $optionArray = [
        1 => 'Yes',
        0 => 'No',
    ];
    
    const SELECTION_OPTION = 0;

    public $shareHolders;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendor_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user', 'vendor_type', 'organization_type', 'business_type', 'is_msme_act_2006'], 'required'],
            [['fk_user', 'status', 'vendor_type', 'organization_type', 'is_msme_act_2006'], 'integer'],
            [['created_at', 'updated_at', 'other_business'], 'safe'],
            [['shareHolders', 'business_type'], 'safe'],
            [['company_name', 'ssi_number'], 'string', 'max' => 256],
            [['trasco_name', 'trasco_document'], 'string', 'max' => 256],
            [['company_image', 'ssi_certificate', 'registration_document', 'company_pan_document'], 'string', 'max' => 512],
            [['registration_number'], 'string', 'max' => 100],
            [['registration_number', 'company_pan_number'], 'trim'],
            [['company_pan_number'], 'string', 'max' => 10],
            [['adhaar_number'], 'string', 'max' => 12],
            [['status_remark'], 'string', 'max' => 256],
            [['status', 'adhaar_document'], 'safe'],
        ];
    }

    public function validateCIN()
    {
        $result = true;
        if ($this->registration_number) {
            $start = substr($this->registration_number, 0, 1);

            if (!in_array($start, ['u', 'l', 'L', 'U'])) {
                $this->addError('registration_number', 'CIN/Registration No. should be maximum 21 characters alphanumeric value and start with U or L');
                $result = false;
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',
            'company_name' => 'Company Name',
            'vendor_type' => 'Vendor Type',
            'organization_type' => 'Organization Type',
            'business_type' => 'Type of Business',
            'other_business' => 'Other Business',
            'trasco_name' => 'Name of TRASCO',
            'trasco_document' => 'Upload Documents',
            'adhaar_number' => 'Adhar Number',
            'adhaar_document' => 'Adhar Document',

            'company_image' => 'Upload Catalogue/Company Profile',

            'is_msme_act_2006' => 'Is Msme Act 2006',

            'ssi_number' => 'SSI Number',
            'ssi_certificate' => 'Upload Certificate',

            'registration_number' => 'CIN/Registration No.',
            'registration_document' => 'Upload CIN /Registration Document',

            'company_pan_number' => 'PAN Number',
            'company_pan_document' => 'PAN Document',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getVendorType()
    {
        return $this->hasOne(VendorTypes::className(), ['id' => 'vendor_type']);
    }

    /**
     * @NOTE SHOULD BE DEPRECATED AND USE getOrganisationType
     *
     * @return type
     */
    public function getCompanyType()
    {
        return $this->hasOne(OrganisationTypes::className(), ['id' => 'organization_type']);
    }

    public function getOrganisationType()
    {
        return $this->hasOne(OrganisationTypes::className(), ['id' => 'organization_type']);
    }

    public function getBusinessType()
    {
        return $this->hasOne(CompanyTypes::className(), ['id' => 'company_types']);
    }

    public function getShareholders()
    {
        return $this->hasMany(ShareHolders::className(), ['fk_vendor_information' => 'id']);
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
            $infoModel = RegistrationVendorInfo::findOne(['fk_user' => $idUser]);
            if (!empty($infoModel->company_name)) {
                $model->company_name = $infoModel->company_name;
            }
        }

        return $model;
    }

    protected function saveImages($isDomestic, $isGovernment, $isLandOwner, $formType = false)
    {
        $result = true;

        $isOverseas = OrganisationTypes::isOverseas($this);

        $companyImage =     UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'company_image');
        $transcoImage =     UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'trasco_document');
        $ssiImage =         UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'ssi_certificate');
        $registrationDoc =  UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'registration_document');
        $adhar =  UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['VENDOR_FILES'], 'adhaar_document');

       	if ($this->is_msme_act_2006 && !$ssiImage) {
            $result = false;
            $this->addError('ssi_certificate', 'This is required');
        }  

        if ($isDomestic && !$adhar && $this->organization_type == OrganisationTypes::ID_INDIVIDUAL) {
            $result = false;
            $this->addError('adhaar_document', 'This is required');
        }

        if ($isDomestic && !$adhar && $this->organization_type == OrganisationTypes::ID_HUF) {
            $result = false;
            $this->addError('adhaar_document', 'This is required');
        }

        if ($formType == OrganisationTypes::TYPE_DIRECTOR && !$registrationDoc && !$isGovernment && !$isLandOwner) {
            if ($this->organization_type != OrganisationTypes::ID_OTHERS || !$isOverseas) {
                $result = false;
                $this->addError('registration_document', 'This is required');
            }
        }

        return $result;
    }

    public function saveModel($organisations, $directorForm, $ownerForm, $partnerForm, $kartaForm, $proprietorForm, $personForm, $vendors)
    {
        $result = false;
//        $isDomestic = true;
        $isGovernment = $isLandOwner = false;
        $type = OrganisationTypes::TYPE_OWNER;
        $shareModel = new ShareHolderForm();
        $this->status = User::APP_STATUS_PENDING;

        $isDomestic = OrganisationTypes::isDomestic($this);
        $isOverseas = OrganisationTypes::isOverseas($this);

        if (!empty($organisations[$this->organization_type])) {
//            if ($organisations[$this->organization_type]['is_domestic']) {
//                $isDomestic = true;
//            } else {
//                $isDomestic = false;
//            }

            if (CompanyTypes::isLandowner($this->attributes)) {
                $isLandOwner = true;
            }
            if (CompanyTypes::isGovernment($this->attributes)) {
                $isGovernment = true;
            }
        }

        if ($this->business_type) {
            $this->business_type = json_encode($this->business_type);
        }

        if ($this->organization_type && !empty($organisations[$this->organization_type])) {
            $type = $organisations[$this->organization_type]['type'];
        }

        if ($this->validate() && $this->saveImages($isDomestic, $isGovernment, $isLandOwner, $type)) {
            $result = true;

            if ($isDomestic && !$this->adhaar_number) {
            	if ($this->organization_type == OrganisationTypes::ID_INDIVIDUAL || $this->organization_type == OrganisationTypes::ID_HUF) {
               		$result = false;
                	$this->addError('adhaar_number', 'This is required');
            	}
            }

            if ($this->is_msme_act_2006 && !$this->ssi_number) {
                $result = false;
                $this->addError('ssi_number', 'This is required');
            } 

            if ($type == OrganisationTypes::TYPE_DIRECTOR && !$this->registration_number
                && !$isGovernment && !$isLandOwner) {
                if ($this->organization_type != OrganisationTypes::ID_OTHERS || !$isOverseas) {
                    $result = false;
                    $this->addError('registration_number', 'Registration number is required');
                }
            }
            
            if ($type == OrganisationTypes::TYPE_DIRECTOR && !DirectorForm::validateForms($directorForm, $isDomestic, $this->organization_type)) {
                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }
            if ($type == OrganisationTypes::TYPE_OWNER && !OwnerForm::validateForms($ownerForm, $isDomestic)) {

                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }

            if ($type == OrganisationTypes::TYPE_PROPRIETOR && !ProprietorForm::validateForms($proprietorForm, $isDomestic)) {
                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }
            if ($type == OrganisationTypes::TYPE_PARTNER && !PartnerForm::validateForms($partnerForm, $isDomestic)) {
                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }
            if ($type == OrganisationTypes::TYPE_KARTA && !KartaForm::validateForms($kartaForm, $isDomestic)) {
                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }
            if ($type == OrganisationTypes::TYPE_PERSON && !PersonForm::validateForms($personForm, $isDomestic)) {
                if (!$isGovernment && !$isLandOwner) {
                    $result = false;
                }
            }

            if (!$shareModel->validateForm($this->shareHolders)) {
                $this->addError('shareHolders', 'Please enter shareholder information');
                $result = false;
            }

        }

        if ($result) {
            $transaction = Yii::$app->db->beginTransaction();
            $orgTypeForm = true;
            $shareForm = true;
            if ($this->save()) {
                if ($type == OrganisationTypes::TYPE_DIRECTOR && !DirectorForm::saveForms($this->id, $directorForm)) {
                    $orgTypeForm = false;
                }
                if ($type == OrganisationTypes::TYPE_OWNER && !OwnerForm::saveForms($this->id, $ownerForm)) {
                    $orgTypeForm = false;
                }
                if ($type == OrganisationTypes::TYPE_PROPRIETOR && !ProprietorForm::saveForms($this->id, $proprietorForm)) {
                    $orgTypeForm = false;
                }
                if ($type == OrganisationTypes::TYPE_PARTNER && !PartnerForm::saveForms($this->id, $partnerForm)) {
                    $orgTypeForm = false;
                }
                if ($type == OrganisationTypes::TYPE_KARTA && !KartaForm::saveForms($this->id, $kartaForm)) {
                    $orgTypeForm = false;
                }
                if ($type == OrganisationTypes::TYPE_PERSON && !PersonForm::saveForms($this->id, $personForm)) {
                    $orgTypeForm = false;
                }
                
                if (!$shareModel->saveForm($this->id, $this->shareHolders)) {
                    $shareForm = false;
                }
                if ($isGovernment || $isLandOwner) {
                    $shareForm = $orgTypeForm = true;
                }

                if ($orgTypeForm && $shareForm) {
                    $result = true;
                    $transaction->commit();
                } else {
                    $result = false;
                    $transaction->rollBack();
                }
            }
        }

        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
