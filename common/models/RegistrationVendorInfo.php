<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "registration_vendor_info".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $bussiness_registration
 * @property string $company_name
 * @property string $trading_name
 * @property int $fk_primary_commodity_type
 * @property string $other_primary_commodity
 * @property int $fk_secondary_commodity_type
 * @property string $other_secondary_commodity
 * @property int $annual_turnover
 * @property int $primary_service_type
 * @property int $primary_service_turnorver
 * @property string $secondary_service_type
 * @property string $contact_person_name
 * @property string $contact_person_email
 * @property int $agreement_check
 * @property int $legal_entity_check
 * @property string $created_at
 * @property string $updated_at
 */
class RegistrationVendorInfo extends \yii\db\ActiveRecord
{
    public $password;
    public $confirmPassword;
    public $sameAsCompanyName;

    public static $bussinessRegistrationTypes = [
        1 => 'I am a business registered in India',
        2 => 'I am a business registered elsewhere'
    ];

    public static $primaryCommodityTypes = [
        1 => 'Module (BIS Certified)',
        2 => 'Wind Turbine Generators',
        3 => 'Lithium Ion batteries',
        4 => 'Power Conditioning System',
        5 => 'Energy Management System',
        6 => 'EV charging station',
        7 => 'Floaters',
        8 => 'Containers for Battery with HVAC &amp; Fire System',
        9 => 'Wind Mast',
        10 => 'Inverter Central',
        11 => 'Module Structure',
        12 => 'Hardware',
        13 => 'Transformer',
        0 => 'Other Products (please specify)'
    ];
    
    public static $secondaryCommodityTypes = [
        1 => 'Module (BIS Certified)',
        2 => 'Wind Turbine Generators',
        3 => 'Lithium Ion batteries',
        4 => 'Power Conditioning System',
        5 => 'Energy Management System',
        6 => 'EV charging station',
        7 => 'Floaters',
        8 => 'Containers for Battery with HVAC &amp; Fire System',
        9 => 'Wind Mast',
        10 => 'Inverter Central',
        11 => 'Module Structure',
        12 => 'Hardware',
        13 => 'Transformer',
        0 => 'Other Products (please specify)'
    ];

    /**
     * Default value display at signup form
     * @var type 
     */
    public static $annualTurnovers = [
//        1 => 'Less than USD 500 Million',
//        2 => 'Higher than USD 500 Million'
    ];

    public static $primaryServiceTypes = [
        1 => 'Wind Resource Assessment',
        2 => 'Power Curve Verification',
        3 => 'Load Flow',
        4 => 'Energy Yeild Assessment',
        5 => 'Environment Social Impact Assessment',
        6 => 'International EPC Contractors',
        7 => 'Indian EPC Contractor',
        8 => 'Civil Contractor (PAN India)',
        9 => 'Civil Contractor (Local)',
        10 => 'Mechanical Contractor (PAN India)',
        11 => 'Mechanical Contractor (Local)',
        12 => 'Electrical Contractor (PAN India)',
        13 => 'Electrical Contractor (Local)',
        14 => 'Boundary Wall Contractors',
        15 => 'Road &amp; Drain Contractors (Local)',
        16 => 'Transmission Line Contractor (upto 66kV)',
        17 => 'Transmission Line Contractor (132 kV and above)',
        18 => 'Finance Consultants / Tax Consultants / CA',
        0 => 'Misc. Consultancy (Specify)',
    ];

    public static $secondaryServiceTypes = [
        1 => 'Wind Resource Assessment',
        2 => 'Power Curve Verification',
        3 => 'Load Flow',
        4 => 'Energy Yeild Assessment',
        5 => 'Environment Social Impact Assessment',
        6 => 'International EPC Contractors',
        7 => 'Indian EPC Contractor',
        8 => 'Civil Contractor (PAN India)',
        9 => 'Civil Contractor (Local)',
        10 => 'Mechanical Contractor (PAN India)',
        11 => 'Mechanical Contractor (Local)',
        12 => 'Electrical Contractor (PAN India)',
        13 => 'Electrical Contractor (Local)',
        14 => 'Boundary Wall Contractors',
        15 => 'Road &amp; Drain Contractors (Local)',
        16 => 'Transmission Line Contractor (upto 66kV)',
        17 => 'Transmission Line Contractor (132 kV and above)',
        18 => 'Finance Consultants / Tax Consultants / CA',
        0 => 'Misc. Consultancy (Specify)',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'registration_vendor_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bussiness_registration', 'company_name', 
                'contact_person_name', 'contact_person_email', 'agreement_check', 'legal_entity_check'], 'required'],
            [['fk_user', 'bussiness_registration', 'fk_primary_commodity_type', 'annual_turnover', 'primary_service_type', 'primary_service_turnorver', 'agreement_check', 'legal_entity_check'], 'integer'],
            [['created_at', 'updated_at', 'fk_user', 'password', 'confirmPassword',
                'fk_secondary_commodity_type', 'secondary_service_type', 'sameAsCompanyName'], 'safe'],
            [['company_name', 'trading_name'], 'string', 'max' => 256],
            [['trading_name'], 'required', 'when' => function ($model) {
                return !$model->sameAsCompanyName;
            }, 'whenClient' => "function (attribute, value) {
                return $('#sameAsCompanyName').is(':checked') == false;
            }"],
            ['contact_person_email', 'email'],
            [[
                'other_primary_commodity', 'other_secondary_commodity',
                'other_secondary_service_type', 'other_primary_service_type',
                'contact_person_name', 'contact_person_email'], 'string', 'max' => 100],

            ['agreement_check', 'required', 'requiredValue' => 1, 'message' => 'Please check this checkbox'],
            ['legal_entity_check', 'required', 'requiredValue' => 1, 'message' => 'Please accept the Legal Entity relation'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['confirmPassword', 'required'],
            ['confirmPassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],
            [['offline_reason', 'offline_document'], 'safe'],

            ['contact_person_email', 'string', 'max' => 255],
            ['contact_person_email', 'unique', 'targetClass' => '\common\models\RegistrationVendorInfo', 'message' => 'This email address has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'bussiness_registration' => 'Bussiness Registration',
            'company_name' => 'Company Name',
            'trading_name' => 'Trading Name',
            'fk_primary_commodity_type' => 'Primary Commodity Type',
            'other_primary_commodity' => 'Other Primary Commodity',
            'fk_secondary_commodity_type' => 'Secondary Commodity Type (optional)',
            'other_secondary_commodity' => 'Other Products',
            'annual_turnover' => 'Annual Turnover',

            'primary_service_type' => 'Primary Service Type',
            'other_primary_service_type' => 'Misc. Consultancy (Specify)',
            'primary_service_turnorver' => 'Annual Turnorver',

            'secondary_service_type' => 'Secondary Service Type  (optional)',
            'other_secondary_service_type' => 'Misc. Consultancy',

            'contact_person_name' => 'Contact Person Name',
            'contact_person_email' => 'Contact Person Email',

            'agreement_check' => 'I hereby declare that the details furnished above are true and correct to the best of my knowledge and belief and I undertake to inform you of any changes therein, immediately. In case any of the above information is found to be false or untrue or misleading or misrepresenting, I am aware that I may be held liable for it. I hereby authorize sharing of the information furnished on this form with the HFE. I agree that the details shared by me on this portal will be stored and processed by Hero Future Energies as declared in their privacy policy.',
            'legal_entity_check' => 'Legal entity does not have relation with sanction country',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'confirmPassword' => 'Confirm Password',

            'offline_document' => 'Extra Documents',
        ];
    }

    public function getPrimaryCommodity()
    {
        return $this->hasOne(Commodities::className(), ['id' => 'fk_primary_commodity_type']);
    }

    public function getPrimaryService()
    {
        return $this->hasOne(Services::className(), ['id' => 'primary_service_type']);
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        return $model;
    }

    public function saveInfo($commodities, $services)
    {
        if ($this->sameAsCompanyName) {
            $this->trading_name = $this->company_name;
        }

        if (empty($this->primary_service_type) && empty($this->fk_primary_commodity_type) && empty($this->annual_turnover) && empty($this->primary_service_turnorver)) {
            $this->addError('primary_service_type', 'Select atleast one from services or commodities with annual turnover');
            $this->addError('fk_primary_commodity_type', 'Select atleast one from services or commodities with annual turnover');
        }
        if (!empty($this->fk_primary_commodity_type) && empty($this->annual_turnover)) {
            $this->addError('annual_turnover', 'This field is mandatory');
        }
        if (!empty($this->primary_service_type) && empty($this->primary_service_turnorver)) {
            $this->addError('primary_service_turnorver', 'This field is mandatory');
        }

        if ($this->primary_service_type && !empty($services[$this->primary_service_type]['display_other_field']) && !trim($this->other_primary_service_type)) {
            $this->addError('other_primary_service_type', 'Please enter other primary service types');
        }

        if ($this->secondary_service_type && !empty($services[$this->secondary_service_type[0]]['display_other_field']) && !trim($this->other_secondary_service_type)) {
            $this->addError('other_secondary_service_type', 'Please enter other secondary service types');
        }

        if ($this->fk_primary_commodity_type && !empty($commodities[$this->fk_primary_commodity_type]['display_other_field']) && empty($this->other_primary_commodity)) {
            $this->addError('other_primary_commodity', 'Please enter other primary service types');
        }

        if ($this->fk_secondary_commodity_type && !empty($commodities[$this->fk_secondary_commodity_type[0]]['display_other_field']) && empty($this->other_secondary_commodity)) {
            $this->addError('other_secondary_commodity', 'Please enter other primary service types');
        }

        if ($this->fk_secondary_commodity_type) {
            $this->fk_secondary_commodity_type = json_encode($this->fk_secondary_commodity_type);
        }

        if ($this->secondary_service_type) {

            $this->secondary_service_type = json_encode($this->secondary_service_type);
        }

        if (!$this->errors) {
            return $this->save();
        } else {
            if ($this->fk_secondary_commodity_type) {
                $this->fk_secondary_commodity_type = json_decode($this->fk_secondary_commodity_type);
            }

            if ($this->secondary_service_type) {
                $this->secondary_service_type = json_decode($this->secondary_service_type);
            }
        }

    }

    public function saveStatus()
    {
        
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
