<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "vendor_types".
 *
 * @property int $id
 * @property string $title
 * @property int $priority
 * @property int $status
 * @property int $phone_required
 * @property string $created_at
 * @property string $updated_at
 */
class VendorTypes extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    const IS_PARENT_DOMESTIC = 1;
    const IS_PARENT_OVERSEAS = 2;

    public static $statusArray = [
        1 => 'Active',
        2 => 'In Active'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendor_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['priority', 'status', 'is_domestic'], 'integer'],
            [['created_at', 'updated_at', 'phone_required', 'is_domestic'], 'safe'],
            [['is_government', 'is_landowner'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'priority' => 'Priority',
            'status' => 'Status',
            'phone_required' => 'Phone Required',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        $query->andWhere(['status' => self::STATUS_ACTIVE]);
        $query->orderBy(['priority' => SORT_DESC]);

        return ArrayHelper::index($query->all(), 'id');
    }
}
