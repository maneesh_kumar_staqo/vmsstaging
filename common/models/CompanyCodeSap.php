<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_code_sap".
 *
 * @property int $id
 * @property int $company_code
 * @property string $company_name
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 */
class CompanyCodeSap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_code_sap';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_code', 'company_name', 'status'], 'required'],
            [['company_code', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['company_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_code' => 'Company Code',
            'company_name' => 'Company Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function getAll()
    {
        $query = self::find();

        return ArrayHelper::index($query->all(), 'id');
    }
}
