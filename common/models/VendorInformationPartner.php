<?php

namespace common\models;

use Yii;
use common\forms\OwnerForm;
use common\forms\DirectorForm;

/**
 * This is the model class for table "vendor_information_partner".
 *
 * @property int $id
 * @property string $list_of_directors
 * @property string $din_number
 * @property string $pan_number
 * @property string $din_document
 * @property string $address_line_1
 * @property string $address_line_2
 * @property string $city
 * @property string $state
 * @property string $zip_code
 * @property int $country
 * @property string $director_id_proof
 * @property string $director_address_proof
 * @property int $is_qualified_under_164
 * @property string $created_at
 * @property string $updated_at
 */
class VendorInformationPartner extends \yii\db\ActiveRecord
{

    public static $typeArray = [
        1 => 'Owners',
        2 => 'Directors',
        3 => 'Proprietor',
        4 => 'Partners​',
        5 => 'Karta',
        6 => 'Person'
    ];

    const TYPE_OWNER = 1;
    const TYPE_DIRECTOR = 2;
    const TYPE_PROPRIETOR = 3;
    const TYPE_PARTNER = 4;
    const TYPE_KARTA = 5;
    const TYPE_PERSON = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendor_information_partner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'address_line_1',
                'address_line_2', 'city', 'state', 'zip_code', 'country'], 'required', 'enableClientValidation' => false],
            [['country', 'is_qualified_under_164', 'fk_vendor_information', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['pan_number', 'din_document', 'director_id_proof', 'director_address_proof', 'upload_passport'], 'string', 'max' => 512],

            [['din_number'], 'string', 'max' => 8],
            [['din_number'], 'integer'],
            [['din_number'], 'validateDIN'],
            [['pan_number', 'passport_no'], 'string', 'max' => 10],

            [['authorized_signatory', 'partnership_deed', 'pan_document'], 'string', 'max' => 256],

            [['address_line_1', 'address_line_2'], 'string', 'max' => 256],
            [['city', 'state'], 'string', 'max' => 100],
            [['zip_code'], 'string', 'max' => 20],
        ];
    }

    public function validateDIN()
    {
        $result = true;
        if ($this->din_number) {
            $start = substr($this->din_number, 0, 1);

            if (!in_array($start, ['0'])) {
                $this->addError('din_number', 'DIN No. should be start with 0');
                $result = false;
            }
        }

        return $result;
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_vendor_information' => 'Vendor Information ID',
            'name' => 'Name',
            'din_number' => 'DIN No',
            'din_document' => 'Upload DIN Document',

            'pan_number' => 'PAN Numbers',
            'address_line_1' => 'Address Line 1',
            'address_line_2' => 'Address Line 2',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'country' => 'Country',

            'director_id_proof' => 'Upload Director ID Proof',
            'director_address_proof' => 'Upload Director Residential Address Proof',

            'passport_no' => 'Passport No.',
            'upload_passport' => 'Upload Passport',

            'is_qualified_under_164' => 'Whether Director is dis-qualified under Section 164 of the Companies Act, 2013?',

            'authorized_signatory' => 'Upload Board Resolution/Authorization Letter in favour of authorized signatory',
            'partnership_deed' => 'Upload Partnership Deed',
            'pan_document' => 'Pan Upload',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findModel($idVendor, $type = self::TYPE_DIRECTOR, $counter = 1)
    {
        $model = self::find()->andWhere(['fk_vendor_information' => $idVendor, 'type' => $type])->all();

        if (!$model) {
            $model = new static();
            $model->fk_vendor_information = $idVendor;
            $model = [$model];
        }

        if ($counter > count($model)) {

            for ($i = count($model); $i < $counter; $i++) {
                $subModel = new static();
                $subModel->fk_vendor_information = $idVendor;
                $model[] = $subModel;
            }
        }

        return $model;
    }

    public static function modelHasErr($models)
    {
        $result = false;

        if ($models) {
            foreach ($models as $model) {
                if ($model->errors) {
//                    echo '<pre>';
//                    print_r($model->errors);
//                    die;
                    $result = true;
                }
            }
        }

        return $result;
    }
}
