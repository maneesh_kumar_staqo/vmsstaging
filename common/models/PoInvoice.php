<?php

namespace common\models;

use Yii;
use common\components\UploadComponent;

/**
 * This is the model class for table "po_invoice".
 *
 * @property int $id
 * @property int $fk_po_tracking
 * @property string $document
 * @property string $invoice_number
 * @property int $status
 * @property int $created_by
 * @property string $created_at
 */
class PoInvoice extends \yii\db\ActiveRecord
{

    public static $statusArray = [
        1 => 'Received',
        2 => 'To be received'
    ];

    public static $paymentArray = [
        1 => 'Processed',
        2 => 'In-process'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'po_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_po_tracking'], 'required'],
            [['fk_po_tracking', 'status', 'payment', 'created_by'], 'integer'],
            [['invoice_date', 'created_at'], 'safe'],
            [['amount', 'cgst', 'sgst', 'igst', 'total_amount'], 'number'],
            [['remark'], 'string', 'max' => 256],
            [['document'], 'string', 'max' => 512],
            [['invoice_number'], 'string', 'max' => 100],
            [['amount'], 'validateGst']
        ];
    }

    public function validateGst()
    {
        $result = true;
        if (!$this->igst && !$this->sgst && !$this->cgst) {
            $this->addError('amount', 'Please add tax information');
            $result = false;
        } elseif (!$this->igst && (!$this->sgst || !$this->cgst)) {
            if (!$this->sgst) {
                $this->addError('sgst', 'SGST is required with CGST');
                $result = false;
            }
            if (!$this->cgst) {
                $this->addError('cgst', 'CGST is required with SGST');
                $result = false;
            }
        }

        return $result;
    }

    public function saveImages()
    {
        $result = true;

        UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['PO_TRACKING'], 'document');

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_po_tracking' => 'Fk Po Tracking',
            'document' => 'Document',
            'invoice_number' => 'Invoice Number',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'cgst' => 'CGST',
            'sgst' => 'SGST',
            'igst' => 'IGST',
        ];
    }
}
