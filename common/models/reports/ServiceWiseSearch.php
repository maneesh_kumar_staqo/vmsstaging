<?php

namespace common\models\reports;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\CsvExport;
use common\helpers\DateTimeHelper;
use common\helpers\ReportHelper;

/**
 * UserRequestSearch represents the model behind the search form of `common\models\UserRequest`.
 */
class ServiceWiseSearch extends UserRequest
{
    public $vendor_type;

    const CSV_FILE_NAME = 'service_wise_registration.csv';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'application_status'], 'integer'],
            [['username', 'name', 'auth_key', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'tax']);

        $query->select(['user.*']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();

        return $dataProvider;
    }

    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function export($params, $services)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'tax']);

        $query->select(['user.*']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();

        $data = ReportHelper::restructureServiceCsv($query->all(), $services);

        return CsvExport::exportToCSV($data, self::CSV_FILE_NAME);
    }
}
