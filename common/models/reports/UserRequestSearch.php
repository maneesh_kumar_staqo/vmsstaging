<?php

namespace common\models\reports;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\components\CsvExport;
use common\helpers\DateTimeHelper;
use common\helpers\ReportHelper;

/**
 * UserRequestSearch represents the model behind the search form of `common\models\UserRequest`.
 */
class UserRequestSearch extends UserRequest
{
    public $vendor_type;

    const CSV_FILE_NAME = 'vendor_registration.csv';
    const CSV_SPAM_FILE_NAME = 'spam_registration.csv';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'application_status'], 'integer'],
            [['username', 'name', 'auth_key', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $isSpam = false)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'contact', 'address']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 10,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
           // 'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);

        if ($isSpam) {
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 12],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 21],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 22],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 02],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 20],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 00],
            ]);
        } else {
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 11],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 10],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 01],
            ]);
        }

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();

        return $dataProvider;
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function searchspam($params)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'contact', 'address']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 9,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            //'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);        

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();

        return $dataProvider;
    }

    public function export($params, $isSpam = false)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'contact', 'address']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 10,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.application_status' => $this->application_status,
        ]);

        if ($isSpam) {
            $fileName = self::CSV_SPAM_FILE_NAME;
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 12],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 21],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 22],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 02],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 20],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 00],
            ]);
        } else {
            $fileName = self::CSV_FILE_NAME;
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 11],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 10],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 01],
            ]);
        }

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();

        $data = ReportHelper::restructureRegisterCsv($query->all());

        return CsvExport::exportToCSV($data, $fileName);
    }

    public function exportspam($params)
    {
        $query = UserRequest::find()->joinWith(['vendor', 'registration', 'contact', 'address']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 9,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $fileName = self::CSV_FILE_NAME;
        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.application_status' => $this->application_status,
        ]);

        if ($this->created_at) {
            $dates = explode('-', $this->created_at);

            $query->andWhere(['>=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[0])]);
            $query->andWhere(['<=', 'DATE(FROM_UNIXTIME(user.created_at))', DateTimeHelper::getDateByMsqlFormat($dates[1])]);
        }

        $query->asArray();
        $data = ReportHelper::restructureRegisterCsv($query->all());
        return CsvExport::exportToCSV($data, $fileName);
    }
}
