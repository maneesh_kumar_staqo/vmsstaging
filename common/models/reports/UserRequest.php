<?php

namespace common\models\reports;

use Yii;
use common\models\RegistrationVendorInfo;
use common\models\VendorInformation;
use common\models\ContactInformation;
use common\models\AddressInformation;
use common\models\TaxInformation;

use common\models\CompanyCodeSap;
use common\models\OrgCodeSap;
use common\models\AccountGroupSap;
use common\models\PaymentTermsSap;
use common\models\BankInformation;
use common\models\ShareHolders;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $name
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $application_status
 * @property int $created_at
 * @property int $updated_at
 * @property string $verification_token
 */
class UserRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'application_status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'name', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'application_status' => 'Application Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
        ];
    }

    public function getCompanyCode()
    {
        return $this->hasOne(CompanyCodeSap::className(), ['id' => 'company_code']);
    }

    public function getOrgCode()
    {
        return $this->hasOne(OrgCodeSap::className(), ['id' => 'purchasing_organisation']);
    }

    public function getAccountCode()
    {
        return $this->hasOne(AccountGroupSap::className(), ['id' => 'account_group']);
    }

    public function getPaymentCode()
    {
        return $this->hasOne(PaymentTermsSap::className(), ['id' => 'payment_terms']);
    }

    public function getRegistration()
    {
        return $this->hasOne(RegistrationVendorInfo::className(), ['fk_user' => 'id'])
            ->joinWith(['primaryService', 'primaryCommodity']);
    }

    public function getVendor()
    {
        return $this->hasOne(VendorInformation::className(), ['fk_user' => 'id'])->joinWith(['vendorType', 'companyType']);
    }

    public function getContact()
    {
        return $this->hasOne(ContactInformation::className(), ['fk_user' => 'id']);
    }

    public function getAddress()
    {
        return $this->hasOne(AddressInformation::className(), ['fk_user' => 'id']);
    }

    public function getTax()
    {
        return $this->hasOne(TaxInformation::className(), ['fk_user' => 'id']);
    }

    public function getBank()
    {
        return $this->hasOne(BankInformation::className(), ['fk_user' => 'id']);
    }

    public function getVendorShareholders()
    {
        return $this->hasOne(VendorInformation::className(), ['fk_user' => 'id'])
            ->joinWith(['vendorType', 'companyType', 'shareholders']);
    }
}
