<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserRequest;

/**
 * UserRequestSearch represents the model behind the search form of `common\models\UserRequest`.
 */
class UserRequestSearch extends UserRequest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'application_status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'name', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $isSpam = false)
    {
        $query = UserRequest::find()->joinWith(['vendor']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 10,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            //'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);

        if ($isSpam) {
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 12],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 21],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 22],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 02],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 20],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 00],
            ]);
        } else {
            $query->andWhere([
                'OR',
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 11],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 10],
                ['CONCAT(annual_turnover,primary_service_turnorver)' => 01],
            ]);
        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'company_name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);

        $query->asArray();

        return $dataProvider;
    }

    public function searchspam($params)
    {
        $query = UserRequest::find()->joinWith(['vendor']);

        $query->select(['user.*']);
        $query->andWhere([
            'user.status' => 9,            
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.id' => $this->id,
            //'user.status' => $this->status,
            'user.application_status' => $this->application_status,
        ]);        

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'company_name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);

        $query->asArray();

        return $dataProvider;
    }
}
