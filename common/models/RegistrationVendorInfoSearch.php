<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RegistrationVendorInfo;

/**
 * RegistrationVendorInfoSearch represents the model behind the search form of `common\models\RegistrationVendorInfo`.
 */
class RegistrationVendorInfoSearch extends RegistrationVendorInfo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'fk_user', 'bussiness_registration', 'fk_primary_commdity_type', 'annual_turnover', 'primary_service_type', 'primary_service_turnorver', 'agreement_check', 'legal_entity_check'], 'integer'],
            [['company_name', 'trading_name', 'secondary_service_type', 'contact_person_name', 'contact_person_email', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RegistrationVendorInfo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fk_user' => $this->fk_user,
            'bussiness_registration' => $this->bussiness_registration,
            'fk_primary_commdity_type' => $this->fk_primary_commdity_type,
            'annual_turnover' => $this->annual_turnover,
            'primary_service_type' => $this->primary_service_type,
            'primary_service_turnorver' => $this->primary_service_turnorver,
            'agreement_check' => $this->agreement_check,
            'legal_entity_check' => $this->legal_entity_check,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'trading_name', $this->trading_name])
            ->andFilterWhere(['like', 'secondary_service_type', $this->secondary_service_type])
            ->andFilterWhere(['like', 'contact_person_name', $this->contact_person_name])
            ->andFilterWhere(['like', 'contact_person_email', $this->contact_person_email]);

        return $dataProvider;
    }
}
