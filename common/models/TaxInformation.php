<?php

namespace common\models;

use Yii;
use common\components\UploadComponent;

/**
 * This is the model class for table "tax_information".
 *
 * @property int $id
 * @property int $fk_user
 * @property int $status
 * @property int $is_gstin_enrolled
 * @property string $gst_non_enrollment_declaration
 * @property string $gstin_number
 * @property int $certification_for_lower_deduction_tds
 * @property string $tds_certificate_number
 * @property string $tds_deduction_certificate
 * @property string $pan_number
 * @property string $no_pan_in_india_declaration
 * @property string $tin_number
 * @property string $cst_number
 * @property string $vat_number
 * @property string $created_at
 * @property string $updated_at
 */
class TaxInformation extends \yii\db\ActiveRecord
{

    public static $gstArray = [
        1 => 'Yes',
        0 => 'No',
    ];

    public static $panArray = [
        1 => 'Yes',
        0 => 'No',
    ];

    public static $tinArray = [
        1 => 'Yes',
        0 => 'No',
    ];

    public static $optionArray = [
        1 => 'Yes',
        0 => 'No',
    ];
    
    const SELECTED_GST = 0;
    const SELECTED_PAN = 0;
    const SELECTED_TIN = 0;
    const SELECTION_OPTION = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tax_information';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fk_user', 'is_gstin_enrolled'], 'required'],
            [['certification_for_lower_deduction_tds', 'pan_number', 'no_pan_in_india_declaration', 'tin_cst_vat_number'], 'safe'],
            [['fk_user', 'status', 'is_gstin_enrolled', 'certification_for_lower_deduction_tds'], 'integer'],
            [['created_at', 'updated_at', 'gst_certificate'], 'safe'],
            [['gst_non_enrollment_declaration', 'tds_deduction_certificate', 'no_pan_in_india_declaration'], 'string', 'max' => 512],
            [['tds_certificate_number', 'tin_cst_vat_number'], 'string', 'max' => 100],
            [['pan_number'], 'string', 'max' => 10],
            [['gstin_number'], 'string', 'max' => 15],

            [['trc', 'permanent_est_india', 'no_pe_india', 'iec_code'], 'safe'],

            [['gst_non_enrollment_declaration', 'gst_address'], 'string', 'max' => 512],
            [['iec_code'], 'string', 'max' => 10],

            [['tin_cst_vat_number'], 'string', 'max' => 12],

            [['status_remark'], 'string', 'max' => 256],
            [['status'], 'safe'],

            [[
                'is_iec', 'iec_code', 'upload_iec', 'is_tin_cst_vat', 'upload_tin_cst_vat', 'is_pan',
                'upload_pan', 'no_pan_in_india_declaration', 'is_trc', 'upload_trc', 'upload_pe_certificate'
            ], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fk_user' => 'Fk User',
            'status' => 'Status',

            'is_gstin_enrolled' => 'Whether GSTIN is enrolled or not?',
            'gst_non_enrollment_declaration' => 'Upload GST Non-Enrollment Declaration',
            'gst_certificate' => 'Upload GSTIN Certificate',
            'gstin_number' => 'GSTIN No.',
            'gst_address' => 'Address as per GST Certificate',

            'certification_for_lower_deduction_tds' => 'Certificate for Lower Deduction of TDS',
            'tds_certificate_number' => 'TDS Certificate No.',
            'tds_deduction_certificate' => 'Upload TDS deduction Certificate',

            'is_pan' => 'Whether PAN is in India?',
            'pan_number' => 'PAN Number',
            'upload_pan' => 'Upload PAN Card',
            'no_pan_in_india_declaration' => 'Upload No PAN in India Declaration',

            'is_trc' => 'Whether Tax Residency Certificate (TRC) is applicable?',
            'trc' => 'Tax Residency Certificate (TRC) Number',
            'upload_trc' => 'Upload Valid Tax Residency Certificate (TRC)',

            'permanent_est_india' => 'Permanent Establishment in India',
            'upload_pe_certificate' => 'Upload PE Certificate',
            'no_pe_india' => 'Upload declaration for \'No PE in India\'',

            'is_iec' => 'IEC Code/ Import, Export License (Y/N)',
            'iec_code' => 'IEC Code/ Import, Export License',
            'upload_iec' => 'Upload IEC Code/ Import, Export License',

            'is_tin_cst_vat' => 'Whether TIN/CST/VAT is applicable?',
            'tin_cst_vat_number' => 'TIN/CST/VAT Number',
            'upload_tin_cst_vat' => 'Upload TIN/CST/VAT Certificate',

            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function findModel($idUser)
    {
        $model = self::findOne(['fk_user' => $idUser]);

        if (!$model) {
            $model = new static();
            $model->fk_user = $idUser;
        }

        return $model;
    }

    public function saveImages($isDomestic, $isLandOwner, $isGovernment)
    {
        $result = true;

        $gstImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'gst_non_enrollment_declaration');
        $gstCertificate = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'gst_certificate');
        $tdsImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'tds_deduction_certificate');        
        $pan = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'upload_pan');
        $tinImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'upload_tin_cst_vat');

        $panImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'no_pan_in_india_declaration');
        $trxImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'upload_trc');
        $peImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'upload_pe_certificate');
        $nopeImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'no_pe_india');
        $iecImage = UploadComponent::uploadModelFile($this, UploadComponent::$fileLocations['TAX_FILES'], 'upload_iec');        

        if (!$isLandOwner && !$isGovernment) {
            if (!$this->is_gstin_enrolled && !$gstImage) {
                $result = false;
                $this->addError('gst_non_enrollment_declaration', 'This is required');
            }
            if ($this->is_gstin_enrolled && !$gstCertificate) {
                $result = false;
                $this->addError('gst_certificate', 'This is required');
            }  

            if ($this->certification_for_lower_deduction_tds && !$tdsImage) {
                $result = false;
                $this->addError('tds_deduction_certificate', 'This is required');
            }
            if ($this->is_pan && !$pan) {
                $result = false;
                $this->addError('upload_pan', 'This is required');
            }
            if (!$isDomestic && !$this->is_pan && !$panImage) {
                $result = false;
                $this->addError('no_pan_in_india_declaration', 'This is required');
            }
            if ($this->is_tin_cst_vat && !$tinImage) {
                $result = false;
                $this->addError('upload_tin_cst_vat', 'This is required');
            }
            if (!$isDomestic && $this->is_trc && !$trxImage) {
                $result = false;
                $this->addError('upload_trc', 'This is required');
            }
            if (!$isDomestic && $this->permanent_est_india && !$peImage) {
                $result = false;
                $this->addError('upload_pe_certificate', 'This is required');
            }
            if (!$isDomestic && !$this->permanent_est_india && !$nopeImage) {
                $result = false;
                $this->addError('no_pe_india', 'This is required');
            }
            if (!$isDomestic && $this->is_iec && !$iecImage) {
                $result = false;
                $this->addError('upload_iec', 'This is required');
            }
        }

        return $result;
    }

    public function saveModel($isDomestic, $isLandOwner, $isGovernment)
    {
        $this->status = User::APP_STATUS_PENDING;
        $result = true;

        if (!$isLandOwner && !$isGovernment) {
            if ($this->is_gstin_enrolled && !$this->gstin_number) {
                $result = false;
                $this->addError('gstin_number', 'This is required');
            }
            if ($this->is_gstin_enrolled && !$this->gst_address) {
                $result = false;
                $this->addError('gst_address', 'This is required');
            }
            if ($this->certification_for_lower_deduction_tds && !$this->tds_certificate_number) {
                $result = false;
                $this->addError('tds_certificate_number', 'This is required');
            }
            if ($this->is_pan && !$this->pan_number) {
                $result = false;
                $this->addError('pan_number', 'This is required');
            }        
            if ($this->is_tin_cst_vat && !$this->tin_cst_vat_number) {
               $result = false;
               $this->addError('tin_cst_vat_number', 'This is required');
            }
            if (!$isDomestic && $this->is_iec && !$this->iec_code) {
                $result = false;
                $this->addError('iec_code', 'This is required');
            }
            if (!$isDomestic && $this->is_trc && !$this->trc) {
                $result = false;
                $this->addError('trc', 'This is required');
            }        
        }

        if ($result && $this->save()) {
            $result = true;
        }

        return $result;
    }

    public static function isExists($idUser)
    {
        $query = self::find();
        $query->cache(5);
        $query->andWhere(['fk_user' => $idUser]);

        return $query->count();
    }

    public function saveStatus()
    {
        self::updateAll(['status' => $this->status, 'status_remark' => $this->status_remark], [
            'id' => $this->id
        ]);
        return true;
    }
}
