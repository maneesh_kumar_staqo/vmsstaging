! function (a, u, h) {
	function f(e, t) {
		return typeof e === t
	}

	function g() {
		return "function" != typeof u.createElement ? u.createElement(arguments[0]) : b ? u.createElementNS.call(u, "http://www.w3.org/2000/svg", arguments[0]) : u.createElement.apply(u, arguments)
	}

	function o(e, t, i, n) {
		var o, a, s, r, l, d = "modernizr",
			c = g("div"),
			p = ((l = u.body) || ((l = g(b ? "svg" : "body")).fake = !0), l);
		if (parseInt(i, 10))
			for (; i--;)(s = g("div")).id = n ? n[i] : d + (i + 1), c.appendChild(s);
		return (o = g("style")).type = "text/css", o.id = "s" + d, (p.fake ? p : c).appendChild(o), p.appendChild(c), o.styleSheet ? o.styleSheet.cssText = e : o.appendChild(u.createTextNode(e)), c.id = d, p.fake && (p.style.background = "", p.style.overflow = "hidden", r = v.style.overflow, v.style.overflow = "hidden", v.appendChild(p)), a = t(c, e), p.fake ? (p.parentNode.removeChild(p), v.style.overflow = r, v.offsetHeight) : c.parentNode.removeChild(c), !!a
	}

	function r(e, t) {
		return function () {
			return e.apply(t, arguments)
		}
	}

	function s(e) {
		return e.replace(/([A-Z])/g, function (e, t) {
			return "-" + t.toLowerCase()
		}).replace(/^ms-/, "-ms-")
	}

	function m(e, t) {
		var i = e.length;
		if ("CSS" in a && "supports" in a.CSS) {
			for (; i--;)
				if (a.CSS.supports(s(e[i]), t)) return !0;
			return !1
		}
		if ("CSSSupportsRule" in a) {
			for (var n = []; i--;) n.push("(" + s(e[i]) + ":" + t + ")");
			return o("@supports (" + (n = n.join(" or ")) + ") { #modernizr { position: absolute; } }", function (e) {
				return "absolute" == function (e, t, i) {
					var n;
					if ("getComputedStyle" in a) {
						n = getComputedStyle.call(a, e, t);
						var o = a.console;
						null !== n ? i && (n = n.getPropertyValue(i)) : o && o[o.error ? "error" : "log"].call(o, "getComputedStyle returning null, its possible modernizr test results are inaccurate")
					} else n = !t && e.currentStyle && e.currentStyle[i];
					return n
				}(e, null, "position")
			})
		}
		return h
	}

	function n(e, t, i, n, o) {
		var a = e.charAt(0).toUpperCase() + e.slice(1),
			s = (e + " " + k.join(a + " ") + a).split(" ");
		return f(t, "string") || f(t, "undefined") ? function (e, t, i, n) {
			function o() {
				s && (delete S.style, delete S.modElem)
			}
			if (n = !f(n, "undefined") && n, !f(i, "undefined")) {
				var a = m(e, i);
				if (!f(a, "undefined")) return a
			}
			for (var s, r, l, d, c, p = ["modernizr", "tspan", "samp"]; !S.style && p.length;) s = !0, S.modElem = g(p.shift()), S.style = S.modElem.style;
			for (l = e.length, r = 0; r < l; r++)
				if (d = e[r], c = S.style[d], !!~("" + d).indexOf("-") && (d = d.replace(/([a-z])-([a-z])/g, function (e, t, i) {
						return t + i.toUpperCase()
					}).replace(/^-/, "")), S.style[d] !== h) {
					if (n || f(i, "undefined")) return o(), "pfx" != t || d;
					try {
						S.style[d] = i
					} catch (e) {}
					if (S.style[d] != c) return o(), "pfx" != t || d
				}
			return o(), !1
		}(s, t, n, o) : function (e, t, i) {
			var n;
			for (var o in e)
				if (e[o] in t) return !1 === i ? e[o] : f(n = t[e[o]], "function") ? r(n, i || t) : n;
			return !1
		}(s = (e + " " + x.join(a + " ") + a).split(" "), t, i)
	}

	function e(e, t, i) {
		return n(e, h, h, t, i)
	}
	var l = [],
		d = [],
		t = {
			_version: "3.6.0",
			_config: {
				classPrefix: "",
				enableClasses: !0,
				enableJSClass: !0,
				usePrefixes: !0
			},
			_q: [],
			on: function (e, t) {
				var i = this;
				setTimeout(function () {
					t(i[e])
				}, 0)
			},
			addTest: function (e, t, i) {
				d.push({
					name: e,
					fn: t,
					options: i
				})
			},
			addAsyncTest: function (e) {
				d.push({
					name: null,
					fn: e
				})
			}
		},
		c = function () {};
	c.prototype = t, c = new c;
	var i = "CSS" in a && "supports" in a.CSS,
		p = "supportsCSS" in a;
	c.addTest("supports", i || p);
	var v = u.documentElement,
		b = "svg" === v.nodeName.toLowerCase(),
		w = t._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : ["", ""];
	t._prefixes = w;
	var y = {}.toString;
	c.addTest("svgclippaths", function () {
		return !!u.createElementNS && /SVGClipPath/.test(y.call(u.createElementNS("http://www.w3.org/2000/svg", "clipPath")))
	});
	var $ = t.testStyles = o;
	c.addTest("touchevents", function () {
		var t;
		if ("ontouchstart" in a || a.DocumentTouch && u instanceof DocumentTouch) t = !0;
		else {
			var e = ["@media (", w.join("touch-enabled),("), "heartz", ")", "{#modernizr{top:9px;position:absolute}}"].join("");
			$(e, function (e) {
				t = 9 === e.offsetTop
			})
		}
		return t
	});
	var C = "Moz O ms Webkit",
		k = t._config.usePrefixes ? C.split(" ") : [];
	t._cssomPrefixes = k;
	var x = t._config.usePrefixes ? C.toLowerCase().split(" ") : [];
	t._domPrefixes = x;
	var T = {
		elem: g("modernizr")
	};
	c._q.push(function () {
		delete T.elem
	});
	var S = {
		style: T.elem.style
	};
	c._q.unshift(function () {
			delete S.style
		}), t.testAllProps = n, t.testAllProps = e, c.addTest("csstransforms", function () {
			return -1 === navigator.userAgent.indexOf("Android 2.") && e("transform", "scale(1)", !0)
		}), c.addTest("csstransforms3d", function () {
			return !!e("perspective", "1px", !0)
		}),
		function () {
			var e, t, i, n, o, a;
			for (var s in d)
				if (d.hasOwnProperty(s)) {
					if (e = [], (t = d[s]).name && (e.push(t.name.toLowerCase()), t.options && t.options.aliases && t.options.aliases.length))
						for (i = 0; i < t.options.aliases.length; i++) e.push(t.options.aliases[i].toLowerCase());
					for (n = f(t.fn, "function") ? t.fn() : t.fn, o = 0; o < e.length; o++) 1 === (a = e[o].split(".")).length ? c[a[0]] = n : (!c[a[0]] || c[a[0]] instanceof Boolean || (c[a[0]] = new Boolean(c[a[0]])), c[a[0]][a[1]] = n), l.push((n ? "" : "no-") + a.join("-"))
				}
		}(),
		function (e) {
			var t = v.className,
				i = c._config.classPrefix || "";
			if (b && (t = t.baseVal), c._config.enableJSClass) {
				var n = new RegExp("(^|\\s)" + i + "no-js(\\s|$)");
				t = t.replace(n, "$1" + i + "js$2")
			}
			c._config.enableClasses && (t += " " + i + e.join(" " + i), b ? v.className.baseVal = t : v.className = t)
		}(l), delete t.addTest, delete t.addAsyncTest;
	for (var _ = 0; _ < c._q.length; _++) c._q[_]();
	a.Modernizr = c
}(window, document),
function (e) {
	"use strict";
	"function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
}(function (d) {
	"use strict";
	var o, s = window.Slick || {};
	o = 0, (s = function (e, t) {
		var i, n = this;
		n.defaults = {
			accessibility: !0,
			adaptiveHeight: !1,
			appendArrows: d(e),
			appendDots: d(e),
			arrows: !0,
			asNavFor: null,
			prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
			nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
			autoplay: !1,
			autoplaySpeed: 3e3,
			centerMode: !1,
			centerPadding: "50px",
			cssEase: "ease",
			customPaging: function (e, t) {
				return d('<button type="button" />').text(t + 1)
			},
			dots: !1,
			dotsClass: "slick-dots",
			draggable: !0,
			easing: "linear",
			edgeFriction: .35,
			fade: !1,
			focusOnSelect: !1,
			focusOnChange: !1,
			infinite: !0,
			initialSlide: 0,
			lazyLoad: "ondemand",
			mobileFirst: !1,
			pauseOnHover: !0,
			pauseOnFocus: !0,
			pauseOnDotsHover: !1,
			respondTo: "window",
			responsive: null,
			rows: 1,
			rtl: !1,
			slide: "",
			slidesPerRow: 1,
			slidesToShow: 1,
			slidesToScroll: 1,
			speed: 500,
			swipe: !0,
			swipeToSlide: !1,
			touchMove: !0,
			touchThreshold: 5,
			useCSS: !0,
			useTransform: !0,
			variableWidth: !1,
			vertical: !1,
			verticalSwiping: !1,
			waitForAnimate: !0,
			zIndex: 1e3
		}, n.initials = {
			animating: !1,
			dragging: !1,
			autoPlayTimer: null,
			currentDirection: 0,
			currentLeft: null,
			currentSlide: 0,
			direction: 1,
			$dots: null,
			listWidth: null,
			listHeight: null,
			loadIndex: 0,
			$nextArrow: null,
			$prevArrow: null,
			scrolling: !1,
			slideCount: null,
			slideWidth: null,
			$slideTrack: null,
			$slides: null,
			sliding: !1,
			slideOffset: 0,
			swipeLeft: null,
			swiping: !1,
			$list: null,
			touchObject: {},
			transformsEnabled: !1,
			unslicked: !1
		}, d.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = d(e), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, i = d(e).data("slick") || {}, n.options = d.extend({}, n.defaults, t, i), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, void 0 !== document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = d.proxy(n.autoPlay, n), n.autoPlayClear = d.proxy(n.autoPlayClear, n), n.autoPlayIterator = d.proxy(n.autoPlayIterator, n), n.changeSlide = d.proxy(n.changeSlide, n), n.clickHandler = d.proxy(n.clickHandler, n), n.selectHandler = d.proxy(n.selectHandler, n), n.setPosition = d.proxy(n.setPosition, n), n.swipeHandler = d.proxy(n.swipeHandler, n), n.dragHandler = d.proxy(n.dragHandler, n), n.keyHandler = d.proxy(n.keyHandler, n), n.instanceUid = o++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0)
	}).prototype.activateADA = function () {
		this.$slideTrack.find(".slick-active").attr({
			"aria-hidden": "false"
		}).find("a, input, button, select").attr({
			tabindex: "0"
		})
	}, s.prototype.addSlide = s.prototype.slickAdd = function (e, t, i) {
		var n = this;
		if ("boolean" == typeof t) i = t, t = null;
		else if (t < 0 || t >= n.slideCount) return !1;
		n.unload(), "number" == typeof t ? 0 === t && 0 === n.$slides.length ? d(e).appendTo(n.$slideTrack) : i ? d(e).insertBefore(n.$slides.eq(t)) : d(e).insertAfter(n.$slides.eq(t)) : !0 === i ? d(e).prependTo(n.$slideTrack) : d(e).appendTo(n.$slideTrack), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slides.each(function (e, t) {
			d(t).attr("data-slick-index", e)
		}), n.$slidesCache = n.$slides, n.reinit()
	}, s.prototype.animateHeight = function () {
		var e = this;
		if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
			var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
			e.$list.animate({
				height: t
			}, e.options.speed)
		}
	}, s.prototype.animateSlide = function (e, t) {
		var i = {},
			n = this;
		n.animateHeight(), !0 === n.options.rtl && !1 === n.options.vertical && (e = -e), !1 === n.transformsEnabled ? !1 === n.options.vertical ? n.$slideTrack.animate({
			left: e
		}, n.options.speed, n.options.easing, t) : n.$slideTrack.animate({
			top: e
		}, n.options.speed, n.options.easing, t) : !1 === n.cssTransitions ? (!0 === n.options.rtl && (n.currentLeft = -n.currentLeft), d({
			animStart: n.currentLeft
		}).animate({
			animStart: e
		}, {
			duration: n.options.speed,
			easing: n.options.easing,
			step: function (e) {
				e = Math.ceil(e), !1 === n.options.vertical ? i[n.animType] = "translate(" + e + "px, 0px)" : i[n.animType] = "translate(0px," + e + "px)", n.$slideTrack.css(i)
			},
			complete: function () {
				t && t.call()
			}
		})) : (n.applyTransition(), e = Math.ceil(e), !1 === n.options.vertical ? i[n.animType] = "translate3d(" + e + "px, 0px, 0px)" : i[n.animType] = "translate3d(0px," + e + "px, 0px)", n.$slideTrack.css(i), t && setTimeout(function () {
			n.disableTransition(), t.call()
		}, n.options.speed))
	}, s.prototype.getNavTarget = function () {
		var e = this.options.asNavFor;
		return e && null !== e && (e = d(e).not(this.$slider)), e
	}, s.prototype.asNavFor = function (t) {
		var e = this.getNavTarget();
		null !== e && "object" == typeof e && e.each(function () {
			var e = d(this).slick("getSlick");
			e.unslicked || e.slideHandler(t, !0)
		})
	}, s.prototype.applyTransition = function (e) {
		var t = this,
			i = {};
		!1 === t.options.fade ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase, !1 === t.options.fade ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i)
	}, s.prototype.autoPlay = function () {
		var e = this;
		e.autoPlayClear(), e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed))
	}, s.prototype.autoPlayClear = function () {
		this.autoPlayTimer && clearInterval(this.autoPlayTimer)
	}, s.prototype.autoPlayIterator = function () {
		var e = this,
			t = e.currentSlide + e.options.slidesToScroll;
		e.paused || e.interrupted || e.focussed || (!1 === e.options.infinite && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll, e.currentSlide - 1 == 0 && (e.direction = 1))), e.slideHandler(t))
	}, s.prototype.buildArrows = function () {
		var e = this;
		!0 === e.options.arrows && (e.$prevArrow = d(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = d(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
			"aria-disabled": "true",
			tabindex: "-1"
		}))
	}, s.prototype.buildDots = function () {
		var e, t, i = this;
		if (!0 === i.options.dots && i.slideCount > i.options.slidesToShow) {
			for (i.$slider.addClass("slick-dotted"), t = d("<ul />").addClass(i.options.dotsClass), e = 0; e <= i.getDotCount(); e += 1) t.append(d("<li />").append(i.options.customPaging.call(this, i, e)));
			i.$dots = t.appendTo(i.options.appendDots), i.$dots.find("li").first().addClass("slick-active")
		}
	}, s.prototype.buildOut = function () {
		var e = this;
		e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
			d(t).attr("data-slick-index", e).data("originalStyling", d(t).attr("style") || "")
		}), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? d('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), d("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable")
	}, s.prototype.buildRows = function () {
		var e, t, i, n, o, a, s, r = this;
		if (n = document.createDocumentFragment(), a = r.$slider.children(), 0 < r.options.rows) {
			for (s = r.options.slidesPerRow * r.options.rows, o = Math.ceil(a.length / s), e = 0; e < o; e++) {
				var l = document.createElement("div");
				for (t = 0; t < r.options.rows; t++) {
					var d = document.createElement("div");
					for (i = 0; i < r.options.slidesPerRow; i++) {
						var c = e * s + (t * r.options.slidesPerRow + i);
						a.get(c) && d.appendChild(a.get(c))
					}
					l.appendChild(d)
				}
				n.appendChild(l)
			}
			r.$slider.empty().append(n), r.$slider.children().children().children().css({
				width: 100 / r.options.slidesPerRow + "%",
				display: "inline-block"
			})
		}
	}, s.prototype.checkResponsive = function (e, t) {
		var i, n, o, a = this,
			s = !1,
			r = a.$slider.width(),
			l = window.innerWidth || d(window).width();
		if ("window" === a.respondTo ? o = l : "slider" === a.respondTo ? o = r : "min" === a.respondTo && (o = Math.min(l, r)), a.options.responsive && a.options.responsive.length && null !== a.options.responsive) {
			for (i in n = null, a.breakpoints) a.breakpoints.hasOwnProperty(i) && (!1 === a.originalSettings.mobileFirst ? o < a.breakpoints[i] && (n = a.breakpoints[i]) : o > a.breakpoints[i] && (n = a.breakpoints[i]));
			null !== n ? null !== a.activeBreakpoint ? (n !== a.activeBreakpoint || t) && (a.activeBreakpoint = n, "unslick" === a.breakpointSettings[n] ? a.unslick(n) : (a.options = d.extend({}, a.originalSettings, a.breakpointSettings[n]), !0 === e && (a.currentSlide = a.options.initialSlide), a.refresh(e)), s = n) : (a.activeBreakpoint = n, "unslick" === a.breakpointSettings[n] ? a.unslick(n) : (a.options = d.extend({}, a.originalSettings, a.breakpointSettings[n]), !0 === e && (a.currentSlide = a.options.initialSlide), a.refresh(e)), s = n) : null !== a.activeBreakpoint && (a.activeBreakpoint = null, a.options = a.originalSettings, !0 === e && (a.currentSlide = a.options.initialSlide), a.refresh(e), s = n), e || !1 === s || a.$slider.trigger("breakpoint", [a, s])
		}
	}, s.prototype.changeSlide = function (e, t) {
		var i, n, o = this,
			a = d(e.currentTarget);
		switch (a.is("a") && e.preventDefault(), a.is("li") || (a = a.closest("li")), i = o.slideCount % o.options.slidesToScroll != 0 ? 0 : (o.slideCount - o.currentSlide) % o.options.slidesToScroll, e.data.message) {
			case "previous":
				n = 0 === i ? o.options.slidesToScroll : o.options.slidesToShow - i, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide - n, !1, t);
				break;
			case "next":
				n = 0 === i ? o.options.slidesToScroll : i, o.slideCount > o.options.slidesToShow && o.slideHandler(o.currentSlide + n, !1, t);
				break;
			case "index":
				var s = 0 === e.data.index ? 0 : e.data.index || a.index() * o.options.slidesToScroll;
				o.slideHandler(o.checkNavigable(s), !1, t), a.children().trigger("focus");
				break;
			default:
				return
		}
	}, s.prototype.checkNavigable = function (e) {
		var t, i;
		if (i = 0, e > (t = this.getNavigableIndexes())[t.length - 1]) e = t[t.length - 1];
		else
			for (var n in t) {
				if (e < t[n]) {
					e = i;
					break
				}
				i = t[n]
			}
		return e
	}, s.prototype.cleanUpEvents = function () {
		var e = this;
		e.options.dots && null !== e.$dots && (d("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", d.proxy(e.interrupt, e, !0)).off("mouseleave.slick", d.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), d(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && d(e.$slideTrack).children().off("click.slick", e.selectHandler), d(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), d(window).off("resize.slick.slick-" + e.instanceUid, e.resize), d("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), d(window).off("load.slick.slick-" + e.instanceUid, e.setPosition)
	}, s.prototype.cleanUpSlideEvents = function () {
		var e = this;
		e.$list.off("mouseenter.slick", d.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", d.proxy(e.interrupt, e, !1))
	}, s.prototype.cleanUpRows = function () {
		var e;
		0 < this.options.rows && ((e = this.$slides.children().children()).removeAttr("style"), this.$slider.empty().append(e))
	}, s.prototype.clickHandler = function (e) {
		!1 === this.shouldClick && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault())
	}, s.prototype.destroy = function (e) {
		var t = this;
		t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), d(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
			d(this).attr("style", d(this).data("originalStyling"))
		}), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t])
	}, s.prototype.disableTransition = function (e) {
		var t = {};
		t[this.transitionType] = "", !1 === this.options.fade ? this.$slideTrack.css(t) : this.$slides.eq(e).css(t)
	}, s.prototype.fadeSlide = function (e, t) {
		var i = this;
		!1 === i.cssTransitions ? (i.$slides.eq(e).css({
			zIndex: i.options.zIndex
		}), i.$slides.eq(e).animate({
			opacity: 1
		}, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
			opacity: 1,
			zIndex: i.options.zIndex
		}), t && setTimeout(function () {
			i.disableTransition(e), t.call()
		}, i.options.speed))
	}, s.prototype.fadeSlideOut = function (e) {
		var t = this;
		!1 === t.cssTransitions ? t.$slides.eq(e).animate({
			opacity: 0,
			zIndex: t.options.zIndex - 2
		}, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
			opacity: 0,
			zIndex: t.options.zIndex - 2
		}))
	}, s.prototype.filterSlides = s.prototype.slickFilter = function (e) {
		var t = this;
		null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit())
	}, s.prototype.focusHandler = function () {
		var i = this;
		i.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (e) {
			e.stopImmediatePropagation();
			var t = d(this);
			setTimeout(function () {
				i.options.pauseOnFocus && (i.focussed = t.is(":focus"), i.autoPlay())
			}, 0)
		})
	}, s.prototype.getCurrent = s.prototype.slickCurrentSlide = function () {
		return this.currentSlide
	}, s.prototype.getDotCount = function () {
		var e = this,
			t = 0,
			i = 0,
			n = 0;
		if (!0 === e.options.infinite)
			if (e.slideCount <= e.options.slidesToShow) ++n;
			else
				for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
		else if (!0 === e.options.centerMode) n = e.slideCount;
		else if (e.options.asNavFor)
			for (; t < e.slideCount;) ++n, t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
		else n = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
		return n - 1
	}, s.prototype.getLeft = function (e) {
		var t, i, n, o, a = this,
			s = 0;
		return a.slideOffset = 0, i = a.$slides.first().outerHeight(!0), !0 === a.options.infinite ? (a.slideCount > a.options.slidesToShow && (a.slideOffset = a.slideWidth * a.options.slidesToShow * -1, o = -1, !0 === a.options.vertical && !0 === a.options.centerMode && (2 === a.options.slidesToShow ? o = -1.5 : 1 === a.options.slidesToShow && (o = -2)), s = i * a.options.slidesToShow * o), a.slideCount % a.options.slidesToScroll != 0 && e + a.options.slidesToScroll > a.slideCount && a.slideCount > a.options.slidesToShow && (s = e > a.slideCount ? (a.slideOffset = (a.options.slidesToShow - (e - a.slideCount)) * a.slideWidth * -1, (a.options.slidesToShow - (e - a.slideCount)) * i * -1) : (a.slideOffset = a.slideCount % a.options.slidesToScroll * a.slideWidth * -1, a.slideCount % a.options.slidesToScroll * i * -1))) : e + a.options.slidesToShow > a.slideCount && (a.slideOffset = (e + a.options.slidesToShow - a.slideCount) * a.slideWidth, s = (e + a.options.slidesToShow - a.slideCount) * i), a.slideCount <= a.options.slidesToShow && (s = a.slideOffset = 0), !0 === a.options.centerMode && a.slideCount <= a.options.slidesToShow ? a.slideOffset = a.slideWidth * Math.floor(a.options.slidesToShow) / 2 - a.slideWidth * a.slideCount / 2 : !0 === a.options.centerMode && !0 === a.options.infinite ? a.slideOffset += a.slideWidth * Math.floor(a.options.slidesToShow / 2) - a.slideWidth : !0 === a.options.centerMode && (a.slideOffset = 0, a.slideOffset += a.slideWidth * Math.floor(a.options.slidesToShow / 2)), t = !1 === a.options.vertical ? e * a.slideWidth * -1 + a.slideOffset : e * i * -1 + s, !0 === a.options.variableWidth && (n = a.slideCount <= a.options.slidesToShow || !1 === a.options.infinite ? a.$slideTrack.children(".slick-slide").eq(e) : a.$slideTrack.children(".slick-slide").eq(e + a.options.slidesToShow), t = !0 === a.options.rtl ? n[0] ? -1 * (a.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, !0 === a.options.centerMode && (n = a.slideCount <= a.options.slidesToShow || !1 === a.options.infinite ? a.$slideTrack.children(".slick-slide").eq(e) : a.$slideTrack.children(".slick-slide").eq(e + a.options.slidesToShow + 1), t = !0 === a.options.rtl ? n[0] ? -1 * (a.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0, t += (a.$list.width() - n.outerWidth()) / 2)), t
	}, s.prototype.getOption = s.prototype.slickGetOption = function (e) {
		return this.options[e]
	}, s.prototype.getNavigableIndexes = function () {
		var e, t = this,
			i = 0,
			n = 0,
			o = [];
		for (e = !1 === t.options.infinite ? t.slideCount : (i = -1 * t.options.slidesToScroll, n = -1 * t.options.slidesToScroll, 2 * t.slideCount); i < e;) o.push(i), i = n + t.options.slidesToScroll, n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
		return o
	}, s.prototype.getSlick = function () {
		return this
	}, s.prototype.getSlideCount = function () {
		var i, n, o = this;
		return n = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (e, t) {
			if (t.offsetLeft - n + d(t).outerWidth() / 2 > -1 * o.swipeLeft) return i = t, !1
		}), Math.abs(d(i).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll
	}, s.prototype.goTo = s.prototype.slickGoTo = function (e, t) {
		this.changeSlide({
			data: {
				message: "index",
				index: parseInt(e)
			}
		}, t)
	}, s.prototype.init = function (e) {
		var t = this;
		d(t.$slider).hasClass("slick-initialized") || (d(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay())
	}, s.prototype.initADA = function () {
		var n = this,
			i = Math.ceil(n.slideCount / n.options.slidesToShow),
			o = n.getNavigableIndexes().filter(function (e) {
				return 0 <= e && e < n.slideCount
			});
		n.$slides.add(n.$slideTrack.find(".slick-cloned")).attr({
			"aria-hidden": "true",
			tabindex: "-1"
		}).find("a, input, button, select").attr({
			tabindex: "-1"
		}), null !== n.$dots && (n.$slides.not(n.$slideTrack.find(".slick-cloned")).each(function (e) {
			var t = o.indexOf(e);
			if (d(this).attr({
					role: "tabpanel",
					id: "slick-slide" + n.instanceUid + e,
					tabindex: -1
				}), -1 !== t) {
				var i = "slick-slide-control" + n.instanceUid + t;
				d("#" + i).length && d(this).attr({
					"aria-describedby": i
				})
			}
		}), n.$dots.attr("role", "tablist").find("li").each(function (e) {
			var t = o[e];
			d(this).attr({
				role: "presentation"
			}), d(this).find("button").first().attr({
				role: "tab",
				id: "slick-slide-control" + n.instanceUid + e,
				"aria-controls": "slick-slide" + n.instanceUid + t,
				"aria-label": e + 1 + " of " + i,
				"aria-selected": null,
				tabindex: "-1"
			})
		}).eq(n.currentSlide).find("button").attr({
			"aria-selected": "true",
			tabindex: "0"
		}).end());
		for (var e = n.currentSlide, t = e + n.options.slidesToShow; e < t; e++) n.options.focusOnChange ? n.$slides.eq(e).attr({
			tabindex: "0"
		}) : n.$slides.eq(e).removeAttr("tabindex");
		n.activateADA()
	}, s.prototype.initArrowEvents = function () {
		var e = this;
		!0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {
			message: "previous"
		}, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {
			message: "next"
		}, e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow.on("keydown.slick", e.keyHandler), e.$nextArrow.on("keydown.slick", e.keyHandler)))
	}, s.prototype.initDotEvents = function () {
		var e = this;
		!0 === e.options.dots && e.slideCount > e.options.slidesToShow && (d("li", e.$dots).on("click.slick", {
			message: "index"
		}, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && e.slideCount > e.options.slidesToShow && d("li", e.$dots).on("mouseenter.slick", d.proxy(e.interrupt, e, !0)).on("mouseleave.slick", d.proxy(e.interrupt, e, !1))
	}, s.prototype.initSlideEvents = function () {
		var e = this;
		e.options.pauseOnHover && (e.$list.on("mouseenter.slick", d.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", d.proxy(e.interrupt, e, !1)))
	}, s.prototype.initializeEvents = function () {
		var e = this;
		e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
			action: "start"
		}, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
			action: "move"
		}, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
			action: "end"
		}, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
			action: "end"
		}, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), d(document).on(e.visibilityChange, d.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && d(e.$slideTrack).children().on("click.slick", e.selectHandler), d(window).on("orientationchange.slick.slick-" + e.instanceUid, d.proxy(e.orientationChange, e)), d(window).on("resize.slick.slick-" + e.instanceUid, d.proxy(e.resize, e)), d("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), d(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), d(e.setPosition)
	}, s.prototype.initUI = function () {
		var e = this;
		!0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(), e.$nextArrow.show()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.show()
	}, s.prototype.keyHandler = function (e) {
		var t = this;
		e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && !0 === t.options.accessibility ? t.changeSlide({
			data: {
				message: !0 === t.options.rtl ? "next" : "previous"
			}
		}) : 39 === e.keyCode && !0 === t.options.accessibility && t.changeSlide({
			data: {
				message: !0 === t.options.rtl ? "previous" : "next"
			}
		}))
	}, s.prototype.lazyLoad = function () {
		var e, t, i, a = this;

		function n(e) {
			d("img[data-lazy]", e).each(function () {
				var e = d(this),
					t = d(this).attr("data-lazy"),
					i = d(this).attr("data-srcset"),
					n = d(this).attr("data-sizes") || a.$slider.attr("data-sizes"),
					o = document.createElement("img");
				o.onload = function () {
					e.animate({
						opacity: 0
					}, 100, function () {
						i && (e.attr("srcset", i), n && e.attr("sizes", n)), e.attr("src", t).animate({
							opacity: 1
						}, 200, function () {
							e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
						}), a.$slider.trigger("lazyLoaded", [a, e, t])
					})
				}, o.onerror = function () {
					e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), a.$slider.trigger("lazyLoadError", [a, e, t])
				}, o.src = t
			})
		}
		if (!0 === a.options.centerMode ? i = !0 === a.options.infinite ? (t = a.currentSlide + (a.options.slidesToShow / 2 + 1)) + a.options.slidesToShow + 2 : (t = Math.max(0, a.currentSlide - (a.options.slidesToShow / 2 + 1)), a.options.slidesToShow / 2 + 1 + 2 + a.currentSlide) : (t = a.options.infinite ? a.options.slidesToShow + a.currentSlide : a.currentSlide, i = Math.ceil(t + a.options.slidesToShow), !0 === a.options.fade && (0 < t && t--, i <= a.slideCount && i++)), e = a.$slider.find(".slick-slide").slice(t, i), "anticipated" === a.options.lazyLoad)
			for (var o = t - 1, s = i, r = a.$slider.find(".slick-slide"), l = 0; l < a.options.slidesToScroll; l++) o < 0 && (o = a.slideCount - 1), e = (e = e.add(r.eq(o))).add(r.eq(s)), o--, s++;
		n(e), a.slideCount <= a.options.slidesToShow ? n(a.$slider.find(".slick-slide")) : a.currentSlide >= a.slideCount - a.options.slidesToShow ? n(a.$slider.find(".slick-cloned").slice(0, a.options.slidesToShow)) : 0 === a.currentSlide && n(a.$slider.find(".slick-cloned").slice(-1 * a.options.slidesToShow))
	}, s.prototype.loadSlider = function () {
		var e = this;
		e.setPosition(), e.$slideTrack.css({
			opacity: 1
		}), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad()
	}, s.prototype.next = s.prototype.slickNext = function () {
		this.changeSlide({
			data: {
				message: "next"
			}
		})
	}, s.prototype.orientationChange = function () {
		this.checkResponsive(), this.setPosition()
	}, s.prototype.pause = s.prototype.slickPause = function () {
		this.autoPlayClear(), this.paused = !0
	}, s.prototype.play = s.prototype.slickPlay = function () {
		var e = this;
		e.autoPlay(), e.options.autoplay = !0, e.paused = !1, e.focussed = !1, e.interrupted = !1
	}, s.prototype.postSlide = function (e) {
		var t = this;
		t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && d(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()))
	}, s.prototype.prev = s.prototype.slickPrev = function () {
		this.changeSlide({
			data: {
				message: "previous"
			}
		})
	}, s.prototype.preventDefault = function (e) {
		e.preventDefault()
	}, s.prototype.progressiveLazyLoad = function (e) {
		e = e || 1;
		var t, i, n, o, a, s = this,
			r = d("img[data-lazy]", s.$slider);
		r.length ? (t = r.first(), i = t.attr("data-lazy"), n = t.attr("data-srcset"), o = t.attr("data-sizes") || s.$slider.attr("data-sizes"), (a = document.createElement("img")).onload = function () {
			n && (t.attr("srcset", n), o && t.attr("sizes", o)), t.attr("src", i).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === s.options.adaptiveHeight && s.setPosition(), s.$slider.trigger("lazyLoaded", [s, t, i]), s.progressiveLazyLoad()
		}, a.onerror = function () {
			e < 3 ? setTimeout(function () {
				s.progressiveLazyLoad(e + 1)
			}, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), s.$slider.trigger("lazyLoadError", [s, t, i]), s.progressiveLazyLoad())
		}, a.src = i) : s.$slider.trigger("allImagesLoaded", [s])
	}, s.prototype.refresh = function (e) {
		var t, i, n = this;
		i = n.slideCount - n.options.slidesToShow, !n.options.infinite && n.currentSlide > i && (n.currentSlide = i), n.slideCount <= n.options.slidesToShow && (n.currentSlide = 0), t = n.currentSlide, n.destroy(!0), d.extend(n, n.initials, {
			currentSlide: t
		}), n.init(), e || n.changeSlide({
			data: {
				message: "index",
				index: t
			}
		}, !1)
	}, s.prototype.registerBreakpoints = function () {
		var e, t, i, n = this,
			o = n.options.responsive || null;
		if ("array" === d.type(o) && o.length) {
			for (e in n.respondTo = n.options.respondTo || "window", o)
				if (i = n.breakpoints.length - 1, o.hasOwnProperty(e)) {
					for (t = o[e].breakpoint; 0 <= i;) n.breakpoints[i] && n.breakpoints[i] === t && n.breakpoints.splice(i, 1), i--;
					n.breakpoints.push(t), n.breakpointSettings[t] = o[e].settings
				}
			n.breakpoints.sort(function (e, t) {
				return n.options.mobileFirst ? e - t : t - e
			})
		}
	}, s.prototype.reinit = function () {
		var e = this;
		e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && d(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e])
	}, s.prototype.resize = function () {
		var e = this;
		d(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
			e.windowWidth = d(window).width(), e.checkResponsive(), e.unslicked || e.setPosition()
		}, 50))
	}, s.prototype.removeSlide = s.prototype.slickRemove = function (e, t, i) {
		var n = this;
		if (e = "boolean" == typeof e ? !0 === (t = e) ? 0 : n.slideCount - 1 : !0 === t ? --e : e, n.slideCount < 1 || e < 0 || e > n.slideCount - 1) return !1;
		n.unload(), !0 === i ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(e).remove(), n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(), n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, n.reinit()
	}, s.prototype.setCSS = function (e) {
		var t, i, n = this,
			o = {};
		!0 === n.options.rtl && (e = -e), t = "left" == n.positionProp ? Math.ceil(e) + "px" : "0px", i = "top" == n.positionProp ? Math.ceil(e) + "px" : "0px", o[n.positionProp] = e, !1 === n.transformsEnabled || (!(o = {}) === n.cssTransitions ? o[n.animType] = "translate(" + t + ", " + i + ")" : o[n.animType] = "translate3d(" + t + ", " + i + ", 0px)"), n.$slideTrack.css(o)
	}, s.prototype.setDimensions = function () {
		var e = this;
		!1 === e.options.vertical ? !0 === e.options.centerMode && e.$list.css({
			padding: "0px " + e.options.centerPadding
		}) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow), !0 === e.options.centerMode && e.$list.css({
			padding: e.options.centerPadding + " 0px"
		})), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), !1 === e.options.vertical && !1 === e.options.variableWidth ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow), e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : !0 === e.options.variableWidth ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth), e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
		var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
		!1 === e.options.variableWidth && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t)
	}, s.prototype.setFade = function () {
		var i, n = this;
		n.$slides.each(function (e, t) {
			i = n.slideWidth * e * -1, !0 === n.options.rtl ? d(t).css({
				position: "relative",
				right: i,
				top: 0,
				zIndex: n.options.zIndex - 2,
				opacity: 0
			}) : d(t).css({
				position: "relative",
				left: i,
				top: 0,
				zIndex: n.options.zIndex - 2,
				opacity: 0
			})
		}), n.$slides.eq(n.currentSlide).css({
			zIndex: n.options.zIndex - 1,
			opacity: 1
		})
	}, s.prototype.setHeight = function () {
		var e = this;
		if (1 === e.options.slidesToShow && !0 === e.options.adaptiveHeight && !1 === e.options.vertical) {
			var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
			e.$list.css("height", t)
		}
	}, s.prototype.setOption = s.prototype.slickSetOption = function () {
		var e, t, i, n, o, a = this,
			s = !1;
		if ("object" === d.type(arguments[0]) ? (i = arguments[0], s = arguments[1], o = "multiple") : "string" === d.type(arguments[0]) && (i = arguments[0], n = arguments[1], s = arguments[2], "responsive" === arguments[0] && "array" === d.type(arguments[1]) ? o = "responsive" : void 0 !== arguments[1] && (o = "single")), "single" === o) a.options[i] = n;
		else if ("multiple" === o) d.each(i, function (e, t) {
			a.options[e] = t
		});
		else if ("responsive" === o)
			for (t in n)
				if ("array" !== d.type(a.options.responsive)) a.options.responsive = [n[t]];
				else {
					for (e = a.options.responsive.length - 1; 0 <= e;) a.options.responsive[e].breakpoint === n[t].breakpoint && a.options.responsive.splice(e, 1), e--;
					a.options.responsive.push(n[t])
				}
		s && (a.unload(), a.reinit())
	}, s.prototype.setPosition = function () {
		var e = this;
		e.setDimensions(), e.setHeight(), !1 === e.options.fade ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(), e.$slider.trigger("setPosition", [e])
	}, s.prototype.setProps = function () {
		var e = this,
			t = document.body.style;
		e.positionProp = !0 === e.options.vertical ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"), void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || !0 === e.options.useCSS && (e.cssTransitions = !0), e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex), void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform", e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform", e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)), void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform", e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)), void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform", e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)), void 0 !== t.transform && !1 !== e.animType && (e.animType = "transform", e.transformType = "transform", e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && !1 !== e.animType
	}, s.prototype.setSlideClasses = function (e) {
		var t, i, n, o, a = this;
		if (i = a.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), a.$slides.eq(e).addClass("slick-current"), !0 === a.options.centerMode) {
			var s = a.options.slidesToShow % 2 == 0 ? 1 : 0;
			t = Math.floor(a.options.slidesToShow / 2), !0 === a.options.infinite && (t <= e && e <= a.slideCount - 1 - t ? a.$slides.slice(e - t + s, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = a.options.slidesToShow + e, i.slice(n - t + 1 + s, n + t + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === e ? i.eq(i.length - 1 - a.options.slidesToShow).addClass("slick-center") : e === a.slideCount - 1 && i.eq(a.options.slidesToShow).addClass("slick-center")), a.$slides.eq(e).addClass("slick-center")
		} else 0 <= e && e <= a.slideCount - a.options.slidesToShow ? a.$slides.slice(e, e + a.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= a.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (o = a.slideCount % a.options.slidesToShow, n = !0 === a.options.infinite ? a.options.slidesToShow + e : e, a.options.slidesToShow == a.options.slidesToScroll && a.slideCount - e < a.options.slidesToShow ? i.slice(n - (a.options.slidesToShow - o), n + o).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + a.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
		"ondemand" !== a.options.lazyLoad && "anticipated" !== a.options.lazyLoad || a.lazyLoad()
	}, s.prototype.setupInfinite = function () {
		var e, t, i, n = this;
		if (!0 === n.options.fade && (n.options.centerMode = !1), !0 === n.options.infinite && !1 === n.options.fade && (t = null, n.slideCount > n.options.slidesToShow)) {
			for (i = !0 === n.options.centerMode ? n.options.slidesToShow + 1 : n.options.slidesToShow, e = n.slideCount; e > n.slideCount - i; e -= 1) t = e - 1, d(n.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - n.slideCount).prependTo(n.$slideTrack).addClass("slick-cloned");
			for (e = 0; e < i + n.slideCount; e += 1) t = e, d(n.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + n.slideCount).appendTo(n.$slideTrack).addClass("slick-cloned");
			n.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
				d(this).attr("id", "")
			})
		}
	}, s.prototype.interrupt = function (e) {
		e || this.autoPlay(), this.interrupted = e
	}, s.prototype.selectHandler = function (e) {
		var t = d(e.target).is(".slick-slide") ? d(e.target) : d(e.target).parents(".slick-slide"),
			i = parseInt(t.attr("data-slick-index"));
		i || (i = 0), this.slideCount <= this.options.slidesToShow ? this.slideHandler(i, !1, !0) : this.slideHandler(i)
	}, s.prototype.slideHandler = function (e, t, i) {
		var n, o, a, s, r, l, d = this;
		if (t = t || !1, !(!0 === d.animating && !0 === d.options.waitForAnimate || !0 === d.options.fade && d.currentSlide === e))
			if (!1 === t && d.asNavFor(e), n = e, r = d.getLeft(n), s = d.getLeft(d.currentSlide), d.currentLeft = null === d.swipeLeft ? s : d.swipeLeft, !1 === d.options.infinite && !1 === d.options.centerMode && (e < 0 || e > d.getDotCount() * d.options.slidesToScroll)) !1 === d.options.fade && (n = d.currentSlide, !0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(s, function () {
				d.postSlide(n)
			}) : d.postSlide(n));
			else if (!1 === d.options.infinite && !0 === d.options.centerMode && (e < 0 || e > d.slideCount - d.options.slidesToScroll)) !1 === d.options.fade && (n = d.currentSlide, !0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(s, function () {
			d.postSlide(n)
		}) : d.postSlide(n));
		else {
			if (d.options.autoplay && clearInterval(d.autoPlayTimer), o = n < 0 ? d.slideCount % d.options.slidesToScroll != 0 ? d.slideCount - d.slideCount % d.options.slidesToScroll : d.slideCount + n : n >= d.slideCount ? d.slideCount % d.options.slidesToScroll != 0 ? 0 : n - d.slideCount : n, d.animating = !0, d.$slider.trigger("beforeChange", [d, d.currentSlide, o]), a = d.currentSlide, d.currentSlide = o, d.setSlideClasses(d.currentSlide), d.options.asNavFor && (l = (l = d.getNavTarget()).slick("getSlick")).slideCount <= l.options.slidesToShow && l.setSlideClasses(d.currentSlide), d.updateDots(), d.updateArrows(), !0 === d.options.fade) return !0 !== i ? (d.fadeSlideOut(a), d.fadeSlide(o, function () {
				d.postSlide(o)
			})) : d.postSlide(o), void d.animateHeight();
			!0 !== i && d.slideCount > d.options.slidesToShow ? d.animateSlide(r, function () {
				d.postSlide(o)
			}) : d.postSlide(o)
		}
	}, s.prototype.startLoad = function () {
		var e = this;
		!0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(), e.$nextArrow.hide()), !0 === e.options.dots && e.slideCount > e.options.slidesToShow && e.$dots.hide(), e.$slider.addClass("slick-loading")
	}, s.prototype.swipeDirection = function () {
		var e, t, i, n, o = this;
		return e = o.touchObject.startX - o.touchObject.curX, t = o.touchObject.startY - o.touchObject.curY, i = Math.atan2(t, e), (n = Math.round(180 * i / Math.PI)) < 0 && (n = 360 - Math.abs(n)), n <= 45 && 0 <= n ? !1 === o.options.rtl ? "left" : "right" : n <= 360 && 315 <= n ? !1 === o.options.rtl ? "left" : "right" : 135 <= n && n <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? 35 <= n && n <= 135 ? "down" : "up" : "vertical"
	}, s.prototype.swipeEnd = function (e) {
		var t, i, n = this;
		if (n.dragging = !1, n.swiping = !1, n.scrolling) return n.scrolling = !1;
		if (n.interrupted = !1, n.shouldClick = !(10 < n.touchObject.swipeLength), void 0 === n.touchObject.curX) return !1;
		if (!0 === n.touchObject.edgeHit && n.$slider.trigger("edge", [n, n.swipeDirection()]), n.touchObject.swipeLength >= n.touchObject.minSwipe) {
			switch (i = n.swipeDirection()) {
				case "left":
				case "down":
					t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(), n.currentDirection = 0;
					break;
				case "right":
				case "up":
					t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(), n.currentDirection = 1
			}
			"vertical" != i && (n.slideHandler(t), n.touchObject = {}, n.$slider.trigger("swipe", [n, i]))
		} else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), n.touchObject = {})
	}, s.prototype.swipeHandler = function (e) {
		var t = this;
		if (!(!1 === t.options.swipe || "ontouchend" in document && !1 === t.options.swipe || !1 === t.options.draggable && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, !0 === t.options.verticalSwiping && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), e.data.action) {
			case "start":
				t.swipeStart(e);
				break;
			case "move":
				t.swipeMove(e);
				break;
			case "end":
				t.swipeEnd(e)
		}
	}, s.prototype.swipeMove = function (e) {
		var t, i, n, o, a, s, r = this;
		return a = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!r.dragging || r.scrolling || a && 1 !== a.length) && (t = r.getLeft(r.currentSlide), r.touchObject.curX = void 0 !== a ? a[0].pageX : e.clientX, r.touchObject.curY = void 0 !== a ? a[0].pageY : e.clientY, r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2))), s = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2))), !r.options.verticalSwiping && !r.swiping && 4 < s ? !(r.scrolling = !0) : (!0 === r.options.verticalSwiping && (r.touchObject.swipeLength = s), i = r.swipeDirection(), void 0 !== e.originalEvent && 4 < r.touchObject.swipeLength && (r.swiping = !0, e.preventDefault()), o = (!1 === r.options.rtl ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1), !0 === r.options.verticalSwiping && (o = r.touchObject.curY > r.touchObject.startY ? 1 : -1), n = r.touchObject.swipeLength, (r.touchObject.edgeHit = !1) === r.options.infinite && (0 === r.currentSlide && "right" === i || r.currentSlide >= r.getDotCount() && "left" === i) && (n = r.touchObject.swipeLength * r.options.edgeFriction, r.touchObject.edgeHit = !0), !1 === r.options.vertical ? r.swipeLeft = t + n * o : r.swipeLeft = t + n * (r.$list.height() / r.listWidth) * o, !0 === r.options.verticalSwiping && (r.swipeLeft = t + n * o), !0 !== r.options.fade && !1 !== r.options.touchMove && (!0 === r.animating ? (r.swipeLeft = null, !1) : void r.setCSS(r.swipeLeft))))
	}, s.prototype.swipeStart = function (e) {
		var t, i = this;
		if (i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow) return !(i.touchObject = {});
		void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]), i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX, i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY, i.dragging = !0
	}, s.prototype.unfilterSlides = s.prototype.slickUnfilter = function () {
		var e = this;
		null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.appendTo(e.$slideTrack), e.reinit())
	}, s.prototype.unload = function () {
		var e = this;
		d(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
	}, s.prototype.unslick = function (e) {
		this.$slider.trigger("unslick", [this, e]), this.destroy()
	}, s.prototype.updateArrows = function () {
		var e = this;
		Math.floor(e.options.slidesToShow / 2), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && !e.options.infinite && (e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === e.currentSlide ? (e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - e.options.slidesToShow && !1 === e.options.centerMode ? (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : e.currentSlide >= e.slideCount - 1 && !0 === e.options.centerMode && (e.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), e.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
	}, s.prototype.updateDots = function () {
		var e = this;
		null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").end(), e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active"))
	}, s.prototype.visibility = function () {
		this.options.autoplay && (document[this.hidden] ? this.interrupted = !0 : this.interrupted = !1)
	}, d.fn.slick = function () {
		var e, t, i = this,
			n = arguments[0],
			o = Array.prototype.slice.call(arguments, 1),
			a = i.length;
		for (e = 0; e < a; e++)
			if ("object" == typeof n || void 0 === n ? i[e].slick = new s(i[e], n) : t = i[e].slick[n].apply(i[e].slick, o), void 0 !== t) return t;
		return i
	}
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery"], e) : e(jQuery)
}(function (g) {
	"use strict";

	function i(e, t, i, n) {
		var o, a = {
			raw: {}
		};
		for (o in n = n || {}) n.hasOwnProperty(o) && ("classes" === e ? (a.raw[n[o]] = t + "-" + n[o], a[n[o]] = "." + t + "-" + n[o]) : (a.raw[o] = n[o], a[o] = n[o] + "." + t));
		for (o in i) i.hasOwnProperty(o) && (a[o] = "classes" === e ? (a.raw[o] = i[o].replace(/{ns}/g, t), i[o].replace(/{ns}/g, "." + t)) : (a.raw[o] = i[o].replace(/.{ns}/g, ""), i[o].replace(/{ns}/g, t)));
		return a
	}

	function e() {
		m.windowWidth = m.$window.width(), m.windowHeight = m.$window.height(), u = c.startTimer(u, h, t)
	}

	function t() {
		for (var e in m.ResizeHandlers) m.ResizeHandlers.hasOwnProperty(e) && m.ResizeHandlers[e].callback.call(window, m.windowWidth, m.windowHeight)
	}

	function n(e, t) {
		return parseInt(e.priority) - parseInt(t.priority)
	}
	var o, a, s, r = "undefined" != typeof window ? window : this,
		l = r.document,
		d = function () {
			this.Version = "@version", this.Plugins = {}, this.DontConflict = !1, this.Conflicts = {
				fn: {}
			}, this.ResizeHandlers = [], this.RAFHandlers = [], this.window = r, this.$window = g(r), this.document = l, this.$document = g(l), this.$body = null, this.windowWidth = 0, this.windowHeight = 0, this.fallbackWidth = 1024, this.fallbackHeight = 768, this.userAgent = window.navigator.userAgent || window.navigator.vendor || window.opera, this.isFirefox = /Firefox/i.test(this.userAgent), this.isChrome = /Chrome/i.test(this.userAgent), this.isSafari = /Safari/i.test(this.userAgent) && !this.isChrome, this.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(this.userAgent), this.isIEMobile = /IEMobile/i.test(this.userAgent), this.isFirefoxMobile = this.isFirefox && this.isMobile, this.transform = null, this.transition = null, this.support = {
				file: !!(window.File && window.FileList && window.FileReader),
				history: !!(window.history && window.history.pushState && window.history.replaceState),
				matchMedia: !(!window.matchMedia && !window.msMatchMedia),
				pointer: !!window.PointerEvent,
				raf: !(!window.requestAnimationFrame || !window.cancelAnimationFrame),
				touch: !!("ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
				transition: !1,
				transform: !1
			}
		},
		c = {
			killEvent: function (e, t) {
				try {
					e.preventDefault(), e.stopPropagation(), t && e.stopImmediatePropagation()
				} catch (e) {}
			},
			killGesture: function (e) {
				try {
					e.preventDefault()
				} catch (e) {}
			},
			lockViewport: function (e) {
				f[e] = !0, g.isEmptyObject(f) || w || (o.length ? o.attr("content", s) : o = g("head").append('<meta name="viewport" content="' + s + '">'), m.$body.on(b.gestureChange, c.killGesture).on(b.gestureStart, c.killGesture).on(b.gestureEnd, c.killGesture), w = !0)
			},
			unlockViewport: function (e) {
				"undefined" !== g.type(f[e]) && delete f[e], g.isEmptyObject(f) && w && (o.length && (a ? o.attr("content", a) : o.remove()), m.$body.off(b.gestureChange).off(b.gestureStart).off(b.gestureEnd), w = !1)
			},
			startTimer: function (e, t, i, n) {
				return c.clearTimer(e), n ? setInterval(i, t) : setTimeout(i, t)
			},
			clearTimer: function (e, t) {
				e && (t ? clearInterval(e) : clearTimeout(e), e = null)
			},
			sortAsc: function (e, t) {
				return parseInt(e, 10) - parseInt(t, 10)
			},
			sortDesc: function (e, t) {
				return parseInt(t, 10) - parseInt(e, 10)
			},
			decodeEntities: function (e) {
				var t = m.document.createElement("textarea");
				return t.innerHTML = e, t.value
			},
			parseQueryString: function (e) {
				for (var t = {}, i = e.slice(e.indexOf("?") + 1).split("&"), n = 0; n < i.length; n++) {
					var o = i[n].split("=");
					t[o[0]] = o[1]
				}
				return t
			}
		},
		m = new d,
		p = g.Deferred(),
		v = {
			base: "{ns}",
			element: "{ns}-element"
		},
		b = {
			namespace: ".{ns}",
			beforeUnload: "beforeunload.{ns}",
			blur: "blur.{ns}",
			change: "change.{ns}",
			click: "click.{ns}",
			dblClick: "dblclick.{ns}",
			drag: "drag.{ns}",
			dragEnd: "dragend.{ns}",
			dragEnter: "dragenter.{ns}",
			dragLeave: "dragleave.{ns}",
			dragOver: "dragover.{ns}",
			dragStart: "dragstart.{ns}",
			drop: "drop.{ns}",
			error: "error.{ns}",
			focus: "focus.{ns}",
			focusIn: "focusin.{ns}",
			focusOut: "focusout.{ns}",
			gestureChange: "gesturechange.{ns}",
			gestureStart: "gesturestart.{ns}",
			gestureEnd: "gestureend.{ns}",
			input: "input.{ns}",
			keyDown: "keydown.{ns}",
			keyPress: "keypress.{ns}",
			keyUp: "keyup.{ns}",
			load: "load.{ns}",
			mouseDown: "mousedown.{ns}",
			mouseEnter: "mouseenter.{ns}",
			mouseLeave: "mouseleave.{ns}",
			mouseMove: "mousemove.{ns}",
			mouseOut: "mouseout.{ns}",
			mouseOver: "mouseover.{ns}",
			mouseUp: "mouseup.{ns}",
			panStart: "panstart.{ns}",
			pan: "pan.{ns}",
			panEnd: "panend.{ns}",
			resize: "resize.{ns}",
			scaleStart: "scalestart.{ns}",
			scaleEnd: "scaleend.{ns}",
			scale: "scale.{ns}",
			scroll: "scroll.{ns}",
			select: "select.{ns}",
			swipe: "swipe.{ns}",
			touchCancel: "touchcancel.{ns}",
			touchEnd: "touchend.{ns}",
			touchLeave: "touchleave.{ns}",
			touchMove: "touchmove.{ns}",
			touchStart: "touchstart.{ns}"
		},
		u = null,
		h = 20,
		f = [],
		w = !1;
	return d.prototype.NoConflict = function () {
			for (var e in m.DontConflict = !0, m.Plugins) m.Plugins.hasOwnProperty(e) && (g[e] = m.Conflicts[e], g.fn[e] = m.Conflicts.fn[e])
		}, d.prototype.Ready = function (e) {
			"complete" === m.document.readyState || "loading" !== m.document.readyState && !m.document.documentElement.doScroll ? e() : m.document.addEventListener("DOMContentLoaded", e)
		}, d.prototype.Plugin = function (e, t) {
			return m.Plugins[e] = function (p, u) {
				function h(e) {
					return e.data(f)
				}
				var e = "fs-" + p,
					f = "fs" + p.replace(/(^|\s)([a-z])/g, function (e, t, i) {
						return t + i.toUpperCase()
					});
				return u.initialized = !1, u.priority = u.priority || 10, u.classes = i("classes", e, v, u.classes), u.events = i("events", p, b, u.events), u.functions = g.extend({
					getData: h,
					iterate: function (e) {
						for (var t = Array.prototype.slice.call(arguments, 1), i = 0, n = this.length; i < n; i++) {
							var o = this.eq(i),
								a = h(o) || {};
							"undefined" !== g.type(a.$el) && e.apply(o, [a].concat(t))
						}
						return this
					}
				}, c, u.functions), u.methods = g.extend(!0, {
					_construct: g.noop,
					_postConstruct: g.noop,
					_destruct: g.noop,
					_resize: !1,
					destroy: function (e) {
						u.functions.iterate.apply(this, [u.methods._destruct].concat(Array.prototype.slice.call(arguments, 1))), this.removeClass(u.classes.raw.element).removeData(f)
					}
				}, u.methods), u.utilities = g.extend(!0, {
					_initialize: !1,
					_delegate: !1,
					defaults: function (e) {
						u.defaults = g.extend(!0, u.defaults, e || {})
					}
				}, u.utilities), u.widget && (m.Conflicts.fn[p] = g.fn[p], g.fn[f] = function (e) {
					if (this instanceof g) {
						var t = u.methods[e];
						if ("object" === g.type(e) || !e) return function (e) {
							var t, i, n, o = "object" === g.type(e),
								a = Array.prototype.slice.call(arguments, o ? 1 : 0),
								s = g();
							for (e = g.extend(!0, {}, u.defaults || {}, o ? e : {}), i = 0, n = this.length; i < n; i++)
								if (!h(t = this.eq(i))) {
									u.guid++;
									var r = "__" + u.guid,
										l = u.classes.raw.base + r,
										d = t.data(p + "-options"),
										c = g.extend(!0, {
											$el: t,
											guid: r,
											numGuid: u.guid,
											rawGuid: l,
											dotGuid: "." + l
										}, e, "object" === g.type(d) ? d : {});
									t.addClass(u.classes.raw.element).data(f, c), u.methods._construct.apply(t, [c].concat(a)), s = s.add(t)
								}
							for (i = 0, n = s.length; i < n; i++) t = s.eq(i), u.methods._postConstruct.apply(t, [h(t)]);
							return this
						}.apply(this, arguments);
						if (t && 0 !== e.indexOf("_")) {
							var i = [t].concat(Array.prototype.slice.call(arguments, 1));
							return u.functions.iterate.apply(this, i)
						}
						return this
					}
				}, m.DontConflict || (g.fn[p] = g.fn[f])), m.Conflicts[p] = g[p], g[f] = u.utilities._delegate || function (e) {
					var t = u.utilities[e] || u.utilities._initialize || !1;
					if (t) {
						var i = Array.prototype.slice.call(arguments, "object" === g.type(e) ? 0 : 1);
						return t.apply(window, i)
					}
				}, m.DontConflict || (g[p] = g[f]), u.namespace = p, u.namespaceClean = f, u.guid = 0, u.methods._resize && (m.ResizeHandlers.push({
					namespace: p,
					priority: u.priority,
					callback: u.methods._resize
				}), m.ResizeHandlers.sort(n)), u.methods._raf && (m.RAFHandlers.push({
					namespace: p,
					priority: u.priority,
					callback: u.methods._raf
				}), m.RAFHandlers.sort(n)), u
			}(e, t), m.Plugins[e]
		}, m.$window.on("resize.fs", e), e(),
		function e() {
			if (m.support.raf)
				for (var t in m.window.requestAnimationFrame(e), m.RAFHandlers) m.RAFHandlers.hasOwnProperty(t) && m.RAFHandlers[t].callback.call(window)
		}(), m.Ready(function () {
			m.$body = g("body"), g("html").addClass(m.support.touch ? "touchevents" : "no-touchevents"), o = g('meta[name="viewport"]'), a = !!o.length && o.attr("content"), s = "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0", p.resolve()
		}), b.clickTouchStart = b.click + " " + b.touchStart,
		function () {
			var e, t = {
					WebkitTransition: "webkitTransitionEnd",
					MozTransition: "transitionend",
					OTransition: "otransitionend",
					transition: "transitionend"
				},
				i = ["transition", "-webkit-transition"],
				n = {
					transform: "transform",
					MozTransform: "-moz-transform",
					OTransform: "-o-transform",
					msTransform: "-ms-transform",
					webkitTransform: "-webkit-transform"
				},
				o = "transitionend",
				a = "",
				s = "",
				r = document.createElement("div");
			for (e in t)
				if (t.hasOwnProperty(e) && e in r.style) {
					o = t[e], m.support.transition = !0;
					break
				}
			for (e in b.transitionEnd = o + ".{ns}", i)
				if (i.hasOwnProperty(e) && i[e] in r.style) {
					a = i[e];
					break
				}
			for (e in m.transition = a, n)
				if (n.hasOwnProperty(e) && n[e] in r.style) {
					m.support.transform = !0, s = n[e];
					break
				}
			m.transform = s
		}(), window.Formstone = m
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (a, e) {
	"use strict";

	function s() {
		(function () {
			for (var e in o = {
					unit: r.unit
				}, g)
				if (g.hasOwnProperty(e))
					for (var t in f[e])
						if (f[e].hasOwnProperty(t)) {
							var i = "Infinity" === t ? 1 / 0 : parseInt(t, 10),
								n = -1 < e.indexOf("max");
							f[e][t].matches && (n ? (!o[e] || i < o[e]) && (o[e] = i) : (!o[e] || i > o[e]) && (o[e] = i))
						}
		})(), i.trigger(c.mqChange, [o])
	}

	function l(e) {
		var t = d(e.media),
			i = h[t],
			n = e.matches,
			o = n ? c.enter : c.leave;
		if (i && (i.active || !i.active && n)) {
			for (var a in i[o]) i[o].hasOwnProperty(a) && i[o][a].apply(i.mq);
			i.active = !0
		}
	}

	function d(e) {
		return e.replace(/[^a-z0-9\s]/gi, "").replace(/[_\s]/g, "").replace(/^\s+|\s+$/g, "")
	}
	var t = e.Plugin("mediaquery", {
			utilities: {
				_initialize: function (e) {
					for (var t in e = e || {}, g) g.hasOwnProperty(t) && (r[t] = e[t] ? a.merge(e[t], r[t]) : r[t]);
					for (var i in (r = a.extend(r, e)).minWidth.sort(u.sortDesc), r.maxWidth.sort(u.sortAsc), r.minHeight.sort(u.sortDesc), r.maxHeight.sort(u.sortAsc), g)
						if (g.hasOwnProperty(i))
							for (var n in f[i] = {}, r[i])
								if (r[i].hasOwnProperty(n)) {
									var o = window.matchMedia("(" + g[i] + ": " + (r[i][n] === 1 / 0 ? 1e5 : r[i][n]) + r.unit + ")");
									o.addListener(s), f[i][r[i][n]] = o
								}
					s()
				},
				state: function () {
					return o
				},
				bind: function (e, t, i) {
					var n = p.matchMedia(t),
						o = d(n.media);
					for (var a in h[o] || (h[o] = {
							mq: n,
							active: !0,
							enter: {},
							leave: {}
						}, h[o].mq.addListener(l)), i) i.hasOwnProperty(a) && h[o].hasOwnProperty(a) && (h[o][a][e] = i[a]);
					var s = h[o],
						r = n.matches;
					r && s[c.enter].hasOwnProperty(e) ? (s[c.enter][e].apply(n), s.active = !0) : !r && s[c.leave].hasOwnProperty(e) && (s[c.leave][e].apply(n), s.active = !1)
				},
				unbind: function (e, t) {
					if (e)
						if (t) {
							var i = d(t);
							h[i] && (h[i].enter[e] && delete h[i].enter[e], h[i].leave[e] && delete h[i].leave[e])
						} else
							for (var n in h) h.hasOwnProperty(n) && (h[n].enter[e] && delete h[n].enter[e], h[n].leave[e] && delete h[n].leave[e])
				}
			},
			events: {
				mqChange: "mqchange"
			}
		}),
		r = {
			minWidth: [0],
			maxWidth: [1 / 0],
			minHeight: [0],
			maxHeight: [1 / 0],
			unit: "px"
		},
		c = a.extend(t.events, {
			enter: "enter",
			leave: "leave"
		}),
		i = e.$window,
		p = i[0],
		u = t.functions,
		o = null,
		h = [],
		f = {},
		g = {
			minWidth: "min-width",
			maxWidth: "max-width",
			minHeight: "min-height",
			maxHeight: "max-height"
		}
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery"], e) : e(jQuery, Formstone)
}(function (l, a) {
	"use strict";

	function t() {
		p.scrollDepth && s()
	}

	function i() {
		var e, t = l(this),
			i = "undefined" !== l.type(t[0].href) ? t[0].href : "",
			n = document.domain.split(".").reverse(),
			o = null !== i.match(n[1] + "." + n[0]);
		i.match(/^mailto\:/i) ? e = "Email, Click, " + i.replace(/^mailto\:/i, "") : i.match(/^tel\:/i) ? e = "Telephone, Click, " + i.replace(/^tel\:/i, "") : i.match(p.fileTypes) ? e = "File, Download:" + (/[.]/.exec(i) ? /[^.]+$/.exec(i) : void 0)[0] + ", " + i.replace(/ /g, "-") : o || (e = "ExternalLink, Click, " + i), e && t.attr(w, e)
	}

	function n(e) {
		g.startTimer($, 250, o)
	}

	function o() {
		for (var e, t = h.scrollTop() + a.windowHeight, i = 1 / p.scrollStops, n = i, o = 1; o <= p.scrollStops; o++) e = Math.round(100 * n).toString(), !y[C][e].passed && t > y[C][e].edge && (y[C][e].passed = !0, d(l.extend(p.scrollFields, {
			eventCategory: "ScrollDepth",
			eventAction: C,
			eventLabel: e,
			nonInteraction: !0
		}))), n += i
	}

	function s() {
		var e, t = l.mediaquery("state"),
			i = f.outerHeight(),
			n = {},
			o = 1 / p.scrollStops,
			a = o,
			s = 0;
		t.minWidth && (C = "MinWidth:" + t.minWidth + "px");
		for (var r = 1; r <= p.scrollStops; r++) s = parseInt(i * a), n[e = Math.round(100 * a).toString()] = {
			edge: "100" === e ? s - 10 : s,
			passsed: !(!y[C] || !y[C][e]) && y[C][e].passed
		}, a += o;
		y[C] = n
	}

	function r(e) {
		var t = l(this),
			i = t.attr("href"),
			n = t.data(b).split(",");
		for (var o in p.eventCallback && e.preventDefault(), n) n.hasOwnProperty(o) && (n[o] = l.trim(n[o]));
		d({
			eventCategory: n[0],
			eventAction: n[1],
			eventLabel: n[2] || i,
			eventValue: n[3],
			nonInteraction: n[4]
		}, t)
	}

	function d(e, t) {
		u.location;
		var i = l.extend({
			hitType: "event"
		}, e);
		if ("undefined" !== l.type(t) && !t.attr("data-analytics-stop")) {
			var n = "undefined" !== l.type(t[0].href) ? t[0].href : "",
				o = !n.match(/^mailto\:/i) && !n.match(/^tel\:/i) && n.indexOf(":") < 0 ? u.location.protocol + "//" + u.location.hostname + "/" + n : n;
			if ("" !== o) {
				var a = t.attr("target");
				a ? u.open(o, a) : p.eventCallback && (i.hitCallback = function () {
					var e;
					k && (g.clearTimer(k), e = o, document.location = e)
				}, k = g.startTimer(k, p.eventTimeout, i.hitCallback))
			}
		}
		c(i)
	}

	function c(e) {
		if ("function" === l.type(u.ga) && "function" === l.type(u.ga.getAll))
			for (var t = u.ga.getAll(), i = 0, n = t.length; i < n; i++) u.ga(t[i].get("name") + ".send", e)
	}
	var e = a.Plugin("analytics", {
			methods: {
				_resize: t
			},
			utilities: {
				_delegate: function () {
					if (arguments.length && "object" !== l.type(arguments[0]))
						if ("destroy" === arguments[0])(function () {
							v && f && f.length && (h.off(m.namespace), f.off(m.namespace), v = !1)
						}).apply(this);
						else {
							var e = Array.prototype.slice.call(arguments, 1);
							switch (arguments[0]) {
								case "pageview":
									(function (e) {
										c(l.extend({
											hitType: "pageview"
										}, e))
									}).apply(this, e);
									break;
								case "event":
									d.apply(this, e)
							}
						}
					else(function (e) {
						!v && f && f.length && (v = !0, (p = l.extend(p, e || {})).autoEvents && f.find("a").not("[" + w + "]").each(i), p.scrollDepth && (s(), h.on(m.scroll, n).one(m.load, t)), f.on(m.click, "*[" + w + "]", r))
					}).apply(this, arguments);
					return null
				}
			}
		}),
		p = {
			autoEvents: !1,
			fileTypes: /\.(zip|exe|dmg|pdf|doc.*|xls.*|ppt.*|mp3|txt|rar|wma|mov|avi|wmv|flv|wav)$/i,
			eventCallback: !1,
			eventTimeout: 1e3,
			scrollDepth: !1,
			scrollStops: 5,
			scrollFields: {}
		},
		u = a.window,
		h = a.$window,
		f = null,
		g = e.functions,
		m = e.events,
		v = !1,
		b = "analytics-event",
		w = "data-" + b,
		y = {},
		$ = null,
		C = "Site",
		k = null;
	a.Ready(function () {
		f = a.$body
	})
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./transition"], e) : e(jQuery, Formstone)
}(function (d, c) {
	"use strict";

	function e() {
		(x = k.scrollTop() + c.windowHeight) < 0 && (x = 0), $.iterate.call(S, s)
	}

	function t() {
		T = d(b.base), S = d(b.lazy), $.iterate.call(S, a)
	}

	function i(e) {
		if (e.visible) {
			var t = e.source;
			e.source = null, n(e, t, !0)
		}
	}

	function n(e, t, i) {
		if (t !== e.source && e.visible) {
			if (e.source = t, e.responsive = !1, e.isYouTube = !1, "object" === d.type(t) && "string" === d.type(t.video)) {
				var n = t.video.match(/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i);
				n && 1 <= n.length && (e.isYouTube = !0, e.videoId = n[1])
			}
			var o = !e.isYouTube && "object" === d.type(t) && (t.hasOwnProperty("mp4") || t.hasOwnProperty("ogg") || t.hasOwnProperty("webm"));
			if (e.video = e.isYouTube || o, e.playing = !1, e.isYouTube) e.playerReady = !1, e.posterLoaded = !1, h(e, t, i);
			else if ("object" === d.type(t) && t.hasOwnProperty("poster")) ! function (t, e, i) {
				if (t.source && t.source.poster && (u(t, t.source.poster, !0, !0), i = !1), !c.isMobile) {
					var n = '<div class="' + [w.media, w.video, !0 !== i ? w.animated : ""].join(" ") + '" aria-hidden="true">';
					n += "<video", t.loop && (n += " loop"), t.mute && (n += " muted"), n += ">", t.source.webm && (n += '<source src="' + t.source.webm + '" type="video/webm" />'), t.source.mp4 && (n += '<source src="' + t.source.mp4 + '" type="video/mp4" />'), t.source.ogg && (n += '<source src="' + t.source.ogg + '" type="video/ogg" />'), n += "</video>";
					var o = d(n += "</div>");
					o.find("video").one(y.loadedMetaData, function (e) {
						o.fsTransition({
							property: "opacity"
						}, function () {
							f(t)
						}).css({
							opacity: 1
						}), m(t), t.$el.trigger(y.loaded), t.autoPlay && g(t)
					}), t.$container.append(o)
				}
			}(e, 0, i);
			else {
				var a = t;
				if ("object" === d.type(t)) {
					var s, r = [],
						l = [];
					for (s in t) t.hasOwnProperty(s) && l.push(s);
					for (s in l.sort($.sortAsc), l) l.hasOwnProperty(s) && r.push({
						width: parseInt(l[s]),
						url: t[l[s]],
						mq: C.matchMedia("(min-width: " + parseInt(l[s]) + "px)")
					});
					e.responsive = !0, e.sources = r, a = p(e)
				}
				u(e, a, !1, i)
			}
		} else e.$el.trigger(y.loaded)
	}

	function p(e) {
		var t = e.source;
		if (e.responsive)
			for (var i in t = e.sources[0].url, e.sources) e.sources.hasOwnProperty(i) && (c.support.matchMedia ? e.sources[i].mq.matches && (t = e.sources[i].url) : e.sources[i].width < c.fallbackWidth && (t = e.sources[i].url));
		return t
	}

	function u(e, t, i, n) {
		var o = [w.media, w.image, !0 !== n ? w.animated : ""].join(" "),
			a = d('<div class="' + o + '" aria-hidden="true"><img alt="' + e.alt + '"></div>'),
			s = a.find("img"),
			r = t;
		s.one(y.load, function () {
			_ && a.addClass(w.native).css({
				backgroundImage: "url('" + r + "')"
			}), a.fsTransition({
				property: "opacity"
			}, function () {
				i || f(e)
			}).css({
				opacity: 1
			}), m(e), i && !n || e.$el.trigger(y.loaded)
		}).one(y.error, e, l).attr("src", r), e.responsive && a.addClass(w.responsive), e.$container.append(a), (s[0].complete || 4 === s[0].readyState) && s.trigger(y.load), e.currentSource = r
	}

	function h(t, e, i) {
		if (!t.videoId) {
			var n = e.match(/^.*(?:youtu.be\/|v\/|e\/|u\/\w+\/|embed\/|v=)([^#\&\?]*).*/);
			t.videoId = n[1]
		}
		if (t.posterLoaded || (t.source.poster || (t.source.poster = "//img.youtube.com/vi/" + t.videoId + "/0.jpg"), t.posterLoaded = !0, u(t, t.source.poster, !0, i), i = !1), !c.isMobile)
			if (d("script[src*='youtube.com/iframe_api']").length || d("head").append('<script src="//www.youtube.com/iframe_api"><\/script>'), H) {
				var o = t.guid + "_" + t.youTubeGuid++,
					a = '<div class="' + [w.media, w.embed, !0 !== i ? w.animated : ""].join(" ") + '" aria-hidden="true">';
				a += '<div id="' + o + '"></div>';
				var s = d(a += "</div>"),
					r = d.extend(!0, {}, {
						controls: 0,
						rel: 0,
						showinfo: 0,
						wmode: "transparent",
						enablejsapi: 1,
						version: 3,
						playerapiid: o,
						loop: t.loop ? 1 : 0,
						autoplay: 1,
						origin: C.location.protocol + "//" + C.location.host
					}, t.youtubeOptions);
				r.autoplay = 1, t.$container.append(s), t.player && (t.oldPlayer = t.player, t.player = null), t.player = new C.YT.Player(o, {
					videoId: t.videoId,
					playerVars: r,
					events: {
						onReady: function (e) {
							t.playerReady = !0, t.mute && t.player.mute(), t.autoPlay || t.player.pauseVideo()
						},
						onStateChange: function (e) {
							t.playing || e.data !== C.YT.PlayerState.PLAYING ? t.loop && t.playing && e.data === C.YT.PlayerState.ENDED && t.player.playVideo() : (t.playing = !0, s.fsTransition({
								property: "opacity"
							}, function () {
								f(t)
							}).css({
								opacity: 1
							}), m(t), t.$el.trigger(y.loaded)), t.$el.find(b.embed).addClass(w.ready)
						},
						onPlaybackQualityChange: function (e) {},
						onPlaybackRateChange: function (e) {},
						onError: function (e) {
							l({
								data: t
							})
						},
						onApiChange: function (e) {}
					}
				}), m(t)
			} else W.push({
				data: t,
				source: e
			})
	}

	function f(e) {
		var t = e.$container.find(b.media);
		1 <= t.length && (t.not(":last").remove(), e.oldPlayer = null)
	}

	function l(e) {
		e.data.$el.trigger(y.error)
	}

	function g(e) {
		if (e.video && !e.playing)
			if (e.isYouTube) e.playerReady ? e.player.playVideo() : e.autoPlay = !0;
			else {
				var t = e.$container.find("video");
				t.length && t[0].play(), e.playing = !0
			}
	}

	function o(e) {
		if (e.visible)
			if (e.responsive) {
				var t = p(e);
				t !== e.currentSource ? u(e, t, !1, !0) : m(e)
			} else m(e)
	}

	function m(e) {
		for (var t = e.$container.find(b.media), i = 0, n = t.length; i < n; i++) {
			var o = t.eq(i),
				a = e.isYouTube ? "iframe" : o.find("video").length ? "video" : "img",
				s = o.find(a);
			if (s.length && ("img" !== a || !_)) {
				var r = e.$el.outerWidth(),
					l = e.$el.outerHeight(),
					d = v(e, s);
				e.width = d.width, e.height = d.height, e.left = 0, e.top = 0;
				var c = e.isYouTube ? e.embedRatio : e.width / e.height;
				e.height = l, e.width = e.height * c, e.width < r && (e.width = r, e.height = e.width / c), e.left = -(e.width - r) / 2, e.top = -(e.height - l) / 2, o.css({
					height: e.height,
					width: e.width,
					left: e.left,
					top: e.top
				})
			}
		}
	}

	function a(e) {
		e.scrollTop = e.$el.offset().top
	}

	function s(e) {
		!e.visible && e.scrollTop < x + e.lazyEdge && (e.visible = !0, i(e))
	}

	function v(e, t) {
		if (e.isYouTube) return {
			height: 500,
			width: 500 / e.embedRatio
		};
		if (t.is("img")) {
			var i = t[0];
			if ("undefined" !== d.type(i.naturalHeight)) return {
				height: i.naturalHeight,
				width: i.naturalWidth
			};
			var n = new Image;
			return n.src = i.src, {
				height: n.height,
				width: n.width
			}
		}
		return {
			height: t[0].videoHeight,
			width: t[0].videoWidth
		}
	}
	var r = c.Plugin("background", {
			widget: !0,
			defaults: {
				alt: "",
				autoPlay: !0,
				customClass: "",
				embedRatio: 1.777777,
				lazy: !1,
				lazyEdge: 100,
				loop: !0,
				mute: !0,
				source: null,
				youtubeOptions: {}
			},
			classes: ["container", "media", "animated", "responsive", "native", "fixed", "ready", "lazy"],
			events: {
				loaded: "loaded",
				ready: "ready",
				loadedMetaData: "loadedmetadata"
			},
			methods: {
				_construct: function (e) {
					e.youTubeGuid = 0, e.$container = d('<div class="' + w.container + '"></div>').appendTo(this), e.thisClasses = [w.base, e.customClass], e.visible = !0, e.lazy && (e.visible = !1, e.thisClasses.push(w.lazy)), this.addClass(e.thisClasses.join(" ")), t(), e.lazy ? (a(e), s(e)) : i(e)
				},
				_destruct: function (e) {
					e.$container.remove(), this.removeClass(e.thisClasses.join(" ")).off(y.namespace), t()
				},
				_resize: function () {
					$.iterate.call(T, o), $.iterate.call(S, a), $.iterate.call(S, s)
				},
				play: g,
				pause: function (e) {
					if (e.video && e.playing) {
						if (e.isYouTube) e.playerReady ? e.player.pauseVideo() : e.autoPlay = !1;
						else {
							var t = e.$container.find("video");
							t.length && t[0].pause()
						}
						e.playing = !1
					}
				},
				mute: function (e) {
					if (e.video)
						if (e.isYouTube && e.playerReady) e.player.mute();
						else {
							var t = e.$container.find("video");
							t.length && (t[0].muted = !0)
						}
					e.mute = !0
				},
				unmute: function (e) {
					if (e.video) {
						if (e.isYouTube && e.playerReady) e.player.unMute();
						else {
							var t = e.$container.find("video");
							t.length && (t[0].muted = !1)
						}
						e.playing = !0
					}
					e.mute = !1
				},
				resize: m,
				load: n,
				unload: function (e) {
					var t = e.$container.find(b.media);
					1 <= t.length && t.fsTransition({
						property: "opacity"
					}, function () {
						t.remove(), delete e.source
					}).css({
						opacity: 0
					})
				}
			}
		}),
		b = r.classes,
		w = b.raw,
		y = r.events,
		$ = r.functions,
		C = c.window,
		k = c.$window,
		x = 0,
		T = [],
		S = [],
		_ = "backgroundSize" in c.document.documentElement.style,
		H = !1,
		W = [];
	c.Ready(function () {
		e(), k.on("scroll", e)
	}), C.onYouTubeIframeAPIReady = function () {
		for (var e in H = !0, W) W.hasOwnProperty(e) && h(W[e].data, W[e].source);
		W = []
	}
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery", "./touch"], e) : e(jQuery, Formstone)
}(function (p, u) {
	"use strict";

	function h() {
		P = p(W.base)
	}

	function f(e) {
		e.enabled && (I.clearTimer(e.autoTimer), e.enabled = !1, e.$subordinate.off(M.update), this.removeClass([j.enabled, j.animated].join(" ")).off(M.namespace), e.$canister.fsTouch("destroy").off(M.namespace).attr("style", "").css(A, "none"), e.$items.css({
			width: "",
			height: ""
		}).removeClass([j.visible, W.item_previous, W.item_next].join(" ")), e.$images.off(M.namespace), e.$controlItems.off(M.namespace), e.$pagination.html("").off(M.namespace), v(e), e.useMargin ? e.$canister.css({
			marginLeft: ""
		}) : e.$canister.css(z, ""), e.index = 0)
	}

	function g(i) {
		i.enabled || (i.enabled = !0, this.addClass(j.enabled), i.$controlItems.on(M.click, i, s), i.$pagination.on(M.click, W.page, i, r), i.$items.on(M.click, i, $), i.$subordinate.on(M.update, i, k), k({
			data: i
		}, 0), i.$canister.fsTouch({
			axis: "x",
			pan: !0,
			swipe: !0
		}).on(M.panStart, i, l).on(M.pan, i, d).on(M.panEnd, i, b).on(M.swipe, i, w).on(M.focusIn, i, C).css(A, ""), m(i), i.$images.on(M.load, i, a), i.autoAdvance && (i.autoTimer = I.startTimer(i.autoTimer, i.autoTime, function () {
			var e, t;
			(t = (e = i).index + 1) >= e.pageCount && (t = 0), c(e, t)
		}, !0)), n.call(this, i))
	}

	function n(e) {
		if (e.enabled) {
			var t, i, n, o, a, s, r;
			if (e.count = e.$items.length, e.count < 1) return v(e), void e.$canister.css({
				height: ""
			});
			for (this.removeClass(j.animated), e.containerWidth = e.$container.outerWidth(!1), e.visible = function (e) {
					var t = 1;
					if (e.single) return t;
					if ("array" === p.type(e.show))
						for (var i in e.show) e.show.hasOwnProperty(i) && (u.support.matchMedia ? e.show[i].mq.matches && (t = e.show[i].count) : e.show[i].width < u.fallbackWidth && (t = e.show[i].count));
					else t = e.show;
					return e.fill && e.count < t ? e.count : t
				}(e), e.perPage = e.paged ? 1 : e.visible, e.itemMarginLeft = parseInt(e.$items.eq(0).css("marginLeft")), e.itemMarginRight = parseInt(e.$items.eq(0).css("marginRight")), e.itemMargin = e.itemMarginLeft + e.itemMarginRight, isNaN(e.itemMargin) && (e.itemMargin = 0), e.itemWidth = (e.containerWidth - e.itemMargin * (e.visible - 1)) / e.visible, e.itemHeight = 0, e.pageWidth = e.paged ? e.itemWidth : e.containerWidth, e.pageCount = Math.ceil(e.count / e.perPage), e.canisterWidth = e.single ? e.containerWidth : (e.pageWidth + e.itemMargin) * e.pageCount, e.$canister.css({
					width: e.matchWidth ? e.canisterWidth : 1e6,
					height: ""
				}), e.$items.css({
					width: e.matchWidth ? e.itemWidth : "",
					height: ""
				}).removeClass([j.visible, j.item_previous, j.item_next].join(" ")), e.pages = [], i = 0; i < e.count; i += e.perPage) {
				for (s = a = 0, (o = e.$items.slice(i, i + e.perPage)).length < e.perPage && (o = 0 === i ? e.$items : e.$items.slice(e.$items.length - e.perPage)), r = (e.rtl ? o.eq(o.length - 1) : o.eq(0)).position().left, n = 0; n < o.length; n++) a += o.eq(n).outerWidth(!0), (t = o.eq(n).outerHeight()) > s && (s = t);
				e.pages.push({
					left: e.rtl ? r - (e.canisterWidth - a) : r,
					height: s,
					width: a,
					$items: o
				}), s > e.itemHeight && (e.itemHeight = s)
			}
			e.paged && (e.pageCount = e.count - e.visible + 1), e.pageCount <= 0 && (e.pageCount = 1), e.maxMove = -e.pages[e.pageCount - 1].left, e.autoHeight ? e.$canister.css({
				height: e.pages[0].height
			}) : e.matchHeight && e.$items.css({
				height: e.itemHeight
			});
			var l = "";
			for (i = 0; i < e.pageCount; i++) l += '<button type="button" class="' + j.page + '">' + (i + 1) + "</button>";
			e.$pagination.html(l), e.pageCount <= 1 ? v(e) : ((d = e).$controls.addClass(j.visible), d.$controlItems.addClass(j.visible), d.$pagination.addClass(j.visible)), e.$paginationItems = e.$pagination.find(W.page), c(e, e.index, !1), setTimeout(function () {
				e.$el.addClass(j.animated)
			}, 5)
		}
		var d
	}

	function m(e) {
		e.$items = e.$canister.children().not(":hidden").addClass(j.item), e.$images = e.$canister.find("img"), e.totalImages = e.$images.length
	}

	function t(e, t) {
		e.$images.off(M.namespace), !1 !== t && e.$canister.html(t), e.index = 0, m(e), n.call(this, e)
	}

	function e(e, t, i, n, o) {
		e.enabled && (I.clearTimer(e.autoTimer), void 0 === o && (o = !0), c(e, t - 1, o, i, n))
	}

	function i(e) {
		var t = e.index - 1;
		e.infinite && t < 0 && (t = e.pageCount - 1), c(e, t)
	}

	function o(e) {
		var t = e.index + 1;
		e.infinite && t >= e.pageCount && (t = 0), c(e, t)
	}

	function a(e) {
		var t = e.data;
		t.resizeTimer = I.startTimer(t.resizeTimer, 1, function () {
			n.call(t.$el, t)
		})
	}

	function s(e) {
		I.killEvent(e);
		var t = e.data,
			i = t.index + (p(e.currentTarget).hasClass(j.control_next) ? 1 : -1);
		I.clearTimer(t.autoTimer), c(t, i)
	}

	function r(e) {
		I.killEvent(e);
		var t = e.data,
			i = t.$paginationItems.index(p(e.currentTarget));
		I.clearTimer(t.autoTimer), c(t, i)
	}

	function c(e, t, i, n, o) {
		if (t < 0 && (t = e.infinite ? e.pageCount - 1 : 0), t >= e.pageCount && (t = e.infinite ? 0 : e.pageCount - 1), !(e.count < 1)) {
			e.pages[t] && (e.leftPosition = -e.pages[t].left), e.leftPosition = x(e, e.leftPosition), e.useMargin ? e.$canister.css({
				marginLeft: e.leftPosition
			}) : !1 === i ? (e.$canister.css(A, "none").css(z, "translateX(" + e.leftPosition + "px)"), setTimeout(function () {
				e.$canister.css(A, "")
			}, 5)) : e.$canister.css(z, "translateX(" + e.leftPosition + "px)"), e.$items.removeClass([j.visible, j.item_previous, j.item_next].join(" "));
			for (var a = 0, s = e.pages.length; a < s; a++) a === t ? e.pages[a].$items.addClass(j.visible).attr("aria-hidden", "false") : e.pages[a].$items.not(e.pages[t].$items).addClass(a < t ? j.item_previous : j.item_next).attr("aria-hidden", "true");
			e.autoHeight && e.$canister.css({
				height: e.pages[t].height
			}), !1 !== i && !0 !== n && t !== e.index && (e.infinite || -1 < t && t < e.pageCount) && e.$el.trigger(M.update, [t]), e.index = t, e.linked && !0 !== o && p(e.linked).not(e.$el)[H]("jumpPage", e.index + 1, !0, !0), (r = e).$paginationItems.removeClass(j.active).eq(r.index).addClass(j.active), r.infinite ? r.$controlItems.addClass(j.visible) : r.pageCount < 1 ? r.$controlItems.removeClass(j.visible) : (r.$controlItems.addClass(j.visible), r.index <= 0 ? r.$controlPrevious.removeClass(j.visible) : (r.index >= r.pageCount - 1 || !r.single && r.leftPosition === r.maxMove) && r.$controlNext.removeClass(j.visible))
		}
		var r
	}

	function v(e) {
		e.$controls.removeClass(j.visible), e.$controlItems.removeClass(j.visible), e.$pagination.removeClass(j.visible)
	}

	function l(e, t) {
		var i = e.data;
		if (I.clearTimer(i.autoTimer), !i.single) {
			if (i.useMargin) i.leftPosition = parseInt(i.$canister.css("marginLeft"));
			else {
				var n = i.$canister.css(z).split(",");
				i.leftPosition = parseInt(n[4])
			}
			if (i.$canister.css(A, "none").css("will-change", "transform"), d(e), i.linked && !0 !== t) {
				var o = e.deltaX / i.pageWidth;
				i.rtl && (o *= -1), p(i.linked).not(i.$el)[H]("panStart", o)
			}
		}
		i.isTouching = !0
	}

	function d(e, t) {
		var i = e.data;
		if (!i.single && (i.touchLeft = x(i, i.leftPosition + e.deltaX), i.useMargin ? i.$canister.css({
				marginLeft: i.touchLeft
			}) : i.$canister.css(z, "translateX(" + i.touchLeft + "px)"), i.linked && !0 !== t)) {
			var n = e.deltaX / i.pageWidth;
			i.rtl && (n *= -1), p(i.linked).not(i.$el)[H]("pan", n)
		}
	}

	function b(e, t) {
		var i = e.data,
			n = Math.abs(e.deltaX),
			o = T(i, e),
			a = !1;
		if (i.didPan = !1, !i.single) {
			var s, r, l = Math.abs(i.touchLeft),
				d = !1,
				c = i.rtl ? "right" : "left";
			if (e.directionX === c)
				for (s = 0, r = i.pages.length; s < r; s++) d = i.pages[s], l > Math.abs(d.left) + 20 && (a = s + 1);
			else
				for (s = i.pages.length - 1, r = 0; r <= s; s--) d = i.pages[s], l < Math.abs(d.left) && (a = s - 1)
		}!1 === a && (a = n < 50 ? i.index : i.index + o), a !== i.index && (i.didPan = !0), i.linked && !0 !== t && p(i.linked).not(i.$el)[H]("panEnd", a), y(i, a)
	}

	function w(e, t) {
		var i = e.data,
			n = T(i, e),
			o = i.index + n;
		i.linked && !0 !== t && p(i.linked).not(i.$el)[H]("swipe", e.directionX), y(i, o)
	}

	function y(e, t) {
		e.$canister.css(A, "").css("will-change", ""), c(e, t), e.isTouching = !1
	}

	function $(e) {
		var t = e.data,
			i = p(e.currentTarget);
		if (!t.didPan && (i.trigger(M.itemClick), t.controller)) {
			var n = t.$items.index(i);
			k(e, n), t.$subordinate[H]("jumpPage", n + 1, !0)
		}
	}

	function C(e) {
		var t = e.data;
		if (t.enabled && !t.isTouching) {
			I.clearTimer(t.autoTimer), t.$container.scrollLeft(0);
			var i, n = p(e.target);
			n.hasClass(j.item) ? i = n : n.parents(W.item).length && (i = n.parents(W.item).eq(0));
			for (var o = 0; o < t.pageCount; o++)
				if (t.pages[o].$items.is(i)) {
					c(t, o);
					break
				}
		}
	}

	function k(e, t) {
		var i = e.data;
		if (i.controller) {
			var n = i.$items.eq(t);
			i.$items.removeClass(j.active), n.addClass(j.active);
			for (var o = 0; o < i.pageCount; o++)
				if (i.pages[o].$items.is(n)) {
					c(i, o, !0, !0);
					break
				}
		}
	}

	function x(e, t) {
		return isNaN(t) ? t = 0 : e.rtl ? (t > e.maxMove && (t = e.maxMove), t < 0 && (t = 0)) : (t < e.maxMove && (t = e.maxMove), 0 < t && (t = 0)), t
	}

	function T(e, t) {
		return e.rtl ? "right" === t.directionX ? 1 : -1 : "left" === t.directionX ? 1 : -1
	}
	var S = u.Plugin("carousel", {
			widget: !0,
			defaults: {
				autoAdvance: !1,
				autoHeight: !1,
				autoTime: 8e3,
				contained: !0,
				controls: !0,
				customClass: "",
				fill: !1,
				infinite: !1,
				labels: {
					next: "Next",
					previous: "Previous",
					controls: "Carousel {guid} Controls",
					pagination: "Carousel {guid} Pagination"
				},
				matchHeight: !1,
				matchWidth: !0,
				maxWidth: 1 / 0,
				minWidth: "0px",
				paged: !1,
				pagination: !0,
				rtl: !1,
				show: 1,
				single: !1,
				theme: "fs-light",
				useMargin: !1
			},
			classes: ["ltr", "rtl", "viewport", "wrapper", "container", "canister", "item", "item_previous", "item_next", "controls", "controls_custom", "control", "control_previous", "control_next", "pagination", "page", "animated", "enabled", "visible", "active", "auto_height", "contained", "single"],
			events: {
				itemClick: "itemClick",
				update: "update"
			},
			methods: {
				_construct: function (e) {
					var t;
					e.didPan = !1, e.carouselClasses = [j.base, e.theme, e.customClass, e.rtl ? j.rtl : j.ltr], e.maxWidth = e.maxWidth === 1 / 0 ? "100000px" : e.maxWidth, e.mq = "(min-width:" + e.minWidth + ") and (max-width:" + e.maxWidth + ")", e.customControls = "object" === p.type(e.controls) && e.controls.previous && e.controls.next, e.customPagination = "string" === p.type(e.pagination), e.id = this.attr("id"), e.id ? e.ariaId = e.id : (e.ariaId = e.rawGuid, this.attr("id", e.ariaId)), u.support.transform || (e.useMargin = !0);
					var i = "",
						n = "",
						o = [j.control, j.control_previous].join(" "),
						a = [j.control, j.control_next].join(" ");
					e.controls && !e.customControls && (e.labels.controls = e.labels.controls.replace("{guid}", e.numGuid), i += '<div class="' + j.controls + '" aria-label="' + e.labels.controls + '" aria-controls="' + e.ariaId + '">', i += '<button type="button" class="' + o + '" aria-label="' + e.labels.previous + '">' + e.labels.previous + "</button>", i += '<button type="button" class="' + a + '" aria-label="' + e.labels.next + '">' + e.labels.next + "</button>", i += "</div>"), e.pagination && !e.customPagination && (e.labels.pagination = e.labels.pagination.replace("{guid}", e.numGuid), n += '<div class="' + j.pagination + '" aria-label="' + e.labels.pagination + '" aria-controls="' + e.ariaId + '" role="navigation">', n += "</div>"), e.autoHeight && e.carouselClasses.push(j.auto_height), e.contained && e.carouselClasses.push(j.contained), e.single && e.carouselClasses.push(j.single), this.addClass(e.carouselClasses.join(" ")).wrapInner('<div class="' + j.wrapper + '" aria-live="polite"><div class="' + j.container + '"><div class="' + j.canister + '"></div></div></div>').append(i).wrapInner('<div class="' + j.viewport + '"></div>').append(n), e.$viewport = this.find(W.viewport).eq(0), e.$container = this.find(W.container).eq(0), e.$canister = this.find(W.canister).eq(0), e.$pagination = this.find(W.pagination).eq(0), e.$controlPrevious = e.$controlNext = p(""), e.customControls ? (e.$controls = p(e.controls.container).addClass([j.controls, j.controls_custom].join(" ")), e.$controlPrevious = p(e.controls.previous).addClass(o), e.$controlNext = p(e.controls.next).addClass(a)) : (e.$controls = this.find(W.controls).eq(0), e.$controlPrevious = e.$controls.find(W.control_previous), e.$controlNext = e.$controls.find(W.control_next)), e.$controlItems = e.$controlPrevious.add(e.$controlNext), e.customPagination && (e.$pagination = p(e.pagination).addClass([j.pagination])), e.$paginationItems = e.$pagination.find(W.page), e.index = 0, e.enabled = !1, e.leftPosition = 0, e.autoTimer = null, e.resizeTimer = null;
					var s = this.data(_ + "-linked");
					e.linked = !!s && "[data-" + _ + '-linked="' + s + '"]', e.linked && (e.paged = !0);
					var r = this.data(_ + "-controller-for") || "";
					if (e.$subordinate = p(r), e.$subordinate.length && (e.controller = !0), "object" === p.type(e.show)) {
						var l = e.show,
							d = [],
							c = [];
						for (t in l) l.hasOwnProperty(t) && c.push(t);
						for (t in c.sort(I.sortAsc), c) c.hasOwnProperty(t) && d.push({
							width: parseInt(c[t]),
							count: l[c[t]],
							mq: window.matchMedia("(min-width: " + parseInt(c[t]) + "px)")
						});
						e.show = d
					}
					m(e), p.fsMediaquery("bind", e.rawGuid, e.mq, {
						enter: function () {
							g.call(e.$el, e)
						},
						leave: function () {
							f.call(e.$el, e)
						}
					}), h(), e.carouselClasses.push(j.enabled), e.carouselClasses.push(j.animated)
				},
				_destruct: function (e) {
					I.clearTimer(e.autoTimer), I.clearTimer(e.resizeTimer), f.call(this, e), p.fsMediaquery("unbind", e.rawGuid), e.id !== e.ariaId && this.removeAttr("id"), e.$controlItems.removeClass([W.control, j.control_previous, W.control_next, W.visible].join(" ")).off(M.namespace), e.$images.off(M.namespace), e.$canister.fsTouch("destroy"), e.$items.removeClass([j.item, j.visible, W.item_previous, W.item_next].join(" ")).unwrap().unwrap().unwrap().unwrap(), e.controls && !e.customControls && e.$controls.remove(), e.customControls && e.$controls.removeClass([j.controls, j.controls_custom, j.visible].join(" ")), e.pagination && !e.customPagination && e.$pagination.remove(), e.customPagination && e.$pagination.html("").removeClass([j.pagination, j.visible].join(" ")), this.removeClass(e.carouselClasses.join(" ")), h()
				},
				_resize: function (e) {
					I.iterate.call(P, n)
				},
				disable: f,
				enable: g,
				jump: e,
				previous: i,
				next: o,
				jumpPage: e,
				previousPage: i,
				nextPage: o,
				jumpItem: function (e, t, i, n, o) {
					if (e.enabled) {
						I.clearTimer(e.autoTimer);
						var a = e.$items.eq(t - 1);
						void 0 === o && (o = !0);
						for (var s = 0; s < e.pageCount; s++)
							if (e.pages[s].$items.is(a)) {
								c(e, s, o, i, n);
								break
							}
					}
				},
				reset: function (e) {
					e.enabled && t.call(this, e, !1)
				},
				resize: n,
				update: t,
				panStart: function (e, t) {
					if (I.clearTimer(e.autoTimer), !e.single) {
						if (e.rtl && (t *= -1), e.useMargin) e.leftPosition = parseInt(e.$canister.css("marginLeft"));
						else {
							var i = e.$canister.css(z).split(",");
							e.leftPosition = parseInt(i[4])
						}
						e.$canister.css(A, "none"), d({
							data: e,
							deltaX: e.pageWidth * t
						}, !0)
					}
					e.isTouching = !0
				},
				pan: function (e, t) {
					if (!e.single) {
						e.rtl && (t *= -1);
						var i = e.pageWidth * t;
						e.touchLeft = x(e, e.leftPosition + i), e.useMargin ? e.$canister.css({
							marginLeft: e.touchLeft
						}) : e.$canister.css(z, "translateX(" + e.touchLeft + "px)")
					}
				},
				panEnd: function (e, t) {
					y(e, t)
				},
				swipe: function (e, t) {
					w({
						data: e,
						directionX: t
					}, !0)
				}
			}
		}),
		_ = S.namespace,
		H = S.namespaceClean,
		W = S.classes,
		j = W.raw,
		M = S.events,
		I = S.functions,
		P = [],
		z = u.transform,
		A = u.transition
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (s, e) {
	"use strict";

	function r(e) {
		e.stopPropagation();
		var t = e.data;
		s(e.target).is(t.$el) || (e.preventDefault(), t.$el.trigger("click"))
	}

	function l(e) {
		var t = e.data,
			i = t.$el.is(":disabled"),
			n = t.$el.is(":checked");
		i || (t.radio ? n && o(e) : n ? o(e) : d(e))
	}

	function o(e) {
		e.data.radio && s('input[name="' + e.data.group + '"]').not(e.data.$el).trigger("deselect"), e.data.$el.trigger(f.focus), e.data.$classable.addClass(h.checked)
	}

	function d(e) {
		e.data.$el.trigger(f.focus), e.data.$classable.removeClass(h.checked)
	}

	function c(e) {
		e.data.$classable.addClass(h.focus)
	}

	function p(e) {
		e.data.$classable.removeClass(h.focus)
	}
	var t = e.Plugin("checkbox", {
			widget: !0,
			defaults: {
				customClass: "",
				toggle: !1,
				labels: {
					on: "ON",
					off: "OFF"
				},
				theme: "fs-light"
			},
			classes: ["element_placeholder", "label", "marker", "flag", "radio", "focus", "checked", "disabled", "toggle", "state", "state_on", "state_off"],
			methods: {
				_construct: function (e) {
					var t = this.closest("label"),
						i = t.length ? t.eq(0) : s("label[for=" + this.attr("id") + "]"),
						n = [h.base, e.theme, e.customClass].join(" "),
						o = [h.label, e.theme, e.customClass].join(" "),
						a = "";
					e.radio = "radio" === this.attr("type"), e.group = this.attr("name"), a += '<div class="' + h.marker + '" aria-hidden="true">', a += '<div class="' + h.flag + '"></div>', e.toggle && (n += " " + h.toggle, o += " " + h.toggle, a += '<span class="' + [h.state, h.state_on].join(" ") + '">' + e.labels.on + "</span>", a += '<span class="' + [h.state, h.state_off].join(" ") + '">' + e.labels.off + "</span>"), e.radio && (n += " " + h.radio, o += " " + h.radio), a += "</div>", e.$placeholder = s('<span class="' + h.element_placeholder + '"></span>'), this.before(e.$placeholder), e.labelParent = i.find(this).length, e.labelClass = o, i.addClass(o), e.labelParent ? i.wrap('<div class="' + n + '"></div>').before(a) : this.before('<div class=" ' + n + '">' + a + "</div>"), e.$checkbox = e.labelParent ? i.parents(u.base) : this.prev(u.base), e.$marker = e.$checkbox.find(u.marker), e.$states = e.$checkbox.find(u.state), e.$label = i, e.$classable = s().add(e.$checkbox).add(e.$label), this.is(":checked") && e.$classable.addClass(h.checked), this.is(":disabled") && e.$classable.addClass(h.disabled), this.appendTo(e.$marker), this.on(f.focus, e, c).on(f.blur, e, p).on(f.change, e, l).on(f.click, e, r).on(f.deselect, e, d), e.$checkbox.on(f.click, e, r)
				},
				_destruct: function (e) {
					e.$checkbox.off(f.namespace), e.$marker.remove(), e.$states.remove(), e.$label.removeClass(e.labelClass), e.labelParent ? e.$label.unwrap() : this.unwrap(), e.$placeholder.before(this), e.$placeholder.remove(), this.off(f.namespace)
				},
				enable: function (e) {
					this.prop("disabled", !1), e.$classable.removeClass(h.disabled)
				},
				disable: function (e) {
					this.prop("disabled", !0), e.$classable.addClass(h.disabled)
				},
				update: function (e) {
					var t = e.$el.is(":disabled"),
						i = e.$el.is(":checked");
					t || (i ? o({
						data: e
					}) : d({
						data: e
					}))
				}
			},
			events: {
				deselect: "deselect"
			}
		}),
		u = t.classes,
		h = u.raw,
		f = t.events;
	t.functions
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (s, e) {
	"use strict";

	function t() {
		u = p.height(), c.iterate.call(g, r)
	}

	function i() {
		g = s(a.base), t()
	}

	function r(e) {
		if (e.initialized) {
			switch (e.windowIntersect) {
				case "top":
					e.windowCheck = 0 - e.offset;
					break;
				case "middle":
				case "center":
					e.windowCheck = u / 2 - e.offset;
					break;
				case "bottom":
					e.windowCheck = u - e.offset
			}
			switch (e.elOffset = e.$target.offset(), e.elIntersect) {
				case "top":
					e.elCheck = e.elOffset.top;
					break;
				case "middle":
					e.elCheck = e.elOffset.top + e.$target.outerHeight() / 2;
					break;
				case "bottom":
					e.elCheck = e.elOffset.top + e.$target.outerHeight()
			}
			n(e)
		}
	}

	function n(e) {
		e.initialized && (h + e.windowCheck >= e.elCheck ? (e.active || e.$el.trigger(d.activate), e.active = !0, e.$el.addClass(l.active)) : e.reverse && (e.active && e.$el.trigger(d.deactivate), e.active = !1, e.$el.removeClass(l.active)))
	}
	var o = e.Plugin("checkpoint", {
			widget: !0,
			defaults: {
				intersect: "bottom-top",
				offset: 0,
				reverse: !1
			},
			classes: ["active"],
			events: {
				activate: "activate",
				deactivate: "deactivate"
			},
			methods: {
				_construct: function (e) {
					e.initialized = !1;
					var t = s(e.$el.data("checkpoint-container")),
						i = e.$el.data("checkpoint-intersect"),
						n = e.$el.data("checkpoint-offset");
					i && (e.intersect = i), n && (e.offset = n);
					var o = e.intersect.split("-");
					e.windowIntersect = o[0], e.elIntersect = o[1], e.visible = !1, e.$target = t.length ? t : e.$el;
					var a = e.$target.find("img");
					a.length && a.on(d.load, e, r), e.$el.addClass(l.base), e.initialized = !0
				},
				_postConstruct: function (e) {
					i(), t()
				},
				_destruct: function (e) {
					i()
				},
				_resize: t,
				_raf: function () {
					(h = p.scrollTop()) < 0 && (h = 0), h !== f && (c.iterate.call(g, n), f = h)
				},
				resize: r
			}
		}),
		a = (o.namespace, o.classes),
		l = a.raw,
		d = o.events,
		c = o.functions,
		p = (e.window, e.$window),
		u = 0,
		h = 0,
		f = 0,
		g = []
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (c, e) {
	"use strict";

	function o(e, t, i) {
		var n = !1,
			o = new Date;
		i.expires && "number" === c.type(i.expires) && (o.setTime(o.getTime() + i.expires), n = o.toGMTString());
		var a = i.domain ? "; domain=" + i.domain : "",
			s = n ? "; expires=" + n : "",
			r = n ? "; max-age=" + i.expires / 1e3 : "",
			l = i.path ? "; path=" + i.path : "",
			d = i.secure ? "; secure" : "";
		p.cookie = e + "=" + t + s + r + a + l + d
	}
	e.Plugin("cookie", {
		utilities: {
			_delegate: function (e, t, i) {
				if ("object" === c.type(e)) a = c.extend(a, e);
				else if (i = c.extend({}, a, i || {}), "undefined" !== c.type(e)) {
					if ("undefined" === c.type(t)) return function (e) {
						for (var t = e + "=", i = p.cookie.split(";"), n = 0; n < i.length; n++) {
							for (var o = i[n];
								" " === o.charAt(0);) o = o.substring(1, o.length);
							if (0 === o.indexOf(t)) return o.substring(t.length, o.length)
						}
						return null
					}(e);
					null === t ? (n = i, o(e, "", c.extend({}, n, {
						expires: -6048e5
					}))) : o(e, t, i)
				}
				var n;
				return null
			}
		}
	});
	var a = {
			domain: null,
			expires: 6048e5,
			path: null,
			secure: null
		},
		p = e.document
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./scrollbar", "./touch"], e) : e(jQuery, Formstone)
}(function (p, u) {
	"use strict";

	function h(e) {
		for (var t = "", i = 0, n = e.$allOptions.length; i < n; i++) {
			var o = e.$allOptions.eq(i),
				a = [];
			if ("OPTGROUP" === o[0].tagName) a.push(k.group), o.prop("disabled") && a.push(k.disabled), t += '<span class="' + a.join(" ") + '">' + o.attr("label") + "</span>";
			else {
				var s = o.val(),
					r = o.data("label"),
					l = e.links ? "a" : 'button type="button"';
				o.attr("value") || o.attr("value", s), a.push(k.item), o.hasClass(k.item_placeholder) && (a.push(k.item_placeholder), l = "span"), o.prop("selected") && a.push(k.item_selected), o.prop("disabled") && a.push(k.item_disabled), t += "<" + l + ' class="' + a.join(" ") + '"', e.links ? "span" === l ? t += ' aria-hidden="true"' : (t += ' href="' + s + '"', e.external && (t += ' target="_blank"')) : t += ' data-value="' + s + '"', t += ' role="option"', o.prop("selected") && (t += ' "aria-selected"="true"'), t += ">", t += r || d.decodeEntities($(o.text(), e.trim)), t += "</" + l + ">"
			}
		}
		e.$items = e.$wrapper.html(p.parseHTML(t)).find(C.item)
	}

	function f(e) {
		d.killEvent(e);
		var t = e.data;
		t.disabled || t.useNative || (t.closed ? n(t) : s(t)), i(t)
	}

	function i(e) {
		p(C.base).not(e.$dropdown).trigger(x.close, [e])
	}

	function n(e) {
		if (e.closed) {
			var t = T.height(),
				i = e.$wrapper.outerHeight(!0);
			e.$dropdown[0].getBoundingClientRect().bottom + i > t - e.bottomEdge && e.$dropdown.addClass(k.bottom), S.on(x.click + e.dotGuid, ":not(" + C.options + ")", e, o), e.$dropdown.trigger(x.focusIn), e.$dropdown.addClass(k.open), r(e), e.closed = !1
		}
	}

	function s(e) {
		e && !e.closed && (S.off(x.click + e.dotGuid), e.$dropdown.removeClass([k.open, k.bottom].join(" ")), e.closed = !0)
	}

	function o(e) {
		d.killEvent(e);
		var t = e.data;
		t && 0 === p(e.currentTarget).parents(C.base).length && (s(t), t.$dropdown.trigger(x.focusOut))
	}

	function g(e) {
		var t = e.data;
		t && (s(t), t.$dropdown.trigger(x.focusOut))
	}

	function m(e) {
		var t = p(this),
			i = e.data;
		if (d.killEvent(e), !i.disabled) {
			var n = i.$items.index(t);
			i.focusIndex = n, i.$wrapper.is(":visible") && (y(n, i, e.shiftKey, e.metaKey || e.ctrlKey), l(i)), i.multiple || s(i), i.$dropdown.trigger(x.focus)
		}
	}

	function v(e, t) {
		p(this);
		var i = e.data;
		if (!t && !i.multiple) {
			var n = i.$options.index(i.$options.filter(":selected"));
			y(i.focusIndex = n, i), l(i, !0)
		}
	}

	function b(e) {
		d.killEvent(e), p(e.currentTarget);
		var t = e.data;
		t.disabled || t.multiple || t.focused || (i(t), t.focused = !0, t.focusIndex = t.index, t.input = "", t.$dropdown.addClass(k.focus).on(x.keyDown + t.dotGuid, t, a))
	}

	function w(e) {
		d.killEvent(e), p(e.currentTarget);
		var t = e.data;
		t.focused && t.closed && (t.focused = !1, t.$dropdown.removeClass(k.focus).off(x.keyDown + t.dotGuid), t.multiple || (s(t), t.index !== t.focusIndex && (l(t), t.focusIndex = t.index)))
	}

	function a(e) {
		var t = e.data;
		if (t.keyTimer = d.startTimer(t.keyTimer, 1e3, function () {
				t.input = ""
			}), 13 === e.keyCode) t.closed || (s(t), y(t.index, t)), l(t);
		else if (!(9 === e.keyCode || e.metaKey || e.altKey || e.ctrlKey || e.shiftKey)) {
			d.killEvent(e);
			var i = t.$items.length - 1,
				n = t.index < 0 ? 0 : t.index;
			if (-1 < p.inArray(e.keyCode, u.isFirefox ? [38, 40, 37, 39] : [38, 40]))(n += 38 === e.keyCode || u.isFirefox && 37 === e.keyCode ? -1 : 1) < 0 && (n = 0), i < n && (n = i);
			else {
				var o, a = String.fromCharCode(e.keyCode).toUpperCase();
				for (t.input += a, o = t.index + 1; o <= i; o++)
					if (t.$options.eq(o).text().substr(0, t.input.length).toUpperCase() === t.input) {
						n = o;
						break
					}
				if (n < 0 || n === t.index)
					for (o = 0; o <= i; o++)
						if (t.$options.eq(o).text().substr(0, t.input.length).toUpperCase() === t.input) {
							n = o;
							break
						}
			}
			0 <= n && (y(n, t), r(t))
		}
	}

	function y(e, t, i, n) {
		var o = t.$items.eq(e),
			a = t.$options.eq(e),
			s = o.hasClass(k.item_selected);
		if (!o.hasClass(k.item_disabled))
			if (t.multiple)
				if (t.useNative) s ? (a.prop("selected", null).attr("aria-selected", null), o.removeClass(k.item_selected)) : (a.prop("selected", !0).attr("aria-selected", !0), o.addClass(k.item_selected));
				else if (i && !1 !== t.lastIndex) {
			var r = t.lastIndex > e ? e : t.lastIndex,
				l = (t.lastIndex > e ? t.lastIndex : e) + 1;
			t.$options.prop("selected", null).attr("aria-selected", null), t.$items.filter(C.item_selected).removeClass(k.item_selected), t.$options.slice(r, l).not("[disabled]").prop("selected", !0), t.$items.slice(r, l).not(C.item_disabled).addClass(k.item_selected)
		} else t.lastIndex = (n ? s ? (a.prop("selected", null).attr("aria-selected", null), o.removeClass(k.item_selected)) : (a.prop("selected", !0).attr("aria-selected", !0), o.addClass(k.item_selected)) : (t.$options.prop("selected", null).attr("aria-selected", null), t.$items.filter(C.item_selected).removeClass(k.item_selected), a.prop("selected", !0).attr("aria-selected", !0), o.addClass(k.item_selected)), e);
		else if (-1 < e && e < t.$items.length) {
			if (e !== t.index) {
				var d = a.data("label") || o.html();
				t.$selected.html(d).removeClass(C.item_placeholder), t.$items.filter(C.item_selected).removeClass(k.item_selected), t.$el[0].selectedIndex = e, o.addClass(k.item_selected), t.index = e
			}
		} else "" !== t.label && t.$selected.html(t.label)
	}

	function r(e) {
		var t = e.$items.eq(e.index),
			i = 0 <= e.index && !t.hasClass(k.item_placeholder) ? t.position() : {
				left: 0,
				top: 0
			},
			n = (e.$wrapper.outerHeight() - t.outerHeight()) / 2;
		void 0 !== p.fn.fsScrollbar ? e.$wrapper.fsScrollbar("resize").fsScrollbar("scroll", e.$wrapper.find(".fs-scrollbar-content").scrollTop() + i.top) : e.$wrapper.scrollTop(e.$wrapper.scrollTop() + i.top - n)
	}

	function l(e, t) {
		var i, n;
		e.links ? (n = (i = e).$el.val(), i.external ? c.open(n) : c.location.href = n) : t || e.$el.trigger(x.raw.change, [!0])
	}

	function $(e, t) {
		return 0 === t ? e : e.length > t ? e.substring(0, t) + "..." : e
	}
	var e = u.Plugin("dropdown", {
			widget: !0,
			defaults: {
				bottomEdge: 0,
				cover: !1,
				customClass: "",
				label: "",
				external: !1,
				links: !1,
				mobile: !1,
				native: !1,
				theme: "fs-light",
				trim: 0
			},
			methods: {
				_construct: function (e) {
					e.multiple = this.prop("multiple"), e.disabled = this.prop("disabled") || this.is("[readonly]"), e.lastIndex = !1, e.native = e.mobile || e.native, e.useNative = e.native || u.isMobile, e.multiple ? e.links = !1 : e.external && (e.links = !0);
					var t = this.find("[selected]").not(":disabled"),
						i = this.find(":selected").not(":disabled"),
						n = i.text(),
						o = this.find("option").index(i);
					e.multiple || "" === e.label || t.length ? e.label = "" : (i = this.prepend('<option value="" class="' + k.item_placeholder + '" selected>' + e.label + "</option>"), n = e.label, o = 0);
					var a = this.find("option, optgroup"),
						s = a.filter("option"),
						r = p('[for="' + this.attr("id") + '"]');
					e.tabIndex = this[0].tabIndex, this[0].tabIndex = -1, r.length && (r[0].tabIndex = -1);
					var l = [k.base, e.theme, e.customClass];
					e.useNative ? l.push(k.native) : e.cover && l.push(k.cover), e.multiple && l.push(k.multiple), e.disabled && l.push(k.disabled), e.id = this.attr("id"), e.id ? e.ariaId = e.id : e.ariaId = e.rawGuid, e.ariaId += "-dropdown", e.selectedAriaId = e.ariaId + "-selected";
					var d = "",
						c = "";
					d += '<div class="' + l.join(" ") + '"id="' + e.ariaId + '" tabindex="' + e.tabIndex + '" role="listbox"', e.multiple ? d += ' aria-label="multi select"' : d += ' aria-haspopup="true" aria-live="polite" aria-labelledby="' + e.selectedAriaId + '"', d += "></div>", e.multiple || (c += '<button type="button" class="' + k.selected + '" id="' + e.selectedAriaId + '" tabindex="-1">', c += p("<span></span>").text($(n, e.trim)).html(), c += "</button>"), c += '<div class="' + k.options + '">', c += "</div>", this.wrap(d).after(c), e.$dropdown = this.parent(C.base), e.$label = r, e.$allOptions = a, e.$options = s, e.$selected = e.$dropdown.find(C.selected), e.$wrapper = e.$dropdown.find(C.options), e.$placeholder = e.$dropdown.find(C.placeholder), e.index = -1, e.closed = !0, e.focused = !1, h(e), e.multiple || y(o, e), void 0 !== p.fn.fsScrollbar && e.$wrapper.fsScrollbar({
						theme: e.theme
					}).find(".fs-scrollbar-content").attr("tabindex", null), e.$dropdown.on(x.click, e, f), e.$selected.on(x.click, e, f), e.$dropdown.on(x.click, C.item, e, m).on(x.close, e, g), this.on(x.change, e, v), e.useNative || (this.on(x.focusIn, e, function (e) {
						e.data.$dropdown.trigger(x.raw.focus)
					}), e.$dropdown.on(x.focusIn, e, b).on(x.focusOut, e, w))
				},
				_destruct: function (e) {
					e.$dropdown.hasClass(k.open) && e.$selected.trigger(x.click), void 0 !== p.fn.fsScrollbar && e.$wrapper.fsScrollbar("destroy"), e.$el[0].tabIndex = e.tabIndex, e.$label.length && (e.$label[0].tabIndex = e.tabIndex), e.$dropdown.off(x.namespace), e.$options.off(x.namespace), e.$placeholder.remove(), e.$selected.remove(), e.$wrapper.remove(), e.$el.off(x.namespace).show().unwrap()
				},
				disable: function (e, t) {
					if (void 0 !== t) {
						var i = e.$items.index(e.$items.filter("[data-value=" + t + "]"));
						e.$items.eq(i).addClass(k.item_disabled), e.$options.eq(i).prop("disabled", !0)
					} else e.$dropdown.hasClass(k.open) && e.$selected.trigger(x.click), e.$dropdown.addClass(k.disabled), e.$el.prop("disabled", !0), e.disabled = !0
				},
				enable: function (e, t) {
					if (void 0 !== t) {
						var i = e.$items.index(e.$items.filter("[data-value=" + t + "]"));
						e.$items.eq(i).removeClass(k.item_disabled), e.$options.eq(i).prop("disabled", !1)
					} else e.$dropdown.removeClass(k.disabled), e.$el.prop("disabled", !1), e.disabled = !1
				},
				update: function (e) {
					void 0 !== p.fn.fsScrollbar && e.$wrapper.fsScrollbar("destroy");
					var t = e.index;
					e.$allOptions = e.$el.find("option, optgroup"), e.$options = e.$allOptions.filter("option"), e.index = -1, t = e.$options.index(e.$options.filter(":selected")), h(e), e.multiple || y(t, e), void 0 !== p.fn.fsScrollbar && e.$wrapper.fsScrollbar({
						theme: e.theme
					}).find(".fs-scrollbar-content").attr("tabindex", null)
				},
				open: n,
				close: s
			},
			classes: ["cover", "bottom", "multiple", "mobile", "native", "open", "disabled", "focus", "selected", "options", "group", "item", "item_disabled", "item_selected", "item_placeholder"],
			events: {
				close: "close"
			}
		}),
		C = e.classes,
		k = C.raw,
		x = e.events,
		d = e.functions,
		c = u.window,
		T = u.$window,
		S = (u.document, null);
	u.Ready(function () {
		S = u.$body
	})
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery"], e) : e(jQuery, Formstone)
}(function (t, e) {
	"use strict";

	function i() {
		d = t(s.element)
	}

	function n(e) {
		if (e.data && (e = e.data), e.enabled)
			for (var t, i, n, o = 0; o < e.target.length; o++) {
				i = t = 0, (n = e.$el.find(e.target[o])).css(e.property, "");
				for (var a = 0; a < n.length; a++)(i = n.eq(a)[e.type]()) > t && (t = i);
				n.css(e.property, t)
			}
	}

	function o(e) {
		for (var t = 0; t < e.target.length; t++) e.$el.find(e.target[t]).css(e.property, "");
		e.$el.find("img").off(r.namespace)
	}
	var a = e.Plugin("equalize", {
			widget: !0,
			priority: 5,
			defaults: {
				maxWidth: 1 / 0,
				minWidth: "0px",
				property: "height",
				target: null
			},
			methods: {
				_construct: function (e) {
					e.maxWidth = e.maxWidth === 1 / 0 ? "100000px" : e.maxWidth, e.mq = "(min-width:" + e.minWidth + ") and (max-width:" + e.maxWidth + ")", e.type = "height" === e.property ? "outerHeight" : "outerWidth", e.target ? t.isArray(e.target) || (e.target = [e.target]) : e.target = ["> *"], i(), t.fsMediaquery("bind", e.rawGuid, e.mq, {
						enter: function () {
							(function (e) {
								if (!e.enabled) {
									e.enabled = !0;
									var t = e.$el.find("img");
									t.length && t.on(r.load, e, n), n(e)
								}
							}).call(e.$el, e)
						},
						leave: function () {
							(function (e) {
								e.enabled && (e.enabled = !1, o(e))
							}).call(e.$el, e)
						}
					})
				},
				_destruct: function (e) {
					o(e), t.fsMediaquery("unbind", e.rawGuid), i()
				},
				_resize: function (e) {
					l.iterate.call(d, n)
				},
				resize: n
			}
		}),
		s = a.classes,
		r = (s.raw, a.events),
		l = a.functions,
		d = []
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./touch", "./transition", "./viewer"], e) : e(jQuery, Formstone)
}(function (y, $) {
	"use strict";

	function i(e) {
		if (!X) {
			var t = e.data;
			!0 === t.overlay && (t.mobile = !0), (X = y.extend({}, {
				visible: !1,
				gallery: {
					active: !1
				},
				isMobile: $.isMobile || t.mobile,
				isTouch: $.support.touch,
				isAnimating: !0,
				isZooming: !1,
				oldContentHeight: 0,
				oldContentWidth: 0,
				metaHeight: 0,
				thumbnailHeight: 0,
				captionOpen: !1,
				thumbnailsOpen: !1,
				tapTimer: null
			}, t)).isViewer = X.isMobile && t.viewer && void 0 !== y.fn.fsViewer;
			var i = t.$el,
				n = t.$object,
				o = i && i[0].href && i[0].href || "",
				a = i && i[0].hash && i[0].hash || "",
				s = (o.toLowerCase().split(".").pop().split(/\#|\?/), i ? i.data(A + "-type") : ""),
				r = "image" === s || o.match(t.fileTypes) || "data:image" === o.substr(0, 10),
				l = P(o),
				d = "url" === s || !r && !l && "http" === o.substr(0, 4) && !a,
				c = "element" === s || !r && !l && !d && "#" === a.substr(0, 1),
				p = void 0 !== n;
			if (c && (o = a), !(r || l || d || c || p)) return void(X = null);
			if (q.killEvent(e), X.margin *= 2, X.type = r ? "image" : l ? "video" : "element", r || l) {
				var u = i.data(A + "-gallery");
				u && (X.gallery.active = !0, X.gallery.id = u, X.gallery.$items = y("a[data-lightbox-gallery= " + X.gallery.id + "], a[rel= " + X.gallery.id + "]"), X.gallery.index = X.gallery.$items.index(X.$el), X.gallery.total = X.gallery.$items.length - 1)
			}
			X.thumbnails && (r || l) && X.gallery.active || (X.thumbnails = !1);
			var h = "";
			X.isMobile || (h += '<div class="' + [O.overlay, X.theme, X.customClass].join(" ") + '"></div>');
			var f = [O.base, O.loading, O.animating, X.theme, X.customClass];
			if (X.fixed && f.push(O.fixed), X.isMobile && f.push(O.mobile), X.isTouch && f.push(O.touch), d && f.push(O.iframed), (c || p) && f.push(O.inline), X.thumbnails && f.push(O.thumbnailed), X.labels.lightbox = X.labels.lightbox.replace("{guid}", t.numGuid), h += '<div class="' + f.join(" ") + '" role="dialog" aria-label="' + X.labels.lightbox + '" tabindex="-1">', h += '<button type="button" class="' + O.close + '">' + X.labels.close + "</button>", h += '<span class="' + O.loading_icon + '"></span>', h += '<div class="' + O.container + '">', X.gallery.active && X.thumbnails) {
				h += '<div class="' + [O.thumbnails] + '">', h += '<div class="' + [O.thumbnail_container] + '">';
				for (var g, m, v = 0, b = X.gallery.$items.length; v < b; v++)(m = (g = X.gallery.$items.eq(v)).data("lightbox-thumbnail")) || (m = g.find("img").attr("src")), h += '<button class="' + [O.thumbnail_item] + '">', h += '<img src="' + m + '" alt="">', h += "</button>";
				h += "</div></div>"
			}
			h += '<div class="' + O.content + '"></div>', (r || l) && (h += '<div class="' + O.tools + '">', h += '<div class="' + O.controls + '">', X.gallery.active && (h += '<button type="button" class="' + [O.control, O.control_previous].join(" ") + '">' + X.labels.previous + "</button>", h += '<button type="button" class="' + [O.control, O.control_next].join(" ") + '">' + X.labels.next + "</button>"), X.isMobile && X.isTouch && (h += '<button type="button" class="' + [O.toggle, O.caption_toggle].join(" ") + '">' + X.labels.captionClosed + "</button>", X.gallery.active && X.thumbnails && (h += '<button type="button" class="' + [O.toggle, O.thumbnail_toggle].join(" ") + '">' + X.labels.thumbnailsClosed + "</button>")), h += "</div>", h += '<div class="' + O.meta + '">', h += '<div class="' + O.meta_content + '">', X.gallery.active && (h += '<p class="' + O.position + '"', X.gallery.total < 1 && (h += ' style="display: none;"'), h += ">", h += '<span class="' + O.position_current + '">' + (X.gallery.index + 1) + "</span> ", h += X.labels.count, h += ' <span class="' + O.position_total + '">' + (X.gallery.total + 1) + "</span>", h += "</p>"), h += '<div class="' + O.caption + '">', h += X.formatter.call(i, t), h += "</div></div></div>", h += "</div>"), h += "</div></div>", R.append(h), X.$overlay = y(L.overlay), X.$lightbox = y(L.base), X.$close = y(L.close), X.$container = y(L.container), X.$content = y(L.content), X.$tools = y(L.tools), X.$meta = y(L.meta), X.$metaContent = y(L.meta_content), X.$position = y(L.position), X.$caption = y(L.caption), X.$controlBox = y(L.controls), X.$controls = y(L.control), X.$thumbnails = y(L.thumbnails), X.$thumbnailContainer = y(L.thumbnail_container), X.$thumbnailItems = y(L.thumbnail_item), X.isMobile ? (X.paddingVertical = X.$close.outerHeight(), X.paddingHorizontal = 0, X.mobilePaddingVertical = parseInt(X.$content.css("paddingTop"), 10) + parseInt(X.$content.css("paddingBottom"), 10), X.mobilePaddingHorizontal = parseInt(X.$content.css("paddingLeft"), 10) + parseInt(X.$content.css("paddingRight"), 10)) : (X.paddingVertical = parseInt(X.$lightbox.css("paddingTop"), 10) + parseInt(X.$lightbox.css("paddingBottom"), 10), X.paddingHorizontal = parseInt(X.$lightbox.css("paddingLeft"), 10) + parseInt(X.$lightbox.css("paddingRight"), 10), X.mobilePaddingVertical = 0, X.mobilePaddingHorizontal = 0), X.contentHeight = X.$lightbox.outerHeight() - X.paddingVertical, X.contentWidth = X.$lightbox.outerWidth() - X.paddingHorizontal, X.controlHeight = X.$controls.outerHeight(), w = k(), X.$lightbox.css({
				top: X.fixed ? 0 : w.top
			}), X.gallery.active && (X.$lightbox.addClass(O.has_controls), j()), D.on(E.keyDown, M), R.on(E.click, [L.overlay, L.close].join(", "), C).on([E.focus, E.focusIn].join(" "), z), X.gallery.active && X.$lightbox.on(E.click, L.control, H), X.thumbnails && X.$lightbox.on(E.click, L.thumbnail_item, W), X.isMobile && X.isTouch && X.$lightbox.on(E.click, L.caption_toggle, x).on(E.click, L.thumbnail_toggle, T), X.$lightbox.fsTransition({
				property: "opacity"
			}, function () {
				var e, t;
				r ? S(o) : l ? _(o) : d ? (t = o, t += -1 < t.indexOf("?") ? "&" + X.requestKey + "=true" : "?" + X.requestKey + "=true", I(y('<iframe class="' + O.iframe + '" src="' + t + '"></iframe>'))) : c ? (e = o, X.$inlineTarget = y(e), X.$inlineContents = X.$inlineTarget.children().detach(), I(X.$inlineContents)) : p && I(X.$object)
			}).addClass(O.open), X.$overlay.addClass(O.open)
		}
		var w
	}

	function e(e) {
		"object" != typeof e && (X.targetHeight = e, X.targetWidth = arguments[1]), "element" === X.type ? c(X.$content.find("> :first-child")) : "image" === X.type ? s() : "video" === X.type && r(),
			function () {
				if (X.visible && !X.isMobile) {
					var e = k();
					X.$controls.css({
						marginTop: (X.contentHeight - X.controlHeight - X.metaHeight + X.thumbnailHeight) / 2
					}), X.$lightbox.css({
						height: X.contentHeight + X.paddingVertical,
						width: X.contentWidth + X.paddingHorizontal,
						top: X.fixed ? 0 : e.top
					}), X.oldContentHeight = X.contentHeight, X.oldContentWidth = X.contentWidth
				}
			}()
	}

	function C(e) {
		q.killEvent(e), X && (X.$lightbox.fsTransition("destroy"), X.$content.fsTransition("destroy"), X.$lightbox.addClass(O.animating).fsTransition({
			property: "opacity"
		}, function (e) {
			void 0 !== X.$inlineTarget && X.$inlineTarget.length && X.$inlineTarget.append(X.$inlineContents.detach()), X.isViewer && X.$imageContainer && X.$imageContainer.length && X.$imageContainer.fsViewer("destroy"), X.$lightbox.off(E.namespace), X.$container.off(E.namespace), D.off(E.keyDown), R.off(E.namespace), R.off(E.namespace), X.$overlay.remove(), X.$lightbox.remove(), void 0 !== X.$el && X.$el && X.$el.length && X.$el.focus(), X = null, D.trigger(E.close)
		}), X.$lightbox.removeClass(O.open), X.$overlay.removeClass(O.open), X.isMobile && (g.removeClass(O.lock), q.unlockViewport(A)))
	}

	function o() {
		var e = k();
		X.isMobile || X.duration, X.isMobile ? q.lockViewport(A) : X.$controls.css({
			marginTop: (X.contentHeight - X.controlHeight - X.metaHeight + X.thumbnailHeight) / 2
		}), "" === X.$caption.html() ? (X.$caption.hide(), X.$lightbox.removeClass(O.has_caption), X.gallery.active || X.$tools.hide()) : (X.$caption.show(), X.$lightbox.addClass(O.has_caption)), X.$lightbox.fsTransition({
			property: X.contentHeight !== X.oldContentHeight ? "height" : "width"
		}, function () {
			var e;
			X.$content.fsTransition({
				property: "opacity"
			}, function () {
				X.$lightbox.removeClass(O.animating), X.isAnimating = !1
			}), X.$lightbox.removeClass(O.loading).addClass(O.ready), X.visible = !0, D.trigger(E.open), X.gallery.active && (e = "", 0 < X.gallery.index && (P(e = X.gallery.$items.eq(X.gallery.index - 1).attr("href")) || y('<img src="' + e + '">')), X.gallery.index < X.gallery.total && (P(e = X.gallery.$items.eq(X.gallery.index + 1).attr("href")) || y('<img src="' + e + '">')), l(), function () {
				if (X.thumbnails) {
					var e = X.$thumbnailItems.eq(X.gallery.index),
						t = e.position().left + e.outerWidth(!1) / 2 - X.$thumbnailContainer.outerWidth(!0) / 2;
					X.$thumbnailContainer.stop().animate({
						scrollLeft: t
					}, 200, "linear")
				}
			}()), X.$lightbox.focus()
		}), X.isMobile || X.$lightbox.css({
			height: X.contentHeight + X.paddingVertical,
			width: X.contentWidth + X.paddingHorizontal,
			top: X.fixed ? 0 : e.top
		});
		var t = X.oldContentHeight !== X.contentHeight || X.oldContentWidth !== X.contentWidth;
		!X.isMobile && t || X.$lightbox.fsTransition("resolve"), X.oldContentHeight = X.contentHeight, X.oldContentWidth = X.contentWidth, X.isMobile && g.addClass(O.lock)
	}

	function k() {
		if (X.isMobile) return {
			left: 0,
			top: 0
		};
		var e = {
			left: ($.windowWidth - X.contentWidth - X.paddingHorizontal) / 2,
			top: X.top <= 0 ? ($.windowHeight - X.contentHeight - X.paddingVertical) / 2 : X.top
		};
		return !0 !== X.fixed && (e.top += D.scrollTop()), e
	}

	function x(e) {
		if (q.killEvent(e), X.captionOpen) n();
		else {
			a();
			var t = parseInt(X.$metaContent.outerHeight(!0));
			t += parseInt(X.$meta.css("padding-top")), t += parseInt(X.$meta.css("padding-bottom")), X.$meta.css({
				height: t
			}), X.$lightbox.addClass(O.caption_open).find(L.caption_toggle).text(X.labels.captionOpen), X.captionOpen = !0
		}
	}

	function n() {
		X.$lightbox.removeClass(O.caption_open).find(L.caption_toggle).text(X.labels.captionClosed), X.captionOpen = !1
	}

	function T(e) {
		q.killEvent(e), X.thumbnailsOpen ? a() : (n(), X.$lightbox.addClass(O.thumbnails_open).find(L.thumbnail_toggle).text(X.labels.thumbnailsOpen), X.thumbnailsOpen = !0)
	}

	function a() {
		X.$lightbox.removeClass(O.thumbnails_open).find(L.thumbnail_toggle).text(X.labels.thumbnailsClosed), X.thumbnailsOpen = !1
	}

	function S(e) {
		X.isViewer ? (X.$imageContainer = y('<div class="' + O.image_container + '"><img></div>'), X.$image = X.$imageContainer.find("img"), X.$image.attr("src", e).addClass(O.image), X.$content.prepend(X.$imageContainer), s(), X.$imageContainer.one("loaded.viewer", function () {
			o()
		}).fsViewer({
			theme: X.theme
		})) : (X.$imageContainer = y('<div class="' + O.image_container + '"><img></div>'), X.$image = X.$imageContainer.find("img"), X.$image.one(E.load, function () {
			var e, t, i, n = (e = X.$image, t = e[0], i = new Image, void 0 !== t.naturalHeight ? {
				naturalHeight: t.naturalHeight,
				naturalWidth: t.naturalWidth
			} : "img" === t.tagName.toLowerCase() && (i.src = t.src, {
				naturalHeight: i.height,
				naturalWidth: i.width
			}));
			X.naturalHeight = n.naturalHeight, X.naturalWidth = n.naturalWidth, X.retina && (X.naturalHeight /= 2, X.naturalWidth /= 2), X.$content.prepend(X.$imageContainer), s(), o()
		}).on(E.error, p).attr("src", e).addClass(O.image), (X.$image[0].complete || 4 === X.$image[0].readyState) && X.$image.trigger(E.load))
	}

	function s() {
		if (X.$image) {
			var e = 0;
			for (X.windowHeight = X.viewportHeight = $.windowHeight - X.mobilePaddingVertical - X.paddingVertical, X.windowWidth = X.viewportWidth = $.windowWidth - X.mobilePaddingHorizontal - X.paddingHorizontal, X.contentHeight = 1 / 0, X.contentWidth = 1 / 0, X.imageMarginTop = 0, X.imageMarginLeft = 0; X.contentHeight > X.viewportHeight && e < 2;) X.imageHeight = 0 === e ? X.naturalHeight : X.$image.outerHeight(), X.imageWidth = 0 === e ? X.naturalWidth : X.$image.outerWidth(), X.metaHeight = 0 === e ? 0 : X.metaHeight, X.spacerHeight = 0 === e ? 0 : X.spacerHeight, X.thumbnailHeight = 0 === e ? 0 : X.thumbnailHeight, 0 === e && (X.ratioHorizontal = X.imageHeight / X.imageWidth, X.ratioVertical = X.imageWidth / X.imageHeight, X.isWide = X.imageWidth > X.imageHeight), X.imageHeight < X.minHeight && (X.minHeight = X.imageHeight), X.imageWidth < X.minWidth && (X.minWidth = X.imageWidth), X.isMobile ? (X.isTouch ? (X.$controlBox.css({
				width: $.windowWidth
			}), X.spacerHeight = X.$controls.outerHeight(!0)) : (X.$tools.css({
				width: $.windowWidth
			}), X.spacerHeight = X.$tools.outerHeight(!0)), X.contentHeight = X.viewportHeight, X.contentWidth = X.viewportWidth, X.isTouch || X.$content.css({
				height: X.contentHeight - X.spacerHeight
			}), X.$thumbnails.length && (X.spacerHeight += X.$thumbnails.outerHeight(!0)), X.spacerHeight += 10, t(), X.imageMarginTop = (X.contentHeight - X.targetImageHeight - X.spacerHeight) / 2, X.imageMarginLeft = (X.contentWidth - X.targetImageWidth) / 2) : (0 === e && (X.viewportHeight -= X.margin + X.paddingVertical, X.viewportWidth -= X.margin + X.paddingHorizontal), X.viewportHeight -= X.metaHeight, X.thumbnails && (X.viewportHeight -= X.thumbnailHeight), t(), X.contentHeight = X.targetImageHeight, X.contentWidth = X.targetImageWidth), X.isMobile || X.isTouch || X.$meta.css({
				width: X.contentWidth
			}), X.$image.css({
				height: X.targetImageHeight,
				width: X.targetImageWidth,
				marginTop: X.imageMarginTop,
				marginLeft: X.imageMarginLeft
			}), X.isMobile || (X.metaHeight = X.$meta.outerHeight(!0), X.contentHeight += X.metaHeight), X.thumbnails && (X.thumbnailHeight = X.$thumbnails.outerHeight(!0), X.contentHeight += X.thumbnailHeight), e++
		}
	}

	function t() {
		var e = X.isMobile ? X.contentHeight - X.spacerHeight : X.viewportHeight,
			t = X.isMobile ? X.contentWidth : X.viewportWidth;
		X.isWide ? (X.targetImageWidth = t, X.targetImageHeight = X.targetImageWidth * X.ratioHorizontal, X.targetImageHeight > e && (X.targetImageHeight = e, X.targetImageWidth = X.targetImageHeight * X.ratioVertical)) : (X.targetImageHeight = e, X.targetImageWidth = X.targetImageHeight * X.ratioVertical, X.targetImageWidth > t && (X.targetImageWidth = t, X.targetImageHeight = X.targetImageWidth * X.ratioHorizontal)), (X.targetImageWidth > X.imageWidth || X.targetImageHeight > X.imageHeight) && (X.targetImageHeight = X.imageHeight, X.targetImageWidth = X.imageWidth), (X.targetImageWidth < X.minWidth || X.targetImageHeight < X.minHeight) && (X.targetImageWidth < X.minWidth ? (X.targetImageWidth = X.minWidth, X.targetImageHeight = X.targetImageWidth * X.ratioHorizontal) : (X.targetImageHeight = X.minHeight, X.targetImageWidth = X.targetImageHeight * X.ratioVertical))
	}

	function _(e) {
		var t = P(e),
			i = e.split("?");
		t ? (2 <= i.length && (t += "?" + i.slice(1)[0].trim()), X.$videoWrapper = y('<div class="' + O.video_wrapper + '"></div>'), X.$video = y('<iframe class="' + O.video + '" frameborder="0" seamless="seamless" allowfullscreen></iframe>'), X.$video.attr("src", t).addClass(O.video).prependTo(X.$videoWrapper), X.$content.prepend(X.$videoWrapper), r(), o()) : p()
	}

	function r() {
		X.windowHeight = X.viewportHeight = $.windowHeight - X.mobilePaddingVertical - X.paddingVertical, X.windowWidth = X.viewportWidth = $.windowWidth - X.mobilePaddingHorizontal - X.paddingHorizontal, X.videoMarginTop = 0, X.videoMarginLeft = 0, X.isMobile ? (X.isTouch ? (X.$controlBox.css({
			width: $.windowWidth
		}), X.spacerHeight = X.$controls.outerHeight(!0) + 10) : (X.$tools.css({
			width: $.windowWidth
		}), X.spacerHeight = X.$tools.outerHeight(!0), X.spacerHeight += X.$thumbnails.outerHeight(!0) + 10), X.viewportHeight -= X.spacerHeight, X.targetVideoWidth = X.viewportWidth, X.targetVideoHeight = X.targetVideoWidth * X.videoRatio, X.targetVideoHeight > X.viewportHeight && (X.targetVideoHeight = X.viewportHeight, X.targetVideoWidth = X.targetVideoHeight / X.videoRatio), X.videoMarginTop = (X.viewportHeight - X.targetVideoHeight) / 2, X.videoMarginLeft = (X.viewportWidth - X.targetVideoWidth) / 2) : (X.viewportHeight = X.windowHeight - X.margin, X.viewportWidth = X.windowWidth - X.margin, X.targetVideoWidth = X.videoWidth > X.viewportWidth ? X.viewportWidth : X.videoWidth, X.targetVideoWidth < X.minWidth && (X.targetVideoWidth = X.minWidth), X.targetVideoHeight = X.targetVideoWidth * X.videoRatio, X.contentHeight = X.targetVideoHeight, X.contentWidth = X.targetVideoWidth), X.isMobile || X.isTouch || X.$meta.css({
			width: X.contentWidth
		}), X.$videoWrapper.css({
			height: X.targetVideoHeight,
			width: X.targetVideoWidth,
			marginTop: X.videoMarginTop,
			marginLeft: X.videoMarginLeft
		}), X.isMobile || (X.metaHeight = X.$meta.outerHeight(!0), X.contentHeight += X.metaHeight), X.thumbnails && (X.thumbnailHeight = X.$thumbnails.outerHeight(!0), X.contentHeight += X.thumbnailHeight)
	}

	function H(e) {
		q.killEvent(e);
		var t = y(e.currentTarget);
		X.isAnimating || t.hasClass(O.control_disabled) || (X.isAnimating = !0, n(), X.gallery.index += t.hasClass(O.control_next) ? 1 : -1, X.gallery.index > X.gallery.total && (X.gallery.index = X.infinite ? 0 : X.gallery.total), X.gallery.index < 0 && (X.gallery.index = X.infinite ? X.gallery.total : 0), l(), X.$lightbox.addClass(O.animating), X.$content.fsTransition({
			property: "opacity"
		}, d), X.$lightbox.addClass(O.loading))
	}

	function W(e) {
		q.killEvent(e);
		var t = y(e.currentTarget);
		X.isAnimating || t.hasClass(O.active) || (X.isAnimating = !0, n(), X.gallery.index = X.$thumbnailItems.index(t), l(), X.$lightbox.addClass(O.animating), X.$content.fsTransition({
			property: "opacity"
		}, d), X.$lightbox.addClass(O.loading))
	}

	function l() {
		if (X.thumbnails) {
			var e = X.$thumbnailItems.eq(X.gallery.index);
			X.$thumbnailItems.removeClass(O.active), e.addClass(O.active)
		}
	}

	function d() {
		void 0 !== X.$imageContainer && (X.isViewer && X.$imageContainer.fsViewer("destroy"), X.$imageContainer.remove()), void 0 !== X.$videoWrapper && X.$videoWrapper.remove(), X.$el = X.gallery.$items.eq(X.gallery.index), X.$caption.html(X.formatter.call(X.$el, X)), X.$position.find(L.position_current).html(X.gallery.index + 1);
		var e = X.$el.attr("href");
		P(e) ? (X.type = "video", _(e)) : (X.type = "image", S(e)), j()
	}

	function j() {
		X.$controls.removeClass(O.control_disabled), X.infinite || (0 === X.gallery.index && X.$controls.filter(L.control_previous).addClass(O.control_disabled), X.gallery.index === X.gallery.total && X.$controls.filter(L.control_next).addClass(O.control_disabled))
	}

	function M(e) {
		!X.gallery.active || 37 !== e.keyCode && 39 !== e.keyCode ? 27 === e.keyCode && X.$close.trigger(E.click) : (q.killEvent(e), X.$controls.filter(37 === e.keyCode ? L.control_previous : L.control_next).trigger(E.click))
	}

	function I(e) {
		X.$content.append(e), c(e), o()
	}

	function c(e) {
		X.windowHeight = $.windowHeight - X.mobilePaddingVertical - X.paddingVertical, X.windowWidth = $.windowWidth - X.mobilePaddingHorizontal - X.paddingHorizontal, X.objectHeight = e.outerHeight(!0), X.objectWidth = e.outerWidth(!0), X.targetHeight = X.targetHeight || (X.$el ? X.$el.data(A + "-height") : null), X.targetWidth = X.targetWidth || (X.$el ? X.$el.data(A + "-width") : null), X.maxHeight = X.windowHeight < 0 ? X.minHeight : X.windowHeight, X.isIframe = e.is("iframe"), X.objectMarginTop = 0, X.objectMarginLeft = 0, X.isMobile || (X.windowHeight -= X.margin, X.windowWidth -= X.margin), X.contentHeight = X.targetHeight ? X.targetHeight : X.isIframe || X.isMobile ? X.windowHeight : X.objectHeight, X.contentWidth = X.targetWidth ? X.targetWidth : X.isIframe || X.isMobile ? X.windowWidth : X.objectWidth, (X.isIframe || X.isObject) && X.isMobile ? (X.contentHeight = X.windowHeight, X.contentWidth = X.windowWidth) : X.isObject && (X.contentHeight = X.contentHeight > X.windowHeight ? X.windowHeight : X.contentHeight, X.contentWidth = X.contentWidth > X.windowWidth ? X.windowWidth : X.contentWidth), X.isMobile || (X.contentHeight > X.maxHeight && (X.contentHeight = X.maxHeight), X.contentWidth > X.maxWidth && (X.contentWidth = X.maxWidth))
	}

	function p() {
		var e = y('<div class="' + O.error + '"><p>Error Loading Resource</p></div>');
		X.type = "element", X.$tools.remove(), X.$image.off(E.namespace), I(e), D.trigger(E.error)
	}

	function P(e) {
		var t, i = X.videoFormatter;
		for (var n in i)
			if (i.hasOwnProperty(n) && null !== (t = e.match(i[n].pattern))) return i[n].format.call(X, t);
		return !1
	}

	function z(e) {
		var t = e.target;
		y.contains(X.$lightbox[0], t) || t === X.$lightbox[0] || t === X.$overlay[0] || (q.killEvent(e), X.$lightbox.focus())
	}
	var u = $.Plugin("lightbox", {
			widget: !0,
			defaults: {
				customClass: "",
				fileTypes: /\.(jpg|sjpg|jpeg|png|gif)/i,
				fixed: !1,
				formatter: function () {
					var e = this.attr("title"),
						t = !(void 0 === e || !e) && e.replace(/^\s+|\s+$/g, "");
					return t ? '<p class="caption">' + t + "</p>" : ""
				},
				infinite: !1,
				labels: {
					close: "Close",
					count: "of",
					next: "Next",
					previous: "Previous",
					captionClosed: "View Caption",
					captionOpen: "Close Caption",
					thumbnailsClosed: "View Thumbnails",
					thumbnailsOpen: "Close Thumbnails",
					lightbox: "Lightbox {guid}"
				},
				margin: 50,
				maxHeight: 1e4,
				maxWidth: 1e4,
				minHeight: 100,
				minWidth: 100,
				mobile: !1,
				overlay: !1,
				retina: !1,
				requestKey: "fs-lightbox",
				theme: "fs-light",
				thumbnails: !1,
				top: 0,
				videoFormatter: {
					youtube: {
						pattern: /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/,
						format: function (e) {
							return "//www.youtube.com/embed/" + e[1]
						}
					},
					vimeo: {
						pattern: /(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)/,
						format: function (e) {
							return "//player.vimeo.com/video/" + e[3]
						}
					}
				},
				videoRatio: .5625,
				videoWidth: 800,
				viewer: !0
			},
			classes: ["loading", "animating", "fixed", "mobile", "touch", "inline", "iframed", "open", "ready", "overlay", "close", "loading_icon", "container", "content", "image", "image_container", "video", "video_wrapper", "tools", "meta", "meta_content", "controls", "control", "control_previous", "control_next", "control_disabled", "position", "position_current", "position_total", "toggle", "caption_toggle", "caption", "caption_open", "thumbnailed", "thumbnails_open", "thumbnail_toggle", "thumbnails", "thumbnail_container", "thumbnail_item", "active", "has_controls", "has_caption", "iframe", "error", "active", "lock"],
			events: {
				open: "open",
				close: "close"
			},
			methods: {
				_construct: function (e) {
					this.on(E.click, e, i);
					var t = this.data(A + "-gallery");
					!v && m && t === m && (v = !0, this.trigger(E.click))
				},
				_destruct: function (e) {
					C(), this.off(E.namespace)
				},
				_resize: function () {
					X && e()
				},
				resize: e
			},
			utilities: {
				_initialize: function (e, t) {
					e instanceof y && i.apply(f, [{
						data: y.extend(!0, {}, {
							$object: e
						}, h, t || {})
					}])
				},
				close: C
			}
		}),
		A = u.namespace,
		h = u.defaults,
		L = u.classes,
		O = L.raw,
		E = u.events,
		q = u.functions,
		f = $.window,
		D = $.$window,
		R = null,
		g = null,
		m = !1,
		v = !1,
		X = null;
	$.Ready(function () {
		R = $.$body, g = y("html, body"), m = $.window.location.hash.replace("#", "")
	})
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery", "./swap"], e) : e(jQuery, Formstone)
}(function (a, e) {
	"use strict";

	function i(e) {
		e.$handle.fsSwap("deactivate")
	}

	function s(e) {
		e.data.$handle.addClass(g.focus)
	}

	function r(e) {
		e.data.$handle.removeClass(g.focus)
	}

	function l(e) {
		var t = e.data;
		13 !== e.keyCode && 32 !== e.keyCode || (v.killEvent(e), t.$handle.trigger(m.raw.click))
	}

	function d(e) {
		if (!e.originalEvent) {
			var t = e.data;
			t.open || (t.$el.trigger(m.open).attr("aria-hidden", !1), t.$content.addClass(t.contentClassesOpen).one(m.click, function () {
				i(t)
			}), t.$handle.attr("aria-expanded", !0), t.label && t.$handle.html(t.labels.open), t.isToggle || b.addClass(g.lock), t.open = !0, t.$nav.focus())
		}
	}

	function c(e) {
		if (!e.originalEvent) {
			var t = e.data;
			t.open && (t.$el.trigger(m.close).attr("aria-hidden", !0), t.$content.removeClass(t.contentClassesOpen).off(m.namespace), t.$handle.attr("aria-expanded", !1), t.label && t.$handle.html(t.labels.closed), n(t), t.open = !1, t.$el.focus())
		}
	}

	function p(e) {
		var t = e.data;
		t.$el.attr("aria-hidden", !0), t.$handle.attr("aria-controls", t.ariaId).attr("aria-expanded", !1), t.$content.addClass(g.enabled), setTimeout(function () {
			t.$animate.addClass(g.animated)
		}, 0), t.label && t.$handle.html(t.labels.closed)
	}

	function u(e) {
		var t = e.data;
		t.$el.removeAttr("aria-hidden"), t.$handle.removeAttr("aria-controls").removeAttr("aria-expanded"), t.$content.removeClass(g.enabled, g.animated), t.$animate.removeClass(g.animated), o(t), n(t)
	}

	function n(e) {
		e.isToggle || b.removeClass(g.lock)
	}

	function o(e) {
		if (e.label)
			if (1 < e.$handle.length)
				for (var t = 0, i = e.$handle.length; t < i; t++) e.$handle.eq(t).html(e.originalLabel[t]);
			else e.$handle.html(e.originalLabel)
	}
	var t = e.Plugin("navigation", {
			widget: !0,
			defaults: {
				customClass: "",
				gravity: "left",
				label: !0,
				labels: {
					closed: "Menu",
					open: "Close"
				},
				maxWidth: "980px",
				theme: "fs-light",
				type: "toggle"
			},
			classes: ["handle", "nav", "content", "animated", "enabled", "focus", "open", "toggle", "push", "reveal", "overlay", "left", "right", "lock"],
			events: {
				open: "open",
				close: "close"
			},
			methods: {
				_construct: function (e) {
					e.handleGuid = g.handle + e.guid, e.isToggle = "toggle" === e.type, e.open = !1, e.isToggle && (e.gravity = "");
					var t = g.base,
						i = [t, e.type].join("-"),
						n = e.gravity ? [i, e.gravity].join("-") : "",
						o = [e.rawGuid, e.theme, e.customClass].join(" ");
					e.handle = this.data(h + "-handle"), e.content = this.data(h + "-content"), e.handleClasses = [g.handle, g.handle.replace(t, i), n ? g.handle.replace(t, n) : "", e.handleGuid, o].join(" "), e.thisClasses = [g.nav.replace(t, i), n ? g.nav.replace(t, n) : "", o], e.contentClasses = [g.content.replace(t, i), o].join(" "), e.contentClassesOpen = [n ? g.content.replace(t, n) : "", g.open].join(" "), e.$nav = this.addClass(e.thisClasses.join(" ")).attr("role", "navigation"), e.$handle = a(e.handle).addClass(e.handleClasses), e.$content = a(e.content).addClass(e.contentClasses), e.$animate = a().add(e.$nav).add(e.$content),
						function (e) {
							if (e.label)
								if (1 < e.$handle.length) {
									e.originalLabel = [];
									for (var t = 0, i = e.$handle.length; t < i; t++) e.originalLabel[t] = e.$handle.eq(t).html()
								} else e.originalLabel = e.$handle.html()
						}(e), e.navTabIndex = e.$nav.attr("tabindex"), e.$nav.attr("tabindex", -1), e.id = this.attr("id"), e.id ? e.ariaId = e.id : (e.ariaId = e.rawGuid, this.attr("id", e.ariaId)), e.$handle.attr("data-swap-target", e.dotGuid).attr("data-swap-linked", e.handleGuid).attr("data-swap-group", g.base).attr("tabindex", 0).on("activate.swap" + e.dotGuid, e, d).on("deactivate.swap" + e.dotGuid, e, c).on("enable.swap" + e.dotGuid, e, p).on("disable.swap" + e.dotGuid, e, u).on(m.focus + e.dotGuid, e, s).on(m.blur + e.dotGuid, e, r).fsSwap({
							maxWidth: e.maxWidth,
							classes: {
								target: e.dotGuid,
								enabled: f.enabled,
								active: f.open,
								raw: {
									target: e.rawGuid,
									enabled: g.enabled,
									active: g.open
								}
							}
						}), e.$handle.is("a, button") || e.$handle.on(m.keyPress + e.dotGuid, e, l)
				},
				_destruct: function (e) {
					e.$content.removeClass([e.contentClasses, e.contentClassesOpen].join(" ")).off(m.namespace), e.$handle.removeAttr("aria-controls").removeAttr("aria-expanded").removeAttr("data-swap-target").removeData("swap-target").removeAttr("data-swap-linked").removeAttr("data-swap-group").removeData("swap-linked").removeData("tabindex").removeClass(e.handleClasses).off(e.dotGuid).html(e.originalLabel).fsSwap("destroy"), e.$nav.attr("tabindex", e.navTabIndex), o(e), n(e), this.removeAttr("aria-hidden").removeClass(e.thisClasses.join(" ")).off(m.namespace), this.attr("id") === e.rawGuid && this.removeAttr("id")
				},
				open: function (e) {
					e.$handle.fsSwap("activate")
				},
				close: i,
				enable: function (e) {
					e.$handle.fsSwap("enable")
				},
				disable: function (e) {
					e.$handle.fsSwap("disable")
				}
			}
		}),
		h = t.namespace,
		f = t.classes,
		g = f.raw,
		m = t.events,
		v = t.functions,
		b = null;
	e.Ready(function () {
		b = a("html, body")
	})
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery"], e) : e(jQuery, Formstone)
}(function (o, e) {
	"use strict";

	function n(e) {
		u.killEvent(e);
		var t = e.data,
			i = t.index + (o(e.currentTarget).hasClass(c.control_previous) ? -1 : 1);
		0 <= i && t.$items.eq(i).trigger(p.raw.click)
	}

	function a(e) {
		u.killEvent(e);
		var t = e.data,
			i = o(e.currentTarget),
			n = parseInt(i.val(), 10);
		t.$items.eq(n).trigger(p.raw.click)
	}

	function s(e) {
		var t = e.data,
			i = o(e.currentTarget),
			n = t.$items.index(i);
		t.ajax ? u.killEvent(e) : i[0].click(), r(t, n)
	}

	function r(e, t) {
		if (t < 0 && (t = 0), t > e.total && (t = e.total), t !== e.index) {
			e.index = t;
			var i = e.index - e.visible,
				n = e.index + (e.visible + 1);
			i < 0 && (i = 0), n > e.total && (n = e.total), e.$items.removeClass(c.visible).removeClass(c.hidden).filter(d.active).removeClass(c.active).end().eq(e.index).addClass(c.active).end().slice(i, n).addClass(c.visible), e.$items.not(d.visible).addClass(c.hidden), e.$position.find(d.current).text(e.index + 1).end().find(d.total).text(e.total + 1), e.$select.val(e.index), e.$controls.removeClass(c.visible), 0 < t && e.$controls.filter(d.control_previous).addClass(c.visible), t < e.total && e.$controls.filter(d.control_next).addClass(c.visible), e.$ellipsis.removeClass(c.visible), t > e.visible + 1 && e.$ellipsis.eq(0).addClass(c.visible), t < e.total - e.visible - 1 && e.$ellipsis.eq(1).addClass(c.visible), e.$el.trigger(p.update, [e.index])
		}
	}
	var l = e.Plugin("pagination", {
			widget: !0,
			defaults: {
				ajax: !1,
				customClass: "",
				labels: {
					count: "of",
					next: "Next",
					previous: "Previous",
					select: "Select Page",
					pagination: "Pagination {guid}"
				},
				maxWidth: "740px",
				theme: "fs-light",
				visible: 2
			},
			classes: ["pages", "page", "active", "first", "last", "ellipsis", "visible", "hidden", "control", "control_previous", "control_next", "position", "select", "mobile", "current", "total"],
			events: {
				update: "update"
			},
			methods: {
				_construct: function (e) {
					e.mq = "(max-width:" + (e.maxWidth === 1 / 0 ? "100000px" : e.maxWidth) + ")";
					var t = "";
					t += '<button type="button" class="' + [c.control, c.control_previous].join(" ") + '">' + e.labels.previous + "</button>", t += '<button type="button" class="' + [c.control, c.control_next].join(" ") + '">' + e.labels.next + "</button>", t += '<div class="' + c.position + '" aria-hidden="true">', t += '<span class="' + c.current + '">0</span>', t += " " + e.labels.count + " ", t += '<span class="' + c.total + '">0</span>', t += '<select class="' + c.select + '" title="' + e.labels.select + '" tabindex="-1" aria-hidden="true"></select>', t += "</div>", e.thisClasses = [c.base, e.theme, e.customClass], e.labels.pagination = e.labels.pagination.replace("{guid}", e.numGuid), this.addClass(e.thisClasses.join(" ")).wrapInner('<div class="' + c.pages + '" aria-label="' + e.labels.pagination + '"></div>').prepend(t), e.$controls = this.find(d.control), e.$pages = this.find(d.pages), e.$items = e.$pages.children().addClass(c.page), e.$position = this.find(d.position), e.$select = this.find(d.select), e.index = -1, e.total = e.$items.length - 1;
					var i = e.$items.index(e.$items.filter("[data-" + l.namespace + "-active]"));
					i < 0 && (i = e.$items.index(e.$items.filter(d.active))), e.$items.eq(0).addClass(c.first).after('<span class="' + c.ellipsis + '">…</span>').end().eq(e.total).addClass(c.last).before('<span class="' + c.ellipsis + '">…</span>'), e.$ellipsis = e.$pages.find(d.ellipsis),
						function (e) {
							for (var t = "", i = 0; i <= e.total; i++) t += '<option value="' + i + '"', i === e.index && (t += 'selected="selected"'), t += ">Page " + (i + 1) + "</option>";
							e.$select.html(t)
						}(e), this.on(p.click, d.page, e, s).on(p.click, d.control, e, n).on(p.change, d.select, e, a), o.fsMediaquery("bind", e.rawGuid, e.mq, {
							enter: function () {
								e.$el.addClass(c.mobile)
							},
							leave: function () {
								e.$el.removeClass(c.mobile)
							}
						}), r(e, i)
				},
				_destruct: function (e) {
					o.fsMediaquery("unbind", e.rawGuid), e.$controls.remove(), e.$ellipsis.remove(), e.$select.remove(), e.$position.remove(), e.$items.removeClass([c.page, c.active, c.visible, c.first, c.last].join(" ")).unwrap(), this.removeClass(e.thisClasses.join(" ")).off(p.namespace)
				}
			}
		}),
		d = l.classes,
		c = d.raw,
		p = l.events,
		u = l.functions
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery"], e) : e(jQuery, Formstone)
}(function (i, e) {
	"use strict";

	function t() {
		h.iterate.call(v, s)
	}

	function n() {
		v = i(c.base), t()
	}

	function o(e) {
		e.enabled = !0, e.$el.addClass(p.enabled), s(e)
	}

	function a(e) {
		e.enabled = !1, e.$el.css({
			height: "",
			width: "",
			top: "",
			bottom: "",
			marginBottom: ""
		}).removeClass(p.enabled), e.$stickys.removeClass([p.passed, p.stuck].join(" "))
	}

	function s(e) {
		if (e.enabled) {
			if (r(e), e.$container.length) {
				var t = e.$container.offset();
				e.min = t.top - e.margin, e.max = e.min + e.$container.outerHeight(!1) - e.height
			} else {
				var i = (e.stuck ? e.$clone : e.$el).offset();
				e.min = i.top - e.margin, e.max = !1
			}
			l(e)
		}
	}

	function r(e) {
		var t;
		t = e.stuck ? e.$clone : e.$el, e.margin = parseInt(t.css("margin-top"), 10), e.$container.length ? e.containerMargin = parseInt(e.$container.css("margin-top"), 10) : e.containerMargin = 0, e.height = t.outerHeight(), e.width = t.outerWidth()
	}

	function l(e) {
		if (e.enabled) {
			var t = g + e.offset;
			if (t >= e.min) {
				e.stuck = !0, e.$stickys.addClass(p.stuck), e.stuck || (e.$el.trigger(u.stuck), r(e));
				var i = e.offset,
					n = "";
				e.max && t > e.max ? (e.passed || e.$el.trigger(u.passed), e.passed = !0, e.$stickys.addClass(p.passed), i = "", n = 0) : (e.passed = !1, e.$stickys.removeClass(p.passed)), e.$el.css({
					height: e.height,
					width: e.width,
					top: i,
					bottom: n,
					marginBottom: 0
				})
			} else e.stuck = !1, e.$stickys.removeClass(p.stuck).removeClass(p.passed), e.stuck && e.$el.trigger(u.unstuck), e.$el.css({
				height: "",
				width: "",
				top: "",
				bottom: "",
				marginBottom: ""
			})
		}
	}
	var d = e.Plugin("sticky", {
			widget: !0,
			defaults: {
				maxWidth: 1 / 0,
				minWidth: "0px",
				offset: 0
			},
			classes: ["enabled", "sticky", "stuck", "clone", "container", "passed"],
			events: {
				passed: "passed",
				stuck: "stuck",
				unstuck: "unstuck"
			},
			methods: {
				_construct: function (e) {
					e.enabled = !1, e.stuck = !1, e.passed = !0, e.$clone = e.$el.clone(), e.container = e.$el.data("sticky-container"), e.$container = i(e.container), e.$el.addClass(p.base), e.$clone.removeClass(p.element).addClass(p.clone), e.$container.addClass(p.container), e.$stickys = i().add(e.$el).add(e.$clone), e.$el.after(e.$clone);
					var t = i().add(e.$el.find("img")).add(e.$container.find("img"));
					t.length && t.on(u.load, e, s), e.maxWidth = e.maxWidth === 1 / 0 ? "100000px" : e.maxWidth, e.mq = "(min-width:" + e.minWidth + ") and (max-width:" + e.maxWidth + ")", i.fsMediaquery("bind", e.rawGuid, e.mq, {
						enter: function () {
							o.call(e.$el, e)
						},
						leave: function () {
							a.call(e.$el, e)
						}
					})
				},
				_postConstruct: function (e) {
					n(), t()
				},
				_destruct: function (e) {
					e.$clone.remove(), e.$container.removeClass(p.container), e.$el.css({
						height: "",
						width: "",
						top: "",
						bottom: "",
						marginBottom: ""
					}).removeClass(p.base), n()
				},
				_resize: t,
				_raf: function () {
					(g = f.scrollTop()) < 0 && (g = 0), g !== m && (h.iterate.call(v, l), m = g)
				},
				disable: a,
				enable: o,
				reset: s,
				resize: s
			}
		}),
		c = (d.namespace, d.classes),
		p = c.raw,
		u = d.events,
		h = d.functions,
		f = (e.window, e.$window),
		g = 0,
		m = 0,
		v = []
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./mediaquery"], e) : e(jQuery, Formstone)
}(function (n, e) {
	"use strict";

	function i(e, t) {
		if (e.enabled && !e.active) {
			e.group && !t && n(e.group).not(e.$el).not(e.linked)[r.namespaceClean]("deactivate", !0);
			var i = e.group ? n(e.group).index(e.$el) : null;
			e.$swaps.addClass(e.classes.raw.active), t || e.linked && n(e.linked).not(e.$el)[r.namespaceClean]("activate", !0), this.trigger(c.activate, [i]), e.active = !0
		}
	}

	function o(e, t) {
		e.enabled && e.active && (e.$swaps.removeClass(e.classes.raw.active), t || e.linked && n(e.linked).not(e.$el)[r.namespaceClean]("deactivate", !0), this.trigger(c.deactivate), e.active = !1)
	}

	function t(e, t) {
		e.enabled || (e.enabled = !0, e.$swaps.addClass(e.classes.raw.enabled), t || n(e.linked).not(e.$el)[r.namespaceClean]("enable"), this.trigger(c.enable), e.onEnable ? (e.active = !1, i.call(this, e)) : (e.active = !0, o.call(this, e)))
	}

	function a(e, t) {
		e.enabled && (e.enabled = !1, e.$swaps.removeClass([e.classes.raw.enabled, e.classes.raw.active].join(" ")), t || n(e.linked).not(e.$el)[r.namespaceClean]("disable"), this.trigger(c.disable))
	}

	function s(e) {
		p.killEvent(e);
		var t = e.data;
		t.active && t.collapse ? o.call(t.$el, t) : i.call(t.$el, t)
	}
	var r = e.Plugin("swap", {
			widget: !0,
			defaults: {
				collapse: !0,
				maxWidth: 1 / 0
			},
			classes: ["target", "enabled", "active"],
			events: {
				activate: "activate",
				deactivate: "deactivate",
				enable: "enable",
				disable: "disable"
			},
			methods: {
				_construct: function (e) {
					e.enabled = !1, e.active = !1, e.classes = n.extend(!0, {}, d, e.classes), e.target = this.data(l + "-target"), e.$target = n(e.target).addClass(e.classes.raw.target), e.mq = "(max-width:" + (e.maxWidth === 1 / 0 ? "100000px" : e.maxWidth) + ")";
					var t = this.data(l + "-linked");
					e.linked = !!t && "[data-" + l + '-linked="' + t + '"]';
					var i = this.data(l + "-group");
					e.group = !!i && "[data-" + l + '-group="' + i + '"]', e.$swaps = n().add(this).add(e.$target), this.on(c.click + e.dotGuid, e, s)
				},
				_postConstruct: function (e) {
					e.collapse || !e.group || n(e.group).filter("[data-" + l + "-active]").length || n(e.group).eq(0).attr("data-" + l + "-active", "true"), e.onEnable = this.data(l + "-active") || !1, n.fsMediaquery("bind", e.rawGuid, e.mq, {
						enter: function () {
							t.call(e.$el, e, !0)
						},
						leave: function () {
							a.call(e.$el, e, !0)
						}
					})
				},
				_destruct: function (e) {
					n.fsMediaquery("unbind", e.rawGuid), e.$swaps.removeClass([e.classes.raw.enabled, e.classes.raw.active].join(" ")).off(c.namespace)
				},
				activate: i,
				deactivate: o,
				enable: t,
				disable: a
			}
		}),
		l = r.namespace,
		d = r.classes,
		c = r.events,
		p = r.functions
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (m, a) {
	"use strict";

	function s(e) {
		e.preventManipulation && e.preventManipulation();
		var t = e.data,
			i = e.originalEvent;
		if (i.type.match(/(up|end|cancel)$/i)) f(e);
		else {
			if (i.pointerId) {
				var n = !1;
				for (var o in t.touches) t.touches[o].id === i.pointerId && (n = !0, t.touches[o].pageX = i.pageX, t.touches[o].pageY = i.pageY);
				n || t.touches.push({
					id: i.pointerId,
					pageX: i.pageX,
					pageY: i.pageY
				})
			} else t.touches = i.touches;
			i.type.match(/(down|start)$/i) ? v(e) : i.type.match(/move$/i) && r(e)
		}
	}

	function v(e) {
		var t = e.data,
			i = "undefined" !== m.type(t.touches) && t.touches.length ? t.touches[0] : null;
		i && t.$el.off($.mouseDown), t.touching || (t.startE = e.originalEvent, t.startX = i ? i.pageX : e.pageX, t.startY = i ? i.pageY : e.pageY, t.startT = (new Date).getTime(), t.scaleD = 1, t.passed = !1), t.$links && t.$links.off($.click);
		var n = w(t.scale ? $.scaleStart : $.panStart, e, t.startX, t.startY, t.scaleD, 0, 0, "", "");
		if (t.scale && t.touches && 2 <= t.touches.length) {
			var o = t.touches;
			t.pinch = {
				startX: g(o[0].pageX, o[1].pageX),
				startY: g(o[0].pageY, o[1].pageY),
				startD: y(o[1].pageX - o[0].pageX, o[1].pageY - o[0].pageY)
			}, n.pageX = t.startX = t.pinch.startX, n.pageY = t.startY = t.pinch.startY
		}
		t.touching || (t.touching = !0, t.pan && !i && k.on($.mouseMove, t, r).on($.mouseUp, t, f), a.support.pointer ? k.on([$.pointerMove, $.pointerUp, $.pointerCancel].join(" "), t, s) : k.on([$.touchMove, $.touchEnd, $.touchCancel].join(" "), t, s), t.$el.trigger(n))
	}

	function r(e) {
		var t = e.data,
			i = "undefined" !== m.type(t.touches) && t.touches.length ? t.touches[0] : null,
			n = i ? i.pageX : e.pageX,
			o = i ? i.pageY : e.pageY,
			a = n - t.startX,
			s = o - t.startY,
			r = 0 < a ? "right" : "left",
			l = 0 < s ? "down" : "up",
			d = Math.abs(a) > x,
			c = Math.abs(s) > x;
		if (!t.passed && t.axis && (t.axisX && c || t.axisY && d)) f(e);
		else {
			!t.passed && (!t.axis || t.axis && t.axisX && d || t.axisY && c) && (t.passed = !0), t.passed && (C.killEvent(e), C.killEvent(t.startE));
			var p = !0,
				u = w(t.scale ? $.scale : $.pan, e, n, o, t.scaleD, a, s, r, l);
			if (t.scale)
				if (t.touches && 2 <= t.touches.length) {
					var h = t.touches;
					t.pinch.endX = g(h[0].pageX, h[1].pageX), t.pinch.endY = g(h[0].pageY, h[1].pageY), t.pinch.endD = y(h[1].pageX - h[0].pageX, h[1].pageY - h[0].pageY), t.scaleD = t.pinch.endD / t.pinch.startD, u.pageX = t.pinch.endX, u.pageY = t.pinch.endY, u.scale = t.scaleD, u.deltaX = t.pinch.endX - t.pinch.startX, u.deltaY = t.pinch.endY - t.pinch.startY
				} else t.pan || (p = !1);
			p && t.$el.trigger(u)
		}
	}

	function f(e) {
		var t = e.data,
			i = "undefined" !== m.type(t.touches) && t.touches.length ? t.touches[0] : null,
			n = i ? i.pageX : e.pageX,
			o = i ? i.pageY : e.pageY,
			a = n - t.startX,
			s = o - t.startY,
			r = (new Date).getTime(),
			l = t.scale ? $.scaleEnd : $.panEnd,
			d = 0 < a ? "right" : "left",
			c = 0 < s ? "down" : "up",
			p = 1 < Math.abs(a),
			u = 1 < Math.abs(s);
		if (t.swipe && Math.abs(a) > x && r - t.startT < T && (l = $.swipe), t.axis && (t.axisX && u || t.axisY && p) || p || u) {
			t.$links = t.$el.find("a");
			for (var h = 0, f = t.$links.length; h < f; h++) b(t.$links.eq(h), t)
		}
		var g = w(l, e, n, o, t.scaleD, a, s, d, c);
		k.off([$.touchMove, $.touchEnd, $.touchCancel, $.mouseMove, $.mouseUp, $.pointerMove, $.pointerUp, $.pointerCancel].join(" ")), t.$el.trigger(g), t.touches = [], t.scale, i && (t.touchTimer = C.startTimer(t.touchTimer, 5, function () {
			t.$el.on($.mouseDown, t, v)
		})), t.touching = !1
	}

	function b(e, t) {
		e.on($.click, t, n);
		var i = m._data(e[0], "events").click;
		i.unshift(i.pop())
	}

	function n(e) {
		C.killEvent(e, !0), e.data.$links.off($.click)
	}

	function w(e, t, i, n, o, a, s, r, l) {
		return m.Event(e, {
			originalEvent: t,
			bubbles: !0,
			pageX: i,
			pageY: n,
			scale: o,
			deltaX: a,
			deltaY: s,
			directionX: r,
			directionY: l
		})
	}

	function g(e, t) {
		return (e + t) / 2
	}

	function y(e, t) {
		return Math.sqrt(e * e + t * t)
	}

	function i(e, t) {
		e.css({
			"-ms-touch-action": t,
			"touch-action": t
		})
	}
	var e = !a.window.PointerEvent,
		t = a.Plugin("touch", {
			widget: !0,
			defaults: {
				axis: !1,
				pan: !1,
				scale: !1,
				swipe: !1
			},
			methods: {
				_construct: function (e) {
					if (e.touches = [], e.touching = !1, this.on($.dragStart, C.killEvent), e.swipe && (e.pan = !0), e.scale && (e.axis = !1), e.axisX = "x" === e.axis, e.axisY = "y" === e.axis, a.support.pointer) {
						var t = "";
						!e.axis || e.axisX && e.axisY ? t = "none" : (e.axisX && (t += " pan-y"), e.axisY && (t += " pan-x")), i(this, t), this.on($.pointerDown, e, s)
					} else this.on($.touchStart, e, s).on($.mouseDown, e, v)
				},
				_destruct: function (e) {
					this.off($.namespace), i(this, "")
				}
			},
			events: {
				pointerDown: e ? "MSPointerDown" : "pointerdown",
				pointerUp: e ? "MSPointerUp" : "pointerup",
				pointerMove: e ? "MSPointerMove" : "pointermove",
				pointerCancel: e ? "MSPointerCancel" : "pointercancel"
			}
		}),
		$ = t.events,
		C = t.functions,
		k = a.$window,
		x = 10,
		T = 50;
	$.pan = "pan", $.panStart = "panstart", $.panEnd = "panend", $.scale = "scale", $.scaleStart = "scalestart", $.scaleEnd = "scaleend", $.swipe = "swipe"
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core"], e) : e(jQuery, Formstone)
}(function (r, o) {
	"use strict";

	function a(e) {
		e.stopPropagation(), e.preventDefault();
		var t = e.data,
			i = e.originalEvent,
			n = t.target ? t.$target : t.$el;
		t.property && i.propertyName !== t.property || !r(i.target).is(n) || s(t)
	}

	function s(e) {
		e.always || e.$el[t.namespaceClean]("destroy"), e.callback.apply(e.$el)
	}

	function l(e) {
		var t = d(e.$check);
		(function (e, t) {
			if (r.type(e) !== r.type(t)) return !1;
			for (var i in e) {
				if (!e.hasOwnProperty(i)) return !1;
				if (!e.hasOwnProperty(i) || !t.hasOwnProperty(i) || e[i] !== t[i]) return !1
			}
			return !0
		})(e.styles, t) || s(e), e.styles = t
	}

	function d(e) {
		var t, i, n, o = {};
		if (e instanceof r && (e = e[0]), u.getComputedStyle)
			for (var a = 0, s = (t = u.getComputedStyle(e, null)).length; a < s; a++) i = t[a], n = t.getPropertyValue(i), o[i] = n;
		else if (e.currentStyle)
			for (i in t = e.currentStyle) o[i] = t[i];
		return o
	}
	var t = o.Plugin("transition", {
			widget: !0,
			defaults: {
				always: !1,
				property: null,
				target: null
			},
			methods: {
				_construct: function (e, t) {
					if (t) {
						e.$target = this.find(e.target), e.$check = e.target ? e.$target : this, e.callback = t, e.styles = d(e.$check), e.timer = null;
						var i = e.$check.css(o.transition + "-duration"),
							n = parseFloat(i);
						o.support.transition && i && n ? this.on(c.transitionEnd, e, a) : e.timer = p.startTimer(e.timer, 50, function () {
							l(e)
						}, !0)
					}
				},
				_destruct: function (e) {
					p.clearTimer(e.timer, !0), this.off(c.namespace)
				},
				resolve: s
			}
		}),
		c = t.events,
		p = t.functions,
		u = o.window
}),
function (e) {
	"function" == typeof define && define.amd ? define(["jquery", "./core", "./transition"], e) : e(jQuery, Formstone)
}(function (l, o) {
	"use strict";

	function e() {
		P.scrollTop() + o.windowHeight < 0 && 0
	}

	function d() {
		(z = l(W.base)).length ? I.lockViewport(H) : I.unlockViewport(H)
	}

	function c(r, e, t) {
		r.isAnimating || (r.isAnimating = !0, r.$container = l('<div class="' + j.container + '"><img></div>'), r.$image = r.$container.find("img"), r.$viewport.append(r.$container), r.$image.one(M.load, function () {
			var e, t, i, n, o, a, s;
			o = (i = e = r).$image, a = o[0], s = new Image, n = void 0 !== a.naturalHeight ? {
				naturalHeight: a.naturalHeight,
				naturalWidth: a.naturalWidth
			} : "img" === a.tagName.toLowerCase() && (s.src = a.src, {
				naturalHeight: s.height,
				naturalWidth: s.width
			}), i.naturalHeight = n.naturalHeight, i.naturalWidth = n.naturalWidth, i.ratioHorizontal = i.naturalHeight / i.naturalWidth, i.ratioVertical = i.naturalWidth / i.naturalHeight, i.isWide = i.naturalWidth > i.naturalHeight, p(e), e.containerTop = e.viewportHeight / 2, e.containerLeft = e.viewportWidth / 2, h(e), e.imageHeight = e.naturalHeight, e.imageWidth = e.naturalWidth, (t = e).imageHeight = t.imageMinHeight, t.imageWidth = t.imageMinWidth, u(e), f(e), g(e), m(e), v(e, {
				containerTop: e.containerTop,
				containerLeft: e.containerLeft,
				imageHeight: e.imageHeight,
				imageWidth: e.imageWidth,
				imageTop: e.imageTop,
				imageLeft: e.imageLeft
			}), e.isRendering = !0, r.isAnimating = !1, r.$container.fsTransition({
				property: "opacity"
			}, function () {}), r.$el.removeClass(j.loading), r.$container.fsTouch({
				pan: !0,
				scale: !0
			}).on(M.scaleStart, r, b).on(M.scaleEnd, r, y).on(M.scale, r, w), r.$el.trigger(M.loaded)
		}).one(M.error, r, i).attr("src", e).addClass(j.image), (r.$image[0].complete || 4 === r.$image[0].readyState) && r.$image.trigger(M.load), r.source = e)
	}

	function i(e) {
		e.data.$el.trigger(M.error)
	}

	function p(e) {
		e.viewportHeight = e.$viewport.outerHeight(), e.viewportWidth = e.$viewport.outerWidth()
	}

	function u(e) {
		e.imageHeight <= e.viewportHeight ? (e.containerMinTop = e.viewportHeight / 2, e.containerMaxTop = e.viewportHeight / 2) : (e.containerMinTop = e.viewportHeight - e.imageHeight / 2, e.containerMaxTop = e.imageHeight / 2), e.imageWidth <= e.viewportWidth ? (e.containerMinLeft = e.viewportWidth / 2, e.containerMaxLeft = e.viewportWidth / 2) : (e.containerMinLeft = e.viewportWidth - e.imageWidth / 2, e.containerMaxLeft = e.imageWidth / 2)
	}

	function h(e) {
		e.isWide ? (e.imageMinWidth = e.viewportWidth, e.imageMinHeight = e.imageMinWidth * e.ratioHorizontal, e.imageMinHeight > e.viewportHeight && (e.imageMinHeight = e.viewportHeight, e.imageMinWidth = e.imageMinHeight * e.ratioVertical)) : (e.imageMinHeight = e.viewportHeight, e.imageMinWidth = e.imageMinHeight * e.ratioVertical, e.imageMinWidth > e.viewportWidth && (e.imageMinWidth = e.viewportWidth, e.imageMinHeight = e.imageMinWidth * e.ratioHorizontal)), (e.imageMinWidth > e.naturalWidth || e.imageMinHeight > e.naturalHeight) && (e.imageMinHeight = e.naturalHeight, e.imageMinWidth = e.naturalWidth), e.imageMaxHeight = e.naturalHeight, e.imageMaxWidth = e.naturalWidth
	}

	function f(e) {
		e.imageTop = -e.imageHeight / 2, e.imageLeft = -e.imageWidth / 2
	}

	function g(e) {
		e.lastContainerTop = e.containerTop, e.lastContainerLeft = e.containerLeft, e.lastImageHeight = e.imageHeight, e.lastImageWidth = e.imageWidth, e.lastImageTop = e.imageTop, e.lastImageLeft = e.imageLeft
	}

	function m(e) {
		e.renderContainerTop = e.lastContainerTop, e.renderContainerLeft = e.lastContainerLeft, e.renderImageTop = e.lastImageTop, e.renderImageLeft = e.lastImageLeft, e.renderImageHeight = e.lastImageHeight, e.renderImageWidth = e.lastImageWidth
	}

	function n(e) {
		e.imageHeight < e.imageMinHeight && (e.imageHeight = e.imageMinHeight), e.imageHeight > e.imageMaxHeight && (e.imageHeight = e.imageMaxHeight), e.imageWidth < e.imageMinWidth && (e.imageWidth = e.imageMinWidth), e.imageWidth > e.imageMaxWidth && (e.imageWidth = e.imageMaxWidth)
	}

	function a(e) {
		e.containerTop < e.containerMinTop && (e.containerTop = e.containerMinTop), e.containerTop > e.containerMaxTop && (e.containerTop = e.containerMaxTop), e.containerLeft < e.containerMinLeft && (e.containerLeft = e.containerMinLeft), e.containerLeft > e.containerMaxLeft && (e.containerLeft = e.containerMaxLeft)
	}

	function s(e) {
		var t, i;
		null === e.tapTimer ? e.tapTimer = I.startTimer(e.tapTimer, 500, function () {
			r(e)
		}) : (r(e), i = (t = e).imageHeight > t.imageMinHeight + 1, t.isZooming = !0, g(t), m(t), t.imageWidth = i ? (t.imageHeight = t.imageMinHeight, t.imageMinWidth) : (t.imageHeight = t.imageMaxHeight, t.imageMaxWidth), u(t), a(t), f(t), t.isRendering = !0)
	}

	function r(e) {
		I.clearTimer(e.tapTimer), e.tapTimer = null
	}

	function v(e, t) {
		if (o.transform) {
			var i = t.imageWidth / e.naturalWidth,
				n = t.imageHeight / e.naturalHeight;
			e.$container.css(o.transform, "translate3d(" + t.containerLeft + "px, " + t.containerTop + "px, 0)"), e.$image.css(o.transform, "translate3d(-50%, -50%, 0) scale(" + i + "," + n + ")")
		} else e.$container.css({
			top: t.containerTop,
			left: t.containerLeft
		}), e.$image.css({
			height: t.imageHeight,
			width: t.imageWidth,
			top: t.imageTop,
			left: t.imageLeft
		})
	}

	function b(e) {
		var t = e.data;
		s(t), g(t)
	}

	function w(e) {
		var t = e.data;
		r(t), t.isRendering = !1, t.isZooming = !1, t.imageHeight, t.imageMinHeight, t.containerTop = t.lastContainerTop + e.deltaY, t.containerLeft = t.lastContainerLeft + e.deltaX, t.imageHeight = t.lastImageHeight * e.scale, t.imageWidth = t.lastImageWidth * e.scale, u(t), a(t), n(t), f(t), v(t, {
			containerTop: t.containerTop,
			containerLeft: t.containerLeft,
			imageHeight: t.imageHeight,
			imageWidth: t.imageWidth,
			imageTop: t.imageTop,
			imageLeft: t.imageLeft
		})
	}

	function y(e) {
		var t = e.data;
		t.isZooming || (g(t), m(t), t.isRendering = !0)
	}

	function $(e) {
		I.killEvent(e);
		var t, i, n = l(e.currentTarget),
			o = e.data;
		"out" == (n.hasClass(j.control_zoom_out) ? "out" : "in") ? ((i = o).keyDownTime = 1, i.action = "zoom_out") : ((t = o).keyDownTime = 1, t.action = "zoom_in")
	}

	function C(e) {
		e.data.action = !1
	}

	function t(e) {
		if (e.isRendering) {
			if (e.action) {
				e.keyDownTime += e.zoomIncrement;
				var t = ("zoom_out" === e.action ? -1 : 1) * Math.round(e.imageWidth * e.keyDownTime - e.imageWidth);
				t > e.zoomDelta && (t = e.zoomDelta), e.isWide ? (e.imageWidth += t, e.imageHeight = Math.round(e.imageWidth / e.ratioVertical)) : (e.imageHeight += t, e.imageWidth = Math.round(e.imageHeight / e.ratioHorizontal)), n(e), f(e), u(e), a(e)
			}
			e.renderContainerTop += Math.round((e.containerTop - e.renderContainerTop) * e.zoomEnertia), e.renderContainerLeft += Math.round((e.containerLeft - e.renderContainerLeft) * e.zoomEnertia), e.renderImageTop += Math.round((e.imageTop - e.renderImageTop) * e.zoomEnertia), e.renderImageLeft += Math.round((e.imageLeft - e.renderImageLeft) * e.zoomEnertia), e.renderImageHeight += Math.round((e.imageHeight - e.renderImageHeight) * e.zoomEnertia), e.renderImageWidth += Math.round((e.imageWidth - e.renderImageWidth) * e.zoomEnertia), v(e, {
				containerTop: e.renderContainerTop,
				containerLeft: e.renderContainerLeft,
				imageHeight: e.renderImageHeight,
				imageWidth: e.renderImageWidth,
				imageTop: e.renderImageTop,
				imageLeft: e.renderImageLeft
			})
		}
	}

	function k(e, t) {
		var i;
		e.isAnimating || (r(e), e.isAnimating = !0, e.isRendering = !1, e.isZooming = !1, (i = e).$container && i.$container.length && i.$container.fsTouch("destroy").off(M.scaleStart, b).off(M.scaleEnd, y).off(M.scale, w), e.$container.fsTransition({
			property: "opacity"
		}, function () {
			e.isAnimating = !1, e.$container.remove(), "function" == typeof t && t.call(window, e)
		}), e.$el.addClass(j.loading))
	}

	function x(e) {
		I.killEvent(e);
		var t = l(e.currentTarget),
			i = e.data,
			n = i.index + (t.hasClass(j.control_next) ? 1 : -1);
		i.isAnimating || (n < 0 && (n = 0), n > i.total && (n = i.total), n !== i.index && (i.index = n, k(i, function () {
			c(i, i.images[i.index])
		})), T(i))
	}

	function T(e) {
		e.$controlItems.removeClass(j.control_disabled), 0 === e.index && e.$controlPrevious.addClass(j.control_disabled), e.index === e.images.length - 1 && e.$controlNext.addClass(j.control_disabled)
	}

	function S(e) {
		p(e), u(e), h(e), f(e), u(e), a(e), n(e)
	}
	var _ = o.Plugin("viewer", {
			widget: !0,
			defaults: {
				controls: !0,
				customClass: "",
				labels: {
					count: "of",
					next: "Next",
					previous: "Previous",
					zoom_in: "Zoom In",
					zoom_out: "Zoom Out"
				},
				theme: "fs-light",
				zoomDelta: 100,
				zoomEnertia: .2,
				zoomIncrement: .01
			},
			classes: ["source", "wrapper", "viewport", "container", "image", "gallery", "loading_icon", "controls", "controls_custom", "control", "control_previous", "control_next", "control_zoom_in", "control_zoom_out", "control_disabled", "loading"],
			events: {
				loaded: "loaded",
				ready: "ready"
			},
			methods: {
				_construct: function (e) {
					var t, i = "",
						n = [j.control, j.control_previous].join(" "),
						o = [j.control, j.control_next].join(" "),
						a = [j.control, j.control_zoom_in].join(" "),
						s = [j.control, j.control_zoom_out].join(" ");
					e.thisClasses = [j.base, j.loading, e.customClass, e.theme], e.images = [], e.source = !1, e.gallery = !1, e.tapTimer = null, e.action = !1, e.isRendering = !1, e.isZooming = !1, e.isAnimating = !1, e.keyDownTime = 1, e.$images = this.find("img").addClass(j.source), e.index = 0, e.total = e.$images.length - 1, e.customControls = "object" === l.type(e.controls) && e.controls.zoom_in && e.controls.zoom_out, 1 < e.$images.length && (e.gallery = !0, e.thisClasses.push(j.gallery), !e.customControls || e.controls.previous && e.controls.next || (e.customControls = !1));
					for (var r = 0; r < e.$images.length; r++) t = e.$images.eq(r), e.images.push(t.attr("src"));
					i += '<div class="' + j.wrapper + '">', i += '<div class="' + j.loading_icon + '"></div>', i += '<div class="' + j.viewport + '"></div>', i += "</div>", e.controls && !e.customControls && (i += '<div class="' + j.controls + '">', i += '<button type="button" class="' + n + '">' + e.labels.previous + "</button>", i += '<button type="button" class="' + s + '">' + e.labels.zoom_out + "</button>", i += '<button type="button" class="' + a + '">' + e.labels.zoom_in + "</button>", i += '<button type="button" class="' + o + '">' + e.labels.next + "</button>", i += "</div>"), this.addClass(e.thisClasses.join(" ")).prepend(i), e.$wrapper = this.find(W.wrapper), e.$viewport = this.find(W.viewport), e.customControls ? (e.$controls = l(e.controls.container).addClass([j.controls, j.controls_custom].join(" ")), e.$controlPrevious = l(e.controls.previous).addClass(n), e.$controlNext = l(e.controls.next).addClass(o), e.$controlZoomIn = l(e.controls.zoom_in).addClass(a), e.$controlZoomOut = l(e.controls.zoom_out).addClass(s)) : (e.$controls = this.find(W.controls), e.$controlPrevious = this.find(W.control_previous), e.$controlNext = this.find(W.control_next), e.$controlZoomIn = this.find(W.control_zoom_in), e.$controlZoomOut = this.find(W.control_zoom_out)), e.$controlItems = e.$controlPrevious.add(e.$controlNext), e.$controlZooms = e.$controlZoomIn.add(e.$controlZoomOut), d(), e.$controlItems.on(M.click, e, x), e.$controlZooms.on([M.touchStart, M.mouseDown].join(" "), e, $).on([M.touchEnd, M.mouseUp].join(" "), e, C), c(e, e.images[e.index]), T(e)
				},
				_destruct: function (e) {
					e.$wrapper.remove(), e.$image.removeClass(j.source), e.controls && !e.customControls && e.$controls.remove(), e.customControls && (e.$controls.removeClass([j.controls, j.controls_custom].join(" ")), e.$controlItems.off(M.click).removeClass([j.control, j.control_previous, j.control_next].join(" ")), e.$controlZooms.off([M.touchStart, M.mouseDown].join(" ")).off([M.touchStart, M.mouseDown].join(" ")).off([M.touchEnd, M.mouseUp].join(" ")).removeClass([j.control, j.control_zoom_in, j.control_zoom_out].join(" "))), this.removeClass(e.thisClasses.join(" ")).off(M.namespace), d()
				},
				_resize: function () {
					I.iterate.call(z, S)
				},
				_raf: function () {
					I.iterate.call(z, t)
				},
				resize: S,
				load: function (e, t) {
					e.index = 0, "string" == typeof t ? (e.total = 0, e.images = [t], e.gallery = !1, e.$el.removeClass(j.gallery)) : (e.total = t.length - 1, 1 < (e.images = t).length && (e.gallery = !0, e.$el.addClass(j.gallery)), t = e.images[e.index]), k(e, function () {
						c(e, t)
					})
				},
				unload: function (e) {
					k(e)
				}
			}
		}),
		H = _.namespace,
		W = _.classes,
		j = W.raw,
		M = _.events,
		I = _.functions,
		P = (o.window, o.$window),
		z = [];
	o.Ready(function () {
		e(), P.on("scroll", e), o.$body
	})
}),
function (e, t) {
	"object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define("whatInput", [], t) : "object" == typeof exports ? exports.whatInput = t() : e.whatInput = t()
}(this, function () {
	return function (i) {
		var n = {};

		function o(e) {
			if (n[e]) return n[e].exports;
			var t = n[e] = {
				exports: {},
				id: e,
				loaded: !1
			};
			return i[e].call(t.exports, t, t.exports, o), t.loaded = !0, t.exports
		}
		return o.m = i, o.c = n, o.p = "", o(0)
	}([function (e, t) {
		"use strict";
		e.exports = function () {
			if ("undefined" == typeof document || "undefined" == typeof window) return {
				ask: function () {
					return "initial"
				},
				element: function () {
					return null
				},
				ignoreKeys: function () {},
				registerOnChange: function () {},
				unRegisterOnChange: function () {}
			};
			var t = document.documentElement,
				i = null,
				a = "initial",
				s = a,
				n = null,
				r = ["input", "select", "textarea"],
				o = [],
				l = [16, 17, 18, 91, 93],
				d = {
					keydown: "keyboard",
					keyup: "keyboard",
					mousedown: "mouse",
					mousemove: "mouse",
					MSPointerDown: "pointer",
					MSPointerMove: "pointer",
					pointerdown: "pointer",
					pointermove: "pointer",
					touchstart: "touch"
				},
				c = !1,
				p = !1,
				u = {
					x: null,
					y: null
				},
				h = {
					2: "touch",
					3: "touch",
					4: "mouse"
				},
				f = !1;
			try {
				var e = Object.defineProperty({}, "passive", {
					get: function () {
						f = !0
					}
				});
				window.addEventListener("test", null, e)
			} catch (e) {}
			var g = function () {
					var e = !!f && {
						passive: !0
					};
					window.PointerEvent ? (window.addEventListener("pointerdown", m), window.addEventListener("pointermove", b)) : window.MSPointerEvent ? (window.addEventListener("MSPointerDown", m), window.addEventListener("MSPointerMove", b)) : (window.addEventListener("mousedown", m), window.addEventListener("mousemove", b), "ontouchstart" in window && (window.addEventListener("touchstart", $, e), window.addEventListener("touchend", m))), window.addEventListener(k(), b, e), window.addEventListener("keydown", $), window.addEventListener("keyup", $), window.addEventListener("focusin", w), window.addEventListener("focusout", y)
				},
				m = function (e) {
					if (!c) {
						var t = e.which,
							i = d[e.type];
						"pointer" === i && (i = C(e));
						var n = "keyboard" === i && t && -1 === l.indexOf(t) || "mouse" === i || "touch" === i;
						if (a !== i && n && (a = i, v("input")), s !== i && n) {
							var o = document.activeElement;
							o && o.nodeName && -1 === r.indexOf(o.nodeName.toLowerCase()) && (s = i, v("intent"))
						}
					}
				},
				v = function (e) {
					t.setAttribute("data-what" + e, "input" === e ? a : s), x(e)
				},
				b = function (e) {
					if (T(e), !c && !p) {
						var t = d[e.type];
						"pointer" === t && (t = C(e)), s !== t && (s = t, v("intent"))
					}
				},
				w = function (e) {
					e.target.nodeName ? (i = e.target.nodeName.toLowerCase(), t.setAttribute("data-whatelement", i), e.target.classList && e.target.classList.length && t.setAttribute("data-whatclasses", e.target.classList.toString().replace(" ", ","))) : y()
				},
				y = function () {
					i = null, t.removeAttribute("data-whatelement"), t.removeAttribute("data-whatclasses")
				},
				$ = function (e) {
					m(e), window.clearTimeout(n), c = !0, n = window.setTimeout(function () {
						c = !1
					}, 100)
				},
				C = function (e) {
					return "number" == typeof e.pointerType ? h[e.pointerType] : "pen" === e.pointerType ? "touch" : e.pointerType
				},
				k = function () {
					return "onwheel" in document.createElement("div") ? "wheel" : void 0 !== document.onmousewheel ? "mousewheel" : "DOMMouseScroll"
				},
				x = function (e) {
					for (var t = 0, i = o.length; t < i; t++) o[t].type === e && o[t].fn.call(void 0, "input" === e ? a : s)
				},
				T = function (e) {
					u.x !== e.screenX || u.y !== e.screenY ? (p = !1, u.x = e.screenX, u.y = e.screenY) : p = !0
				};
			return "addEventListener" in window && Array.prototype.indexOf && (d[k()] = "mouse", g(), v("input"), v("intent")), {
				ask: function (e) {
					return "intent" === e ? s : a
				},
				element: function () {
					return i
				},
				ignoreKeys: function (e) {
					l = e
				},
				registerOnChange: function (e, t) {
					o.push({
						fn: e,
						type: t || "input"
					})
				},
				unRegisterOnChange: function (e) {
					var t = function (e) {
						for (var t = 0, i = o.length; t < i; t++)
							if (o[t].fn === e) return t
					}(e);
					t && o.splice(t, 1)
				}
			}
		}()
	}])
});
var Site = function (t, i) {
	Formstone.Ready(function () {
		t("body").removeClass("preload")
	});
	var e = function () {
		this.namespace = "", this.minWidth = 320, this.window = null, this.doc = null, this.$window = null, this.$doc = null, this.$body = null, this.touch = !1, this.modules = [], this.onInit = [], this.onRespond = [], this.onResize = [], this.onScroll = [], this.minXS = "320", this.minSM = "500", this.minMD = "740", this.minLG = "980", this.minXL = "1220", this.minXXL = "1270"
	};
	t.extend(e.prototype, {
		init: function (e) {
			this.namespace = e, this.window = i, this.doc = document, this.$window = t(i), this.$doc = t(document), this.$body = t("body"), this.touch = t("html").hasClass("touchevents"), t.mediaquery && t.mediaquery({
				minWidth: [this.minXS, this.minSM, this.minMD, this.minLG, this.minXL, this.minXXL]
			}), t.cookie && t.cookie({
				path: "/"
			}), o(this.onInit), this.$window.on("mqchange.mediaquery", a).on(n.ns("resize"), s).on(n.ns("scroll"), r), this.resize()
		},
		ns: function (e) {
			return e + "." + this.namespace
		},
		resize: function () {
			this.$window.trigger(n.ns("resize"))
		},
		scroll: function () {
			this.$window.trigger(n.ns("scroll"))
		},
		killEvent: function (e) {
			e && e.preventDefault && (e.preventDefault(), e.stopPropagation())
		},
		startTimer: function (e, t, i, n) {
			return this.clearTimer(e), n ? setInterval(i, t) : setTimeout(i, t)
		},
		clearTimer: function (e, t) {
			e && (t ? clearInterval(e) : clearTimeout(e), e = null)
		},
		icon: function (e) {
			var t = '<svg class="icon icon_' + e + '">';
			return 0 < i.navigator.userAgent.indexOf("MSIE ") || navigator.userAgent.match(/Trident.*rv\:11\./) ? t += '<use xlink:href="#' + e + '">' : t += '<use xlink:href="' + STATIC_ROOT + "images/icons.svg#" + e + '">', t + "</use></svg>"
		}
	});
	var n = new e;

	function o(e) {
		for (var t in e) e.hasOwnProperty(t) && e[t].apply(n, Array.prototype.slice.call(arguments, 1))
	}

	function a(e, t) {
		o(n.onRespond, t)
	}

	function s() {
		o(n.onResize)
	}

	function r() {
		o(n.onScroll)
	}
	return n
}(jQuery, window);
jQuery(document).ready(function () {
	Site.init("@namespace")
}), Site.modules.BarGraph = function (s, e) {
	function r(e) {
		return parseInt(e.replace(/\$|,/g, ""))
	}

	function l(e, t) {
		100 < t ? e.find(".bar_graph_value").addClass("evaluated").css("width", "100%") : e.find(".bar_graph_value").addClass("evaluated").css("width", t + "%")
	}

	function d(e, t, i) {
		i * (t / 100) < 190 ? e.find(".bar_graph_value_label").addClass("small_value") : e.find(".bar_graph_value_label").removeClass("small_value")
	}
	return Site.onInit.push(function () {
		s(".js-bar-graph").length && s(".js-bar-graph").each(function () {
			var e = s(this),
				t = s(this).find(".js-bargraph-fullwidth"),
				i = e.find(".bar_graph_value_label").attr("data-value"),
				n = e.find(".bar_graph_denominator").attr("data-value");
			valueNum = r(i), denominatorNum = r(n);
			var o = t.width(),
				a = valueNum / denominatorNum * 100;
			s(window).on("load", function () {
				d(e, a, o), e[0].getBoundingClientRect().top < s(window).innerHeight() / 1.5 && l(e, a)
			}), s(window).on("resize", function () {
				d(e, a, o), e[0].getBoundingClientRect().top < s(window).innerHeight() / 1.5 && l(e, a)
			}), s(window).on("scroll", function () {
				e[0].getBoundingClientRect().top < s(window).innerHeight() / 1.5 && l(e, a)
			})
		})
	}), {}
}(jQuery), Site.modules.FactsStats = function (t, e) {
	var i, n = t(".js-facts-stats"),
		o = t(".js-facts-stats-display"),
		a = t(".js-facts-stats-items");
	return e.onInit.push(function () {
		n.length && (".facts_stats_nav .nav_prev", ".facts_stats_nav .nav_next", i = window.location.href, o.slick({
			arrows: !1,
			centerMode: !0,
			centerPadding: "0px",
			dots: !0,
			infinite: !1,
			mobileFirst: !0,
			slidesToShow: 1,
			responsive: [{
				breakpoint: 740,
				settings: {
					centerMode: !1,
					dots: !1,
					draggable: !1,
					fade: !0,
					infinite: !0,
					swipe: !1
				}
			}]
		}), a.slick({
			asNavFor: ".js-facts-stats-display",
			appendArrows: ".facts_stats_nav",
			prevArrow: ".facts_stats_nav .nav_prev",
			nextArrow: ".facts_stats_nav .nav_next",
			dots: !0,
			mobileFirst: !0,
			slidesToShow: 1,
			responsive: [{
				breakpoint: 740,
				settings: {
					dots: !1,
					slidesToShow: 2
				}
			}, {
				breakpoint: 1220,
				settings: {
					dots: !1,
					slidesToShow: 3
				}
			}]
		}), t(".facts_stats_nav .nav_prev").on("click", function () {
			var e = t(this).parent(".facts_stats_nav").siblings(".facts_stats_heading").text();
			gtag("event", "Facts & Stats Control", {
				event_category: "Facts and Stats",
				event_action: "Previous",
				event_label: e + " - " + i
			})
		}), t(".facts_stats_nav .nav_next").on("click", function () {
			var e = t(this).parent(".facts_stats_nav").siblings(".facts_stats_heading").text();
			gtag("event", "Facts & Stats Control", {
				event_category: "Facts and Stats",
				event_action: "Next",
				event_label: e + " - " + i
			})
		}))
	}), {}
}(jQuery, Site), Site.modules.Hero = function (e, t) {
	var i;

	function n() {
		i.addClass("loaded")
	}
	return Site.onInit.push(function () {
		(i = e(".js-home-pillars")).length && i.find(".js-hero-background").one("loaded.background", n).background()
	}), {}
}(jQuery), Site.modules.HomePillars = function (o, e) {
	var t, i, n, a, s, r, l, d, c, p, u = o(".js-home-pillars");

	function h() {
		o(".home_pillars_media").hasClass("paused") ? (o(".home_pillars_media").background("play").removeClass("paused"), o(".video_pause_label").text("Pause Background Video"), o(".video_pause").removeClass("paused")) : (o(".home_pillars_media").background("pause").addClass("paused"), o(".video_pause_label").text("Resume Background Video"), o(".video_pause").addClass("paused"))
	}

	function f() {
		p = .01 * window.innerHeight, document.documentElement.style.setProperty("--vh", p + "px")
	}
	return e.onInit.push(function () {
		u.length && (n = o(".js-pillar"), a = o(".js-pillar-title-link"), i = o(".js-pillar-content"), o(".js-home-pillar-nav"), t = o(".js-video-play-pause"), o(".page_content"), d = !1, f(), n.hover(function () {
			var e;
			(e = o(this)).hasClass("pillar_section_1") ? u.attr("data-position", "one") : e.hasClass("pillar_section_2") ? u.attr("data-position", "two") : e.hasClass("pillar_section_3") ? u.attr("data-position", "three") : u.attr("data-position", "one"), n.removeClass("active"), o(this).addClass("active")
		}), a.click(function (e) {
			var t, i, n;
			o(window).width() < 980 && (e.preventDefault(), o(".js-pillar").each(function () {
				o(this).hasClass("open") && o(this).find(".home_pillar_inner_content").slideToggle("1500", function () {
					o(this).parent().removeClass("open")
				})
			}), t = o(this), i = t.closest(".home_pillar_section"), n = i.find(".home_pillar_inner_content"), i.hasClass("open") || n.slideToggle("1500", function () {
				i.addClass("open")
			}))
		}), t.click(function (e) {
			e.preventDefault(), h()
		}), o(window).on("scroll", function () {
			s = o(window).scrollTop(), 980 <= o(window).width() && (s <= .25 * u.outerHeight() ? o("body").hasClass("full-nav") && (o("body").removeClass("full-nav"), h()) : (o("body").hasClass("full-nav") || (h(), o("body").addClass("full-nav")), !1 === d && (c = s, r = 1 / (o(".home_pillars_logo").outerHeight() + o(".home_pillars_sections").outerHeight() + 100), d = !0), l = 1 - (s - c) * r, i.css("opacity", l)))
		}))
	}), e.onResize.push(f), {}
}(jQuery, Site), Site.modules.Module = function (a, s) {
	var e;

	function r() {
		a(window).innerWidth() <= 740 ? a(".fs-lightboxing-iframe-youtube").parent().css({
			width: "100%",
			"padding-bottom": "56.25%"
		}) : a(".fs-lightboxing-iframe-youtube").parent().css({
			width: "70%",
			"padding-bottom": "calc(56.25% * 0.7)"
		})
	}

	function l() {
		console.log("background toggle called by: " + l.caller), a(".js-video-play-pause").trigger("click")
	}
	return s.onInit.push(function () {
		a(".js-lightboxing").length && (e = a(".js-lightboxing"), closeLabel = "Close", a(e).each(function () {
			-1 < a(this).attr("href").indexOf("youtu") ? a(this).addClass("js-video js-youtube") : -1 < a(this).attr("href").indexOf("vimeo") ? a(this).addClass("js-video js-vimeo") : a(this).addClass("js-image")
		}), a(e).on("click", function (e) {
			var t, i, n, o;
			e.preventDefault(), a("body").addClass("fs-navigation-lock"), l(), t = '<div class="fs-lightboxing hidden"><button class="fs-lightboxing-close" title="close lightbox"><span class="fs-lightboxing-close-icon">' + s.icon("close") + '</span><span class="fs-lightboxing-close-label">' + closeLabel + '</span></button><figure class="fs-lightboxing-figure loading"></figure></div>', a("body").after(t), i = 0, a('[class^="fs-lightboxing"]:not(.fs-lightboxing-iframe-wrapper)').on("click", function () {
				0 == i && (a("body").removeClass("fs-navigation-lock"), l(), a(".fs-lightboxing").addClass("hidden"), setTimeout(function () {
					a(".fs-lightboxing").remove()
				}, 150), i += 1)
			}), setTimeout(function () {
				a(".fs-lightboxing").removeClass("hidden")
			}, 150), n = a(this), o = a(n).attr("href"), a(n).hasClass("js-youtube") ? (o = o.replace("watch?v=", "embed/"), o += "?rel=0&controls=0&showinfo=0&autoplay=1&enablejsapi=1", o += "&origin=" + encodeURIComponent(window.location.protocol + "//" + window.location.hostname), a(".fs-lightboxing-figure").append('<div class="fs-lightboxing-iframe-wrapper"><iframe class="fs-lightboxing-object fs-lightboxing-iframe fs-lightboxing-iframe-youtube" src="' + o + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen /></div>'), r()) : a(n).hasClass("js-vimeo") ? (o = o.replace("vimeo.com", "player.vimeo.com/video"), o += "?background=1&autoplay=1&loop=1&autopause=0", a(".fs-lightboxing-figure").append('<iframe class="fs-lightboxing-object fs-lightboxing-iframe fs-lightboxing-iframe-vimeo" src="' + o + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen />')) : a(".fs-lightboxing-figure").append('<img class="fs-lightboxing-object fs-lightboxing-image" src="' + o + '" />'), a(".fs-lightboxing-object").on("load", function () {
				a(".fs-lightboxing-figure").removeClass("loading")
			})
		}), s.onResize.push(r))
	}), {}
}(jQuery, Site), Site.modules.MediaGallery = function (o, e) {
	var a = o(".js-media-gallery"),
		s = 1,
		r = [];
	return e.onInit.push(function () {
		if (a.length) {
			var e = o(".js-media-gallery-items"),
				t = window.location.href;
			e.each(function () {
				o(this).addClass("js-carousel-" + s), o(this).parents(".js-media-gallery").find(".media_gallery_nav").addClass("js-media-gallery-nav-" + s), r.push(".js-carousel-" + s), s += 1
			});
			for (var i = 0; i < r.length; i++) {
				var n = ".js-media-gallery-nav-" + (i + 1);
				o(r[i]).slick({
					appendArrows: ".media_gallery_nav",
					prevArrow: n + " .media_gallery_nav_prev",
					nextArrow: n + " .media_gallery_nav_next",
					dots: !0,
					infinite: !1,
					mobileFirst: !0,
					slidesToShow: 1,
					customPaging: function (e, t) {
						var i = "0" + (t + 1),
							n = e.slideCount;
						return n < 10 && (n = "0" + e.slideCount), '<span class="slideCount">' + i + " / " + n + "</span>"
					},
					responsive: [{
						breakpoint: 740,
						settings: {
							centerMode: !0,
							centerPadding: "0px",
							slidesToShow: 1.25
						}
					}, {
						breakpoint: 980,
						settings: {
							slidesToShow: 1.45
						}
					}]
				})
			}
			o(".media_gallery_nav_prev").on("click", function () {
				var e = o(this).parent(".media_gallery_nav").siblings(".media_gallery_header").find(".media_gallery_title").text();
				gtag("event", "Media Gallery Control", {
					event_category: "Media Gallery",
					event_action: "Previous",
					event_label: e + " - " + t
				})
			}), o(".media_gallery_nav_next").on("click", function () {
				var e = o(this).parent(".media_gallery_nav").siblings(".media_gallery_header").find(".media_gallery_title").text();
				gtag("event", "Media Gallery Control", {
					event_category: "Media Gallery",
					event_action: "Next",
					event_label: e + " - " + t
				})
			}), o(".slick-arrow").click(function () {
				o(".js-video-item-youtube").each(function () {
					o(this)[0].contentWindow.postMessage('{"event": "command", "func": "pauseVideo", "args": ""}', "*")
				}), o(".js-video-item-vimeo").each(function () {
					o(this)[0].contentWindow.postMessage({
						method: "pause"
					}, o(this)[0].src)
				})
			})
		}
	}), {}
}(jQuery, Site), Site.modules.MenuButton = function (o, e) {
	var t, i, n, a, s;

	function r() {
		a = o(this), s = o("#" + o(this).data("menu-opens")), "false" == a.attr("aria-expanded") ? (a.attr("aria-expanded", !0), e.modules.Page.ariaShow(s), s.find(".js-menu-link").removeAttr("tabindex").first().focus()) : c()
	}

	function l(e) {
		38 === e.keyCode && (e.preventDefault(), "false" == o(this).attr("aria-expanded") && (o(this).trigger("click"), s.find(".js-menu-link").last().focus())), 40 === e.keyCode && (e.preventDefault(), "false" == o(this).attr("aria-expanded") && o(this).trigger("click"))
	}

	function d(e) {
		var t = o(":focus"),
			i = t.closest(".js-menu-item").index(),
			n = s.find(".js-menu-link"); - 1 < [27, 38, 40, 36, 35].indexOf(e.keyCode) && e.preventDefault(), 27 === e.keyCode && c(), 38 === e.keyCode && (0 < i ? n.eq(i - 1).focus() : n.last().focus()), 40 === e.keyCode && (t.closest(".js-menu-item").is(":last-of-type") ? n.first().focus() : n.eq(i + 1).focus()), 36 === e.keyCode && n.first().focus(), 35 === e.keyCode && n.last().focus()
	}

	function c() {
		a.attr("aria-expanded", !1).focus(), e.modules.Page.ariaHide(s), s.find(".js-menu-item").attr("tabindex", "-1")
	}
	return e.onInit.push(function () {
		o(".js-menu").length && (t = o(".js-menu-trigger"), $LabelTrigger = o(".js-menu-label-trigger"), i = o(".js-menu-panel"), n = o(".js-menu-link"), t.attr("aria-haspopup", "true").attr("aria-expanded", "false"), e.modules.Page.ariaHide(i), n.attr("tabindex", "-1"), t.on("click", r), t.on("keyup", l), i.on("keyup", d))
	}), {}
}(jQuery, Site), Site.modules.NewsListing = function (t, e) {
	var i = t(".js-stories-item");
	return e.onInit.push(function () {
		i.length && i.each(function () {
			var e;
			(e = t(this)).find(".story_item_image_link, .story_title_link, .story_link").hover(function () {
				e.addClass("hovered")
			}, function () {
				e.removeClass("hovered")
			})
		})
	}), {}
}(jQuery, Site), Site.modules.Page = function (o, a) {
	var t, s = "caret_left",
		r = "caret_right",
		i = {
			theme: "fs-light",
			videoWidth: 1e3,
			labels: {
				close: "<span class='fs-lightbox-icon-close'>" + a.icon("close") + "</span>",
				previous: "<span class='fs-lightbox-icon-previous'>" + a.icon(s) + "</span>",
				count: "<span class='fs-lightbox-meta-divider'></span>",
				next: "<span class='fs-lightbox-icon-next'>" + a.icon(r) + "</span>"
			}
		},
		l = null,
		e = 0;

	function n() {}

	function d() {
		var e;
		g(), void 0 !== (e = t) && (l = e.outerHeight(), bt_bar_height = o("#bigtree_bar").outerHeight(), wp_bar_height = o("#wpadminbar").outerHeight(), 0 < bt_bar_height ? (e.css("top", bt_bar_height), l += bt_bar_height) : 0 < wp_bar_height && (e.css("top", wp_bar_height), l += wp_bar_height))
	}

	function c() {}

	function p() {
		(o(window).trigger("resize"), window.location.hash) && h(window.location.hash)
	}

	function u(e) {
		a.killEvent(e), h(o(e.delegateTarget).attr("href"))
	}

	function h(e) {
		var t, i = o(e);
		if (i.length) {
			var n = i.offset();
			t = n.top, o("html, body").animate({
				scrollTop: t - l
			})
		}
	}

	function f(e) {
		a.killEvent(e);
		var t = o(e.delegateTarget),
			i = "js-toggle-active";
		t.hasClass(i) ? t.removeClass(i) : t.addClass(i)
	}

	function g() {
		o(".table_wrapper").each(function () {
			o(this).find("table").outerWidth() > o(this).width() + 1 ? o(this).addClass("table_wrapper_overflow") : o(this).removeClass("table_wrapper_overflow")
		})
	}

	function m() {
		o("body").addClass("fs-navigation-lock fs-mobile-lock"), e = window.pageYOffset, o("body").css({
			position: "fixed",
			top: -1 * e,
			width: "100%"
		}), T(o(".js-mobile-sidebar")), o(".js-mobile-sidebar").focus()
	}

	function v() {
		o("body").removeClass("fs-navigation-lock fs-mobile-lock"), o("body").css({
			position: "",
			top: "",
			width: ""
		}), o("html, body").scrollTop(e), x(o(".js-mobile-sidebar")), o(".js-mobile-sidebar-handle").focus()
	}

	function b() {
		o(this).closest(".main_nav_item").find(".main_nav_link").attr("aria-expanded", "true"), T(o(this).closest(".main_nav_item").find(".js-main-nav-children"))
	}

	function w() {
		o(this).closest(".main_nav_item").find(".main_nav_link").attr("aria-expanded", "false"), x(o(this).closest(".main_nav_item").find(".js-main-nav-children"))
	}

	function y() {
		o(this).attr("aria-expanded", "true").find(".sub_nav_handle_label").text("Close"), T(o(".js-sub-nav-list"))
	}

	function $() {
		o(this).attr("aria-expanded", "false").find(".sub_nav_handle_label").text(o(this).data("swap-title")), x(o(".js-sub-nav-list"))
	}

	function C() {
		x(o(".js-sub-nav-list"))
	}

	function k() {
		T(o(".js-sub-nav-list"))
	}

	function x(e) {
		e.attr("aria-hidden", "true").attr("hidden", "")
	}

	function T(e) {
		e.attr("aria-hidden", "false").removeAttr("hidden")
	}
	return a.onInit.push(function () {
		var e;
		o(".js-background").on("loaded.background", function () {
			o(this).addClass("fs-background-loaded")
		}).background(), o(".js-carousel").carousel(), o(".js-checkbox, .js-radio").checkbox(), o(".js-dropdown").dropdown(), o(".js-equalize").equalize(), o(".js-lightbox").lightbox(i), o(".js-swap").swap(), o(".js-mobile-sidebar-handle").on("activate.swap", m).on("deactivate.swap", v), o(".js-main-nav-toggle").on("activate.swap", b).on("deactivate.swap", w), o(".js-sub-nav-handle").on("activate.swap", y).on("deactivate.swap", $).on("enable.swap", C).on("disable.swap", k), (0 < window.navigator.userAgent.indexOf("MSIE ") || navigator.userAgent.match(/Trident.*rv\:11\./)) && o.get(STATIC_ROOT + "images/icons.svg", function (e) {
			var t = document.createElement("div");
			o(t).hide(), t.innerHTML = (new XMLSerializer).serializeToString(e.documentElement), document.body.insertBefore(t, document.body.childNodes[0]), o("svg use").each(function () {
				var e = o(this).attr("xlink:href").split("#");
				o(this).attr("xlink:href", "#" + e[1])
			})
		}), o(".js-main-nav-lg").find("a").hover(function () {
			o(this).attr("aria-expanded", "true"), T(o(this).closest(".main_nav_item").find(".js-main-nav-children"))
		}, function () {
			o(this).attr("aria-expanded", "false"), x(o(this).closest(".main_nav_item").find(".js-main-nav-children"))
		}), o(".js-carousel").each(function () {
			var e = o(this).find(".fs-carousel-control_previous"),
				t = e.text(),
				i = o(this).find(".fs-carousel-control_next"),
				n = i.text();
			e.html("<span class='fs-carousel-control-icon'>" + a.icon(s) + "</span><span class='fs-carousel-control-label'>" + t + "</span>"), i.html("<span class='fs-carousel-control-icon'>" + a.icon(r) + "</span><span class='fs-carousel-control-label'>" + n + "</span>")
		}), e = o(".js-mobile-sidebar-handle"), $this = e, $this.each(function () {
			var e = $this.prop("attributes");
			$this.swap("destroy").wrapInner("<button />"), o.each(e, function () {
				$this.find("button").attr(this.name, this.value)
			}), $this.find("button").unwrap().removeAttr("href").swap().on("activate.swap", m).on("deactivate.swap", v)
		}), o(".js-toggle").not(".js-bound").on("click", ".js-toggle-handle", f).addClass("js-bound"), o(".js-scroll-to").not(".js-bound").on("click", u).addClass("js-bound"), o(".typography table").wrap('<div class="table_wrapper"><div class="table_wrapper_inner"></div></div>'), o("iframe[src*='vimeo.com'], iframe[src*='youtube.com']", ".typography").each(function () {
			o(this).wrap('<div class="video_frame"></div>')
		}), g(), x(o(".js-mobile-sidebar, .js-main-nav-children")), o(".main_nav_link").attr("aria-expanded", "false"), o(".js-sub-nav-handle").attr("aria-expanded", "false").attr("aria-haspopup", "true"), o.mediaquery("bind", "mq-key", "(min-width: " + a.minLG + "px)", {
			enter: function () {
				T(o(".js-sub-nav-list")), o(".js-sub-nav-handle").removeAttr("aria-expanded").removeAttr("aria-haspopup"), o(".fs-navigation-lock").length && o(".js-mobile-sidebar-handle").trigger("click")
			},
			leave: function () {
				x(o(".js-sub-nav-list")), o(".js-sub-nav-handle").attr("aria-expanded", "false").attr("aria-haspopup", "true")
			}
		}), o(window).on("load", p), a.onScroll.push(n), a.onResize.push(d), a.onRespond.push(c)
	}), {
		ariaHide: x,
		ariaShow: T,
		getScrollbarWidth: function () {
			var e = document.createElement("div");
			e.style.visibility = "hidden", e.style.width = "100px", e.style.msOverflowStyle = "scrollbar", document.body.appendChild(e);
			var t = e.offsetWidth;
			e.style.overflow = "scroll";
			var i = document.createElement("div");
			i.style.width = "100%", e.appendChild(i);
			var n = i.offsetWidth;
			return e.parentNode.removeChild(e), t - n
		}
	}
}(jQuery, Site), Site.modules.Timeline = function (e, t) {
	var i = e(".js-timeline-content");
	return t.onInit.push(function () {
		i.length && 740 <= e(window).width() && i.checkpoint({
			intersect: "center-top",
			offset: -250
		})
	}), {}
}(jQuery, Site), Site.modules.VideoAppender = function (o, e) {
	var t = o(".js-video-appender");
	return e.onInit.push(function () {
		t.length && t.on("click", function (e) {
			var t, i, n;
			e.preventDefault(), t = o(this), i = o(t).data("video-service"), n = o(t).data("video-id"), "youtube" == i ? o(t).data("embed", "<iframe class='video_item_iframe js-video-item-iframe js-video-item-youtube' tabindex='-1' src='https://www.youtube.com/embed/" + n + "?rel=0&controls=0&showinfo=0&autoplay=1&playsinline=1&enablejsapi=1' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>") : "vimeo" == i && o(t).data("embed", "<iframe class='video_item_iframe js-video-item-iframe js-video-item-vimeo' tabindex='-1' src='https://player.vimeo.com/video/" + n + "?api=1&autoplay=1&title=0&byline=0&portrait=0' style='position:absolute;top:0;left:0;width:100%;height:100%;' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe><script src='https://player.vimeo.com/api/player.js'><\/script>"), o(t).after(o(t).data("embed")).attr("tabindex", "-1").siblings(".js-video-item-iframe").focus()
		})
	}), {}
}(jQuery, Site);