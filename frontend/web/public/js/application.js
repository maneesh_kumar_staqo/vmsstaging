/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//SIMPLE DROPDOWN WITH OTHER FIELD STARTS
$(document).ready(function () {
    if ($('[data-action="simple-dropdown"]').length) {
        $('[data-action="simple-dropdown"]').change(function () {
            manageOtherField(this);
        });
    }
});

manageOtherField = (elm) => {
    const otherField = $(elm).data('other-field');

    if ($(elm).data('display-other-feild')) {
        $(otherField).removeClass('d-none');
    } else {
        $(otherField).addClass('d-none');
    }
};

//SIMPLE DROPDOWN WITH OTHER FIELD ENDS

$(document).ready(function () {
    $('#show-hidden-menu').click(function () {
        $('.hidden-menu').slideToggle("slow");
        // Alternative animation for example
        slideToggle("fast");
    });

    if ($('.multiSelect').length) {
        $('.multiSelect').fSelect();
    }

    if ($('.singleSelect').length) {
        $('.singleSelect').fSelect({
            showSearch: false
        });
    }
});

$(document).ready(function () {
    new WOW().init();
});

(function ($) {
    $(function () {
        window.fs_test = $('.test').fSelect();
    });
})(jQuery);


//Complete page loading Image show and hide STARTS
$(window).on('load', function () { // makes sure the whole site is loaded 
    $('#status').fadeOut(); // will first fade out the loading animation 
    $('#preloader').delay(1000).fadeOut('slow'); // will fade out the white DIV that covers the website. 
    $('body').delay(1000).css({'overflow': 'visible'});
})
function openSearch() {
    document.getElementById("myOverlay").style.display = "block";
}

function closeSearch() {
    document.getElementById("myOverlay").style.display = "none";
}
//Complete page loading Image show and hide ENDS

//initialize dropdown STARTS
initializerDropDown = (reInitClass = false) => {
    if (reInitClass) {
        const dropdown = $(reInitClass).parent();
        console.log(reInitClass, dropdown);
        dropdown.find('button').text(dropdown.find('button').data('default-text'))
    }
    $('.dropdown-radio').find('input').unbind();
    $('.dropdown-radio').find('input').change(function () {
        const elm = $(this);
        const dropdown = elm.closest('.dropdown');
        const radioname = elm.attr('name');
        elm.parents('.dropdown-menu').find(".chb").prop('checked', false);

        setTimeout(function () {
            elm.prop('checked', true);
            const checked = 'input[name="' + radioname + '"]:checked';
            const checkedtext = $(checked).closest('.dropdown-radio').text().trim();

            dropdown.find('button').text(checkedtext);
        }, 100);
    });
};

if ($('.dropdown-radio').length) {
    initializerDropDown();
}
//initialize dropdown ENDS

//edit profile page
//$(document).ready(function () {
//    // Add minus icon for collapse element which is open by default
//    $(".collapse.show").each(function () {
//        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
//    });
//
//    // Toggle plus minus icon on show hide of collapse element
//    $(".collapse").on('show.bs.collapse', function () {
//        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
//    }).on('hide.bs.collapse', function () {
//        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
//    });
//});

$('[data-action="reveal-side-bar"]').click(function () {
    var x = document.getElementById("mySidenav");
    var element = document.getElementById("body");
    // console.log(element);
    //alert(x);
    if (x.style.display === "none") {
        // x.style.display = "none";
        // element.classList.remove("bodyhidden");
        document.getElementById("mySidenav").style.width = "0";

        // document.getElementById("body").style.overflow = "hidden";

    } else {
        document.getElementById("mySidenav").style.width = "100%";
        x.style.display = "block";
        //  element.classList.add("bodyhidden");
        //document.getElementById("body").style.overflow = "hidden";

    }
});