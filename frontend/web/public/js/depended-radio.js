/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {
    const DEPENDED_RADIO = {
//        MAIN_SELECTOR: '[data-action="is_gstin_enrolled"]',
//        GST_NO: '[data-field="gst-no"]',
//        GST_YES: '[data-field="gst-yes"]',

        MAIN_SELECTOR: '[data-action="depended-radio"]',
    };

    const OPTION_NO = 0;
    const OPTION_YES = 1;

    handleRadio = (elm) => {
        const selected = $(elm).find(':checked').val();
        const noField = $(elm).data('no-field');
        const yesField = $(elm).data('yes-field');

        $('[data-field="'+noField+'"]').hide();
        $('[data-field="'+yesField+'"]').hide();
        
        if (selected == OPTION_NO && noField) {
            $('[data-field="'+noField+'"]').show();
        }

        if (selected == OPTION_YES && yesField) {
            $('[data-field="'+yesField+'"]').show();
        }
    };

    if ($(DEPENDED_RADIO.MAIN_SELECTOR).length) {
        $(DEPENDED_RADIO.MAIN_SELECTOR).each(function () {
           handleRadio($(this));
        });

        $(DEPENDED_RADIO.MAIN_SELECTOR).change(function () {
            handleRadio($(this));
        })
    }
});