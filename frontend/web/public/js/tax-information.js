/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//$("#defaultGroupExample3").click(function () {
//    $(".hide-certificatetds-sec").show();
//});
//$("#defaultGroupExample4").click(function () {
//    $(".hide-certificatetds-sec").hide();
//});
//$(".hide-certificatetds-sec").hide();


$(document).ready(function () {
    const TAX_SELECTOR = {
        MAIN_SELECTOR: '[data-action="is_gstin_enrolled"]',
        GST_NO: '[data-field="gst-no"]',
        GST_YES: '[data-field="gst-yes"]',

        TDS_RADIO: '[data-action="certification_for_lower_deduction_tds"]',
        TDS_NO: '[data-field="tds-no"]',
        TDS_YES: '[data-field="tds-yes"]',
        EST_SELECTOR: '[data-action="permanent_est_india"]',
        EST_NO: '[data-field="est-no"]'
    };

    const OPTION_NO = 0;
    const OPTION_YES = 1;

    handleGst = (elm) => {
        const selected = $(elm).find(':checked').val();
        $(TAX_SELECTOR.GST_YES).hide();
        $(TAX_SELECTOR.GST_NO).hide();

        if (selected == OPTION_NO) {
            $(TAX_SELECTOR.GST_NO).show();
        } else {
            $(TAX_SELECTOR.GST_YES).show();
        }
    };

    handleEST = (elm) => {
        const selected = $(elm).find(':checked').val();

        $(TAX_SELECTOR.EST_NO).hide();

        if (selected == OPTION_NO) {
            $(TAX_SELECTOR.EST_NO).show();
        }
    };

    handleTds = (elm) => {
        const selected = $(elm).find(':checked').val();

        $(TAX_SELECTOR.TDS_YES).hide();
        $(TAX_SELECTOR.TDS_NO).hide();

        if (selected == OPTION_NO) {
            $(TAX_SELECTOR.TDS_NO).show();
        } else {
            $(TAX_SELECTOR.TDS_YES).show();
        }
    }

    if ($(TAX_SELECTOR.MAIN_SELECTOR).length) {
        handleGst($(TAX_SELECTOR.MAIN_SELECTOR))

        $(TAX_SELECTOR.MAIN_SELECTOR).change(function () {
            handleGst($(this));
        })
    }

    if ($(TAX_SELECTOR.TDS_RADIO).length) {
        handleTds($(TAX_SELECTOR.TDS_RADIO))

        $(TAX_SELECTOR.TDS_RADIO).change(function () {
            handleTds($(this));
        });
    }

    if ($(TAX_SELECTOR.EST_SELECTOR).length) {
        handleEST($(TAX_SELECTOR.EST_SELECTOR));

        $(TAX_SELECTOR.EST_SELECTOR).change(function () {
            handleEST($(this));
        });
    }


});

//$(".hide-onchekboxyes").hide();
//$("#defaultGroupExample1").click(function () {
//
//    $(".hide-onchekboxyes").show();
//    $(".hide-onchekboxno").hide();
//});
//$("#defaultGroupExample2").click(function () {
//
//    $(".hide-onchekboxyes").hide();
//    $(".hide-onchekboxno").show();
//});