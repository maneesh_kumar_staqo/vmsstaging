//search 
$(window).on('load', function () { // makes sure the whole site is loaded 
    $('#status').fadeOut(); // will first fade out the loading animation 
    $('#preloader').delay(1000).fadeOut('slow'); // will fade out the white DIV that covers the website. 
    $('body').delay(1000).css({'overflow': 'visible'});
})
function openSearch() {
    document.getElementById("myOverlay").style.display = "block";
}

function closeSearch() {
    document.getElementById("myOverlay").style.display = "none";
}

(function ($) {

    if ($('.test').length) {
        window.fs_test = $('.test').fSelect();
    }
    $("#defaultUnchecked3").click(function () {
        if ($(this).is(":checked")) {

            $('#tradingname').prop("disabled", true);


        } else {
            $('#tradingname').prop("disabled", false);

        }
    });
})(jQuery);

$('#drowdownclickid4').click(function () {
    $('#addcode1').addClass('addup');
})
$('#drowdownclickid').click(function () {
    $('#addcode').addClass('addup');
})
$('#drowdownclickid1').click(function () {
    $('#addcode3').addClass('addup');
})
$('#drowdownclickid3').click(function () {
    $('#addcode2').addClass('addup');
})

/********* */
$('body').click(function () {
    $('#addcode').removeClass('addup');
    $('#addcode1').removeClass('addup');
    $('#addcode2').removeClass('addup');
    $('#addcode3').removeClass('addup');
});

$(document).ready(function () {
    $('.fs-option-label, .fs-checkbox').click(function () {
        var hidett1 = $(this).parent().find('.fs-option-label').text();

        if (hidett1 == 'Other Products (please specify) ') {
            $('#OtherProductspecify12').toggle();
        }
    });

    $('.fs-option-label, .fs-checkbox').click(function () {
        var hidett1 = $(this).parent().find('.fs-option-label').text();

        if (hidett1 == 'Misc. Consultancy (Specify)') {

            $("#secondaryhide222222").toggle();
        }
    });
})

//Vender page 
$(document).ready(function () {
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});

$('#vendordrowdownclickid1').click(function () {
    $('#vendoraddcode1').addClass('addup');
})
$('#vendordrowdownclickid2').click(function () {
    $('#vendoraddcode2').addClass('addup');
})
$('body').click(function () {
    $('#vendoraddcode1').removeClass('addup');
    $('#vendoraddcode2').removeClass('addup');

});

//Contact information 
$(document).ready(function () {
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});

$(".onefilecomplete90").click(function () {
//    $('#bankimgupload1 input[type="file"]').val('');
    $('#bankimgupload1 input[type="file"]').prop('readonly', false);
    $('.addtext90').removeClass('addbgcolor');
    $('.hidetext90').show();
    $('.onefilecomplete90').html('<span id="remove" style="color:#fff;font-size:16px;line-height:40px;">Upload</span>');
    $('.hidetext90').removeClass('uplaodenable');
    $('.addtext90').html('<span id="remove" style="color:#fff;font-size:16px;line-height:40px;">Upload</span>');
    $('.onefilecomplete90').hide();
});

$('#bankdetailsfile2  input[type="file"]').change(function () {

    var count = $('#bankdetailsfile2 input[type="file"]').get(0).files.length
    //console.log(count);
    $('.hidetext99').hide();
    $('.addtext99').html('<span style="color:green;font-size:16px;line-height:40px;">Complete!</span>');
    $('.addtext99').addClass('addbgcolor');
    $('.onefilecomplete99').html('<span>' + count + ' completed</span> <span style="color:red;padding-left:15px;">x</span>')
    $('#bankdetailsfile2 input[type="file"]').prop('readonly', true);
    $('.onefilecomplete99').show();
});
$(".onefilecomplete99").click(function () {
//    $('#bankdetailsfile2 input[type="file"]').val('');
    $('#bankdetailsfile2 input[type="file"]').prop('readonly', false);
    $('.addtext99').removeClass('addbgcolor');
    $('.hidetext99').show();
    $('.addtext99').html('<span id="remove" style="color:#fff;font-size:16px;line-height:40px;">Upload</span>');
    $('.hidetext99').removeClass('uplaodenable');
    $('.onefilecomplete99').hide();
});

$('#accreditationimgupload input[type="file"]').change(function () {
    var count = $('#accreditationimgupload input[type="file"]').get(0).files.length
    $('.hidetext').addClass('uplaodenable');
    $('.addtext').show();
    $('.onefilecomplete_accred').show();
    $('.addtext').html('<span id="remove" style="color:green;font-size:16px;line-height:40px;">Complete!</span>');
    $('#accreditationimgupload label').addClass('addbgcolor');
    $('.onefilecomplete_accred').html('<span style="color:green">' + count + ' file uploaded</span> <span style="color:red;padding-left:15px;">x</span>')
    $('#accreditationimgupload input[type="file"]').prop('disabled', true);
});

$(".onefilecomplete_accred").click(function () {
    $('.hidetext').removeClass('uplaodenable');
    $('#accreditationimgupload input[type="file"]').val('');
    $('#accreditationimgupload input[type="file"]').prop('disabled', false);
    $('.addtext').removeClass('addbgcolor');
    $('.addtext').html('<span id="remove" style="color:#fff;font-size:16px;line-height:40px;">Upload</span>');
    $('.onefilecomplete_accred').hide();
});

$(".dropdown-radio").click(function(){
    $(".dropdown").removeClass("show");
    $(".dropdown-menu-select").removeClass("show");
});