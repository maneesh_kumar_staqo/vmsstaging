<?php
use yii\helpers\Url;
?>
<footer class="footer footer-section mb-3">
    <div class="row ml-0">
        <div class="col-md-4 pt-5 bg" style="padding-right:0px">
            <div class="f-list mx-auto">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="list-conatiner ftleft wow fadeInLeft">
                            <ul>
                                <li><a href="<?= BASE_URL_WEB; ?>projects">Our Projects </a></li>
                                <li><a href="<?= BASE_URL_WEB; ?>careers">Careers</a></li>
                                <li><a href="<?= BASE_URL_WEB; ?>contact-us">Contact Us</a></li>
                                <li><a href="<?= BASE_URL_WEB; ?>policies">Policies & Compliances</a></li>
                                <li><a href="<?= BASE_URL_WEB; ?>bgel">BGEL Disclosures</a></li> 
                            </ul>
                            <hr>
                        </div>
                    </div> 
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="list-conatiner wow fadeInLeft">
                            <ul class="mt-1 commonul">
                                <li><a href="<?= BASE_URL_WEB; ?>newsroom">HFE Newsroom</a></li>
                            </ul>
                            <ul style="list-style-type:disc; padding-left: 4rem!important;" class="mt-1 ftright">
                                <li><a href="<?= BASE_URL_WEB; ?>newsroom#MediaReleases"></i>Media Releases</a></li>
                                <li><a href="<?= BASE_URL_WEB; ?>newsroom"></i>Media Coverage</a></li>
                            </ul>
                            <hr>
                            <ul class="mb-1 commonul">
                                <li><a href="<?= BASE_URL_WEB; ?>VMS/site/login">Vendor Login</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="copy-right col-md-10 ml-4 wow fadeInLeft">
                  <p>&copy; 2019 Hero Future Energies | All Rights Reserved</p>
                </div>
            </div>
        </div>
        <div class="col-md-8 " style="position:relative; padding-left:0px">            
            <div class="footer-map-container">
                <img src="<?= Url::toRoute('/public/img/map.jpg') ?>" class="map-img">
                <div class="mapdot-7 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">London Office</span>  <br>The Pavilion, Kensington Pavilion 96 Kensington High Street London W8 4SG</div>
                </div>
                <div class="mapdot-1 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Registered Office</span>  <br>202, Third Floor, Okhla Industrial Estate, Phase – III, New Delhi – 110 020, India</div>
                </div>
                <div class="mapdot-6 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Corporate Office</span>  <br>201, Third Floor,Okhla Industrial Estate, Phase – III, New Delhi – 110 020, India</div>
                </div>
                <div class="mapdot-2 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Bangalore Office</span> <br>Unit no 1002 & 3, 10th  floor, Prestige Meridian- Block – I, No. 29, M.G. Road, Bangalore 560001, India</div>
                </div>
                <div class="mapdot-3 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Chennai office</span> <br>No. 45/6, Shri Karthik Bajaj Flats, Ground Floor, B Block, Nerkundram Pathai, Chennai 600026, India</div>
                </div>
                <div class="mapdot-4 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Vietnam office</span> <br>613, 6F Me Linh Point Tower, 02 Ngo Duc Ke, District 1, Ho Chi Minh City, Vietnam</div>
                </div>
                <div class="mapdot-5 map-common">
                    <div class="dotoneaddress-1 arrow_box"><span style="font-weight: bold">Singapore office</span> <br>1, Fullerton Road, #02-01 One Fullerton, Singapore 049213</div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="fixed-bottom MobFixed">
    <div class="container">     
        <div class="row">
            <div class="col-6 col-6 MobLeft"><p><a href="<?= BASE_URL_WEB; ?>rooftop-solar">ROOFTOP SOLAR</a></p></div>          
            <div class="col-6 col-6 MobRight"><p><a href="<?= BASE_URL_WEB; ?>projects#case_study">CASE STUDIES</p></a></div>
        </div>
    </div>
</div>

<div id="cookieConsent">
    <div id="closeCookieConsent">x</div>
    This site uses cookies to provide you with a more responsive and personalized service. By using this site you agree to HFE's use of cookies. 
    Please read the <a href="https://www.herofutureenergies.com/backend/web/uploads/policies/1570788269website-privacy-policy.pdf#view=fitV" target="_blank">
    cookie policy</a>
    for more information on the cookies HFE uses. 
    <a class="cookieConsentOK">Ok</a>
</div>

<style>
/*Cookie Consent Begin*/
#cookieConsent {
    background-color: rgba(20,20,20,0.8);
    min-height: 26px;
    font-size: 14px;
    color: #ccc;
    line-height: 26px;
    padding: 8px 0 8px 30px;
    font-family: "Trebuchet MS",Helvetica,sans-serif;
    position: fixed;
    bottom: 0;
    width: 100%;
    float:left;
    left: 0;
    display: none;
    z-index: 9999;
}
#cookieConsent a {
    color: #4B8EE7;
    text-decoration: none;
}
#closeCookieConsent {
    float: right;
    display: inline-block;
    cursor: pointer;
    height: 20px;
    width: 20px;
    margin: -15px 0 0 0;
    font-weight: bold;
    font-size: 17px;
}
#closeCookieConsent:hover {
    color: #FFF;
}
#cookieConsent a.cookieConsentOK {
    background-color: #F1D600;
    color: #000;
    display: inline-block;
    border-radius: 5px;
    padding: 0 20px;
    cursor: pointer;
    float: right;
    margin: 0 60px 0 10px;
}
#cookieConsent a.cookieConsentOK:hover { background-color: #E0C91F; }
/*Cookie Consent End*/
.menu-icon-fixed { overflow: hidden!important; }
.copy-right { padding-top:20px; }
.copy-right p { font-size: 12px; font-family: lighttext; }
.footer hr{ margin-top: 1rem; margin-bottom: 1rem; border: 0; border-top: 1px solid #fff; width: 57%; margin-left: 39px; }
</style>
<script type="text/javascript">
//    $(".hamburger").click(function(){
//        $("body").toggleClass("menu-icon-fixed");
//    });
</script>