<?php
use yii\helpers\Url;
?>
<section>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <nav class="navbar mt-3 cust-bar">
                <input type="checkbox" id="checkbox3" class="checkbox3 visuallyHidden">
                <label for="checkbox3" style="position: relative;">
                    <div class="hamburger hamburger3" data-action="reveal-side-bar">
                        <span class="bar bar1"></span>
                        <span class="bar bar2"></span>
                        <span class="bar bar3"></span>
                        <span class="bar bar4"></span>
                    </div>
                </label>

                <div id="mySidenav" class="sidenav">
                        <ul class="cust-navigation">
                            <li><a href="<?= BASE_URL_WEB; ?>about-us"><span>About Us </span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>projects"><span>Our Projects </span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>rooftop-solar"><span>Rooftop Solar </span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>csr"><span>Corporate Social Responsibility</span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>blogs"><span>Blogs</span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>video-gallery"><span>Video Gallery</span></a></li>
                            <li><a href="<?= BASE_URL_WEB; ?>VMS/site/login"><span>Vendor Login</span></a></li>
                        </ul>
                        <div class="row">
                            <div class="col-lg-5 col-md-5">
                                <ul class="menu-bottm-list-left">
                                    <li><a href="<?= BASE_URL_WEB; ?>careers">Careers </a></li>
                                    <li><a href="<?= BASE_URL_WEB; ?>contact-us">| &nbsp; Contact</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-7 col-md-7">
                                <ul class="menu-bottm-list-right float-right menu-social">
                                    <li><a href="https://www.facebook.com/herofutureenergies"><img src="<?= BASE_URL_WEB; ?>img/facebook.png"></a></li>
                                    <li><a href="https://www.linkedin.com/company/hero-future-energies"><img src="<?= BASE_URL_WEB; ?>img/linked-in.png"></a></li>
                                    <li><a href="https://twitter.com/HeroFuture_HFE"><img src="<?= BASE_URL_WEB; ?>img/twitter.png"></a></li>
                                    <li><a href="https://www.youtube.com/channel/UC_i6ZrJLBLhmU1VDZq5wvTw"><img src="<?= BASE_URL_WEB; ?>img/youtube.png"></a></li>
                                    <li><a href="https://www.instagram.com/hero_futureenergies/"><img src="<?= BASE_URL_WEB; ?>img/insta.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                <div class="mx-auto text-center">
                    <a href="<?= Url::toRoute('/') ?>">
                        <img src="<?= Url::toRoute('/public/img/hfe-logo.jpg') ?>" class="img-fluid logo" style="width: 80%"></a>
                </div>
                <div class="mr">
                    <div class="search-icon-overlay" onclick="openSearch()"><i class="fas fa-search"></i></div>
                    <div id="myOverlay" class="overlay-search wow fadeInRight">
                        <span class="closebtn" onclick="closeSearch()">×</span>
                        <div class="overlay-search-content">
                            <form action="<?= BASE_URL_WEB; ?>search" onSubmit="return searchMainVal();">
                                <input type="text" id="main_search" placeholder="Search Here" name="q" value="<?= !empty($_GET['q']) ? preg_replace('#[^a-z0-9_]#', '-', $_GET['q']) : ''; ?>">
                                <button type="submit"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                </div>                 
            </nav>
        </div>
    </div>
</section>
<script>
function searchMainVal(){
    if($("#main_search").val() == ""){
        alert("Please enter search keyword.");
        $("#main_search").focus();
        return false;
    }else{
        var str = $("#main_search").val();
        str = str.toLowerCase().replace(/[_\W]+/g, "-");
        window.location.href = JS_BASE_URL + 'search?q=' + str;
        return false;
    }
}
</script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83744022-1', 'auto');
  ga('send', 'pageview');
</script>


