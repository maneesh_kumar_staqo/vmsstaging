<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <script>
            const BASE_URL = '<?= Url::base() ?>'
        </script>
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) . ' - ' . Yii::$app->name ?></title>
        <?php $this->head() ?>
    </head>
    <body id="body" class="bodyscroll">
        <?php $this->beginBody() ?>
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <div class="container cust-pd">
            <?= $this->render(Yii::$app->params['elements'] . '_flash_message') ?>
            <?= $this->render('partials/header') ?>
            <?= Alert::widget() ?>
            <?= $content ?>
            <?= $this->render('partials/footer') ?>
        </div>
        <?php $this->endBody() ?>
        
        <script>
    (function($) {
        $(function() {
            window.fs_test = $('.test').fSelect();
        });
    })(jQuery);
    </script>
    <script>
      var $div2blink1 = $(".mapdot-1"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
        $div2blink1.toggleClass("backgroundRed1");
     },500);
     var $div2blink2 = $(".mapdot-2"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
        $div2blink2.toggleClass("backgroundRed2");
     },500);
     var $div2blink3 = $(".mapdot-3"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
        $div2blink3.toggleClass("backgroundRed3");
     },500);
     var $div2blink4 = $(".mapdot-4"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
        $div2blink4.toggleClass("backgroundRed4");
     },500);
     var $div2blink5 = $(".mapdot-5"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
      
        $div2blink5.toggleClass("backgroundRed5");
     },500);
      var $div2blink6 = $(".mapdot-6"); // Save reference, only look this item up once, then save
      var backgroundInterval = setInterval(function(){
        $div2blink6.toggleClass("backgroundRed6");
     },500);
    var $div2blink7 = $(".mapdot-7"); // Save reference, only look this item up once, then save
    var backgroundInterval = setInterval(function(){
    $div2blink7.toggleClass("backgroundRed7");
    },500);
    </script>

    <script>
    $(document).ready(function(){   
        function setCookie(key, value, expiry) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (expiry * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        function getCookie(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }
        
        if(!getCookie('cookieVal')){
            setTimeout(function () {
                $("#cookieConsent").fadeIn(200);
                setCookie("cookieVal", 1, 1);
            }, 2000);
            
            $("#closeCookieConsent, .cookieConsentOK").click(function() {
                $("#cookieConsent").fadeOut(200);
            }); 
        }
    });
    </script>
    </body>
</html>
<?php $this->endPage() ?>
