<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PO Tracking';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login', [
            'active' =>'po-tracking'
        ]) ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="panel panel-success">

                    <div class="panel-body">
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'project_title',
                                'po_number',
                                'date_of_issue:date',
                                'amount',
                                'tax',
                                'total_amount',
                                [
                                    'label' => 'Remarks',
                                    'contentOptions' => ['style' => 'word-wrap: break-word;max-width: 100px;'],
                                    'attribute' => 'description'
                                ],
                                [
                                    'label' => 'Total Invoice',
                                    'attribute' => 'total_invoice',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return Html::a($model['total_invoice'] . ' &nbsp; <b>View Invoices </b>', ['/po-tracking/view?id=' . $model['id']]);
                                    }
                                ],
                                //'document',
                                //'status',
                                //'created_by',
                                //'created_at',
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>