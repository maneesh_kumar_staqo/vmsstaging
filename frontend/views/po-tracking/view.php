<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\BrandsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'PO Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?=
        $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login', [
            'active' => 'po-tracking'
        ])
        ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>
                    <?= Html::encode($this->title) ?>
                </h2>
                <div class="panel panel-success">

                    <div class="panel-body">
                        <?=
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'invoice_number',
                                'amount',
                                'remark',
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

