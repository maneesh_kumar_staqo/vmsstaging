<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Edit Profile';

$this->registerJsFile(Url::toRoute('/public/js/edit-profile.js'), [
    'depends' => \yii\web\JqueryAsset::className()
])

?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Edit Basic Info</h2>
                <?php $form = ActiveForm::begin(); ?>


                    <div class="row" style="padding-top:15px;padding-left:5px">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'name')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'email')->textInput() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-6">
                            <div class="changespassword">
                                <div class="changespasswordbtn">Change Password</div>
                            </div> 
                            <?= $form->field($model, 'changePassword')->hiddenInput(['class' => 'changePassword'])->label(false) ?>
                        </div>
                    </div>

                <div class="d-none">
                    <input type="password" />
                </div>

                    <div class="row" style="padding-left:5px;padding-left:5px">
                        <div class="col-md-6 editprofilehidepasswordsec" style="margin-top:30px">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'oldPassword')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6 editprofilehidepasswordsec" style="margin-top:30px">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'password')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="col-md-6 editprofilehidepasswordsec">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'confirmPassword')->passwordInput() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-6">
                            <div class="login-btn">
                                <div class="save-and-go-next">
                                    <button type="submit">
                                        <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                            Save & Go Next
                                        </span>
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>