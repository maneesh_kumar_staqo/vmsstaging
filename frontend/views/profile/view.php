<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\RegistrationVendorInfo;
use common\helpers\FormHelper;
use yii\helpers\Html;
use common\models\User;

$this->title = 'My Profile';

$this->registerJsFile(Url::toRoute('/public/js/signup.js'), [
    'depends' => \yii\web\JqueryAsset::className()
]);

$serviceTurnover = $communityTurnover = '';

if (!empty($commodities[$model->fk_primary_commodity_type])) {
    if ($model->annual_turnover == 1) {
        $communityTurnover = $commodities[$model->fk_primary_commodity_type]['upper_turnover'];
    } else {
        $communityTurnover = $commodities[$model->fk_primary_commodity_type]['lower_turnover'];
    }
}
if (!empty($services[$model->primary_service_type])) {
    if ($model->primary_service_turnorver == 1) {
        $serviceTurnover = $services[$model->primary_service_type]['upper_turnover'];
    } else {
        $serviceTurnover = $services[$model->primary_service_type]['lower_turnover'];
    }
}
?>

<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>
        <div class="col-md-9">
            <div class="hfe-reg-vender-middle ">
                <h2><?= $this->title ?></h2>
                <div class="row" style="padding-top:15px;padding-left:5px;">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Business Registration Status : <?= RegistrationVendorInfo::$bussinessRegistrationTypes[$model->bussiness_registration] ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Company Name : <?= $model->company_name ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Trading Name : <?= $model->trading_name ?>                           
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $html = '';
                            if ($model['primaryCommodity']) {
                                if ($model['primaryCommodity']['display_other_field']) {
                                    $html = $model['other_primary_commodity'];
                                } else {
                                    $html = $model['primaryCommodity']['title'];
                                }
                            }
                            ?>
                            Primary Commodity Type : <?= $html ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $turnover = 'N/A';

                            if ($model['primaryCommodity']) {

                                if (!empty($model['annual_turnover'])) {
                                    if ($model['annual_turnover'] == 1) {
                                        $turnover = $model['primaryCommodity']['upper_turnover'];
                                    } else {
                                        $turnover = $model['primaryCommodity']['lower_turnover'];
                                    }
                                } else {
                                    $turnover = 'N/A';
                                }
                            }
                            ?>
                            Primary Commodity Annual Turnover : <?= $turnover ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $html = '';
                            if ($model['fk_secondary_commodity_type']) {
                                $selected = json_decode($model['fk_secondary_commodity_type']);
                                if (is_array($selected)) {
                                    foreach ($selected as $single) {
                                        if (!empty($commodities[$single]) && $commodities[$single]['display_other_field']) {
                                            $html .= $model['other_secondary_commodity'] . ',';
                                        } else if (!empty($commodities[$single])) {
                                            $html .= $commodities[$single]['title'] . ',';
                                        }
                                    }
                                }
                            } else {
                                $html = 'N/A';
                            }
                            ?>
                            Secondary Commodity Type : <?= $html ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $html = '';
                            if ($model['primaryService']) {
                                if ($model['primaryService']['display_other_field']) {
                                    $html = $model['other_primary_service_type'];
                                } else {
                                    $html = $model['primaryService']['title'];
                                }
                            }
                            ?>
                            Primary Service Type : <?= $html ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $turnover = 'N/A';

                            if ($model['primaryService']) {

                                if (!empty($model['primary_service_turnorver'])) {
                                    if ($model['primary_service_turnorver'] == 1) {
                                        $turnover = $model['primaryService']['upper_turnover'];
                                    } else {
                                        $turnover = $model['primaryService']['lower_turnover'];
                                    }
                                } else {
                                    $turnover = 'N/A';
                                }
                            }
                            ?>
                            Primary Service Annual Turnover : <?= $turnover ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <?php
                            $html = '';
                            if ($model['secondary_service_type']) {
                                $selected = json_decode($model['secondary_service_type']);

                                if (is_array($selected)) {
                                    foreach ($selected as $single) {
                                        if (!empty($services[$single]) && $services[$single]['display_other_field']) {
                                            $html .= $model['other_secondary_service_type'] . ',';
                                        } else if (!empty($services[$single])) {
                                            $html .= $services[$single]['title'] . ',';
                                        }
                                    }
                                }
                            } else {
                                $html = 'N/A';
                            }
                            ?>
                            Secondary Service Type : <?= $html ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Contact Person Name : <?= $model->contact_person_name ?>                          
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Contact Person Email : <?= $model->contact_person_email ?>                          
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            Password : *******                         
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>