<?php

use yii\helpers\Url;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}
$isSet = false;
if (!empty($session['alert'])) {
    $isSet = true;
    $type = $session['alert']['type'];
    $h1 = $session['alert']['h1'];
    $p = $session['alert']['p'];
    $image = $session['alert']['image'];
    $actionButton = $session['alert']['actionButton'];
    $btnText = $session['alert']['btnText'];
    unset($session['alert']);
}
$session->close();
if ($isSet):
    ?>
    <div class="modal fade myModalStyle" id="flashMessagePopup">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">

                <!-- Modal body -->
                <div class="modal-body text-center">
                    <?php if (!empty($image)): ?>
                        <div class="statusImg">
                            <i class="fa fa-2x <?= $image ?>"></i>
                        </div>
                    <?php endif; ?>
                    <?php if (!empty($h1)): ?>
                        <h2 class="text_L font-weight-bold line_Hinitial mt-2">
                            <?= $h1 ?>
                        </h2>
                    <?php endif; ?>
                    <?php if (!empty($p)): ?>
                        <p class="mt-3">
                            <?= $p ?>
                        </p>
                    <?php endif; ?>
                    <div class="actBtn">
                        <?php
                        if (!empty($actionButton)):
                            echo $actionButton;
                        endif;
                        ?>
                        <button class="btn btn-primary btn-sm" data-dismiss="modal">
                            <?= ($btnText) ? $btnText : 'Dismiss' ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            $("#flashMessagePopup").modal('show');
        });
    </script>
<?php endif; ?>