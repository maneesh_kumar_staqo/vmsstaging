<?php

use yii\helpers\Url;
?>
<div class="col-md-3">
    <div class="hfe-login-sidebar">
        <h1>About Vendor Management System</h1>
        <p>Welcome to our vendor management portal. This is the interface for managing professional services sourcing and procurement for wind, solar and rooftop solar projects of Hero Future Energies in India and abroad. Our effort has been to offer our partners and potential partners an easy to navigate and a time worthy experience.</p>
        <div class="row">
            <div class="col-md-12 register-btn">
                <a href="<?= Url::toRoute('/site/login') ?>">
                    <button>

                        <img src="<?= Url::toRoute('/public/img/register-button.png') ?>" />
                        <span>Login</span>
                    </button>
                </a>
            </div>
        </div>
    </div>
</div>