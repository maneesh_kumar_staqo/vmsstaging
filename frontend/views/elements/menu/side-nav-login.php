<?php

use yii\helpers\Url;
use common\models\VendorInformation;
use common\models\AccreditationInformation;
use common\models\DeclarationInformation;
use common\models\ContactInformation;
use common\models\AddressInformation;
use common\models\BankInformation;
use common\models\TaxInformation;
use common\models\User;

$registrationFormActions = [
    'declaration-information', 'vendor-information', 'contact-information', 'address-information',
    'bank-information', 'tax-information', 'accreditation-information'
];

$poController = [
    'po-tracking'
];
$profileActions = [
    'edit', 'view'
];
$idUser = Yii::$app->user->id;

if (!isset($active)) {
    $active = Yii::$app->controller->action->id;
}
?>
<div class="col-md-3">
    <div class="hfe-login-sidebar">
        <div
            class="row"
            style="border-bottom: 1px solid #21526f;padding-bottom: 17px;">
            <div class="col-md-10">
                <span style="color:#fff">
                    Welcome, <?= Yii::$app->user->identity->email ?>
                </span>
                <?php if (!Yii::$app->user->isGuest): ?>
                <span style="color:#fff">
                    Application Status: <b><?= !empty(User::$applicationStatus[Yii::$app->user->identity->application_status]) ?
                        User::$applicationStatus[Yii::$app->user->identity->application_status] : '' ?></b>
                </span>
                <?php endif; ?>
            </div>
            <div class="col-md-2">
                <a href="<?= Url::toRoute('/site/logout') ?>">
                    <img src="<?= Url::toRoute('/public/img/signup.png') ?>" class="signupicon"/>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="sidebar-baraccod">
                    <div class="accordion" id="accordionExample">
                        <div class="card-header" id="headingOne">
                            <div  class="commonfont"  data-toggle="collapse" data-target="#collapseOne"> Profile<i class="fa fa-plus"></i></div>	
                        </div>
                        <div
                            id="collapseOne"
                            class="collapse <?= in_array($active, $profileActions) ? 'show' : '' ?>"
                            aria-labelledby="headingOne"
                            data-parent="#accordionExample">
                            <ul>
                                <li>
                                    <a href='<?= Url::toRoute('/profile/view') ?>'>
                                        <?php if ($active == 'view'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php endif; ?>
                                        View Profile
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/profile/edit') ?>'>
                                        <?php if ($active == 'edit'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php else: ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Edit Basic Info
                                    </a>
                                </li>
                            </ul>  
                        </div>

                        <div class="card-header" id="headingTwo">								
                            <div class="collapsed commonfont" data-toggle="collapse" data-target="#collapseTwo">
                                Complete Registration Form
                                <i class="fa fa-plus"></i>
                            </div>
                        </div>
                        <div
                            id="collapseTwo"
                            class="collapse <?= in_array($active, $registrationFormActions) ? 'show' : '' ?>"
                            aria-labelledby="headingTwo"
                            data-parent="#accordionExample"
                            >
                            <ul>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/vendor-information') ?>'>
                                        <?php if ($active == 'vendor-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (VendorInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Vendor Details
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/contact-information') ?>'>
                                        <?php if ($active == 'contact-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (ContactInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Contact information
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/address-information') ?>'>
                                        <?php if ($active == 'address-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (AddressInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Address Information
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/bank-information') ?>'>
                                        <?php if ($active == 'bank-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (BankInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Bank Details
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/tax-information') ?>'>
                                        <?php if ($active == 'tax-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (TaxInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Tax Information
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/accreditation-information') ?>'>
                                        <?php if ($active == 'accreditation-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (AccreditationInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Accreditations
                                    </a>
                                </li>
                                <li>
                                    <a href='<?= Url::toRoute('/detail-completion/declaration-information') ?>'>
                                        <?php if ($active == 'declaration-information'): ?>
                                            <span  class="sidelistcircleo"></span>
                                        <?php elseif (DeclarationInformation::isExists($idUser)): ?>
                                            <span  class="sidelistcircleg"></span>
                                        <?php endif; ?>
                                        Declarations
                                    </a>
                                </li>
                            </ul>
                        </div>                      

                        <div class="card-header" id="headingPO">
                            <div class="commonfont"><a href='<?= Url::toRoute('/po-tracking') ?>' style="color:#fff;"> PO Tracking </a></div>	
                        </div>

                        <!--                        <div class="card-header" id="headingFour">
                                                    <div class="collapsed commonfont"  data-toggle="collapse" data-target="#collapseThree">Contract Tracking<i class="fa fa-plus"></i></div>                     
                                                </div>
                                                <div class="card-header" id="headingFive">
                                                    <div class="collapsed commonfont"  data-toggle="collapse" data-target="#collapseThree">Notification Preferences<i class="fa fa-plus"></i></div>                     
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>