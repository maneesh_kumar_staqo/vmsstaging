<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-guest') ?>
        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Login</h2>
                <p style="padding-bottom:0px;">
                    Use your credentials to login and complete the vendor registration process.
                </p>
                <div>
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'username')->textInput() ?>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'password')->passwordInput() ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 login-btn">
                            <button>
                                <img src="<?= Url::toRoute('/public/img/register-button.png') ?>" />
                                <span>Login</span>
                            </button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>