<?php
$this->title = 'Thank You';

use common\helpers\MessageHelper;
?>
<section class="section hfe-login">
    <div class="row">
        <?php if (Yii::$app->user->isGuest): ?>
            <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-guest') ?>
        <?php else: ?>
            <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login', [
                'active' => 'vendor-information'
            ]) ?>
        <?php endif; ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <div class="">
                    <?php if (!empty(MessageHelper::$messages[$type])): ?>
                        <?= MessageHelper::$messages[$type] ?>
                    <?php else: ?>
                        A verification link has been sent to your email account.
                        <br>
                        Please click on the link that has just been sent to your email account to verify your email. After completing the activation process, please click on Login button to proceed to the login screen.
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>