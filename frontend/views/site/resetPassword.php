<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-guest') ?>
        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">

                <p>Please choose your new password:</p>

                <div class="row">
                    <div class="col-lg-12">
                        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                        <div class="col-md-8">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                            </div>
                        </div>

                        <div class="">
                            <div class="col-md-12 login-btn">
                                <button>
                                    <img src="<?= Url::toRoute('/public/img/register-button.png') ?>" />
                                    <span>Save</span>
                                </button>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
