<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;

if (Yii::$app->request->url == '/VMS/frontend/web/site/login') {
    return Yii::$app->response->redirect(Url::toRoute('/site/login'), 301);
} else if (Yii::$app->request->url == '/VMS/frontend/web/site/signup') {
    return Yii::$app->response->redirect(Url::toRoute('/site/signup'), 301);
} else if (Yii::$app->request->url == '/VMS/frontend/web/') {
    return Yii::$app->response->redirect(Url::toRoute('/'), 301);
}
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div>
