<?php
$this->title = 'Thank You';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login', [
            'active' => 'declaration-information'
        ]) ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <div class="text-center">
                    Thanks for your submission. We will get back to you soon.
                </div>
            </div>
        </div>
    </div>
</section>