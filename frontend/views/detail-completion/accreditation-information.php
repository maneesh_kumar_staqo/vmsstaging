<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;

$this->title = 'Accreditations Information';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Accreditations</h2>
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="row">
                    <?= FormHelper::fileInput($form, $model, 'document_one') ?>
                    <?= FormHelper::fileInput($form, $model, 'document_two') ?>
                </div>
                <div class="row">
                    <?= FormHelper::fileInput($form, $model, 'document_three') ?>
                    <?= FormHelper::fileInput($form, $model, 'document_four') ?>
                </div>
                <div class="row">
                    <?= FormHelper::fileInput($form, $model, 'document_five') ?>
                    <?= FormHelper::fileInput($form, $model, 'document_six') ?>
                </div>
                <div class="row">
                    <?= FormHelper::fileInput($form, $model, 'document_seven') ?>
                    <?= FormHelper::fileInput($form, $model, 'document_eight') ?>
                </div>
                <div class="row">
                    <?= FormHelper::fileInput($form, $model, 'document_nine') ?>
                    <?= FormHelper::fileInput($form, $model, 'document_ten') ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="multioption">
                                <label style="padding-top:0px">Other Accreditation Details (optional)</label>
                            </div>
                            <?= $form->field($model, 'accreditation_details')->textarea(['rows' => 5])->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?= FormHelper::fileInputMulti($form, $model, 'documents') ?>
                </div>
                
                <div class="row">
                    <div class="col-md-8 col-6">
                        <div class="login-btn">
                            <div class="vender-circle-btn">
                                <a href="<?= Url::toRoute('/detail-completion/tax-information') ?>">
                                    <img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
                                        Go Back
                                    </span>
                                </a>
                            </div> 
                        </div>
                        <div class="login-btn">
                            <div class="save-and-go-next">
                                <button type="submit">
                                    <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                        Save & Go Next
                                    </span>
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>