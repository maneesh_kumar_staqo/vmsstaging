<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Contact Information';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Business Person Details</h2>
                <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'b_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'b_designation')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'b_land_line_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'b_mobile_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'b_email')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="margin-top:15px;">Finance & Accounts Person Details</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'f_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'f_designation')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'f_land_line_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'f_mobile_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'f_email')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-6">
                            <div class="login-btn">
                                <div class="vender-circle-btn">
                                    <a href="<?= Url::toRoute('/detail-completion/vendor-information') ?>">
                                        <img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
                                            Go Back
                                        </span>
                                    </a>
                                </div> 
                            </div>
                            <div class="login-btn">
                                <div class="save-and-go-next">
                                    <button type="submit">
                                        <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                            Save & Go Next
                                        </span>
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>