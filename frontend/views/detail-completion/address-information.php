<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\AddressInformation */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Address Information';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Registered Address</h2>
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_house_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_street_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_city')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_state')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_post_office')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_telephone_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_fax_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_country')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ra_pin_code')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="margin-top:15px;">Communication Address</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_house_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_street_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_city')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_state')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_post_office')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_telephone_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_fax_number')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_country')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <?= $form->field($model, 'ca_pin_code')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-6">
                    <div class="login-btn">
                        <div class="vender-circle-btn">
                            <a href="<?= Url::toRoute('/detail-completion/contact-information') ?>">
                                <img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
                                    Go Back
                                </span>
                            </a>
                        </div> 
                    </div>
                    <div class="login-btn">
                        <div class="save-and-go-next">
                            <button type="submit">
                                <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                    Save & Go Next
                                </span>
                            </button>
                        </div> 
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>