<?php

use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;
use common\models\DeclarationInformation;

$this->title = 'Declaration Information';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?=
        $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login', [
            'active' => 'declaration-information',
        ])
        ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Declarations</h2>
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="multioption">
                            <p>1. If there are any insolvency & bankruptcy / liquidation proceedings pending/adjudicated against you under applicable laws in relevant jurisdiction?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?=
                        FormHelper::radioListV1($form, $model, 'relevant_jurisdiction_check', DeclarationInformation::$optionArray, [
                            'selected' => $model->relevant_jurisdiction_check ? $model->relevant_jurisdiction_check : DeclarationInformation::SELECTION_OPTION
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="multioption">
                            <p>2. Have you ever been involved or convicted in any offence of Corruption & Bribery?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?=
                        FormHelper::radioListV1($form, $model, 'offence_of_corruption_or_bribery_check', DeclarationInformation::$optionArray, [
                            'selected' => $model->offence_of_corruption_or_bribery_check ? $model->offence_of_corruption_or_bribery_check : DeclarationInformation::SELECTION_OPTION
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="multioption">
                            <p>3. Have you ever been involved or convicted in any offence of Slavery and Human Trafficking?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?=
                        FormHelper::radioListV1($form, $model, 'human_trafficking_check', DeclarationInformation::$optionArray, [
                            'selected' => $model->human_trafficking_check ? $model->human_trafficking_check : DeclarationInformation::SELECTION_OPTION
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="multioption">
                            <p>4. Have you ever been involved or convicted in any offence of Money Laundering and/or Terrorist Financing?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?=
                        FormHelper::radioListV1($form, $model, 'terrorist_financing_check', DeclarationInformation::$optionArray, [
                            'selected' => $model->terrorist_financing_check ? $model->terrorist_financing_check : DeclarationInformation::SELECTION_OPTION
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="multioption">
                            <p>5. Have you ever been involved in any offence involving tax evasion or the criminal facilitation of tax evasion?</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <?=
                        FormHelper::radioListV1($form, $model, 'tax_evasion_check', DeclarationInformation::$optionArray, [
                            'selected' => $model->tax_evasion_check ? $model->tax_evasion_check : DeclarationInformation::SELECTION_OPTION
                        ])
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="margin-top:23px">Undertakings</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 disclmr">
                        By Visiting HFE Website ou Agree and Undertake as Under:
                    </div>
                    <div class="col-md-12">
                        <ul>
                            <li><span>1.</span> Information disclosed herein relating to your company /firm and/or business are true and correct.</li>
                            <li><span>2.</span> No insolvency/bankruptcy proceedings against you has been initiated and/or is pending before any tribunal/court under applicable laws to which you are subject in the relevant jurisdictions.</li>
                            <li><span>3.</span>You assure HFE that all goods/services to be provided by you are not and shall continue to be not in violation of or does not infringe any Intellectual Property Rights.You are the owner of or sufficiently entitled to use all signage and logos that you propose to use for the business.</li>
                            <li><span>4. </span>You have not been and shall continue to be not part of or involved in any offence involving tax evasion or the criminal facilitation of tax evasion; nor has you been subject of any investigation, inquiry or enforcement proceedings regarding any offence or alleged offence of or in connection with tax evasion or the criminal facilitation of tax evasion.</li>
                            <li> <span>5. </span><b>Data Protection:</b> You hereby consent to the holding and processing of information and other data provided to HFE for all purposes necessary for administering and maintaining Vendor records.</li>
                            <li><span>6.</span> <b>Anti-Corruption &amp; Bribery: </b>You are in compliance with the anti-corruption and bribery laws, to which You are subject in the jurisdictions in which you are operating, including but not limited to <em>Prevention of Corruption Act, 1988</em><em>, </em><em>Foreign Contribution (Regulation) Act, 2010</em><em>, </em><em>The Black Money (Undisclosed Foreign Income and Assets) and Imposition of Tax Act, 2015</em><em>, </em><em>The Fugitive Economic Offenders Act, 2018</em><em>, </em><em>The Benami Transactions (Prohibition) Act, 1988</em><b>. </b>Neither you nor your subsidiaries have ever received any written communication that alleges that you and your subsidiaries or any of your agent has violated the anti-corruption and bribery laws.</li>
                            <li><span>7. </span><b>Anti-Slavery and Human Trafficking</b><b>:</b> You are in compliance with all applicable anti-slavery and human trafficking laws, statutes, regulations and codes from time to time in force, to which you are subject in the jurisdictions in which you are operating, including but not limited to <em>Immoral Trafficking (Prevention) Act, 1956, Bonded Labour System (Abolition) Act, 1976, The Juvenile Justice (Care and Protection of Children) Act, 2015</em><b>.</b></li>
                            <li><span>8. </span><b>Anti-Money Laundering and Anti-Terrorist Financing: </b>You have never been involved in any money laundering or terrorist financing activities. You have taken, and shall continue to take, all reasonable measures, under applicable laws, to ensure that you are and shall be in compliance with the relevant prevention of money laundering and terrorist financing Act to which the you are subject.</li>
                            <li><span>9. </span>You understand that regulatory authorities may take legal action, and HFE may seek damages if the information provided is false or misleading.</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="margin-top:0px">Disclaimer</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 disclmr">
                        <p>The information provided/sought herein is for general purposes only and no warranties, promises and/or representations of any kind express or implied is made through it. Filling up of the form or providing any specific information to HFE does not provide an assurance or entitle you to claim any award of contract for any project from HFE. In no event the HFE shall be liable for any specific, direct, indirect, consequential, or incidental damages or any damages whatsoever whether in an action of contract, negligence or other tort in relation to the transaction contemplated herein.</p>
                        <p><em>** In the context of abovementioned declarations &amp; undertakings, except as otherwise provided therein, “You” shall mean and includes - company, Limited Liability Partnerships, proprietorship entity, partnership firm, foreign company, company incorporated outside India.</em></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-6">
                        <div class="login-btn">
                            <div class="vender-circle-btn">
                                <a href="<?= Url::toRoute('/detail-completion/accreditation-information') ?>">
                                    <img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
                                        Go Back
                                    </span>
                                </a>
                            </div> 
                        </div>
                        <div class="login-btn">
                            <div class="save-and-go-next">
                                <button type="submit">
                                    <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                        Save & Submit
                                    </span>
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>