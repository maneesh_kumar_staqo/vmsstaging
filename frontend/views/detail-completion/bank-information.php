<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\FormHelper;

/* @var $this yii\web\View */
/* @var $model common\models\BankInformation */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Bank Information';
?>
<section class="section hfe-login">
    <div class="row ml-0">
        <?= $this->render(Yii::$app->params['elements'] . 'menu/side-nav-login') ?>

        <div class="col-md-9">
            <div class="hfe-reg-vender-middle">
                <h2>Primary Bank Details</h2>
                <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_account_holder_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_account_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_account_type')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_bank_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_branch')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_city')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_micr_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'pb_ifsc_code')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= FormHelper::fileInputMulti($form, $model, 'pb_documents', [
                                'class' => ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="margin-top:15px;">Secondary Bank Details</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_account_holder_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_account_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_account_type')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_bank_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_branch')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_city')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_micr_number')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="md-form mb-0">
                                <?= $form->field($model, 'sb_ifsc_code')->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?= FormHelper::fileInputMulti($form, $model, 'sb_documents', [
                                'class' => ''
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-6">
                            <div class="login-btn">
                                <div class="vender-circle-btn">
                                    <a href="<?= Url::toRoute('/detail-completion/address-information') ?>">
                                        <img src="<?= Url::toRoute('/public/img/register-button-flip.png') ?>"><span>
                                            Go Back
                                        </span>
                                    </a>
                                </div> 
                            </div>
                            <div class="login-btn">
                                <div class="save-and-go-next">
                                    <button type="submit">
                                        <img src="<?= Url::toRoute('/public/img/register-button.png') ?>"><span>
                                            Save & Go Next
                                        </span>
                                    </button>
                                </div> 
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
</section>