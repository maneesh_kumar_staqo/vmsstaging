<?php

use common\helpers\FormHelper;
use common\models\VendorTypes;
use common\models\OrganisationTypes;
use common\forms\DirectorForm;
?>

<div class="single-director single-director-<?= $key ?>">
    <hr>
    <div class="row">
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']name')->textInput(['maxlength' => true])->label('Director Name') ?>
            </div>
        </div>
        <div
            class="col-md-6"
            data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>"
            data-organisation-action="org-conditions"
            data-organisation-field-hide-on="*"
            data-organisation-field-show-on="<?=
            json_encode([
                OrganisationTypes::ID_OPC, OrganisationTypes::ID_JV, OrganisationTypes::ID_OTHERS,
                OrganisationTypes::ID_LIMITED_COMPANY, OrganisationTypes::ID_PRIVATE_LIMITED_COMPANY
            ])
            ?>"
            >
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']din_number')->textInput(['maxlength' => true])->label('DIN No') ?>
            </div>
        </div>
        <div class="col-md-6" data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>">
            <div class="md-form mb-0">
                <?=
                        $form->field($model, '[' . $key . ']pan_number')
                        ->textInput([
                            'maxlength' => true
                        ])->label('PAN Number')
                ?>
            </div>
        </div>        
        <div class="col-md-6" data-vendor-field="<?= VendorTypes::IS_PARENT_OVERSEAS ?>" >
            <div class="md-form mb-0">
                <?=
                $form->field($model, '[' . $key . ']passport_no')->textInput([
                    'maxlength' => true
                ])->label('Passport Number')
                ?>
            </div>
        </div>
        <div class="" data-vendor-field="<?= VendorTypes::IS_PARENT_OVERSEAS ?>" >
            <?=
            FormHelper::fileInput($form, $model, '[' . $key . ']upload_passport', [
                'originalField' => 'upload_passport',
                'key' => $key,
                'class' => 'col-sm-12'
                    ], $disableError)
            ?>
        </div>
        <div class="" data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>">
            <?=
            FormHelper::fileInput($form, $model, '[' . $key . ']pan_document', [
                'originalField' => 'pan_document',
                'key' => $key,
                'class' => 'col-sm-12'
                    ], $disableError)
            ?>
        </div>
        <div
            data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>"
            data-organisation-action="org-conditions"
            data-organisation-field-hide-on="*"
            data-organisation-field-show-on="<?=
            json_encode([
                OrganisationTypes::ID_OPC, OrganisationTypes::ID_JV
            ])
            ?>"
            >
                <?=
                FormHelper::fileInput($form, $model, '[' . $key . ']din_document', [
                    'originalField' => 'din_document',
                    'key' => $key,
                    'class' => 'col-sm-12'
                        ], $disableError)
                ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="multioption">
                <label>Director Residential Address</label>
            </div>
        </div>
        <div class="col-md-12">
            <div class="md-form mb-0" style="width:98%;">
                <?= $form->field($model, '[' . $key . ']address_line_1')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="md-form mb-0" style="width:98%;">
                <?= $form->field($model, '[' . $key . ']address_line_2')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']city')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']state')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="md-form mb-0">
                <?= $form->field($model, '[' . $key . ']zip_code')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="col-md-6" style="margin-top:20px">
            <?=
            FormHelper::simpleDropDownList($model, '[' . $key . ']country', $countries, [
                'hideLabel' => true,
                'originalField' => 'country',
                'key' => $key
            ]);
            ?>
        </div>
    </div>
    <div class="row">        
        <?=
        FormHelper::fileInput($form, $model, '[' . $key . ']director_address_proof', [
            'originalField' => 'director_address_proof',
            'key' => $key
                ], $disableError)
        ?>
        <?php if ($key == DirectorForm::FIELD_REQUIRED_INDEX): ?>
            <?=
            FormHelper::fileInput($form, $model, '[' . $key . ']authorized_signatory', [
                'originalField' => 'authorized_signatory',
                'key' => $key
                    ], $disableError)
            ?>
        <?php endif; ?>
    </div>
    <?php if ($key == DirectorForm::FIELD_REQUIRED_INDEX): ?>
        <div
            data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>"
            data-organisation-action="org-conditions"
            data-organisation-field-hide-on="*"
            data-organisation-field-show-on="<?=
            json_encode([
                OrganisationTypes::ID_JV
            ])
            ?>"
            >
                <?=
                FormHelper::fileInput($form, $model, '[' . $key . ']partnership_deed', [
                    'originalField' => 'partnership_deed',
                    'key' => $key
                        ], $disableError)
                ?>
        </div>
    <?php endif; ?>

    <div class="row" style="padding:20px 0px 5px 0px;" data-vendor-field="<?= VendorTypes::IS_PARENT_DOMESTIC ?>">
        <div class="col-md-12" >
            <div class="custom-control custom-checkbox">
                <?= $form->field($model, '[' . $key . ']is_qualified_under_164')->checkbox(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
</div>