<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<hr style="border-top: 3px dotted #225776;">
<div class="row">
    <div class="col-md-12">
        <div class="multioption">
            <label class="owners-type-title">Shareholder Details</label>
            <div class="owners-type-text">
                <span style="font-size:14px;font-family:FreightSansProMedium;"><i>(min 1 and max 200)</i> <br/>If a company is the direct shareholder, then list key shareholders up the chain until there are individual name to be screen.
                </span>
            </div>

        </div>
    </div>
</div>
<!--$shareHolder-->
<?php if ($shareHolders): ?>
    <?php foreach ($shareHolders as $key => $shareHolder): ?>
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => $key,
        'fistName' => $shareHolder['first_name'],
        'lastName' => $shareHolder['last_name'],
        'id' => $shareHolder['id']
    ])
    ?>
<?php endforeach; ?>
<?php else: ?>
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => 0,
    ])
    ?>
<?php endif ?>

<div data-append="share-holders"></div>
<div class="row">
    <div class="col-md-12 cursor-pointer" style="text-align:right" data-action="add-share-holders">
        <i class="fa fa-plus"></i>
    </div>
</div>

<div class="d-none hidden-shareholder">
    <?=
    $this->render('single-shareholder', [
        'model' => $model,
        'form' => $form,
        'key' => '{counter}',
    ])
    ?>
</div>