<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use common\models\LoginForm;
use frontend\models\SignupForm;
use common\models\RegistrationVendorInfo;
use common\models\Commodities;
use common\models\Services;
use common\helpers\MessageHelper;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use common\components\EmailComponent;

/**
 * Site controller
 */
class SiteController extends MainController
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['/detail-completion/vendor-information']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new RegistrationVendorInfo();
        $signupForm = new SignupForm();
        $commodities = Commodities::getAll();
        $services = Services::getAll();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $transaction = Yii::$app->db->beginTransaction();

            $idUser = $signupForm->signup($model);
            $model->fk_user = $idUser;

            if ($idUser && $model->saveInfo($commodities, $services)) {
                $verifyModel = new ResendVerificationEmailForm();
                $verifyModel->email = $signupForm->email;

                if ($verifyModel->sendEmail()) {
                    $transaction->commit();
                    return $this->redirect(['thank-you?type=' . MessageHelper::SIGNUP_COMPLETED]);
                } else {
                    $transaction->rollBack();
                    $model->addError('contact_person_email', 'Unable to send email right now, please try again');
                }
            } else {
                $transaction->rollBack();
            }
        }

        return $this->render('signup', [
            'model'         => $model,
            'commodities'   => $commodities,
            'services'      => $services
        ]);
    }

    public function actionThankYou($type = 3)
    {
        return $this->render('thank-you', [
            'type' => $type
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $emailer = new EmailComponent();

        if ($user = $model->verifyEmail()) {
            if ($emailer->sendEmail(EmailComponent::TYPE_WELCOME, $user['email'], $user) && Yii::$app->user->login($user)) {
                return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_EMAIL_VERIFY]);
            }
        }

        return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_VERIFICATION_FAIL]);
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_RESEND]);
            }
            return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_VERIFICATION_FAIL_AGAIN]);
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_REQUEST_PASSWORD_SUCCESS]);

            } else {
                return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_REQUEST_PASSWORD_FAIL]);
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            return $this->redirect(['thank-you?type=' . MessageHelper::TYPE_PASSWORD_RESET_SUCCESS]);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
