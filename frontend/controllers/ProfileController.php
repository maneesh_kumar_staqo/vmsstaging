<?php

namespace frontend\controllers;

use Yii;
use common\forms\EditBasicInfo;
use yii\web\NotFoundHttpException;
use common\models\RegistrationVendorInfo;
use common\models\Commodities;
use common\models\Services;

class ProfileController extends MainController
{
    public function actionEdit()
    {

        $model = $this->findModel();

        if ($model->load(Yii::$app->request->post()) && $model->saveInfo()) {
            Yii::$app->session->setFlash('success', 'Details updated successfully');
            return $this->redirect(['/']);
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionView()
    {
        $model = RegistrationVendorInfo::findModel(Yii::$app->user->id);

        $commodities = Commodities::getAll();
        $services = Services::getAll();

        return $this->render('view', [
            'id'            => Yii::$app->user->id,
            'model'         => $model,
            'commodities'   => $commodities,
            'services'      => $services
        ]);
    }

    protected function findModel()
    {
        $idUser = Yii::$app->user->id;
        $model = EditBasicInfo::findOne(['id' => $idUser]);

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }
}