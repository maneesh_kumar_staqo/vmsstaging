<?php

namespace frontend\controllers;

use Yii;
use common\models\ContactInformation;
use common\models\AddressInformation;
use common\models\BankInformation;
use common\models\TaxInformation;
use common\models\VendorInformation;
use common\models\VendorTypes;
use common\models\OrganisationTypes;
use common\models\CompanyTypes;
use common\models\AccreditationInformation;
use common\models\DeclarationInformation;
use common\forms\DirectorForm;
use common\forms\OwnerForm;
use common\models\ShareHolders;
use common\models\Country;
use common\helpers\MessageHelper;
use common\models\User;
use common\components\EmailComponent;
use common\models\VendorInformationPartner;
use common\forms\PartnerForm;
use common\forms\PersonForm;
use common\forms\ProprietorForm;
use common\forms\KartaForm;
use common\models\RegistrationVendorInfo;

/**
 * Detail Completion Controller
 *
 * @author Aabir Hussain <aabir.hussian1@gmail.com>
 */
class DetailCompletionController extends MainController
{

    public $nextStep = '/';

    public function beforeAction($action) {
        if (Yii::$app->request->isPost && !$this->isAbleToEdit()) {
            $this->refresh();
            //$this->redirect(['/site/thank-you?type=' . MessageHelper::TYPE_CANNOT_EDIT]);
            return FALSE;
        }

        return parent::beforeAction($action);
    }

    /**
     * 
     */
    public function actionVendorInformation()
    {
        $model = VendorInformation::findModel(Yii::$app->user->id);
        $vendors = VendorTypes::getAll();
        $organisations = OrganisationTypes::getAll();
        $business = CompanyTypes::getAll();
        $countries = Country::getAll();
        $shareHolders = ShareHolders::getAll($model->id);
        $disableError = false;

        $directorForm = DirectorForm::findModel($model->id, DirectorForm::TYPE_DIRECTOR, count(Yii::$app->request->post('DirectorForm', [])));
        $ownerForm = OwnerForm::findModel($model->id, OwnerForm::TYPE_OWNER, count(Yii::$app->request->post('OwnerForm', [])));

        $partnerForm = PartnerForm::findModel($model->id, PartnerForm::TYPE_PARTNER, count(Yii::$app->request->post('PartnerForm', [])));
        $kartaForm = KartaForm::findModel($model->id, KartaForm::TYPE_KARTA, count(Yii::$app->request->post('KartaForm', [])));
        $proprietorForm = ProprietorForm::findModel($model->id, ProprietorForm::TYPE_PROPRIETOR, count(Yii::$app->request->post('ProprietorForm', [])));
        $personForm = PersonForm::findModel($model->id, PersonForm::TYPE_PERSON, count(Yii::$app->request->post('PersonForm', [])));

        DirectorForm::loadMultiple($directorForm, Yii::$app->request->post());
        OwnerForm::loadMultiple($ownerForm, Yii::$app->request->post());
        PartnerForm::loadMultiple($partnerForm, Yii::$app->request->post());
        KartaForm::loadMultiple($kartaForm, Yii::$app->request->post());
        ProprietorForm::loadMultiple($proprietorForm, Yii::$app->request->post());
        PersonForm::loadMultiple($personForm, Yii::$app->request->post());

        if ($model->load(Yii::$app->request->post()) && $model->saveModel($organisations, $directorForm, $ownerForm, $partnerForm, $kartaForm, $proprietorForm, $personForm, $vendors)) {
            return $this->redirect(['contact-information']);
        }

        if ($model->errors || OwnerForm::modelHasErr($ownerForm) || DirectorForm::modelHasErr($directorForm)
            || PartnerForm::modelHasErr($partnerForm) || KartaForm::modelHasErr($kartaForm) || ProprietorForm::modelHasErr($proprietorForm)
            || PersonForm::modelHasErr($personForm)) {
            $disableError = true;
        }

        if ($model->business_type && !is_array($model->business_type)) {
            $model->business_type = json_decode($model->business_type);
        }

        return $this->render('vendor-information', [
            'model' => $model,
            'vendors' => $vendors,
            'organisations' => $organisations,
            'business' => $business,
            'directorForm' => $directorForm,
            'ownerForm' => $ownerForm,
            'countries' => $countries,
            'shareHolders' => $shareHolders,
            'disableError' => $disableError,
            'partnerForm' => $partnerForm,
            'kartaForm' => $kartaForm,
            'proprietorForm' => $proprietorForm,
            'personForm' => $personForm
        ]);
    }

    public function actionContactInformation()
    {
        $idUser = Yii::$app->user->id;

        if (!VendorInformation::isExists($idUser)) {
            Yii::$app->Flash->error('Please complete Vendor Details first.');
            return $this->redirect(['vendor-information']);
        } else{

            $model = ContactInformation::findModel(Yii::$app->user->id);
            $vendorModel = VendorInformation::findModel(Yii::$app->user->id);

            $isGovernment = CompanyTypes::isGovernment($vendorModel);
            $isLandOwner = CompanyTypes::isLandowner($vendorModel);

            if ($model->load(Yii::$app->request->post()) && $model->saveModel($isLandOwner, $isGovernment)) {
                return $this->redirect(['address-information']);
            }

            return $this->render('contact-information', [
                'model' => $model,
                'isGovernment' => $isGovernment,
                'isLandOwner' => $isLandOwner
            ]);
        }
    }

    public function actionAddressInformation()
    {
        $idUser = Yii::$app->user->id;
        if (!VendorInformation::isExists($idUser)) {
            Yii::$app->Flash->error('Please complete Vendor Details first.');
            return $this->redirect(['vendor-information']);
        } else{
            $model = AddressInformation::findModel(Yii::$app->user->id);
            $vendorModel = VendorInformation::findModel(Yii::$app->user->id);
            $isGovernment = CompanyTypes::isGovernment($vendorModel);
            $isLandOwner = CompanyTypes::isLandowner($vendorModel);

            if ($model->load(Yii::$app->request->post()) && $model->saveModel($isLandOwner, $isGovernment)) {
                return $this->redirect(['bank-information']);
            }

            return $this->render('address-information', [
                'model' => $model,
                'isGovernment' => $isGovernment,
                'isLandOwner' => $isLandOwner
            ]);
        }
    }

    public function actionBankInformation()
    {
        $idUser = Yii::$app->user->id;
        if (!VendorInformation::isExists($idUser)) {
            Yii::$app->Flash->error('Please complete Vendor Details first.');
            return $this->redirect(['vendor-information']);
        } else{
            $model = BankInformation::findModel(Yii::$app->user->id);
            $registration = RegistrationVendorInfo::findModel(Yii::$app->user->id);
            $vendorModel = VendorInformation::findModel(Yii::$app->user->id);

            $isGovernment = CompanyTypes::isGovernment($vendorModel);
            $isLandOwner = CompanyTypes::isLandowner($vendorModel);

            if (!empty($registration['trading_name']) && !$model->pb_account_holder_name) {
                $model->pb_account_holder_name = $registration['trading_name'];
            }

            if ($model->load(Yii::$app->request->post()) && $model->saveImages($isLandOwner) && $model->saveModel($isLandOwner)) {
                return $this->redirect(['tax-information']);
            }

            return $this->render('bank-information', [
                'model' => $model,
                'isLandOwner' => $isLandOwner
            ]);
        }
    }

    public function actionTaxInformation()
    {
        $idUser = Yii::$app->user->id;
        if (!VendorInformation::isExists($idUser)) {
            Yii::$app->Flash->error('Please complete Vendor Details first.');
            return $this->redirect(['vendor-information']);
        } else{
            $model = TaxInformation::findModel(Yii::$app->user->id);
            $vendorModel = VendorInformation::findModel(Yii::$app->user->id);

            $isGovernment = CompanyTypes::isGovernment($vendorModel);
            $isLandOwner = CompanyTypes::isLandowner($vendorModel);

            if (!$vendorModel) {
                return $this->redirect(['vendor-information']);
            }

            $isDomestic = OrganisationTypes::isDomestic($vendorModel);
            $isOverseas = OrganisationTypes::isOverseas($vendorModel);

            if ($model->load(Yii::$app->request->post()) && $model->saveImages($isDomestic, $isLandOwner, $isGovernment) && $model->saveModel($isDomestic, $isLandOwner, $isGovernment)) {
                return $this->redirect(['accreditation-information']);
            }

            return $this->render('tax-information', [
                'model' => $model,
                'isDomestic' => $isDomestic,
                'isOverseas' => $isOverseas,
                'isGovernment' => $isGovernment,
                'isLandOwner' => $isLandOwner
            ]);
        }
    }

    public function actionAccreditationInformation()
    {
        $model = AccreditationInformation::findModel(Yii::$app->user->id);

        $model->status = User::APP_STATUS_PENDING;
        if ($model->load(Yii::$app->request->post()) && $model->saveImages() && $model->save()) {
            return $this->redirect(['declaration-information']);
        }

        return $this->render('accreditation-information', [
            'model' => $model,
        ]);
    }

    public function actionDeclarationInformation()
    {

        $model = DeclarationInformation::findModel(Yii::$app->user->id);

        $model->status = User::APP_STATUS_PENDING;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!$this->allStepsDone()) {
                return $this->redirect([$this->nextStep]);
            } else {
                User::updateState();
                $user = Yii::$app->user->identity;
                $emailer = new EmailComponent();
                $emailer->sendEmail(EmailComponent::TYPE_APPLICATION_SUBMITTED, $user['email'], $user);
                $emailer->sendEmailtohfe(EmailComponent::TYPE_APPLICATION_SUBMITTED, $user['email'], $user);

                return $this->redirect(['thank-you']);
            }
        } 

        return $this->render('declaration-information', [
            'model' => $model,
        ]);
    }

    public function actionThankYou()
    {
        return $this->render('thank-you');
    }

    /**
     * check all steps are not or not before submitting to admin panel.
     *
     * @return boolean
     */
    protected function allStepsDone()
    {
        $result = false;
        $idUser = Yii::$app->user->id;

        if (!VendorInformation::isExists($idUser)) {
            $this->nextStep = 'vendor-information';
        } else if (!ContactInformation::isExists($idUser)) {
            $this->nextStep = 'contact-information';
        } else if (!AddressInformation::isExists($idUser)) {
            $this->nextStep = 'address-information';
        } else if (!BankInformation::isExists($idUser)) {
            $this->nextStep = 'bank-information';
        } else if (!TaxInformation::isExists($idUser)) {
            $this->nextStep = 'tax-information';
        } else if (!AccreditationInformation::isExists($idUser)) {
            $this->nextStep = 'tax-information';
        } else {
            $result = true;
        }

        return $result;
    }

    /**
     * check all steps are not or not before submitting to admin panel.
     *
     * @return boolean
     */
    protected function isAbleToEdit()
    {
        $result = true;
        $idUser = Yii::$app->user->id;

        if (in_array(Yii::$app->user->identity->application_status, [User::APP_STATUS_APPROVE, User::APP_STATUS_INCOMPLETE, User::APP_STATUS_DISAPPROVE])) {
            $result = true;
        } elseif (Yii::$app->user->identity->application_status == User::APP_STATUS_REJECTED) {
            $result = false;
            Yii::$app->Flash->error('Sorry your application is rejected, please contact admin.');
        } elseif (Yii::$app->user->identity->application_status == User::APP_STATUS_PENDING) {
            Yii::$app->Flash->error('Sorry your application is under review, please wait untill the review process has been completed.');
            $result = false;
        } elseif (VendorInformation::isExists($idUser) && ContactInformation::isExists($idUser) &&
            AddressInformation::isExists($idUser) && BankInformation::isExists($idUser) &&
            TaxInformation::isExists($idUser) && AccreditationInformation::isExists($idUser) &&
            DeclarationInformation::isExists($idUser)) {
            Yii::$app->Flash->error('Sorry your application is under review, please wait untill the review process has been completed.');
            $result = false;
        }

        return $result;
    }

    public function actionLoadList()
    {
        $vendorModel = VendorInformation::findModel(Yii::$app->user->id);
        $type = Yii::$app->request->post('type', VendorInformationPartner::TYPE_DIRECTOR);
        $key = Yii::$app->request->post('total', 1);
        $form = \yii\widgets\ActiveForm::begin();
//        $form = json_decode(Yii::$app->request->post('form'));
        $countries = Country::getAll();
        $result = true;

        switch ($type):
            case VendorInformationPartner::TYPE_DIRECTOR:
                $model = new DirectorForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-directors';
                break;

            case VendorInformationPartner::TYPE_OWNER:
                $model = new OwnerForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-owners';                
                break;

            case VendorInformationPartner::TYPE_PARTNER:
                $model = new PartnerForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-partner';                
                break;

            case VendorInformationPartner::TYPE_PERSON:
                $model = new PersonForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-person';
                break;

            case VendorInformationPartner::TYPE_KARTA:
                $model = new KartaForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-karta';
                break;

            case VendorInformationPartner::TYPE_PROPRIETOR:
                $model = new ProprietorForm();
                $model->fk_vendor_information = $vendorModel->id;
                $fileName = 'list-proprietor';
                break;
            default :
                $result = false;
        endswitch;

        if ($result) {
            $result = $this->renderPartial('partials/' . $fileName, [
                'key' => $key,
                'form' => $form,
                'model' => $model,
                'countries' => $countries,
                'disableError' => false
            ]);
        }

        return $result;
    }
}