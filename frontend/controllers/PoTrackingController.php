<?php

namespace frontend\controllers;

use Yii;
use common\models\PoTracking;
use common\models\PoTrackingSearch;
use yii\web\NotFoundHttpException;
use common\models\PoInvoiceSearch;

/**
 * PoTrackingController implements the CRUD actions for PoTracking model.
 */
class PoTrackingController extends MainController
{

    /**
     * Lists all PoTracking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PoTrackingSearch();
        $searchModel->fk_vendor = Yii::$app->user->id;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PoTracking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new PoInvoiceSearch();
        $searchModel->fk_po_tracking = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    /**
     * Finds the PoTracking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PoTracking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PoTracking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
