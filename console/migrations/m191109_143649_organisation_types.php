<?php

use yii\db\Migration;

/**
 * Class m191109_143649_organisation_types
 */
class m191109_143649_organisation_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->execute("CREATE TABLE `hfe`.`organisation_types` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `title` VARCHAR(255) NOT NULL , `priority` INT NULL , `status` TINYINT(2) NOT NULL , `type` TINYINT(2) NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP NOT NUL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
//
//        $this->execute("ALTER TABLE `vendor_information` ADD `other_business` VARCHAR(256) NULL AFTER `business_type`;");
//
//        $this->execute("ALTER TABLE `vendor_information` CHANGE `registration_number` `registration_number` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
//
//        $this->execute("ALTER TABLE `company_types` ADD `display_trascco` TINYINT(2) NULL AFTER `display_other_field`;");
//
//        $this->execute("ALTER TABLE `vendor_information` ADD `trasco_name` VARCHAR(256) NULL AFTER `other_business`, ADD `trasco_document` VARCHAR(256) NULL AFTER `trasco_name`;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` CHANGE `list_of_directors` `fk_vendor_information` BIGINT(20) NOT NULL;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` ADD `type` TINYINT(2) NOT NULL AFTER `fk_vendor_information`;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` ADD `name` VARCHAR(100) NOT NULL AFTER `id`;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` CHANGE `din_number` `din_number` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` CHANGE `director_pan_numbers` `pan_number` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;");
//
//        $this->execute("ALTER TABLE `vendor_information_partner` ADD `authorized_signatory` VARCHAR(256) NULL AFTER `is_qualified_under_164`, ADD `incorporation_certificate` VARCHAR(256) NULL AFTER `authorized_signatory`, ADD `partnership_deed` VARCHAR(256) NULL AFTER `incorporation_certificate`, ADD `authorisation_letter` VARCHAR(256) NULL AFTER `partnership_deed`;");
//
//        $this->execute('ALTER TABLE `vendor_information_partner` ADD `pan_document` VARCHAR(256) NULL AFTER `pan_number`;');
//
//        $this->execute("ALTER TABLE `vendor_information_partner` CHANGE `is_qualified_under_164` `is_qualified_under_164` TINYINT(2) NULL;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191109_143649_organisation_types cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191109_143649_organisation_types cannot be reverted.\n";

        return false;
    }
    */
}
