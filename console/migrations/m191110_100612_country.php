<?php

use yii\db\Migration;

/**
 * Class m191110_100612_country
 */
class m191110_100612_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $this->execute("
//            CREATE TABLE `country` (
//              `id` bigint(20) NOT NULL,
//              `title` varchar(255) NOT NULL,
//              `priority` smallint(6) DEFAULT NULL,
//              `status` tinyint(2) NOT NULL,
//              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
//            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
//
//            ALTER TABLE `country`
//              ADD PRIMARY KEY (`id`);");
//
//        $this->execute('ALTER TABLE `country` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m191110_100612_country cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191110_100612_country cannot be reverted.\n";

        return false;
    }
    */
}
